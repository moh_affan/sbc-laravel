-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2018 at 08:03 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `afn_salon`
--

-- --------------------------------------------------------

--
-- Table structure for table `diskon`
--

CREATE TABLE `diskon` (
  `id` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tgl_awal` date NOT NULL,
  `jumlah_diskon` double NOT NULL,
  `tgl_akhir` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diskon`
--

INSERT INTO `diskon` (`id`, `nama`, `tgl_awal`, `jumlah_diskon`, `tgl_akhir`, `created_at`) VALUES
('D1111', '50%', '2018-06-08', 187500, '2018-06-08', '2018-06-08 07:33:44');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kd` varchar(4) NOT NULL,
  `jabatan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kd`, `jabatan`) VALUES
('ADM', 'STAFF ADMINISTRASI'),
('CEO', 'Chief Executive Officer'),
('KRY', 'KARYAWAN'),
('KSR', 'KASIR'),
('MGR', 'MANAGER'),
('SPV', 'SUPERVISOR');

-- --------------------------------------------------------

--
-- Table structure for table `jatah`
--

CREATE TABLE `jatah` (
  `id` int(1) NOT NULL,
  `nominal` double NOT NULL,
  `keterangan` varchar(255) NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jatah`
--

INSERT INTO `jatah` (`id`, `nominal`, `keterangan`) VALUES
(1, 110000, 'treatment'),
(2, 220000, 'produk');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` varchar(10) NOT NULL,
  `kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `kategori`) VALUES
('KAT000', 'UNCATEGORIZED');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kd_jabatan` varchar(4) DEFAULT NULL,
  `telp` varchar(15) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama`, `kd_jabatan`, `telp`, `alamat`) VALUES
('A031088', 'WIDIA ARIESTIN', 'SPV', '081931022201', 'KOLOR'),
('B200788', 'RATNA SUSILOWATI', 'KSR', '085230780609', 'KALIANGET'),
('PEG001', 'EKA AYU SETYANINGRUM', 'ADM', '081233369383', 'SATELIT'),
('T00001', 'OKTAVIM', 'KRY', '081939474007', 'BANGSELOK'),
('T00002', 'LITA YULIANA', 'KRY', '087850350790', 'BATUAN'),
('T00003', 'VINA HIMATUL ULYA', 'KRY', '087850350790', 'SARONGGI');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id`, `nama`, `telp`, `alamat`, `tgl_lahir`) VALUES
('CUS000', 'TANPA NAMA', '081234567890', 'Tanpa alamat', '2017-01-05');

-- --------------------------------------------------------

--
-- Table structure for table `penyusutan`
--

CREATE TABLE `penyusutan` (
  `id_treatment` varchar(10) NOT NULL,
  `penyusutan` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penyusutan`
--

INSERT INTO `penyusutan` (`id_treatment`, `penyusutan`) VALUES
('TR001', 0),
('TR003', 10000),
('TR004', 0),
('TR005', 15000),
('TR006', 150000),
('TR007', 0),
('TR008', 10000),
('TR010', 10000),
('TR011', 50000),
('TR012', 15000),
('TR013', 15000),
('TR014', 15000),
('TR017', 15000),
('TR018', 50000),
('TR019', 50000),
('TR020', 50000),
('TR021', 50000),
('TR022', 5000),
('TR023', 50000),
('TR024', 15000),
('TR025', 0),
('TR026', 0),
('TR027', 30000),
('TR030', 15000),
('TR031', 0),
('TR032', 50000),
('TR033', 50000),
('TR034', 50000),
('TR035', 50000),
('TR036', 185000),
('TR037', 50000),
('TR038', 50000),
('TR039', 50000),
('TR040', 50000),
('TR043', 0),
('TR044', 0),
('TR045', 0),
('TR046', 10000),
('TR047', 10000),
('TR048', 10000),
('TR049', 10000),
('TR050', 10000),
('TR051', 50000),
('TR052', 50000),
('TR053', 50000),
('TR054', 50000),
('TR060', 5000),
('TR061', 5000),
('TR062', 15000),
('TR063', 25000),
('TR064', 15000),
('TR065', 10000),
('TR066', 15000),
('TR067', 50000),
('TR068', 50000),
('TR069', 15000),
('TR070', 50000),
('TR072', 15000),
('TR073', 0),
('TR075', 15000),
('TR076', 15000),
('TR077', 0),
('TR078', 50000),
('TR079', 50000),
('TR081', 0),
('TR082', 0),
('TR083', 0),
('TR084', 0),
('TR085', 0),
('TR086', 0),
('TR087', 50000);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` varchar(10) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `id_supplier` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `ref_harga_jual` double NOT NULL,
  `ref_harga_beli` double NOT NULL,
  `id_diskon` varchar(10) NOT NULL DEFAULT 'NODISCON'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `id_kategori`, `id_supplier`, `nama`, `ref_harga_jual`, `ref_harga_beli`, `id_diskon`) VALUES
('PR001', 'KAT000', 'SUP000', 'CLEANSER N', 99000, 39000, 'NODISCON'),
('PR002', 'KAT000', 'SUP000', 'CLEANSER O', 99000, 40200, 'NODISCON'),
('PR003', 'KAT000', 'SUP000', 'F WASH', 110000, 74400, 'NODISCON'),
('PR004', 'KAT000', 'SUP000', 'CO2', 125000, 75900, 'NODISCON'),
('PR005', 'KAT000', 'SUP000', 'BB CHUSION', 200000, 120000, 'NODISCON'),
('PR006', 'KAT000', 'SUP000', 'celeteque', 65000, 52000, 'NODISCON'),
('PR007', 'KAT000', 'SUP000', 'SERUM GOLD BESAR', 250000, 100000, 'NODISCON'),
('PR008', 'KAT000', 'SUP000', 'TONER ACNE', 55000, 36500, 'NODISCON'),
('PR009', 'KAT000', 'SUP000', 'MILK CLEAN SBC', 55000, 36500, 'NODISCON'),
('PR010', 'KAT000', 'SUP000', 'WHITE', 80000, 60480, 'NODISCON'),
('PR011', 'KAT000', 'SUP000', 'WHITE 1', 80000, 60480, 'NODISCON'),
('PR013', 'KAT000', 'SUP000', 'WHITE 3', 105000, 49700, 'NODISCON'),
('PR014', 'KAT000', 'SUP000', 'WHITE 4', 125000, 49700, 'NODISCON'),
('PR019', 'KAT000', 'SUP000', 'SN', 65000, 35000, 'NODISCON'),
('PR021', 'KAT000', 'SUP000', 'SN 2', 93000, 43500, 'NODISCON'),
('PR022', 'KAT000', 'SUP000', 'SN 3', 93000, 38500, 'NODISCON'),
('PR023', 'KAT000', 'SUP000', 'SO', 65000, 35000, 'NODISCON'),
('PR024', 'KAT000', 'SUP000', 'SO 1', 93000, 38900, 'NODISCON'),
('PR025', 'KAT000', 'SUP000', 'SO 2', 93000, 38900, 'NODISCON'),
('PR026', 'KAT000', 'SUP000', 'B.NJ', 65000, 37000, 'NODISCON'),
('PR027', 'KAT000', 'SUP000', 'B.NW', 65000, 32500, 'NODISCON'),
('PR028', 'KAT000', 'SUP000', 'NEW TIN', 134000, 108800, 'NODISCON'),
('PR029', 'KAT000', 'SUP000', 'PS', 171000, 138800, 'NODISCON'),
('PR030', 'KAT000', 'SUP000', 'SUNWHITE', 163000, 133080, 'NODISCON'),
('PR032', 'KAT000', 'SUP000', 'SOF', 152000, 123400, 'NODISCON'),
('PR034', 'KAT000', 'SUP000', 'BASE CREAM', 303000, 246900, 'NODISCON'),
('PR035', 'KAT000', 'SUP000', 'ACNE CREAM', 70000, 52900, 'NODISCON'),
('PR036', 'KAT000', 'SUP000', 'ACNE GEL', 75000, 57740, 'NODISCON'),
('PR037', 'KAT000', 'SUP000', 'NEW EYE', 73000, 60480, 'NODISCON'),
('PR038', 'KAT000', 'SUP000', 'FLEK KRIM', 125000, 70480, 'NODISCON'),
('PR039', 'KAT000', 'SUP000', 'MD', 97000, 75000, 'NODISCON'),
('PR040', 'KAT000', 'SUP000', 'COMEDO', 90000, 45000, 'NODISCON'),
('PR042', 'KAT000', 'SUP000', 'C BIBIR', 73000, 60480, 'NODISCON'),
('PR046', 'KAT000', 'SUP000', 'BODY-B', 65000, 45000, 'NODISCON'),
('PR047', 'KAT000', 'SUP000', 'HAND BLIC', 85000, 55000, 'NODISCON'),
('PR048', 'KAT000', 'SUP000', 'DOXICOR', 13000, 10144, 'NODISCON'),
('PR049', 'KAT000', 'SUP000', 'PINOCARE', 22000, 17000, 'NODISCON'),
('PR051', 'KAT000', 'SUP000', 'ANTI IRITA', 35000, 5000, 'NODISCON'),
('PR052', 'KAT000', 'SUP000', 'ID CARD', 15000, 10000, 'NODISCON'),
('PR055', 'KAT000', 'SUP000', 'SERUM GOLD', 100000, 50000, 'NODISCON'),
('PR059', 'KAT000', 'SUP000', 'SERUM WHITE', 85000, 50000, 'NODISCON'),
('PR061', 'KAT000', 'SUP000', 'SERUM SCAR', 85000, 50000, 'NODISCON'),
('PR062', 'KAT000', 'SUP000', 'ED', 85000, 65000, 'NODISCON'),
('PR063', 'KAT000', 'SUP000', 'MC', 130000, 85000, 'NODISCON'),
('PR064', 'KAT000', 'SUP000', 'BODY WASH', 35000, 10000, 'NODISCON'),
('PR065', 'KAT000', 'SUP000', 'NK', 106000, 74200, 'NODISCON'),
('PR066', 'KAT000', 'SUP000', 'FLEK G', 220000, 96000, 'NODISCON'),
('PR068', 'KAT000', 'SUP000', 'DIVA', 10000, 8500, 'NODISCON'),
('PRO67', 'KAT000', 'SUP000', 'SUN PROTECTOR', 225000, 168750, 'NODISCON');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('8gLG1r3tNdi3iLZtjTN2XDMA8N9UBnbqkOH4FJs1', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiM1lMVjVnWlgxMllYN3hRdUZiM1hmTVdpU2Job0xrZ1p3dW5KOXBQRyI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJuZXciO2E6MDp7fXM6Mzoib2xkIjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTQ6Imh0dHA6Ly9zYmMuY29tIjt9fQ==', 1529834162),
('hmVWUV6jf4bzQEwO8hRO01wQBtUgit2IUTBj4dcr', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiRkxDa0thQTJtME94aVFzTVFzckxFclZxTUltMEVObkczRTFVNktKeiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJuZXciO2E6MDp7fXM6Mzoib2xkIjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTQ6Imh0dHA6Ly9zYmMuY29tIjt9fQ==', 1529904996),
('9j3XwmLaqNRvs005VMtTDHDnB8BbiOGCvWCq02su', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiU3dpWjQwaUFYcTI4SFROdmd2Z3dPTWVDdlVMUE9GVU5RaXgzWm9xNCI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJuZXciO2E6MDp7fXM6Mzoib2xkIjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTY6Imh0dHA6Ly9sb2NhbGhvc3QiO319', 1529906126);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` varchar(10) NOT NULL,
  `supplier` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supplier`, `alamat`, `telp`) VALUES
('SUP000', 'TANPA SUPPLIER', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `treatment`
--

CREATE TABLE `treatment` (
  `id` varchar(10) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `id_supplier` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `ref_harga_jual` double NOT NULL,
  `ref_harga_beli` double NOT NULL,
  `id_diskon` varchar(10) NOT NULL DEFAULT 'NODISCON'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `treatment`
--

INSERT INTO `treatment` (`id`, `id_kategori`, `id_supplier`, `nama`, `ref_harga_jual`, `ref_harga_beli`, `id_diskon`) VALUES
('TR001', 'KAT000', 'SUP000', 'MPO', 50000, 10500, 'NODISCON'),
('TR003', 'KAT000', 'SUP000', 'Standart facial', 110000, 13500, 'NODISCON'),
('TR004', 'KAT000', 'SUP000', 'MASKER WHITE', 99000, 75000, 'NODISCON'),
('TR005', 'KAT000', 'SUP000', 'BOTANICAL', 300000, 59692, 'NODISCON'),
('TR006', 'KAT000', 'SUP000', 'COLAGEN ', 185000, 30000, 'NODISCON'),
('TR007', 'KAT000', 'SUP000', 'MASKER PASTA', 75000, 60000, 'NODISCON'),
('TR008', 'KAT000', 'SUP000', 'BRIGHTENING PELL', 375000, 72500, 'NODISCON'),
('TR010', 'KAT000', 'SUP000', 'STANDART LENGKAP', 250000, 37000, 'NODISCON'),
('TR011', 'KAT000', 'SUP000', 'DERMAL INFUS', 575000, 161250, 'NODISCON'),
('TR012', 'KAT000', 'SUP000', 'CHEMICAL PELLING', 175000, 26140, 'NODISCON'),
('TR013', 'KAT000', 'SUP000', 'ACNE SOLUTION', 220000, 32500, 'NODISCON'),
('TR014', 'KAT000', 'SUP000', 'DETOX', 275000, 57000, 'NODISCON'),
('TR017', 'KAT000', 'SUP000', 'DERMA PELL', 375000, 72500, 'NODISCON'),
('TR018', 'KAT000', 'SUP000', 'IPL SKIN R', 525000, 127500, 'NODISCON'),
('TR019', 'KAT000', 'SUP000', 'IPL ACNE', 425000, 97500, 'NODISCON'),
('TR020', 'KAT000', 'SUP000', 'IPL PIGMEN', 425000, 97500, 'NODISCON'),
('TR021', 'KAT000', 'SUP000', 'IPL KETIAK/JENGGOT', 325000, 82000, 'NODISCON'),
('TR022', 'KAT000', 'SUP000', 'COUTER', 30000, 3000, 'NODISCON'),
('TR023', 'KAT000', 'SUP000', 'IPL VASCULAR', 425000, 97500, 'NODISCON'),
('TR024', 'KAT000', 'SUP000', 'WHITE COMPLATE', 250000, 32500, 'NODISCON'),
('TR025', 'KAT000', 'SUP000', 'SERUM EX 2', 65000, 27000, 'NODISCON'),
('TR026', 'KAT000', 'SUP000', 'SERUM EX 5', 65000, 27000, 'NODISCON'),
('TR027', 'KAT000', 'SUP000', 'MIKRODERMA', 250000, 30000, 'NODISCON'),
('TR030', 'KAT000', 'SUP000', 'INJEKSI JERAWAT', 130000, 39000, 'NODISCON'),
('TR031', 'KAT000', 'SUP000', 'INJEKSI WHIT', 350000, 206500, 'NODISCON'),
('TR032', 'KAT000', 'SUP000', 'SKINL WAJAH', 350000, 32500, 'NODISCON'),
('TR033', 'KAT000', 'SUP000', 'SKIN MATA', 275000, 32500, 'NODISCON'),
('TR034', 'KAT000', 'SUP000', 'SLIM PAHA', 400000, 56000, 'NODISCON'),
('TR035', 'KAT000', 'SUP000', 'SLIM PERUT', 350000, 56000, 'NODISCON'),
('TR036', 'KAT000', 'SUP000', 'SLIM LENGKAP', 850000, 100000, 'NODISCON'),
('TR037', 'KAT000', 'SUP000', 'MESO WHITE', 375000, 57500, 'NODISCON'),
('TR038', 'KAT000', 'SUP000', 'injeksi paket', 1200000, 683500, 'NODISCON'),
('TR039', 'KAT000', 'SUP000', 'ROLLER', 650000, 219500, 'NODISCON'),
('TR040', 'KAT000', 'SUP000', 'BODY PELL', 550000, 64000, 'NODISCON'),
('TR043', 'KAT000', 'SUP000', 'KRIMBAT', 50000, 10000, 'NODISCON'),
('TR044', 'KAT000', 'SUP000', 'MASKER RAMBUT', 90000, 25000, 'NODISCON'),
('TR045', 'KAT000', 'SUP000', 'SPA RAMBUT', 90000, 25000, 'NODISCON'),
('TR046', 'KAT000', 'SUP000', 'SPA TRADISI', 200000, 67000, 'NODISCON'),
('TR047', 'KAT000', 'SUP000', 'SPA MODEREN', 225000, 83000, 'NODISCON'),
('TR048', 'KAT000', 'SUP000', 'PAKET SPA 1', 260000, 80500, 'NODISCON'),
('TR049', 'KAT000', 'SUP000', 'PAKET SPA 2', 355000, 104000, 'NODISCON'),
('TR050', 'KAT000', 'SUP000', 'PAKET PENGANTIN', 550000, 231000, 'NODISCON'),
('TR051', 'KAT000', 'SUP000', 'IPL R 45', 725000, 157500, 'NODISCON'),
('TR052', 'KAT000', 'SUP000', 'IPL R 60', 925000, 202500, 'NODISCON'),
('TR053', 'KAT000', 'SUP000', 'IPL APV 35', 625000, 127500, 'NODISCON'),
('TR054', 'KAT000', 'SUP000', 'IPL APV 50', 825000, 172500, 'NODISCON'),
('TR060', 'KAT000', 'SUP000', 'SKIN TEST', 25000, 500, 'NODISCON'),
('TR061', 'KAT000', 'SUP000', 'CC GLOW', 35000, 10000, 'NODISCON'),
('TR062', 'KAT000', 'SUP000', 'MIKROPELL', 50000, 35000, 'NODISCON'),
('TR063', 'KAT000', 'SUP000', 'MASKER LED', 150000, 75000, 'NODISCON'),
('TR064', 'KAT000', 'SUP000', 'RF', 125000, 50000, 'NODISCON'),
('TR065', 'KAT000', 'SUP000', 'EXLUSIVE ACNE', 220000, 29500, 'NODISCON'),
('TR066', 'KAT000', 'SUP000', 'NEW GLOWING', 375000, 63000, 'NODISCON'),
('TR067', 'KAT000', 'SUP000', 'SWT', 400000, 57500, 'NODISCON'),
('TR068', 'KAT000', 'SUP000', 'DERMAL THP 2', 975000, 300000, 'NODISCON'),
('TR069', 'KAT000', 'SUP000', 'COUTER PAKET', 750000, 75000, 'NODISCON'),
('TR070', 'KAT000', 'SUP000', 'ROLLER PAKET', 1400000, 648000, 'NODISCON'),
('TR072', 'KAT000', 'SUP000', 'BODY PELL 2', 750000, 128000, 'NODISCON'),
('TR073', 'KAT000', 'SUP000', 'BODY PAKET', 1650000, 256000, 'NODISCON'),
('TR075', 'KAT000', 'SUP000', 'PAKET WEDDING 2', 750000, 315500, 'NODISCON'),
('TR076', 'KAT000', 'SUP000', 'PAKET WEDDING 3', 1500000, 480500, 'NODISCON'),
('TR077', 'KAT000', 'SUP000', 'SPA RAMBUT', 90000, 25000, 'NODISCON'),
('TR078', 'KAT000', 'SUP000', 'SKINLIFTING 2', 500000, 70000, 'NODISCON'),
('TR079', 'KAT000', 'SUP000', 'SERUM AKTIF', 75000, 30000, 'NODISCON'),
('TR081', 'KAT000', 'SUP000', 'LULUR', 75000, 20000, 'NODISCON'),
('TR082', 'KAT000', 'SUP000', 'TOONING', 50000, 10000, 'NODISCON'),
('TR083', 'KAT000', 'SUP000', 'TOONING', 90000, 25000, 'NODISCON'),
('TR084', 'KAT000', 'SUP000', 'BEKAM', 75000, 25000, 'NODISCON'),
('TR085', 'KAT000', 'SUP000', 'STANDART 50', 50000, 13500, 'NODISCON'),
('TR086', 'KAT000', 'SUP000', 'STANDART 75', 75000, 20000, 'NODISCON'),
('TR087', 'KAT000', 'SUP000', 'ROLLER PAKET 2', 1750000, 675500, 'NODISCON');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `id_pegawai` varchar(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `id_pegawai`, `username`, `password`, `level`) VALUES
(1, 'PEG000', 'AYU', '080788', 1),
(2, 'PEG001', 'EKA', '070292', 1),
(3, 'A031088', 'WIDI', '150415', 0),
(4, 'B200788', 'RATNA', '000000', 1),
(5, 'A031088', 'affan', 'affan123', 0);

-- --------------------------------------------------------

--
-- Table structure for table `_detail_jatah_bon_produk`
--

CREATE TABLE `_detail_jatah_bon_produk` (
  `id_transaksi` int(10) NOT NULL,
  `id_produk` varchar(10) NOT NULL,
  `harga_aktual` double NOT NULL,
  `diskon_aktual` double NOT NULL,
  `qty` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_detail_jatah_bon_produk`
--

INSERT INTO `_detail_jatah_bon_produk` (`id_transaksi`, `id_produk`, `harga_aktual`, `diskon_aktual`, `qty`) VALUES
(1, 'PR036', 75000, 0, 1),
(2, 'PR047', 85000, 0, 5),
(3, 'PR001', 99000, 0, 1),
(3, 'PR010', 80000, 0, 1),
(3, 'PR013', 105000, 0, 1),
(3, 'PR014', 125000, 0, 3),
(3, 'PR035', 70000, 0, 1),
(4, 'PR019', 65000, 0, 1),
(5, 'PR010', 80000, 0, 4),
(5, 'PR023', 65000, 0, 2),
(5, 'PR024', 93000, 0, 2),
(5, 'PR035', 70000, 0, 4),
(6, 'PR049', 22000, 0, 30),
(7, 'PR029', 171000, 0, 1),
(8, 'PR010', 80000, 0, 1),
(9, 'PR010', 80000, 0, 1),
(9, 'PR019', 65000, 0, 1),
(9, 'PR038', 99000, 0, 1),
(10, 'PR001', 99000, 0, 1),
(10, 'PR010', 80000, 0, 1),
(10, 'PR019', 65000, 0, 1),
(11, 'PR003', 110000, 0, 1),
(11, 'PR013', 105000, 0, 1),
(11, 'PR024', 93000, 0, 1),
(12, 'PR010', 80000, 0, 1),
(12, 'PR036', 75000, 0, 1),
(13, 'PR019', 65000, 0, 1),
(13, 'PR023', 65000, 0, 1),
(14, 'PR003', 110000, 0, 1),
(14, 'PR010', 80000, 0, 2),
(14, 'PR013', 105000, 0, 1),
(14, 'PR014', 125000, 0, 1),
(15, 'PR023', 65000, 0, 1),
(15, 'PR051', 35000, 0, 1),
(16, 'PR013', 105000, 0, 1),
(17, 'PR019', 65000, 0, 1),
(17, 'PR021', 93000, 0, 1),
(18, 'PR002', 99000, 0, 2),
(18, 'PR010', 80000, 0, 2),
(18, 'PR014', 125000, 0, 1),
(18, 'PR024', 93000, 0, 2),
(18, 'PR025', 93000, 0, 2),
(18, 'PR035', 70000, 0, 3),
(19, 'PR022', 93000, 0, 1),
(20, 'PR001', 99000, 0, 1),
(20, 'PR002', 99000, 0, 1),
(20, 'PR019', 65000, 0, 1),
(21, 'PR021', 93000, 0, 1),
(22, 'PR014', 125000, 0, 1),
(23, 'PR051', 35000, 0, 1),
(24, 'PR001', 99000, 0, 1),
(24, 'PR014', 125000, 0, 1),
(24, 'PR022', 93000, 0, 2),
(25, 'PR023', 65000, 0, 1),
(25, 'PR025', 93000, 0, 1),
(26, 'PR013', 105000, 0, 1),
(27, 'PR010', 80000, 0, 1),
(27, 'PR019', 65000, 0, 1),
(27, 'PR046', 65000, 0, 1),
(28, 'PR010', 80000, 0, 2),
(29, 'PR001', 99000, 0, 1),
(29, 'PR010', 80000, 0, 1),
(29, 'PR019', 65000, 0, 1),
(30, 'PR002', 99000, 0, 1),
(30, 'PR013', 105000, 0, 1),
(30, 'PR022', 93000, 0, 1),
(31, 'PR021', 93000, 0, 1),
(32, 'PR039', 97000, 0, 1),
(33, 'PR013', 105000, 0, 1),
(33, 'PR022', 93000, 0, 1),
(34, 'PR014', 125000, 0, 1),
(35, 'PR010', 80000, 0, 3),
(35, 'PR023', 65000, 0, 1),
(35, 'PR025', 93000, 0, 2),
(35, 'PR035', 70000, 0, 1),
(36, 'PR013', 105000, 0, 1),
(36, 'PR019', 65000, 0, 1),
(37, 'PR010', 80000, 0, 1),
(38, 'PR014', 125000, 0, 1),
(38, 'PR022', 93000, 0, 1),
(39, 'PR010', 80000, 0, 3),
(39, 'PR014', 125000, 0, 2),
(39, 'PR023', 65000, 0, 2),
(39, 'PR024', 93000, 0, 2),
(40, 'PR013', 105000, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `_detail_jatah_bon_treatment`
--

CREATE TABLE `_detail_jatah_bon_treatment` (
  `id_transaksi` int(10) NOT NULL,
  `id_treatment` varchar(10) NOT NULL,
  `harga_aktual` double NOT NULL,
  `diskon_aktual` double NOT NULL,
  `penyusutan` double NOT NULL DEFAULT '0',
  `qty` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_detail_jatah_bon_treatment`
--

INSERT INTO `_detail_jatah_bon_treatment` (`id_transaksi`, `id_treatment`, `harga_aktual`, `diskon_aktual`, `penyusutan`, `qty`) VALUES
(1, 'TR003', 110000, 0, 10000, 1),
(1, 'TR019', 425000, 0, 50000, 1),
(1, 'TR020', 425000, 0, 50000, 1),
(2, 'TR003', 110000, 0, 10000, 1),
(3, 'TR003', 110000, 0, 10000, 1),
(4, 'TR003', 110000, 0, 10000, 1),
(4, 'TR031', 350000, 0, 0, 1),
(4, 'TR040', 550000, 0, 50000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `_detail_pembelian_produk`
--

CREATE TABLE `_detail_pembelian_produk` (
  `id_transaksi` int(10) NOT NULL,
  `id_produk` varchar(10) NOT NULL,
  `harga_aktual` double NOT NULL,
  `diskon_aktual` double NOT NULL,
  `qty` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_detail_pembelian_produk`
--

INSERT INTO `_detail_pembelian_produk` (`id_transaksi`, `id_produk`, `harga_aktual`, `diskon_aktual`, `qty`) VALUES
(1, 'PR001', 39000, 0, 16),
(1, 'PR002', 40200, 0, 17),
(1, 'PR003', 74400, 0, 19),
(1, 'PR004', 75900, 0, 40),
(1, 'PR005', 120000, 0, 0),
(1, 'PR006', 52000, 0, 0),
(1, 'PR008', 36500, 0, 11),
(1, 'PR009', 36500, 0, 11),
(1, 'PR010', 60480, 0, 68),
(1, 'PR011', 60480, 0, 0),
(1, 'PR013', 49700, 0, 0),
(1, 'PR014', 49700, 0, 4),
(1, 'PR019', 35000, 0, 1),
(1, 'PR021', 43500, 0, 31),
(1, 'PR022', 38500, 0, 46),
(1, 'PR023', 35000, 0, 42),
(1, 'PR024', 38900, 0, 47),
(1, 'PR025', 38900, 0, 21),
(1, 'PR026', 37000, 0, 2),
(1, 'PR027', 32500, 0, 0),
(1, 'PR028', 108800, 0, 2),
(1, 'PR029', 138800, 0, 28),
(1, 'PR032', 123400, 0, 0),
(1, 'PR034', 246900, 0, 15),
(1, 'PR035', 52900, 0, 13),
(1, 'PR036', 57740, 0, 25),
(1, 'PR037', 60480, 0, 4),
(1, 'PR038', 60480, 0, 13),
(1, 'PR039', 75000, 0, 1),
(1, 'PR040', 45000, 0, 2),
(1, 'PR046', 45000, 0, 9),
(1, 'PR047', 55000, 0, 10),
(1, 'PR048', 10144, 0, 0),
(1, 'PR049', 17000, 0, 130),
(1, 'PR051', 5000, 0, 3),
(1, 'PR052', 10000, 0, 118),
(1, 'PR055', 50000, 0, 2),
(1, 'PR059', 50000, 0, 9),
(1, 'PR061', 50000, 0, 6),
(1, 'PR062', 65000, 0, 1),
(1, 'PR064', 10000, 0, 0),
(1, 'PR065', 74200, 0, 3),
(2, 'PR042', 60480, 0, 1),
(3, 'PR013', 49700, 0, 5),
(4, 'PR002', 40200, 0, 10),
(4, 'PR008', 36500, 0, 25),
(4, 'PR009', 36500, 0, 10),
(4, 'PR014', 49700, 0, 4),
(4, 'PR019', 35000, 0, 50),
(4, 'PR025', 38900, 0, 30),
(4, 'PR047', 55000, 0, 10),
(4, 'PR049', 17000, 0, 30),
(4, 'PR051', 5000, 0, 20),
(4, 'PR055', 50000, 0, 10),
(4, 'PR059', 50000, 0, 14),
(4, 'PR061', 50000, 0, 10),
(5, 'PR036', 57740, 0, 1),
(6, 'PR013', 49700, 0, 25),
(6, 'PR014', 49700, 0, 25),
(7, 'PR007', 100000, 0, 6),
(8, 'PR006', 52000, 0, 5),
(8, 'PR010', 60480, 0, 75),
(8, 'PR011', 60480, 0, 10),
(8, 'PR026', 37000, 0, 10),
(8, 'PR027', 32500, 0, 7),
(8, 'PR034', 246900, 0, 10),
(8, 'PR035', 52900, 0, 25),
(8, 'PR036', 57740, 0, 5),
(8, 'PR038', 60480, 0, 10),
(8, 'PR039', 75000, 0, 2),
(9, 'PR019', 35000, 0, 51),
(9, 'PR022', 38500, 0, 51),
(9, 'PR023', 35000, 0, 51),
(9, 'PR024', 38900, 0, 51),
(9, 'PR025', 38900, 0, 51),
(10, 'PR001', 39000, 0, 20),
(10, 'PR003', 74400, 0, 20),
(10, 'PR013', 49700, 0, 25),
(10, 'PR014', 49700, 0, 25),
(11, 'PR066', 96000, 0, 1),
(12, 'PR021', 43500, 0, 51),
(13, 'PR005', 120000, 0, 10),
(14, 'PR048', 10144, 0, 150),
(15, 'PR001', 39000, 0, 10),
(15, 'PR002', 40200, 0, 20),
(15, 'PR003', 74400, 0, 30),
(16, 'PR006', 52000, 0, 10),
(16, 'PR010', 60480, 0, 100),
(16, 'PR011', 60480, 0, 10),
(16, 'PR026', 37000, 0, 10),
(16, 'PR027', 32500, 0, 10),
(16, 'PR034', 246900, 0, 10),
(16, 'PR035', 52900, 0, 20),
(16, 'PR036', 57740, 0, 20),
(16, 'PR038', 60480, 0, 15),
(17, 'PR001', 39000, 0, 31),
(17, 'PR013', 49700, 0, 20),
(17, 'PR014', 49700, 0, 20),
(17, 'PR055', 50000, 0, 15),
(17, 'PR064', 10000, 0, 13),
(18, 'PR027', 32500, 0, 5),
(18, 'PR029', 138800, 0, 10),
(18, 'PR039', 75000, 0, 3),
(18, 'PR042', 60480, 0, 5),
(19, 'PR002', 40200, 0, 20),
(19, 'PR005', 120000, 0, 5),
(19, 'PR013', 49700, 0, 30),
(19, 'PR014', 49700, 0, 31),
(20, 'PR021', 43500, 0, 25),
(21, 'PR010', 60480, 0, 50),
(21, 'PR011', 60480, 0, 5),
(21, 'PR028', 108800, 0, 2),
(21, 'PR034', 246900, 0, 10),
(21, 'PR035', 52900, 0, 20),
(21, 'PR036', 57740, 0, 20),
(21, 'PR038', 60480, 0, 20),
(22, 'PR002', 40200, 0, 1),
(23, 'PR003', 74400, 0, 1),
(24, 'PR010', 60480, 0, 3),
(25, 'PR013', 49700, 0, 2),
(26, 'PR014', 49700, 0, 3),
(27, 'PR068', 8500, 0, 36),
(28, 'PR001', 39000, 0, 20),
(28, 'PR003', 74400, 0, 50),
(28, 'PR009', 36500, 0, 20),
(28, 'PR019', 35000, 0, 20),
(28, 'PR023', 35000, 0, 30),
(29, 'PR049', 17000, 0, 150),
(29, 'PR055', 50000, 0, 30),
(29, 'PR066', 96000, 0, 1),
(30, 'PRO67', 168750, 0, 5),
(31, 'PR013', 49700, 0, 30),
(31, 'PR014', 49700, 0, 30),
(32, 'PR002', 40200, 0, 51),
(32, 'PR013', 49700, 0, 12),
(32, 'PR014', 49700, 0, 16),
(32, 'PR019', 35000, 0, 39),
(32, 'PR023', 35000, 0, 39),
(33, 'PR005', 120000, 0, 11),
(34, 'PR007', 100000, 0, 18);

-- --------------------------------------------------------

--
-- Table structure for table `_detail_pembelian_treatment`
--

CREATE TABLE `_detail_pembelian_treatment` (
  `id_transaksi` int(10) NOT NULL,
  `id_treatment` varchar(10) NOT NULL,
  `harga_aktual` double NOT NULL,
  `diskon_aktual` double NOT NULL,
  `qty` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_detail_pembelian_treatment`
--

INSERT INTO `_detail_pembelian_treatment` (`id_transaksi`, `id_treatment`, `harga_aktual`, `diskon_aktual`, `qty`) VALUES
(1, 'TR004', 75000, 0, 0),
(1, 'TR007', 60000, 0, 16),
(1, 'TR025', 27000, 0, 11),
(1, 'TR026', 27000, 0, 3),
(1, 'TR079', 30000, 0, 12),
(2, 'TR004', 75000, 0, 35),
(3, 'TR003', 13500, 0, 12),
(3, 'TR005', 59692, 0, 10),
(3, 'TR010', 37000, 0, 11),
(3, 'TR018', 127500, 0, 10),
(3, 'TR019', 97500, 0, 6),
(3, 'TR020', 97500, 0, 8),
(3, 'TR024', 32500, 0, 6),
(3, 'TR037', 57500, 0, 11),
(3, 'TR051', 157500, 0, 5),
(3, 'TR052', 202500, 0, 5),
(3, 'TR053', 127500, 0, 15),
(3, 'TR054', 172500, 0, 13),
(3, 'TR066', 63000, 0, 5),
(3, 'TR067', 57500, 0, 15),
(4, 'TR006', 30000, 0, 4),
(4, 'TR008', 72500, 0, 7),
(5, 'TR001', 10500, 0, 20),
(5, 'TR011', 161250, 0, 20),
(5, 'TR068', 300000, 0, 2),
(6, 'TR012', 26140, 0, 10),
(6, 'TR013', 32500, 0, 10),
(6, 'TR014', 57000, 0, 20),
(7, 'TR017', 72500, 0, 1),
(7, 'TR021', 82000, 0, 5),
(7, 'TR023', 97500, 0, 5),
(8, 'TR022', 3000, 0, 100),
(8, 'TR027', 30000, 0, 14),
(8, 'TR030', 39000, 0, 13),
(9, 'TR031', 206500, 0, 3),
(9, 'TR032', 32500, 0, 6),
(9, 'TR034', 56000, 0, 2),
(9, 'TR035', 56000, 0, 2),
(9, 'TR036', 100000, 0, 2),
(9, 'TR038', 683500, 0, 2),
(9, 'TR078', 70000, 0, 5),
(10, 'TR039', 219500, 0, 5),
(10, 'TR040', 64000, 0, 10),
(10, 'TR055', 648000, 0, 8),
(10, 'TR072', 128000, 0, 5),
(10, 'TR073', 256000, 0, 3),
(11, 'TR043', 10000, 0, 10),
(11, 'TR044', 25000, 0, 10),
(11, 'TR045', 25000, 0, 10),
(11, 'TR046', 67000, 0, 10),
(11, 'TR047', 83000, 0, 10),
(12, 'TR048', 80500, 0, 10),
(12, 'TR049', 104000, 0, 10),
(12, 'TR050', 231000, 0, 10),
(12, 'TR075', 315500, 0, 5),
(12, 'TR076', 480500, 0, 5),
(13, 'TR060', 500, 0, 100),
(13, 'TR062', 35000, 0, 200),
(13, 'TR063', 75000, 0, 15),
(13, 'TR064', 50000, 0, 10),
(14, 'TR045', 25000, 0, 10),
(14, 'TR065', 29500, 0, 5),
(15, 'TR082', 10000, 0, 10),
(15, 'TR083', 25000, 0, 10),
(16, 'TR085', 13500, 0, 100),
(16, 'TR086', 20000, 0, 50),
(17, 'TR003', 13500, 0, 50),
(18, 'TR087', 675500, 0, 1),
(19, 'TR033', 32500, 0, 5),
(20, 'TR026', 27000, 0, 10),
(21, 'TR019', 97500, 0, 10),
(21, 'TR064', 50000, 0, 50),
(21, 'TR066', 63000, 0, 5),
(22, 'TR013', 32500, 0, 7),
(22, 'TR017', 72500, 0, 1),
(22, 'TR020', 97500, 0, 4),
(22, 'TR061', 10000, 0, 4),
(22, 'TR069', 75000, 0, 100),
(23, 'TR087', 675500, 0, 5),
(24, 'TR065', 29500, 0, 6),
(25, 'TR081', 20000, 0, 5),
(26, 'TR020', 97500, 0, 10),
(27, 'TR065', 29500, 0, 6),
(28, 'TR043', 10000, 0, 10),
(29, 'TR043', 10000, 0, 10),
(30, 'TR003', 13500, 0, 10),
(31, 'TR066', 63000, 0, 10),
(32, 'TR019', 97500, 0, 20),
(32, 'TR063', 75000, 0, 20),
(33, 'TR003', 13500, 0, 50),
(33, 'TR014', 57000, 0, 10),
(34, 'TR001', 10500, 0, 20),
(34, 'TR005', 59692, 0, 10),
(34, 'TR010', 37000, 0, 10),
(34, 'TR018', 127500, 0, 10),
(34, 'TR027', 30000, 0, 10),
(34, 'TR031', 206500, 0, 5),
(34, 'TR038', 683500, 0, 5),
(34, 'TR068', 300000, 0, 5),
(34, 'TR084', 25000, 0, 10),
(34, 'TR087', 675500, 0, 5),
(35, 'TR018', 127500, 0, 10),
(35, 'TR020', 97500, 0, 10),
(35, 'TR045', 25000, 0, 2),
(35, 'TR066', 63000, 0, 10),
(35, 'TR070', 648000, 0, 1),
(36, 'TR065', 29500, 0, 10),
(37, 'TR060', 500, 0, 100),
(37, 'TR066', 63000, 0, 20);

-- --------------------------------------------------------

--
-- Table structure for table `_detail_penjualan_produk`
--

CREATE TABLE `_detail_penjualan_produk` (
  `id_transaksi` int(10) NOT NULL,
  `id_produk` varchar(10) NOT NULL,
  `harga_aktual` double NOT NULL,
  `diskon_aktual` double NOT NULL,
  `qty` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_detail_penjualan_produk`
--

INSERT INTO `_detail_penjualan_produk` (`id_transaksi`, `id_produk`, `harga_aktual`, `diskon_aktual`, `qty`) VALUES
(1, 'PR002', 99000, 0, 1),
(1, 'PR003', 110000, 0, 1),
(1, 'PR008', 55000, 0, 1),
(1, 'PR009', 55000, 0, 2),
(1, 'PR010', 80000, 0, 4),
(1, 'PR014', 125000, 0, 3),
(1, 'PR019', 65000, 0, 1),
(1, 'PR021', 93000, 0, 1),
(1, 'PR023', 65000, 0, 2),
(1, 'PR024', 93000, 0, 2),
(1, 'PR036', 75000, 0, 1),
(1, 'PR038', 99000, 0, 1),
(1, 'PR051', 35000, 0, 1),
(2, 'PR001', 99000, 0, 1),
(2, 'PR002', 99000, 0, 1),
(2, 'PR003', 110000, 0, 2),
(2, 'PR010', 80000, 0, 3),
(2, 'PR013', 105000, 0, 2),
(2, 'PR014', 125000, 0, 1),
(2, 'PR022', 93000, 0, 1),
(2, 'PR023', 65000, 0, 4),
(2, 'PR025', 93000, 0, 1),
(2, 'PR026', 65000, 0, 1),
(2, 'PR034', 303000, 0, 2),
(2, 'PR035', 70000, 0, 2),
(2, 'PR036', 75000, 0, 1),
(2, 'PR052', 15000, 0, 1),
(3, 'PR001', 99000, 0, 2),
(3, 'PR004', 125000, 0, 1),
(3, 'PR010', 80000, 0, 5),
(3, 'PR013', 105000, 0, 2),
(3, 'PR019', 65000, 0, 6),
(3, 'PR021', 93000, 0, 1),
(3, 'PR022', 93000, 0, 2),
(3, 'PR029', 171000, 0, 1),
(3, 'PR036', 75000, 0, 1),
(3, 'PR038', 99000, 0, 1),
(3, 'PR042', 73000, 0, 1),
(3, 'PR047', 85000, 0, 1),
(3, 'PR051', 35000, 0, 1),
(3, 'PR052', 15000, 0, 1),
(4, 'PR001', 99000, 0, 1),
(4, 'PR002', 99000, 0, 1),
(4, 'PR010', 80000, 0, 3),
(4, 'PR014', 125000, 0, 1),
(4, 'PR019', 65000, 0, 1),
(4, 'PR021', 93000, 0, 1),
(4, 'PR022', 93000, 0, 2),
(4, 'PR023', 65000, 0, 1),
(4, 'PR035', 70000, 0, 1),
(4, 'PR036', 75000, 0, 1),
(4, 'PR038', 99000, 0, 1),
(4, 'PR047', 85000, 0, 1),
(5, 'PR001', 99000, 0, 1),
(5, 'PR002', 99000, 0, 4),
(5, 'PR008', 55000, 0, 1),
(5, 'PR009', 55000, 0, 2),
(5, 'PR010', 80000, 0, 9),
(5, 'PR019', 65000, 0, 2),
(5, 'PR021', 93000, 0, 3),
(5, 'PR022', 93000, 0, 1),
(5, 'PR023', 65000, 0, 3),
(5, 'PR025', 93000, 0, 1),
(5, 'PR026', 65000, 0, 1),
(5, 'PR029', 171000, 0, 1),
(5, 'PR034', 303000, 0, 1),
(5, 'PR036', 75000, 0, 1),
(5, 'PR038', 99000, 0, 1),
(5, 'PR047', 85000, 0, 1),
(5, 'PR051', 35000, 0, 1),
(5, 'PR052', 15000, 0, 1),
(5, 'PR055', 100000, 0, 1),
(6, 'PR002', 99000, 0, 1),
(6, 'PR010', 80000, 0, 2),
(6, 'PR019', 65000, 0, 1),
(6, 'PR023', 65000, 0, 2),
(6, 'PR024', 93000, 0, 1),
(6, 'PR029', 171000, 0, 1),
(6, 'PR038', 99000, 0, 1),
(7, 'PR001', 99000, 0, 1),
(7, 'PR002', 99000, 0, 1),
(7, 'PR003', 110000, 0, 1),
(7, 'PR004', 125000, 0, 1),
(7, 'PR010', 80000, 0, 7),
(7, 'PR019', 65000, 0, 2),
(7, 'PR021', 93000, 0, 1),
(7, 'PR022', 93000, 0, 1),
(7, 'PR023', 65000, 0, 3),
(7, 'PR024', 93000, 0, 3),
(7, 'PR025', 93000, 0, 1),
(7, 'PR038', 99000, 0, 1),
(7, 'PR059', 85000, 0, 1),
(8, 'PR001', 99000, 0, 2),
(8, 'PR002', 99000, 0, 1),
(8, 'PR010', 80000, 0, 3),
(8, 'PR019', 65000, 0, 1),
(8, 'PR021', 93000, 0, 2),
(8, 'PR023', 65000, 0, 1),
(8, 'PR024', 93000, 0, 1),
(8, 'PR029', 171000, 0, 1),
(8, 'PR034', 303000, 0, 2),
(8, 'PR036', 75000, 0, 1),
(8, 'PR038', 99000, 0, 1),
(8, 'PR049', 22000, 0, 10),
(8, 'PR052', 15000, 0, 1),
(9, 'PR001', 99000, 0, 2),
(9, 'PR003', 110000, 0, 2),
(9, 'PR010', 80000, 0, 1),
(9, 'PR014', 125000, 0, 3),
(9, 'PR019', 65000, 0, 1),
(9, 'PR021', 93000, 0, 1),
(9, 'PR022', 93000, 0, 3),
(9, 'PR024', 93000, 0, 2),
(9, 'PR025', 93000, 0, 1),
(9, 'PR034', 303000, 0, 1),
(9, 'PR055', 100000, 0, 2),
(10, 'PR010', 80000, 0, 2),
(10, 'PR013', 105000, 0, 2),
(10, 'PR014', 125000, 0, 2),
(10, 'PR021', 93000, 0, 1),
(10, 'PR023', 65000, 0, 1),
(10, 'PR024', 93000, 0, 1),
(10, 'PR029', 171000, 0, 1),
(10, 'PR038', 99000, 0, 1),
(10, 'PR052', 15000, 0, 1),
(11, 'PR001', 99000, 0, 2),
(11, 'PR002', 99000, 0, 1),
(11, 'PR003', 110000, 0, 1),
(11, 'PR010', 80000, 0, 7),
(11, 'PR013', 105000, 0, 1),
(11, 'PR014', 125000, 0, 3),
(11, 'PR019', 65000, 0, 3),
(11, 'PR021', 93000, 0, 1),
(11, 'PR022', 93000, 0, 1),
(11, 'PR023', 65000, 0, 3),
(11, 'PR036', 75000, 0, 2),
(11, 'PR038', 99000, 0, 1),
(11, 'PR052', 15000, 0, 1),
(12, 'PR003', 110000, 0, 1),
(12, 'PR010', 80000, 0, 2),
(12, 'PR013', 105000, 0, 5),
(12, 'PR023', 65000, 0, 3),
(12, 'PR024', 93000, 0, 1),
(12, 'PR036', 75000, 0, 2),
(12, 'PR055', 100000, 0, 1),
(13, 'PR001', 99000, 0, 2),
(13, 'PR003', 110000, 0, 2),
(13, 'PR004', 125000, 0, 1),
(13, 'PR009', 55000, 0, 1),
(13, 'PR010', 80000, 0, 3),
(13, 'PR013', 105000, 0, 2),
(13, 'PR019', 65000, 0, 2),
(13, 'PR021', 93000, 0, 1),
(13, 'PR022', 93000, 0, 1),
(13, 'PR023', 65000, 0, 1),
(13, 'PR025', 93000, 0, 1),
(13, 'PR028', 134000, 0, 1),
(13, 'PR029', 171000, 0, 1),
(13, 'PR034', 303000, 0, 2),
(13, 'PR035', 70000, 0, 1),
(13, 'PR036', 75000, 0, 1),
(13, 'PR038', 99000, 0, 1),
(13, 'PR049', 22000, 0, 20),
(13, 'PR051', 35000, 0, 2),
(13, 'PR052', 15000, 0, 1),
(14, 'PR002', 99000, 0, 1),
(14, 'PR003', 110000, 0, 2),
(14, 'PR008', 55000, 0, 1),
(14, 'PR009', 55000, 0, 1),
(14, 'PR010', 80000, 0, 3),
(14, 'PR013', 105000, 0, 2),
(14, 'PR014', 125000, 0, 3),
(14, 'PR021', 93000, 0, 1),
(14, 'PR023', 65000, 0, 2),
(14, 'PR024', 93000, 0, 2),
(14, 'PR028', 134000, 0, 1),
(14, 'PR029', 171000, 0, 1),
(14, 'PR034', 303000, 0, 1),
(14, 'PR035', 70000, 0, 1),
(14, 'PR036', 75000, 0, 1),
(14, 'PR037', 73000, 0, 1),
(14, 'PR052', 15000, 0, 2),
(14, 'PR059', 85000, 0, 1),
(15, 'PR003', 110000, 0, 1),
(15, 'PR010', 80000, 0, 1),
(15, 'PR013', 105000, 0, 2),
(15, 'PR014', 125000, 0, 1),
(15, 'PR021', 93000, 0, 1),
(15, 'PR023', 65000, 0, 1),
(15, 'PR035', 70000, 0, 1),
(15, 'PR038', 99000, 0, 1),
(15, 'PR046', 65000, 0, 1),
(15, 'PR047', 85000, 0, 1),
(16, 'PR002', 99000, 0, 1),
(16, 'PR003', 110000, 0, 1),
(16, 'PR006', 65000, 0, 1),
(16, 'PR010', 80000, 0, 1),
(16, 'PR013', 105000, 0, 3),
(16, 'PR014', 125000, 0, 3),
(16, 'PR021', 93000, 0, 2),
(16, 'PR022', 93000, 0, 1),
(16, 'PR026', 65000, 0, 1),
(16, 'PR029', 171000, 0, 1),
(16, 'PR039', 97000, 0, 1),
(16, 'PR051', 35000, 0, 1),
(17, 'PR003', 110000, 0, 2),
(17, 'PR006', 65000, 0, 1),
(17, 'PR008', 55000, 0, 1),
(17, 'PR009', 55000, 0, 1),
(17, 'PR010', 80000, 0, 2),
(17, 'PR011', 80000, 0, 3),
(17, 'PR013', 105000, 0, 2),
(17, 'PR014', 125000, 0, 1),
(17, 'PR019', 65000, 0, 1),
(17, 'PR021', 93000, 0, 1),
(17, 'PR023', 65000, 0, 2),
(17, 'PR034', 303000, 0, 1),
(17, 'PR036', 75000, 0, 1),
(18, 'PR010', 80000, 0, 1),
(18, 'PR013', 105000, 0, 2),
(18, 'PR024', 93000, 0, 1),
(18, 'PR035', 70000, 0, 1),
(18, 'PR052', 15000, 0, 2),
(19, 'PR003', 110000, 0, 1),
(19, 'PR008', 55000, 0, 1),
(19, 'PR010', 80000, 0, 4),
(19, 'PR013', 105000, 0, 1),
(19, 'PR014', 125000, 0, 3),
(19, 'PR019', 65000, 0, 1),
(19, 'PR021', 93000, 0, 1),
(19, 'PR024', 93000, 0, 1),
(19, 'PR026', 65000, 0, 2),
(19, 'PR029', 171000, 0, 2),
(19, 'PR034', 303000, 0, 3),
(19, 'PR035', 70000, 0, 1),
(19, 'PR038', 99000, 0, 1),
(19, 'PR047', 85000, 0, 1),
(20, 'PR001', 99000, 0, 2),
(20, 'PR003', 110000, 0, 1),
(20, 'PR004', 125000, 0, 1),
(20, 'PR007', 250000, 0, 1),
(20, 'PR010', 80000, 0, 3),
(20, 'PR014', 125000, 0, 6),
(20, 'PR019', 65000, 0, 3),
(20, 'PR021', 93000, 0, 4),
(20, 'PR023', 65000, 0, 2),
(20, 'PR024', 93000, 0, 1),
(20, 'PR027', 65000, 0, 1),
(20, 'PR029', 171000, 0, 1),
(20, 'PR035', 70000, 0, 1),
(20, 'PR038', 99000, 0, 2),
(20, 'PR049', 22000, 0, 10),
(20, 'PR052', 15000, 0, 1),
(20, 'PR066', 220000, 0, 1),
(21, 'PR001', 99000, 0, 1),
(21, 'PR002', 99000, 0, 1),
(21, 'PR004', 125000, 0, 1),
(21, 'PR006', 65000, 0, 1),
(21, 'PR010', 80000, 0, 2),
(21, 'PR013', 105000, 0, 2),
(21, 'PR019', 65000, 0, 3),
(21, 'PR023', 65000, 0, 2),
(21, 'PR036', 75000, 0, 1),
(21, 'PR039', 97000, 0, 1),
(22, 'PR003', 110000, 0, 1),
(22, 'PR009', 55000, 0, 1),
(22, 'PR010', 80000, 0, 1),
(22, 'PR013', 105000, 0, 3),
(22, 'PR014', 125000, 0, 3),
(22, 'PR021', 93000, 0, 1),
(22, 'PR027', 65000, 0, 1),
(22, 'PR036', 75000, 0, 1),
(23, 'PR001', 99000, 0, 1),
(23, 'PR003', 110000, 0, 1),
(23, 'PR010', 80000, 0, 2),
(23, 'PR013', 105000, 0, 1),
(23, 'PR014', 125000, 0, 1),
(23, 'PR021', 93000, 0, 3),
(23, 'PR022', 93000, 0, 1),
(23, 'PR034', 303000, 0, 1),
(24, 'PR001', 99000, 0, 6),
(24, 'PR003', 110000, 0, 1),
(24, 'PR004', 125000, 0, 1),
(24, 'PR010', 80000, 0, 5),
(24, 'PR013', 105000, 0, 1),
(24, 'PR014', 125000, 0, 1),
(24, 'PR019', 65000, 0, 1),
(24, 'PR021', 93000, 0, 3),
(24, 'PR022', 93000, 0, 2),
(24, 'PR024', 93000, 0, 2),
(24, 'PR029', 171000, 0, 1),
(24, 'PR034', 303000, 0, 1),
(24, 'PR036', 75000, 0, 1),
(24, 'PR047', 85000, 0, 1),
(25, 'PR002', 99000, 0, 3),
(25, 'PR003', 110000, 0, 2),
(25, 'PR010', 80000, 0, 1),
(25, 'PR013', 105000, 0, 3),
(25, 'PR014', 125000, 0, 3),
(25, 'PR019', 65000, 0, 1),
(25, 'PR022', 93000, 0, 1),
(25, 'PR023', 65000, 0, 2),
(25, 'PR035', 70000, 0, 1),
(25, 'PR038', 99000, 0, 2),
(25, 'PR052', 15000, 0, 1),
(26, 'PR001', 99000, 0, 2),
(26, 'PR002', 99000, 0, 1),
(26, 'PR003', 110000, 0, 4),
(26, 'PR004', 125000, 0, 1),
(26, 'PR010', 80000, 0, 7),
(26, 'PR013', 105000, 0, 1),
(26, 'PR014', 125000, 0, 2),
(26, 'PR019', 65000, 0, 3),
(26, 'PR022', 93000, 0, 2),
(26, 'PR023', 65000, 0, 1),
(26, 'PR025', 93000, 0, 1),
(26, 'PR026', 65000, 0, 1),
(26, 'PR027', 65000, 0, 1),
(26, 'PR029', 171000, 0, 1),
(26, 'PR035', 70000, 0, 1),
(26, 'PR036', 75000, 0, 1),
(26, 'PR052', 15000, 0, 1),
(27, 'PR002', 99000, 0, 1),
(27, 'PR003', 110000, 0, 1),
(27, 'PR005', 200000, 0, 2),
(27, 'PR010', 80000, 0, 3),
(27, 'PR013', 105000, 0, 3),
(27, 'PR019', 65000, 0, 3),
(27, 'PR023', 65000, 0, 3),
(27, 'PR024', 93000, 0, 1),
(27, 'PR027', 65000, 0, 1),
(27, 'PR029', 171000, 0, 1),
(27, 'PR035', 70000, 0, 2),
(27, 'PR036', 75000, 0, 2),
(27, 'PR038', 99000, 0, 1),
(27, 'PR052', 15000, 0, 2),
(27, 'PR055', 100000, 0, 2),
(27, 'PR059', 85000, 0, 1),
(28, 'PR001', 99000, 0, 2),
(28, 'PR002', 99000, 0, 2),
(28, 'PR005', 200000, 0, 2),
(28, 'PR010', 80000, 0, 4),
(28, 'PR013', 105000, 0, 1),
(28, 'PR014', 125000, 0, 3),
(28, 'PR019', 65000, 0, 1),
(28, 'PR021', 93000, 0, 3),
(28, 'PR022', 93000, 0, 2),
(28, 'PR023', 65000, 0, 1),
(28, 'PR035', 70000, 0, 1),
(28, 'PR036', 75000, 0, 2),
(28, 'PR038', 99000, 0, 1),
(28, 'PR048', 13000, 0, 5),
(28, 'PR052', 15000, 0, 3),
(28, 'PR055', 100000, 0, 1),
(29, 'PR001', 99000, 0, 4),
(29, 'PR002', 99000, 0, 1),
(29, 'PR004', 125000, 0, 1),
(29, 'PR010', 80000, 0, 4),
(29, 'PR011', 80000, 0, 1),
(29, 'PR013', 105000, 0, 3),
(29, 'PR019', 65000, 0, 4),
(29, 'PR021', 93000, 0, 3),
(29, 'PR023', 65000, 0, 4),
(29, 'PR024', 93000, 0, 1),
(29, 'PR027', 65000, 0, 2),
(29, 'PR035', 70000, 0, 1),
(29, 'PR036', 75000, 0, 1),
(29, 'PR052', 15000, 0, 4),
(29, 'PR055', 100000, 0, 1),
(30, 'PR010', 80000, 0, 1),
(30, 'PR013', 105000, 0, 1),
(30, 'PR021', 93000, 0, 1),
(30, 'PR022', 93000, 0, 1),
(30, 'PR029', 171000, 0, 1),
(30, 'PR036', 75000, 0, 1),
(31, 'PR002', 99000, 0, 2),
(31, 'PR007', 250000, 0, 2),
(31, 'PR009', 55000, 0, 1),
(31, 'PR010', 80000, 0, 3),
(31, 'PR013', 105000, 0, 2),
(31, 'PR014', 125000, 0, 5),
(31, 'PR019', 65000, 0, 1),
(31, 'PR021', 93000, 0, 1),
(31, 'PR022', 93000, 0, 1),
(31, 'PR023', 65000, 0, 1),
(31, 'PR024', 93000, 0, 1),
(31, 'PR047', 85000, 0, 1),
(32, 'PR001', 99000, 0, 1),
(32, 'PR007', 250000, 0, 1),
(32, 'PR010', 80000, 0, 2),
(32, 'PR011', 80000, 0, 2),
(32, 'PR013', 105000, 0, 1),
(32, 'PR014', 125000, 0, 1),
(32, 'PR019', 65000, 0, 2),
(32, 'PR021', 93000, 0, 1),
(32, 'PR022', 93000, 0, 1),
(32, 'PR023', 65000, 0, 2),
(32, 'PR024', 93000, 0, 2),
(32, 'PR027', 65000, 0, 1),
(32, 'PR035', 70000, 0, 1),
(32, 'PR036', 75000, 0, 1),
(32, 'PR038', 99000, 0, 4),
(32, 'PR046', 65000, 0, 1),
(32, 'PR047', 85000, 0, 2),
(32, 'PR051', 35000, 0, 1),
(32, 'PR055', 100000, 0, 1),
(33, 'PR001', 99000, 0, 4),
(33, 'PR003', 110000, 0, 2),
(33, 'PR004', 125000, 0, 1),
(33, 'PR006', 65000, 0, 1),
(33, 'PR010', 80000, 0, 1),
(33, 'PR011', 80000, 0, 2),
(33, 'PR013', 105000, 0, 1),
(33, 'PR014', 125000, 0, 2),
(33, 'PR019', 65000, 0, 1),
(33, 'PR022', 93000, 0, 1),
(33, 'PR023', 65000, 0, 3),
(33, 'PR024', 93000, 0, 1),
(33, 'PR035', 70000, 0, 1),
(33, 'PR039', 97000, 0, 1),
(33, 'PR047', 85000, 0, 2),
(33, 'PR052', 15000, 0, 3),
(33, 'PR059', 85000, 0, 1),
(34, 'PR001', 99000, 0, 3),
(34, 'PR002', 99000, 0, 3),
(34, 'PR003', 110000, 0, 1),
(34, 'PR010', 80000, 0, 5),
(34, 'PR019', 65000, 0, 1),
(34, 'PR021', 93000, 0, 3),
(34, 'PR023', 65000, 0, 4),
(34, 'PR024', 93000, 0, 1),
(34, 'PR034', 303000, 0, 1),
(34, 'PR035', 70000, 0, 1),
(34, 'PR036', 75000, 0, 2),
(34, 'PR046', 65000, 0, 1),
(34, 'PR048', 13000, 0, 10),
(34, 'PR052', 15000, 0, 2),
(34, 'PR055', 100000, 0, 1),
(35, 'PR001', 99000, 0, 1),
(35, 'PR002', 99000, 0, 1),
(35, 'PR003', 110000, 0, 1),
(35, 'PR010', 80000, 0, 5),
(35, 'PR011', 80000, 0, 1),
(35, 'PR019', 65000, 0, 1),
(35, 'PR021', 93000, 0, 1),
(35, 'PR022', 93000, 0, 4),
(35, 'PR024', 93000, 0, 1),
(35, 'PR034', 303000, 0, 1),
(35, 'PR055', 100000, 0, 1),
(36, 'PR003', 110000, 0, 2),
(36, 'PR010', 80000, 0, 5),
(36, 'PR019', 65000, 0, 1),
(36, 'PR021', 93000, 0, 4),
(36, 'PR022', 93000, 0, 1),
(36, 'PR023', 65000, 0, 2),
(36, 'PR024', 93000, 0, 1),
(36, 'PR026', 65000, 0, 1),
(36, 'PR034', 303000, 0, 2),
(36, 'PR049', 22000, 0, 10),
(36, 'PR052', 15000, 0, 1),
(37, 'PR003', 110000, 0, 1),
(37, 'PR010', 80000, 0, 2),
(37, 'PR011', 80000, 0, 1),
(37, 'PR019', 65000, 0, 3),
(37, 'PR021', 93000, 0, 8),
(37, 'PR023', 65000, 0, 2),
(37, 'PR027', 65000, 0, 1),
(37, 'PR029', 171000, 0, 1),
(37, 'PR035', 70000, 0, 1),
(37, 'PR036', 75000, 0, 1),
(37, 'PR049', 22000, 0, 10),
(38, 'PR010', 80000, 0, 2),
(38, 'PR013', 105000, 0, 1),
(38, 'PR019', 65000, 0, 2),
(38, 'PR021', 93000, 0, 2),
(38, 'PR022', 93000, 0, 2),
(38, 'PR027', 65000, 0, 1),
(38, 'PR029', 171000, 0, 1),
(38, 'PR034', 303000, 0, 1),
(38, 'PR038', 99000, 0, 1),
(38, 'PR047', 85000, 0, 2),
(38, 'PR051', 35000, 0, 1),
(38, 'PR055', 100000, 0, 1),
(39, 'PR001', 99000, 0, 2),
(39, 'PR003', 110000, 0, 2),
(39, 'PR010', 80000, 0, 1),
(39, 'PR013', 105000, 0, 5),
(39, 'PR014', 125000, 0, 12),
(39, 'PR021', 93000, 0, 2),
(39, 'PR022', 93000, 0, 1),
(39, 'PR023', 65000, 0, 1),
(39, 'PR024', 93000, 0, 2),
(39, 'PR026', 65000, 0, 1),
(39, 'PR038', 99000, 0, 1),
(39, 'PR048', 13000, 0, 10),
(39, 'PR064', 35000, 0, 2),
(40, 'PR002', 99000, 0, 1),
(40, 'PR003', 110000, 0, 2),
(40, 'PR010', 80000, 0, 1),
(40, 'PR013', 105000, 0, 2),
(40, 'PR014', 125000, 0, 1),
(40, 'PR019', 65000, 0, 1),
(40, 'PR023', 65000, 0, 1),
(40, 'PR024', 93000, 0, 1),
(40, 'PR025', 93000, 0, 2),
(40, 'PR035', 70000, 0, 1),
(40, 'PR038', 99000, 0, 1),
(40, 'PR047', 85000, 0, 1),
(41, 'PR002', 99000, 0, 1),
(41, 'PR003', 110000, 0, 2),
(41, 'PR010', 80000, 0, 3),
(41, 'PR013', 105000, 0, 4),
(41, 'PR014', 125000, 0, 2),
(41, 'PR019', 65000, 0, 1),
(41, 'PR023', 65000, 0, 4),
(41, 'PR024', 93000, 0, 1),
(41, 'PR034', 303000, 0, 1),
(41, 'PR035', 70000, 0, 1),
(41, 'PR038', 99000, 0, 1),
(41, 'PR052', 15000, 0, 1),
(42, 'PR001', 99000, 0, 1),
(42, 'PR002', 99000, 0, 2),
(42, 'PR003', 110000, 0, 1),
(42, 'PR007', 250000, 0, 1),
(42, 'PR009', 55000, 0, 1),
(42, 'PR010', 80000, 0, 4),
(42, 'PR011', 80000, 0, 1),
(42, 'PR013', 105000, 0, 2),
(42, 'PR014', 125000, 0, 3),
(42, 'PR019', 65000, 0, 2),
(42, 'PR023', 65000, 0, 1),
(42, 'PR024', 93000, 0, 1),
(42, 'PR025', 93000, 0, 1),
(42, 'PR027', 65000, 0, 1),
(42, 'PR035', 70000, 0, 2),
(42, 'PR036', 75000, 0, 1),
(42, 'PR038', 99000, 0, 1),
(42, 'PR047', 85000, 0, 1),
(42, 'PR051', 35000, 0, 1),
(42, 'PR052', 15000, 0, 1),
(43, 'PR001', 99000, 0, 2),
(43, 'PR003', 110000, 0, 2),
(43, 'PR007', 250000, 0, 1),
(43, 'PR008', 55000, 0, 1),
(43, 'PR010', 80000, 0, 6),
(43, 'PR011', 80000, 0, 1),
(43, 'PR013', 105000, 0, 1),
(43, 'PR019', 65000, 0, 3),
(43, 'PR021', 93000, 0, 2),
(43, 'PR022', 93000, 0, 1),
(43, 'PR023', 65000, 0, 1),
(43, 'PR026', 65000, 0, 1),
(43, 'PR029', 171000, 0, 1),
(43, 'PR034', 303000, 0, 2),
(43, 'PR038', 99000, 0, 2),
(44, 'PR002', 99000, 0, 1),
(44, 'PR005', 200000, 0, 1),
(44, 'PR010', 80000, 0, 3),
(44, 'PR013', 105000, 0, 1),
(44, 'PR019', 65000, 0, 1),
(44, 'PR022', 93000, 0, 1),
(44, 'PR023', 65000, 0, 1),
(44, 'PR024', 93000, 0, 1),
(44, 'PR026', 65000, 0, 1),
(44, 'PR029', 171000, 0, 1),
(44, 'PR036', 75000, 0, 2),
(44, 'PR064', 35000, 0, 1),
(45, 'PR001', 99000, 0, 1),
(45, 'PR002', 99000, 0, 2),
(45, 'PR003', 110000, 0, 2),
(45, 'PR005', 200000, 0, 1),
(45, 'PR010', 80000, 0, 3),
(45, 'PR013', 105000, 0, 2),
(45, 'PR014', 125000, 0, 4),
(45, 'PR019', 65000, 0, 2),
(45, 'PR021', 93000, 0, 3),
(45, 'PR023', 65000, 0, 2),
(45, 'PR024', 93000, 0, 2),
(45, 'PR027', 65000, 0, 1),
(45, 'PR035', 70000, 0, 2),
(45, 'PR036', 75000, 0, 2),
(45, 'PR038', 99000, 0, 4),
(45, 'PR052', 15000, 0, 1),
(45, 'PR055', 100000, 0, 1),
(46, 'PR002', 99000, 0, 1),
(46, 'PR008', 55000, 0, 1),
(46, 'PR009', 55000, 0, 1),
(46, 'PR010', 80000, 0, 3),
(46, 'PR019', 65000, 0, 1),
(46, 'PR022', 93000, 0, 1),
(46, 'PR023', 65000, 0, 1),
(46, 'PR025', 93000, 0, 1),
(46, 'PR035', 70000, 0, 1),
(46, 'PR036', 75000, 0, 1),
(47, 'PR001', 99000, 0, 1),
(47, 'PR003', 110000, 0, 2),
(47, 'PR006', 65000, 0, 1),
(47, 'PR008', 55000, 0, 1),
(47, 'PR009', 55000, 0, 1),
(47, 'PR010', 80000, 0, 2),
(47, 'PR013', 105000, 0, 2),
(47, 'PR014', 125000, 0, 1),
(47, 'PR019', 65000, 0, 1),
(47, 'PR021', 93000, 0, 1),
(47, 'PR023', 65000, 0, 2),
(47, 'PR024', 93000, 0, 1),
(47, 'PR035', 70000, 0, 2),
(47, 'PR036', 75000, 0, 1),
(47, 'PR038', 99000, 0, 1),
(47, 'PR048', 13000, 0, 10),
(48, 'PR002', 99000, 0, 2),
(48, 'PR006', 65000, 0, 1),
(48, 'PR010', 80000, 0, 5),
(48, 'PR013', 105000, 0, 1),
(48, 'PR019', 65000, 0, 2),
(48, 'PR022', 93000, 0, 1),
(48, 'PR023', 65000, 0, 4),
(48, 'PR024', 93000, 0, 2),
(48, 'PR025', 93000, 0, 1),
(48, 'PR035', 70000, 0, 1),
(48, 'PR036', 75000, 0, 2),
(48, 'PR049', 22000, 0, 10),
(48, 'PR064', 35000, 0, 1),
(49, 'PR001', 99000, 0, 1),
(49, 'PR002', 99000, 0, 1),
(49, 'PR003', 110000, 0, 1),
(49, 'PR006', 65000, 0, 1),
(49, 'PR010', 80000, 0, 4),
(49, 'PR013', 105000, 0, 1),
(49, 'PR014', 125000, 0, 1),
(49, 'PR019', 65000, 0, 1),
(49, 'PR023', 65000, 0, 2),
(49, 'PR024', 93000, 0, 1),
(49, 'PR034', 303000, 0, 2),
(49, 'PR035', 70000, 0, 1),
(49, 'PR049', 22000, 0, 10),
(49, 'PR052', 15000, 0, 1),
(50, 'PR001', 99000, 0, 1),
(50, 'PR002', 99000, 0, 2),
(50, 'PR003', 110000, 0, 2),
(50, 'PR004', 125000, 0, 2),
(50, 'PR006', 65000, 0, 2),
(50, 'PR009', 55000, 0, 1),
(50, 'PR010', 80000, 0, 7),
(50, 'PR011', 80000, 0, 1),
(50, 'PR013', 105000, 0, 2),
(50, 'PR014', 125000, 0, 1),
(50, 'PR019', 65000, 0, 1),
(50, 'PR023', 65000, 0, 3),
(50, 'PR024', 93000, 0, 1),
(50, 'PR025', 93000, 0, 1),
(50, 'PR026', 65000, 0, 3),
(50, 'PR034', 303000, 0, 2),
(50, 'PR035', 70000, 0, 2),
(50, 'PR036', 75000, 0, 1),
(50, 'PR038', 99000, 0, 1),
(50, 'PR052', 15000, 0, 3),
(50, 'PR064', 35000, 0, 1),
(51, 'PR001', 99000, 0, 2),
(51, 'PR009', 55000, 0, 1),
(51, 'PR010', 80000, 0, 2),
(51, 'PR011', 80000, 0, 1),
(51, 'PR013', 105000, 0, 2),
(51, 'PR014', 125000, 0, 2),
(51, 'PR019', 65000, 0, 1),
(51, 'PR021', 93000, 0, 1),
(51, 'PR022', 93000, 0, 2),
(51, 'PR036', 75000, 0, 1),
(51, 'PR038', 99000, 0, 1),
(51, 'PR040', 90000, 0, 1),
(51, 'PR059', 85000, 0, 1),
(52, 'PR001', 99000, 0, 1),
(52, 'PR002', 99000, 0, 2),
(52, 'PR005', 200000, 0, 1),
(52, 'PR010', 80000, 0, 1),
(52, 'PR013', 105000, 0, 4),
(52, 'PR014', 125000, 0, 1),
(52, 'PR019', 65000, 0, 2),
(52, 'PR021', 93000, 0, 2),
(52, 'PR023', 65000, 0, 2),
(52, 'PR024', 93000, 0, 1),
(52, 'PR026', 65000, 0, 1),
(52, 'PR035', 70000, 0, 1),
(52, 'PR036', 75000, 0, 1),
(52, 'PR065', 106000, 0, 1),
(53, 'PR002', 99000, 0, 1),
(53, 'PR003', 110000, 0, 2),
(53, 'PR004', 125000, 0, 2),
(53, 'PR005', 200000, 0, 1),
(53, 'PR006', 65000, 0, 1),
(53, 'PR010', 80000, 0, 3),
(53, 'PR013', 105000, 0, 1),
(53, 'PR014', 125000, 0, 5),
(53, 'PR019', 65000, 0, 1),
(53, 'PR021', 93000, 0, 1),
(53, 'PR023', 65000, 0, 1),
(53, 'PR024', 93000, 0, 1),
(53, 'PR035', 70000, 0, 1),
(53, 'PR038', 99000, 0, 1),
(53, 'PR055', 100000, 0, 3),
(54, 'PR002', 99000, 0, 1),
(54, 'PR003', 110000, 0, 1),
(54, 'PR014', 125000, 0, 1),
(54, 'PR019', 65000, 0, 1),
(54, 'PR022', 93000, 0, 1),
(54, 'PR024', 93000, 0, 1),
(55, 'PR002', 99000, 0, 3),
(55, 'PR003', 110000, 0, 1),
(55, 'PR010', 80000, 0, 5),
(55, 'PR013', 105000, 0, 2),
(55, 'PR019', 65000, 0, 1),
(55, 'PR023', 65000, 0, 3),
(55, 'PR034', 303000, 0, 1),
(55, 'PR035', 70000, 0, 2),
(55, 'PR036', 75000, 0, 1),
(55, 'PR064', 35000, 0, 2),
(56, 'PR001', 99000, 0, 1),
(56, 'PR003', 110000, 0, 1),
(56, 'PR008', 55000, 0, 1),
(56, 'PR010', 80000, 0, 2),
(56, 'PR013', 105000, 0, 3),
(56, 'PR014', 125000, 0, 1),
(56, 'PR019', 65000, 0, 2),
(56, 'PR021', 93000, 0, 2),
(56, 'PR022', 93000, 0, 1),
(56, 'PR024', 93000, 0, 2),
(56, 'PR027', 65000, 0, 1),
(56, 'PR034', 303000, 0, 1),
(57, 'PR001', 99000, 0, 1),
(57, 'PR002', 99000, 0, 1),
(57, 'PR003', 110000, 0, 1),
(57, 'PR010', 80000, 0, 2),
(57, 'PR011', 80000, 0, 1),
(57, 'PR013', 105000, 0, 2),
(57, 'PR014', 125000, 0, 2),
(57, 'PR019', 65000, 0, 2),
(57, 'PR021', 93000, 0, 1),
(57, 'PR029', 171000, 0, 1),
(57, 'PR038', 99000, 0, 1),
(57, 'PR049', 22000, 0, 10),
(58, 'PR001', 99000, 0, 2),
(58, 'PR002', 99000, 0, 1),
(58, 'PR003', 110000, 0, 2),
(58, 'PR008', 55000, 0, 1),
(58, 'PR010', 80000, 0, 5),
(58, 'PR011', 80000, 0, 2),
(58, 'PR013', 105000, 0, 2),
(58, 'PR014', 125000, 0, 1),
(58, 'PR019', 65000, 0, 4),
(58, 'PR021', 93000, 0, 2),
(58, 'PR022', 93000, 0, 1),
(58, 'PR024', 93000, 0, 1),
(58, 'PR025', 93000, 0, 1),
(58, 'PR034', 303000, 0, 1),
(58, 'PR036', 75000, 0, 1),
(58, 'PR038', 99000, 0, 1),
(58, 'PR042', 73000, 0, 1),
(59, 'PR002', 99000, 0, 1),
(59, 'PR003', 110000, 0, 1),
(59, 'PR010', 80000, 0, 3),
(59, 'PR013', 105000, 0, 2),
(59, 'PR014', 125000, 0, 3),
(59, 'PR019', 65000, 0, 3),
(59, 'PR022', 93000, 0, 2),
(59, 'PR035', 70000, 0, 1),
(60, 'PR068', 10000, 0, 19),
(61, 'PR001', 99000, 0, 1),
(61, 'PR002', 99000, 0, 1),
(61, 'PR003', 110000, 0, 3),
(61, 'PR006', 65000, 0, 1),
(61, 'PR010', 80000, 0, 5),
(61, 'PR013', 105000, 0, 1),
(61, 'PR019', 65000, 0, 4),
(61, 'PR021', 93000, 0, 1),
(61, 'PR024', 93000, 0, 2),
(61, 'PR027', 65000, 0, 1),
(61, 'PR035', 70000, 0, 1),
(61, 'PR036', 75000, 0, 2),
(61, 'PR038', 99000, 0, 2),
(61, 'PR052', 15000, 0, 1),
(62, 'PR003', 110000, 0, 1),
(62, 'PR013', 105000, 0, 3),
(62, 'PR019', 65000, 0, 1),
(63, 'PR002', 99000, 0, 3),
(63, 'PR003', 110000, 0, 2),
(63, 'PR005', 200000, 0, 1),
(63, 'PR010', 80000, 0, 4),
(63, 'PR011', 80000, 0, 1),
(63, 'PR013', 105000, 0, 1),
(63, 'PR014', 125000, 0, 2),
(63, 'PR019', 65000, 0, 3),
(63, 'PR023', 65000, 0, 6),
(63, 'PR024', 93000, 0, 1),
(63, 'PR026', 65000, 0, 1),
(63, 'PR035', 70000, 0, 4),
(63, 'PR036', 75000, 0, 2),
(63, 'PR037', 73000, 0, 1),
(63, 'PR048', 13000, 0, 17),
(63, 'PR061', 85000, 0, 1),
(63, 'PR066', 220000, 0, 1),
(64, 'PR001', 99000, 0, 1),
(64, 'PR002', 99000, 0, 1),
(64, 'PR003', 110000, 0, 1),
(64, 'PR005', 200000, 0, 1),
(64, 'PR010', 80000, 0, 8),
(64, 'PR014', 125000, 0, 1),
(64, 'PR019', 65000, 0, 1),
(64, 'PR021', 93000, 0, 1),
(64, 'PR022', 93000, 0, 1),
(64, 'PR023', 65000, 0, 4),
(64, 'PR024', 93000, 0, 2),
(64, 'PR027', 65000, 0, 1),
(64, 'PR029', 171000, 0, 1),
(64, 'PR034', 303000, 0, 3),
(64, 'PR035', 70000, 0, 3),
(64, 'PR038', 125000, 0, 2),
(65, 'PR002', 99000, 0, 1),
(65, 'PR003', 110000, 0, 2),
(65, 'PR004', 125000, 0, 1),
(65, 'PR010', 80000, 0, 2),
(65, 'PR014', 125000, 0, 1),
(65, 'PR021', 93000, 0, 1),
(65, 'PR022', 93000, 0, 1),
(65, 'PR023', 65000, 0, 2),
(65, 'PR036', 75000, 0, 2),
(65, 'PR038', 125000, 0, 2),
(66, 'PR001', 99000, 0, 2),
(66, 'PR002', 99000, 0, 1),
(66, 'PR003', 110000, 0, 3),
(66, 'PR010', 80000, 0, 6),
(66, 'PR014', 125000, 0, 1),
(66, 'PR019', 65000, 0, 5),
(66, 'PR023', 65000, 0, 4),
(66, 'PR034', 303000, 0, 1),
(66, 'PR035', 70000, 0, 2),
(66, 'PR051', 35000, 0, 1),
(67, 'PR001', 99000, 0, 2),
(67, 'PR002', 99000, 0, 2),
(67, 'PR003', 110000, 0, 1),
(67, 'PR008', 55000, 0, 1),
(67, 'PR010', 80000, 0, 3),
(67, 'PR011', 80000, 0, 1),
(67, 'PR019', 65000, 0, 1),
(67, 'PR021', 93000, 0, 2),
(67, 'PR022', 93000, 0, 1),
(67, 'PR023', 65000, 0, 1),
(67, 'PR024', 93000, 0, 1),
(67, 'PR029', 171000, 0, 1),
(67, 'PR035', 70000, 0, 2),
(67, 'PR036', 75000, 0, 1),
(67, 'PR037', 73000, 0, 1),
(67, 'PR038', 125000, 0, 2),
(67, 'PR051', 35000, 0, 3),
(68, 'PR002', 99000, 0, 2),
(68, 'PR003', 110000, 0, 2),
(68, 'PR004', 125000, 0, 1),
(68, 'PR005', 200000, 0, 2),
(68, 'PR010', 80000, 0, 4),
(68, 'PR013', 105000, 0, 5),
(68, 'PR014', 125000, 0, 1),
(68, 'PR019', 65000, 0, 1),
(68, 'PR021', 93000, 0, 1),
(68, 'PR022', 93000, 0, 1),
(68, 'PR023', 65000, 0, 5),
(68, 'PR024', 93000, 0, 1),
(68, 'PR027', 65000, 0, 1),
(68, 'PR034', 303000, 0, 1),
(68, 'PR035', 70000, 0, 1),
(68, 'PR036', 75000, 0, 2),
(68, 'PR038', 125000, 0, 1),
(68, 'PR039', 97000, 0, 1),
(68, 'PR055', 100000, 0, 2),
(68, 'PR064', 35000, 0, 2),
(69, 'PR001', 99000, 0, 2),
(69, 'PR002', 99000, 0, 3),
(69, 'PR004', 125000, 0, 2),
(69, 'PR010', 80000, 0, 6),
(69, 'PR013', 105000, 0, 7),
(69, 'PR014', 125000, 0, 5),
(69, 'PR019', 65000, 0, 1),
(69, 'PR021', 93000, 0, 3),
(69, 'PR022', 93000, 0, 3),
(69, 'PR023', 65000, 0, 5),
(69, 'PR034', 303000, 0, 1),
(69, 'PR035', 70000, 0, 3),
(69, 'PR049', 22000, 0, 1),
(70, 'PR001', 99000, 0, 2),
(70, 'PR002', 99000, 0, 2),
(70, 'PR003', 110000, 0, 2),
(70, 'PR010', 80000, 0, 8),
(70, 'PR013', 105000, 0, 1),
(70, 'PR019', 65000, 0, 7),
(70, 'PR021', 93000, 0, 1),
(70, 'PR022', 93000, 0, 1),
(70, 'PR023', 65000, 0, 1),
(70, 'PR024', 93000, 0, 1),
(70, 'PR026', 65000, 0, 1),
(70, 'PR027', 65000, 0, 1),
(70, 'PR029', 171000, 0, 3),
(70, 'PR035', 70000, 0, 1),
(70, 'PR038', 125000, 0, 2),
(70, 'PR046', 65000, 0, 3),
(70, 'PR049', 22000, 0, 10),
(71, 'PR049', 22000, 0, 9),
(72, 'PR001', 99000, 0, 6),
(72, 'PR002', 99000, 0, 1),
(72, 'PR003', 110000, 0, 2),
(72, 'PR005', 200000, 0, 2),
(72, 'PR010', 80000, 0, 7),
(72, 'PR013', 105000, 0, 5),
(72, 'PR014', 125000, 0, 5),
(72, 'PR019', 65000, 0, 5),
(72, 'PR021', 93000, 0, 1),
(72, 'PR022', 93000, 0, 4),
(72, 'PR023', 65000, 0, 4),
(72, 'PR024', 93000, 0, 3),
(72, 'PR035', 70000, 0, 1),
(72, 'PR036', 75000, 0, 1),
(72, 'PR048', 13000, 0, 6),
(73, 'PR001', 99000, 0, 2),
(73, 'PR004', 125000, 0, 1),
(73, 'PR005', 200000, 0, 1),
(73, 'PR008', 55000, 0, 1),
(73, 'PR010', 80000, 0, 9),
(73, 'PR011', 80000, 0, 2),
(73, 'PR013', 105000, 0, 2),
(73, 'PR014', 125000, 0, 1),
(73, 'PR019', 65000, 0, 3),
(73, 'PR021', 93000, 0, 2),
(73, 'PR023', 65000, 0, 2),
(73, 'PR024', 93000, 0, 1),
(73, 'PR026', 65000, 0, 1),
(73, 'PR029', 171000, 0, 3),
(73, 'PR034', 303000, 0, 3),
(73, 'PR035', 70000, 0, 2),
(73, 'PR046', 65000, 0, 1),
(73, 'PR055', 100000, 0, 2),
(73, 'PR065', 106000, 0, 1),
(74, 'PR001', 99000, 0, 2),
(74, 'PR002', 99000, 0, 1),
(74, 'PR003', 110000, 0, 2),
(74, 'PR006', 65000, 0, 1),
(74, 'PR008', 55000, 0, 1),
(74, 'PR009', 55000, 0, 1),
(74, 'PR010', 80000, 0, 4),
(74, 'PR013', 105000, 0, 2),
(74, 'PR014', 125000, 0, 5),
(74, 'PR021', 93000, 0, 4),
(74, 'PR023', 65000, 0, 3),
(74, 'PR024', 93000, 0, 1),
(74, 'PR034', 303000, 0, 1),
(74, 'PR035', 70000, 0, 1),
(74, 'PR036', 75000, 0, 2),
(74, 'PR038', 125000, 0, 1),
(75, 'PR005', 200000, 0, 1),
(75, 'PR049', 22000, 0, 10),
(75, 'PR059', 85000, 0, 1),
(75, 'PRO67', 225000, 0, 1),
(76, 'PR001', 99000, 0, 3),
(76, 'PR002', 99000, 0, 3),
(76, 'PR003', 110000, 0, 2),
(76, 'PR006', 65000, 0, 1),
(76, 'PR010', 80000, 0, 3),
(76, 'PR013', 105000, 0, 4),
(76, 'PR014', 125000, 0, 2),
(76, 'PR019', 65000, 0, 3),
(76, 'PR021', 93000, 0, 1),
(76, 'PR023', 65000, 0, 2),
(76, 'PR024', 93000, 0, 3),
(76, 'PR025', 93000, 0, 1),
(76, 'PR036', 75000, 0, 1),
(76, 'PR038', 125000, 0, 2),
(76, 'PR055', 100000, 0, 2),
(78, 'PR001', 99000, 0, 4),
(78, 'PR002', 99000, 0, 1),
(78, 'PR004', 125000, 0, 1),
(78, 'PR005', 200000, 0, 1),
(78, 'PR010', 80000, 0, 2),
(78, 'PR011', 80000, 0, 1),
(78, 'PR014', 125000, 0, 5),
(78, 'PR019', 65000, 0, 1),
(78, 'PR021', 93000, 0, 2),
(78, 'PR023', 65000, 0, 4),
(78, 'PR025', 93000, 0, 2),
(78, 'PR027', 65000, 0, 1),
(78, 'PR029', 171000, 0, 2),
(78, 'PR034', 303000, 0, 2),
(78, 'PR046', 65000, 0, 1),
(78, 'PR049', 22000, 0, 60),
(79, 'PR009', 55000, 0, 1),
(79, 'PR010', 80000, 0, 2),
(79, 'PR013', 105000, 0, 4),
(79, 'PR014', 125000, 0, 3),
(79, 'PR019', 65000, 0, 3),
(79, 'PR021', 93000, 0, 2),
(79, 'PR022', 93000, 0, 3),
(79, 'PR024', 93000, 0, 1),
(79, 'PR027', 65000, 0, 1),
(79, 'PR029', 171000, 0, 1),
(79, 'PR038', 125000, 0, 1),
(79, 'PR042', 73000, 0, 1),
(80, 'PR001', 99000, 0, 1),
(80, 'PR002', 99000, 0, 2),
(80, 'PR005', 200000, 0, 1),
(80, 'PR010', 80000, 0, 4),
(80, 'PR011', 80000, 0, 1),
(80, 'PR013', 105000, 0, 1),
(80, 'PR019', 65000, 0, 2),
(80, 'PR022', 93000, 0, 1),
(80, 'PR023', 65000, 0, 2),
(80, 'PR024', 93000, 0, 3),
(80, 'PR034', 303000, 0, 1),
(80, 'PR035', 70000, 0, 1),
(80, 'PR036', 75000, 0, 1),
(80, 'PR059', 85000, 0, 1),
(80, 'PRO67', 225000, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `_detail_penjualan_treatment`
--

CREATE TABLE `_detail_penjualan_treatment` (
  `id_transaksi` int(10) NOT NULL,
  `id_treatment` varchar(10) NOT NULL,
  `harga_aktual` double NOT NULL,
  `diskon_aktual` double NOT NULL,
  `penyusutan` double NOT NULL,
  `qty` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_detail_penjualan_treatment`
--

INSERT INTO `_detail_penjualan_treatment` (`id_transaksi`, `id_treatment`, `harga_aktual`, `diskon_aktual`, `penyusutan`, `qty`) VALUES
(1, 'TR026', 65000, 0, 0, 1),
(2, 'TR004', 99000, 0, 0, 1),
(3, 'TR079', 75000, 0, 0, 1),
(4, 'TR004', 99000, 0, 0, 1),
(5, 'TR004', 99000, 0, 0, 1),
(5, 'TR026', 65000, 0, 0, 1),
(5, 'TR079', 75000, 0, 0, 1),
(6, 'TR004', 99000, 0, 0, 1),
(7, 'TR001', 50000, 0, 0, 2),
(7, 'TR003', 110000, 0, 10000, 2),
(7, 'TR024', 250000, 0, 15000, 1),
(7, 'TR062', 50000, 0, 15000, 1),
(8, 'TR003', 110000, 0, 10000, 2),
(8, 'TR014', 275000, 0, 15000, 1),
(8, 'TR019', 425000, 0, 50000, 1),
(8, 'TR030', 130000, 0, 15000, 1),
(8, 'TR043', 50000, 0, 0, 1),
(8, 'TR062', 50000, 0, 15000, 1),
(9, 'TR001', 50000, 0, 0, 1),
(9, 'TR003', 110000, 0, 10000, 1),
(9, 'TR017', 375000, 0, 15000, 1),
(9, 'TR019', 425000, 0, 50000, 1),
(9, 'TR063', 150000, 0, 25000, 1),
(10, 'TR010', 250000, 0, 10000, 1),
(10, 'TR043', 50000, 0, 0, 1),
(11, 'TR003', 110000, 0, 10000, 1),
(11, 'TR043', 50000, 0, 0, 1),
(11, 'TR083', 90000, 0, 0, 1),
(12, 'TR019', 425000, 0, 50000, 1),
(12, 'TR062', 50000, 0, 15000, 1),
(13, 'TR001', 50000, 0, 0, 1),
(13, 'TR003', 110000, 0, 10000, 1),
(13, 'TR010', 250000, 0, 10000, 1),
(13, 'TR022', 30000, 0, 5000, 3),
(13, 'TR040', 550000, 0, 50000, 1),
(13, 'TR053', 625000, 0, 0, 1),
(13, 'TR064', 125000, 0, 15000, 3),
(14, 'TR085', 50000, 0, 0, 2),
(15, 'TR014', 275000, 0, 15000, 3),
(15, 'TR018', 525000, 0, 50000, 1),
(15, 'TR020', 425000, 0, 50000, 2),
(15, 'TR062', 50000, 0, 15000, 1),
(15, 'TR063', 150000, 0, 25000, 1),
(16, 'TR003', 110000, 0, 10000, 1),
(16, 'TR005', 300000, 0, 15000, 1),
(16, 'TR062', 50000, 0, 15000, 1),
(17, 'TR003', 110000, 0, 10000, 1),
(17, 'TR011', 575000, 0, 50000, 1),
(17, 'TR018', 525000, 0, 50000, 1),
(17, 'TR063', 150000, 0, 25000, 1),
(18, 'TR011', 575000, 0, 50000, 2),
(18, 'TR014', 275000, 0, 15000, 1),
(18, 'TR022', 30000, 0, 5000, 5),
(18, 'TR062', 50000, 0, 15000, 2),
(19, 'TR010', 250000, 0, 10000, 1),
(19, 'TR011', 575000, 0, 50000, 1),
(19, 'TR018', 525000, 0, 50000, 1),
(19, 'TR027', 250000, 0, 30000, 1),
(19, 'TR031', 350000, 0, 0, 1),
(19, 'TR043', 50000, 0, 0, 1),
(19, 'TR063', 150000, 0, 25000, 2),
(19, 'TR064', 125000, 0, 15000, 1),
(19, 'TR065', 220000, 0, 10000, 1),
(19, 'TR085', 50000, 0, 0, 1),
(20, 'TR001', 50000, 0, 0, 2),
(20, 'TR003', 110000, 0, 10000, 2),
(20, 'TR018', 525000, 0, 50000, 1),
(20, 'TR020', 425000, 0, 50000, 2),
(20, 'TR022', 30000, 0, 5000, 8),
(20, 'TR062', 50000, 0, 15000, 3),
(20, 'TR063', 150000, 0, 25000, 3),
(21, 'TR085', 50000, 0, 0, 1),
(22, 'TR003', 110000, 0, 10000, 4),
(22, 'TR014', 275000, 0, 15000, 2),
(22, 'TR020', 425000, 0, 50000, 1),
(22, 'TR062', 50000, 0, 15000, 3),
(22, 'TR063', 150000, 0, 25000, 1),
(22, 'TR064', 125000, 0, 15000, 1),
(23, 'TR001', 50000, 0, 0, 1),
(23, 'TR003', 110000, 0, 10000, 3),
(23, 'TR010', 250000, 0, 10000, 1),
(23, 'TR011', 575000, 0, 50000, 1),
(23, 'TR027', 250000, 0, 30000, 1),
(23, 'TR043', 50000, 0, 0, 1),
(23, 'TR062', 50000, 0, 15000, 3),
(23, 'TR065', 220000, 0, 10000, 1),
(24, 'TR001', 50000, 0, 0, 1),
(24, 'TR003', 110000, 0, 10000, 11),
(24, 'TR012', 175000, 0, 15000, 1),
(24, 'TR022', 30000, 0, 5000, 11),
(24, 'TR037', 375000, 0, 50000, 1),
(24, 'TR062', 50000, 0, 15000, 3),
(24, 'TR063', 150000, 0, 25000, 3),
(24, 'TR064', 125000, 0, 15000, 2),
(25, 'TR027', 250000, 0, 30000, 1),
(26, 'TR014', 275000, 0, 15000, 1),
(26, 'TR019', 425000, 0, 50000, 1),
(26, 'TR037', 375000, 0, 50000, 1),
(26, 'TR038', 1200000, 0, 50000, 1),
(26, 'TR062', 50000, 0, 15000, 2),
(26, 'TR082', 50000, 0, 0, 1),
(27, 'TR020', 425000, 0, 50000, 1),
(27, 'TR027', 250000, 0, 30000, 1),
(28, 'TR004', 99000, 0, 0, 1),
(29, 'TR003', 110000, 0, 10000, 1),
(29, 'TR013', 220000, 0, 15000, 5),
(29, 'TR022', 30000, 0, 5000, 9),
(29, 'TR024', 250000, 0, 15000, 1),
(29, 'TR030', 130000, 0, 15000, 1),
(29, 'TR054', 825000, 0, 0, 1),
(29, 'TR062', 50000, 0, 15000, 2),
(29, 'TR064', 125000, 0, 15000, 1),
(30, 'TR026', 65000, 0, 0, 1),
(30, 'TR079', 75000, 0, 0, 1),
(31, 'TR038', 1200000, 0, 50000, 1),
(31, 'TR087', 1750000, 0, 0, 1),
(32, 'TR003', 110000, 0, 10000, 3),
(32, 'TR004', 99000, 0, 0, 2),
(32, 'TR005', 300000, 0, 15000, 1),
(32, 'TR027', 250000, 0, 30000, 1),
(32, 'TR062', 50000, 0, 15000, 2),
(32, 'TR063', 150000, 0, 25000, 1),
(32, 'TR064', 125000, 0, 15000, 1),
(33, 'TR001', 50000, 0, 0, 1),
(33, 'TR003', 110000, 0, 10000, 1),
(33, 'TR040', 550000, 0, 50000, 1),
(33, 'TR043', 50000, 0, 0, 1),
(33, 'TR062', 50000, 0, 15000, 1),
(34, 'TR001', 50000, 0, 0, 1),
(34, 'TR003', 110000, 0, 10000, 2),
(34, 'TR004', 99000, 0, 0, 1),
(34, 'TR043', 50000, 0, 0, 1),
(34, 'TR062', 50000, 0, 15000, 1),
(35, 'TR013', 220000, 0, 15000, 1),
(35, 'TR062', 50000, 0, 15000, 1),
(36, 'TR004', 99000, 0, 0, 2),
(36, 'TR007', 75000, 0, 0, 1),
(36, 'TR014', 275000, 0, 15000, 1),
(36, 'TR019', 425000, 0, 50000, 2),
(36, 'TR026', 65000, 0, 0, 1),
(36, 'TR043', 50000, 0, 0, 1),
(36, 'TR062', 50000, 0, 15000, 1),
(36, 'TR064', 125000, 0, 15000, 1),
(36, 'TR066', 375000, 0, 15000, 4),
(37, 'TR003', 110000, 0, 10000, 1),
(37, 'TR004', 99000, 0, 0, 1),
(37, 'TR007', 75000, 0, 0, 2),
(37, 'TR010', 250000, 0, 10000, 2),
(37, 'TR020', 425000, 0, 50000, 1),
(37, 'TR062', 50000, 0, 15000, 4),
(37, 'TR063', 150000, 0, 25000, 1),
(37, 'TR065', 220000, 0, 10000, 1),
(37, 'TR066', 375000, 0, 15000, 3),
(38, 'TR003', 110000, 0, 10000, 1),
(38, 'TR019', 425000, 0, 50000, 1),
(38, 'TR022', 30000, 0, 5000, 3),
(38, 'TR062', 50000, 0, 15000, 1),
(38, 'TR065', 220000, 0, 10000, 1),
(39, 'TR003', 110000, 0, 10000, 2),
(39, 'TR004', 99000, 0, 0, 2),
(39, 'TR030', 130000, 0, 15000, 1),
(39, 'TR032', 350000, 0, 50000, 1),
(39, 'TR062', 50000, 0, 15000, 2),
(39, 'TR065', 220000, 0, 10000, 2),
(39, 'TR081', 75000, 0, 0, 1),
(40, 'TR003', 110000, 0, 10000, 2),
(40, 'TR062', 50000, 0, 15000, 1),
(41, 'TR003', 110000, 0, 10000, 1),
(41, 'TR004', 99000, 0, 0, 1),
(41, 'TR011', 575000, 0, 50000, 1),
(41, 'TR014', 275000, 0, 15000, 1),
(41, 'TR018', 525000, 0, 50000, 1),
(41, 'TR025', 65000, 0, 0, 1),
(41, 'TR027', 250000, 0, 30000, 1),
(41, 'TR062', 50000, 0, 15000, 1),
(42, 'TR003', 110000, 0, 10000, 1),
(42, 'TR004', 99000, 0, 0, 2),
(42, 'TR019', 425000, 0, 50000, 1),
(42, 'TR022', 30000, 0, 5000, 5),
(42, 'TR027', 250000, 0, 30000, 1),
(42, 'TR043', 50000, 0, 0, 1),
(42, 'TR062', 50000, 0, 15000, 1),
(42, 'TR065', 220000, 0, 10000, 2),
(42, 'TR066', 375000, 0, 15000, 1),
(43, 'TR003', 110000, 0, 10000, 2),
(43, 'TR004', 99000, 0, 0, 1),
(43, 'TR014', 275000, 0, 15000, 1),
(43, 'TR046', 200000, 0, 0, 1),
(43, 'TR062', 50000, 0, 15000, 1),
(44, 'TR003', 110000, 0, 10000, 1),
(44, 'TR013', 220000, 0, 15000, 1),
(44, 'TR019', 425000, 0, 50000, 1),
(44, 'TR020', 425000, 0, 50000, 3),
(44, 'TR053', 625000, 0, 0, 1),
(44, 'TR062', 50000, 0, 15000, 4),
(44, 'TR065', 220000, 0, 10000, 1),
(45, 'TR003', 110000, 0, 10000, 1),
(45, 'TR005', 300000, 0, 15000, 1),
(45, 'TR031', 350000, 0, 0, 1),
(45, 'TR062', 50000, 0, 15000, 1),
(46, 'TR019', 425000, 0, 50000, 2),
(46, 'TR020', 425000, 0, 50000, 1),
(46, 'TR062', 50000, 0, 15000, 1),
(46, 'TR065', 220000, 0, 10000, 1),
(47, 'TR003', 110000, 0, 10000, 1),
(47, 'TR004', 99000, 0, 0, 1),
(47, 'TR020', 425000, 0, 50000, 1),
(47, 'TR065', 220000, 0, 10000, 1),
(48, 'TR003', 110000, 0, 10000, 1),
(48, 'TR007', 75000, 0, 0, 1),
(48, 'TR026', 65000, 0, 0, 1),
(49, 'TR004', 99000, 0, 0, 2),
(49, 'TR007', 75000, 0, 0, 1),
(49, 'TR053', 625000, 0, 0, 1),
(49, 'TR079', 75000, 0, 50000, 1),
(50, 'TR013', 220000, 0, 15000, 1),
(50, 'TR020', 425000, 0, 50000, 1),
(50, 'TR043', 50000, 0, 0, 1),
(50, 'TR062', 50000, 0, 15000, 1),
(51, 'TR003', 110000, 0, 10000, 3),
(51, 'TR027', 250000, 0, 30000, 1),
(51, 'TR043', 50000, 0, 0, 1),
(51, 'TR068', 975000, 0, 50000, 1),
(52, 'TR001', 50000, 0, 0, 1),
(52, 'TR003', 110000, 0, 10000, 2),
(52, 'TR004', 99000, 0, 0, 1),
(52, 'TR040', 550000, 0, 50000, 1),
(52, 'TR062', 50000, 0, 15000, 1),
(52, 'TR065', 220000, 0, 10000, 1),
(53, 'TR001', 50000, 0, 0, 1),
(53, 'TR003', 110000, 0, 10000, 2),
(53, 'TR004', 99000, 0, 0, 2),
(53, 'TR005', 300000, 0, 15000, 1),
(53, 'TR014', 275000, 0, 15000, 1),
(54, 'TR004', 99000, 0, 0, 2),
(54, 'TR032', 350000, 0, 50000, 1),
(54, 'TR062', 50000, 0, 15000, 1),
(54, 'TR066', 375000, 0, 15000, 2),
(55, 'TR001', 50000, 0, 0, 1),
(55, 'TR003', 110000, 0, 10000, 3),
(55, 'TR006', 185000, 0, 150000, 1),
(55, 'TR011', 575000, 0, 50000, 1),
(55, 'TR018', 525000, 0, 50000, 1),
(55, 'TR043', 50000, 0, 0, 1),
(55, 'TR062', 50000, 0, 15000, 2),
(56, 'TR043', 50000, 0, 0, 1),
(57, 'TR014', 275000, 0, 15000, 1),
(57, 'TR020', 425000, 0, 50000, 1),
(57, 'TR053', 625000, 0, 0, 1),
(57, 'TR066', 375000, 0, 15000, 1),
(58, 'TR004', 99000, 0, 0, 1),
(58, 'TR013', 220000, 0, 15000, 1),
(58, 'TR027', 250000, 0, 30000, 1),
(58, 'TR062', 50000, 0, 15000, 1),
(58, 'TR063', 150000, 0, 25000, 1),
(58, 'TR065', 220000, 0, 10000, 1),
(58, 'TR066', 375000, 0, 15000, 1),
(59, 'TR003', 110000, 0, 10000, 1),
(59, 'TR014', 275000, 0, 15000, 1),
(59, 'TR040', 550000, 0, 50000, 1),
(59, 'TR062', 50000, 0, 15000, 1),
(60, 'TR004', 99000, 0, 0, 1),
(61, 'TR003', 110000, 0, 10000, 1),
(61, 'TR007', 75000, 0, 0, 2),
(61, 'TR011', 575000, 0, 50000, 1),
(61, 'TR014', 275000, 0, 15000, 2),
(61, 'TR019', 425000, 0, 50000, 3),
(61, 'TR020', 425000, 0, 50000, 2),
(61, 'TR027', 250000, 0, 30000, 1),
(61, 'TR062', 50000, 0, 15000, 2),
(61, 'TR064', 125000, 0, 15000, 1),
(62, 'TR005', 300000, 0, 15000, 1),
(63, 'TR019', 425000, 0, 50000, 1),
(63, 'TR037', 375000, 0, 50000, 2),
(63, 'TR043', 50000, 0, 0, 1),
(63, 'TR063', 150000, 0, 25000, 2),
(64, 'TR003', 110000, 0, 10000, 1),
(64, 'TR005', 300000, 0, 15000, 1),
(64, 'TR011', 575000, 0, 50000, 1),
(64, 'TR019', 425000, 0, 50000, 1),
(64, 'TR062', 50000, 0, 15000, 2),
(64, 'TR067', 400000, 0, 50000, 1),
(65, 'TR003', 110000, 0, 10000, 1),
(65, 'TR019', 425000, 0, 50000, 1),
(66, 'TR004', 99000, 0, 0, 1),
(66, 'TR007', 75000, 0, 0, 1),
(67, 'TR001', 50000, 0, 0, 1),
(67, 'TR003', 110000, 0, 10000, 2),
(67, 'TR005', 300000, 0, 15000, 1),
(67, 'TR062', 50000, 0, 15000, 1),
(67, 'TR063', 150000, 0, 25000, 1),
(67, 'TR067', 400000, 0, 50000, 1),
(67, 'TR068', 975000, 0, 50000, 1),
(68, 'TR001', 50000, 0, 0, 1),
(68, 'TR018', 525000, 0, 50000, 1),
(68, 'TR019', 425000, 0, 50000, 1),
(68, 'TR037', 375000, 0, 50000, 1),
(68, 'TR060', 25000, 0, 5000, 3),
(68, 'TR063', 150000, 0, 25000, 1),
(69, 'TR003', 110000, 0, 10000, 2),
(69, 'TR004', 99000, 0, 0, 1),
(69, 'TR014', 275000, 0, 15000, 1),
(69, 'TR018', 525000, 0, 50000, 1),
(69, 'TR043', 50000, 0, 0, 1),
(69, 'TR062', 50000, 0, 15000, 1),
(70, 'TR003', 110000, 0, 10000, 2),
(70, 'TR004', 99000, 0, 0, 1),
(70, 'TR007', 75000, 0, 0, 1),
(70, 'TR020', 425000, 0, 50000, 1),
(70, 'TR060', 25000, 0, 5000, 5),
(70, 'TR062', 50000, 0, 15000, 2),
(70, 'TR066', 375000, 0, 15000, 1),
(71, 'TR003', 110000, 0, 10000, 1),
(71, 'TR018', 525000, 0, 50000, 1),
(71, 'TR043', 50000, 0, 0, 1),
(71, 'TR060', 25000, 0, 5000, 2),
(71, 'TR062', 50000, 0, 15000, 1),
(71, 'TR063', 150000, 0, 25000, 1),
(79, 'TR007', 75000, 0, 0, 1),
(79, 'TR019', 425000, 0, 50000, 1),
(79, 'TR060', 25000, 0, 5000, 4),
(80, 'TR003', 110000, 0, 10000, 1),
(80, 'TR019', 425000, 0, 50000, 1),
(80, 'TR060', 25000, 0, 5000, 3),
(80, 'TR062', 50000, 0, 15000, 1),
(81, 'TR018', 525000, 0, 50000, 1),
(81, 'TR019', 425000, 0, 50000, 1),
(81, 'TR060', 25000, 0, 5000, 2),
(81, 'TR062', 50000, 0, 15000, 1),
(82, 'TR018', 525000, 0, 50000, 1),
(82, 'TR022', 30000, 0, 5000, 6),
(82, 'TR060', 25000, 0, 5000, 4),
(83, 'TR010', 250000, 0, 10000, 1),
(83, 'TR012', 175000, 0, 15000, 1),
(83, 'TR014', 275000, 0, 15000, 1),
(83, 'TR018', 525000, 0, 50000, 2),
(83, 'TR019', 425000, 0, 50000, 1),
(83, 'TR043', 50000, 0, 0, 1),
(83, 'TR053', 625000, 0, 50000, 1),
(83, 'TR060', 25000, 0, 5000, 7),
(83, 'TR063', 150000, 0, 25000, 1),
(83, 'TR064', 125000, 0, 15000, 1),
(83, 'TR065', 220000, 0, 10000, 1),
(84, 'TR003', 110000, 0, 10000, 1),
(84, 'TR010', 250000, 0, 10000, 1),
(84, 'TR053', 625000, 0, 50000, 1),
(84, 'TR060', 25000, 0, 5000, 4),
(84, 'TR062', 50000, 0, 15000, 1),
(84, 'TR066', 375000, 0, 15000, 1),
(85, 'TR004', 99000, 0, 0, 1),
(85, 'TR007', 75000, 0, 0, 2),
(85, 'TR014', 275000, 0, 15000, 1),
(85, 'TR020', 425000, 0, 50000, 1),
(85, 'TR026', 65000, 0, 0, 1),
(85, 'TR060', 25000, 0, 5000, 1),
(85, 'TR062', 50000, 0, 15000, 1),
(86, 'TR003', 110000, 0, 10000, 1),
(86, 'TR007', 75000, 0, 0, 1),
(86, 'TR013', 220000, 0, 15000, 1),
(86, 'TR018', 525000, 0, 50000, 2),
(86, 'TR019', 425000, 0, 50000, 1),
(86, 'TR031', 350000, 0, 0, 1),
(86, 'TR060', 25000, 0, 5000, 6),
(86, 'TR062', 50000, 0, 15000, 1),
(86, 'TR064', 125000, 0, 15000, 1),
(87, 'TR003', 110000, 0, 10000, 2),
(87, 'TR004', 99000, 0, 0, 1),
(87, 'TR007', 75000, 0, 0, 1),
(87, 'TR027', 250000, 0, 30000, 1),
(87, 'TR060', 25000, 0, 5000, 5),
(87, 'TR062', 50000, 0, 15000, 1),
(87, 'TR063', 150000, 0, 25000, 1),
(87, 'TR065', 220000, 0, 10000, 1),
(88, 'TR001', 50000, 0, 0, 1),
(88, 'TR003', 110000, 0, 10000, 4),
(88, 'TR004', 99000, 0, 0, 3),
(88, 'TR005', 300000, 0, 15000, 1),
(88, 'TR011', 575000, 0, 50000, 3),
(88, 'TR019', 425000, 0, 50000, 3),
(88, 'TR020', 425000, 0, 50000, 1),
(88, 'TR022', 30000, 0, 5000, 3),
(88, 'TR027', 250000, 0, 30000, 1),
(88, 'TR040', 550000, 0, 50000, 2),
(88, 'TR053', 625000, 0, 50000, 2),
(88, 'TR060', 25000, 0, 5000, 15),
(88, 'TR062', 50000, 0, 15000, 3),
(88, 'TR063', 150000, 0, 25000, 1),
(88, 'TR064', 125000, 0, 15000, 1),
(88, 'TR065', 220000, 0, 10000, 2),
(88, 'TR079', 75000, 0, 50000, 1),
(89, 'TR013', 220000, 0, 15000, 2),
(89, 'TR018', 525000, 0, 50000, 2),
(89, 'TR019', 425000, 0, 50000, 1),
(89, 'TR020', 425000, 0, 50000, 2),
(89, 'TR027', 250000, 0, 30000, 1),
(89, 'TR037', 375000, 0, 50000, 1),
(89, 'TR060', 25000, 0, 5000, 10),
(89, 'TR062', 50000, 0, 15000, 2),
(89, 'TR063', 150000, 0, 25000, 1),
(89, 'TR066', 375000, 0, 15000, 1),
(90, 'TR066', 375000, 0, 15000, 1),
(91, 'TR001', 50000, 0, 0, 1),
(91, 'TR003', 110000, 0, 10000, 10),
(91, 'TR010', 250000, 0, 10000, 4),
(91, 'TR011', 575000, 0, 50000, 2),
(91, 'TR018', 525000, 0, 50000, 1),
(91, 'TR019', 425000, 0, 50000, 1),
(91, 'TR032', 350000, 0, 50000, 1),
(91, 'TR051', 725000, 0, 50000, 1),
(91, 'TR060', 25000, 0, 5000, 14),
(91, 'TR062', 50000, 0, 15000, 11),
(91, 'TR066', 375000, 0, 15000, 1),
(92, 'TR001', 50000, 0, 0, 3),
(92, 'TR003', 110000, 0, 10000, 7),
(92, 'TR005', 300000, 0, 15000, 1),
(92, 'TR007', 75000, 0, 0, 1),
(92, 'TR010', 250000, 0, 10000, 5),
(92, 'TR011', 575000, 0, 50000, 2),
(92, 'TR018', 525000, 0, 50000, 1),
(92, 'TR019', 425000, 0, 50000, 1),
(92, 'TR030', 130000, 0, 15000, 1),
(92, 'TR031', 350000, 0, 0, 1),
(92, 'TR053', 625000, 0, 50000, 1),
(92, 'TR060', 25000, 0, 5000, 26),
(92, 'TR062', 50000, 0, 15000, 6),
(92, 'TR063', 150000, 0, 25000, 1),
(92, 'TR066', 375000, 0, 15000, 6),
(92, 'TR068', 975000, 0, 50000, 1),
(92, 'TR079', 75000, 0, 50000, 1),
(93, 'TR010', 250000, 0, 10000, 1),
(93, 'TR027', 250000, 0, 30000, 2),
(93, 'TR060', 25000, 0, 5000, 3),
(93, 'TR062', 50000, 0, 15000, 2),
(94, 'TR001', 50000, 0, 0, 1),
(94, 'TR003', 110000, 0, 10000, 1),
(94, 'TR010', 250000, 0, 10000, 1),
(94, 'TR019', 425000, 0, 50000, 1),
(94, 'TR020', 425000, 0, 50000, 1),
(94, 'TR060', 25000, 0, 5000, 3),
(94, 'TR062', 50000, 0, 15000, 1),
(94, 'TR066', 375000, 0, 15000, 1),
(95, 'TR003', 110000, 0, 10000, 1),
(95, 'TR060', 25000, 0, 5000, 5),
(95, 'TR062', 50000, 0, 15000, 1),
(96, 'TR011', 575000, 0, 50000, 1),
(96, 'TR014', 275000, 0, 15000, 2),
(96, 'TR022', 30000, 0, 5000, 16),
(96, 'TR043', 50000, 0, 0, 2),
(96, 'TR065', 220000, 0, 10000, 1),
(97, 'TR003', 110000, 0, 10000, 1),
(97, 'TR007', 75000, 0, 0, 1),
(97, 'TR019', 425000, 0, 50000, 1),
(97, 'TR022', 30000, 0, 5000, 9),
(97, 'TR031', 350000, 0, 0, 1),
(97, 'TR060', 25000, 0, 5000, 3),
(97, 'TR062', 50000, 0, 15000, 2);

-- --------------------------------------------------------

--
-- Table structure for table `_jatah_bon`
--

CREATE TABLE `_jatah_bon` (
  `id` int(10) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_peg` varchar(10) NOT NULL,
  `id_pegawai` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  `total` double NOT NULL DEFAULT '0',
  `jatah` double NOT NULL DEFAULT '0',
  `bon` double NOT NULL DEFAULT '0',
  `batas_bon` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_jatah_bon`
--

INSERT INTO `_jatah_bon` (`id`, `tgl`, `id_peg`, `id_pegawai`, `keterangan`, `total`, `jatah`, `bon`, `batas_bon`) VALUES
(1, '2018-04-18 07:23:14', 'T00001', 'B200788', ' - ', 75000, 75000, 0, '2018-06-18'),
(2, '2018-04-18 07:23:28', 'A031088', 'B200788', ' - ', 425000, 220000, 205000, '2018-06-18'),
(3, '2018-04-18 08:04:55', 'A031088', 'B200788', ' - ', 729000, 0, 729000, '2018-06-18'),
(4, '2018-04-18 08:12:20', 'PEG001', 'B200788', ' - ', 65000, 65000, 0, '2018-06-18'),
(5, '2018-04-18 08:14:03', 'A031088', 'B200788', ' - ', 916000, 0, 916000, '2018-06-18'),
(6, '2018-04-18 08:14:42', 'A031088', 'B200788', ' - ', 660000, 0, 660000, '2018-06-18'),
(7, '2018-04-18 08:25:32', 'A031088', 'B200788', ' - ', 171000, 0, 171000, '2018-06-18'),
(8, '2018-04-18 08:25:54', 'B200788', 'B200788', ' - ', 80000, 80000, 0, '2018-06-18'),
(9, '2018-04-18 08:30:15', 'A031088', 'B200788', ' - ', 244000, 0, 244000, '2018-06-18'),
(10, '2018-04-18 08:39:17', 'A031088', 'B200788', ' - ', 244000, 0, 244000, '2018-06-18'),
(11, '2018-04-18 08:49:03', 'B200788', 'B200788', ' - ', 308000, 140000, 168000, '2018-06-18'),
(12, '2018-04-18 08:49:41', 'T00001', 'B200788', ' - ', 155000, 145000, 10000, '2018-06-18'),
(13, '2018-04-18 08:50:25', 'A031088', 'B200788', ' - ', 130000, 0, 130000, '2018-06-18'),
(14, '2018-04-18 08:59:16', 'A031088', 'B200788', ' - ', 500000, 0, 500000, '2018-06-18'),
(15, '2018-04-18 09:05:39', 'A031088', 'B200788', ' - ', 100000, 0, 100000, '2018-06-18'),
(16, '2018-04-18 09:44:14', 'A031088', 'B200788', ' - ', 105000, 0, 105000, '2018-06-18'),
(17, '2018-04-19 02:11:56', 'A031088', 'B200788', ' - ', 158000, 0, 158000, '2018-06-19'),
(18, '2018-04-19 02:24:38', 'A031088', 'B200788', ' - ', 1065000, 0, 1065000, '2018-06-19'),
(19, '2018-04-19 04:00:06', 'A031088', 'B200788', ' - ', 93000, 0, 93000, '2018-06-19'),
(20, '2018-04-20 09:38:43', 'A031088', 'B200788', ' - ', 263000, 0, 263000, '2018-06-20'),
(21, '2018-04-20 09:39:06', 'T00006', 'B200788', ' - ', 93000, 93000, 0, '2018-06-20'),
(22, '2018-04-20 09:39:33', 'T00003', 'B200788', ' - ', 125000, 125000, 0, '2018-06-20'),
(23, '2018-04-21 08:50:19', 'A031088', 'B200788', ' - ', 35000, 0, 35000, '2018-06-21'),
(24, '2018-04-24 07:05:05', 'A031088', 'A031088', ' - ', 410000, 0, 410000, '2018-06-24'),
(25, '2018-04-26 08:04:06', 'A031088', 'B200788', ' - ', 158000, 0, 158000, '2018-06-26'),
(26, '2018-04-27 07:58:54', 'T00006', 'B200788', ' - ', 105000, 105000, 0, '2018-06-27'),
(27, '2018-05-01 08:46:44', 'C00002', 'B200788', ' - ', 210000, 210000, 0, '2018-07-01'),
(28, '2018-05-01 08:47:05', 'T00003', 'B200788', ' - ', 160000, 160000, 0, '2018-07-01'),
(29, '2018-05-04 07:35:35', 'D00001', 'B200788', ' - ', 244000, 220000, 24000, '2018-07-04'),
(30, '2018-05-04 07:37:09', 'T00004', 'B200788', ' - ', 297000, 220000, 77000, '2018-07-04'),
(31, '2018-05-05 08:19:24', 'C00001', 'B200788', ' - ', 93000, 93000, 0, '2018-07-05'),
(32, '2018-05-17 03:36:54', 'B200788', 'B200788', ' - ', 97000, 97000, 0, '2018-07-17'),
(33, '2018-05-17 03:41:37', 'C00001', 'B200788', ' - ', 198000, 127000, 71000, '2018-07-17'),
(34, '2018-05-17 03:42:13', 'T00002', 'B200788', ' - ', 125000, 125000, 0, '2018-07-17'),
(35, '2018-05-17 04:16:30', 'A031088', 'B200788', ' - ', 561000, 220000, 341000, '2018-07-17'),
(36, '2018-05-19 07:44:52', 'A031088', 'B200788', ' - ', 170000, 0, 170000, '2018-07-19'),
(37, '2018-05-27 07:58:05', 'T00005', 'B200788', ' - ', 80000, 80000, 0, '2018-07-27');

-- --------------------------------------------------------

--
-- Table structure for table `_jatah_bon_tr`
--

CREATE TABLE `_jatah_bon_tr` (
  `id` int(10) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_peg` varchar(10) NOT NULL,
  `id_pegawai` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  `total` double NOT NULL DEFAULT '0',
  `jatah` double NOT NULL DEFAULT '0',
  `bon` double NOT NULL DEFAULT '0',
  `batas_bon` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_jatah_bon_tr`
--

INSERT INTO `_jatah_bon_tr` (`id`, `tgl`, `id_peg`, `id_pegawai`, `keterangan`, `total`, `jatah`, `bon`, `batas_bon`) VALUES
(1, '2018-04-19 08:58:55', 'T00001', 'A031088', ' - ', 960000, 110000, 850000, '2018-06-19'),
(2, '2018-05-04 09:35:33', 'C00001', 'B200788', ' - ', 110000, 110000, 0, '2018-07-04'),
(3, '2018-05-04 09:35:51', 'T00005', 'B200788', ' - ', 110000, 110000, 0, '2018-07-04'),
(4, '2018-05-04 09:36:35', 'T00006', 'B200788', ' - ', 1010000, 110000, 900000, '2018-07-04');

-- --------------------------------------------------------

--
-- Table structure for table `_pembelian_produk`
--

CREATE TABLE `_pembelian_produk` (
  `id` int(10) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `no_nota` varchar(20) NOT NULL,
  `id_supplier` varchar(10) NOT NULL,
  `id_pegawai` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  `total` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_pembelian_produk`
--

INSERT INTO `_pembelian_produk` (`id`, `tgl`, `no_nota`, `id_supplier`, `id_pegawai`, `keterangan`, `total`) VALUES
(1, '2018-04-18 07:09:24', '', 'SUP000', 'B200788', '-', 34844600),
(2, '2018-04-18 07:11:23', '', 'SUP000', 'B200788', '-', 60480),
(3, '2018-04-18 07:45:03', '', 'SUP000', 'B200788', '-', 248500),
(4, '2018-04-18 07:56:17', '', 'SUP000', 'B200788', '-', 7655300),
(5, '2018-04-18 08:06:14', '', 'SUP000', 'B200788', '-', 57740),
(6, '2018-04-18 08:40:36', '', 'SUP000', 'B200788', '-', 2485000),
(7, '2018-04-18 08:41:42', '', 'SUP000', 'B200788', '-', 600000),
(8, '2018-04-19 01:56:10', '', 'SUP000', 'B200788', '-', 10833300),
(9, '2018-04-19 02:04:02', '', 'SUP000', 'B200788', '-', 9501300),
(10, '2018-04-20 09:14:14', '', 'SUP000', 'B200788', '-', 4753000),
(11, '2018-04-20 09:26:23', '', 'SUP000', 'A031088', '-', 96000),
(12, '2018-04-24 06:07:58', '', 'SUP000', 'A031088', '-', 2218500),
(13, '2018-04-25 09:06:15', '', 'SUP000', 'B200788', '-', 1200000),
(14, '2018-04-29 08:53:54', '', 'SUP000', 'B200788', '-', 1521600),
(15, '2018-05-02 08:24:19', '', 'SUP000', 'B200788', '-', 3426000),
(16, '2018-05-17 02:43:39', '', 'SUP000', 'B200788', '-', 13456800),
(17, '2018-05-17 02:57:05', '', 'SUP000', 'B200788', '-', 4077000),
(18, '2018-05-17 03:09:00', '', 'SUP000', 'B200788', '-', 2077900),
(19, '2018-05-17 04:24:56', '', 'SUP000', 'B200788', '-', 4435700),
(20, '2018-05-23 08:13:05', '', 'SUP000', 'B200788', '-', 1087500),
(21, '2018-05-26 07:28:56', '', 'SUP000', 'B200788', '-', 9435400),
(22, '2018-05-30 03:04:28', '', 'SUP000', 'A031088', '-', 40200),
(23, '2018-05-30 03:09:51', '', 'SUP000', 'A031088', '-', 74400),
(24, '2018-05-30 03:14:09', '', 'SUP000', 'A031088', '-', 181440),
(25, '2018-05-30 03:41:55', '', 'SUP000', 'A031088', '-', 99400),
(26, '2018-05-30 03:42:10', '', 'SUP000', 'A031088', '-', 149100),
(27, '2018-05-30 07:31:49', '', 'SUP000', 'A031088', '-', 306000),
(28, '2018-05-31 06:44:14', '', 'SUP000', 'A031088', '-', 6980000),
(29, '2018-06-01 08:01:40', '', 'SUP000', 'B200788', '-', 4146000),
(30, '2018-06-01 08:19:31', '', 'SUP000', 'B200788', '-', 843750),
(31, '2018-06-06 06:40:48', '', 'SUP000', 'A031088', '-', 2982000),
(32, '2018-06-07 08:26:52', '', 'SUP000', 'A031088', '-', 6171800),
(33, '2018-06-11 07:54:05', '', 'SUP000', 'A031088', '-', 1320000),
(34, '2018-06-12 03:47:56', '', 'SUP000', 'A031088', '-', 1800000);

-- --------------------------------------------------------

--
-- Table structure for table `_pembelian_treatment`
--

CREATE TABLE `_pembelian_treatment` (
  `id` int(10) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `no_nota` varchar(20) NOT NULL,
  `id_supplier` varchar(10) NOT NULL,
  `id_pegawai` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  `total` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_pembelian_treatment`
--

INSERT INTO `_pembelian_treatment` (`id`, `tgl`, `no_nota`, `id_supplier`, `id_pegawai`, `keterangan`, `total`) VALUES
(1, '2018-04-18 07:14:44', '', 'SUP000', 'B200788', '-', 1698000),
(2, '2018-04-18 07:56:40', '', 'SUP000', 'B200788', '-', 2625000),
(3, '2018-04-19 07:24:36', '', 'SUP000', 'A031088', '-', 11765920),
(4, '2018-04-19 07:33:00', '', 'SUP000', 'A031088', '-', 0),
(5, '2018-04-19 07:36:27', '', 'SUP000', 'A031088', '-', 4035000),
(6, '2018-04-19 07:37:44', '', 'SUP000', 'A031088', '-', 1726400),
(7, '2018-04-19 07:38:47', '', 'SUP000', 'A031088', '-', 970000),
(8, '2018-04-19 07:39:52', '', 'SUP000', 'A031088', '-', 1227000),
(9, '2018-04-19 07:42:03', '', 'SUP000', 'A031088', '-', 2955500),
(10, '2018-04-19 07:44:14', '', 'SUP000', 'A031088', '-', 8329500),
(11, '2018-04-19 07:46:50', '', 'SUP000', 'A031088', '-', 2100000),
(12, '2018-04-19 07:48:16', '', 'SUP000', 'A031088', '-', 8135000),
(13, '2018-04-19 07:50:50', '', 'SUP000', 'A031088', '-', 8675000),
(14, '2018-04-19 07:51:46', '', 'SUP000', 'A031088', '-', 397500),
(15, '2018-04-19 08:06:37', '', 'SUP000', 'B200788', '-', 350000),
(16, '2018-04-19 08:13:57', '', 'SUP000', 'A031088', '-', 2350000),
(17, '2018-04-19 08:45:54', '', 'SUP000', 'A031088', '-', 675000),
(18, '2018-04-21 09:07:44', '', 'SUP000', 'A031088', '-', 675500),
(19, '2018-04-24 06:12:00', '', 'SUP000', 'A031088', '-', 162500),
(20, '2018-04-25 08:45:41', '', 'SUP000', 'B200788', '-', 270000),
(21, '2018-04-26 08:10:11', '', 'SUP000', 'B200788', '-', 3790000),
(22, '2018-04-27 02:13:05', '', 'SUP000', 'B200788', '-', 8230000),
(23, '2018-04-27 02:15:17', '', 'SUP000', 'B200788', '-', 3377500),
(24, '2018-04-29 08:57:32', '', 'SUP000', 'B200788', '-', 177000),
(25, '2018-04-29 09:07:05', '', 'SUP000', 'B200788', '-', 100000),
(26, '2018-05-17 02:50:01', '', 'SUP000', 'B200788', '-', 975000),
(27, '2018-05-17 02:53:54', '', 'SUP000', 'B200788', '-', 177000),
(28, '2018-05-17 03:44:16', '', 'SUP000', 'B200788', '-', 100000),
(29, '2018-05-17 03:47:57', '', 'SUP000', 'B200788', '-', 100000),
(30, '2018-05-17 04:38:59', '', 'SUP000', 'B200788', '-', 135000),
(31, '2018-05-17 08:56:58', '', 'SUP000', 'B200788', '-', 630000),
(32, '2018-05-22 08:32:10', '', 'SUP000', 'B200788', '-', 3450000),
(33, '2018-05-28 08:11:10', '', 'SUP000', 'B200788', '-', 1245000),
(34, '2018-06-01 08:15:18', '', 'SUP000', 'B200788', '-', 12329420),
(35, '2018-06-07 08:49:23', '', 'SUP000', 'A031088', '-', 3578000),
(36, '2018-06-24 09:00:31', '', 'SUP000', 'A031088', '-', 295000),
(37, '2018-06-24 09:04:43', '', 'SUP000', 'A031088', '-', 1310000);

-- --------------------------------------------------------

--
-- Table structure for table `_penjualan_produk`
--

CREATE TABLE `_penjualan_produk` (
  `id` int(10) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_pelanggan` varchar(10) NOT NULL,
  `id_pegawai` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  `total` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_penjualan_produk`
--

INSERT INTO `_penjualan_produk` (`id`, `tgl`, `id_pelanggan`, `id_pegawai`, `keterangan`, `total`) VALUES
(1, '2018-04-18 07:22:01', 'CUS000', 'B200788', ' - ', 1752000),
(2, '2018-04-18 07:51:14', 'CUS000', 'B200788', ' - ', 2340000),
(3, '2018-04-18 08:02:28', 'CUS000', 'B200788', ' - ', 2155000),
(4, '2018-04-18 08:11:12', 'CUS000', 'B200788', ' - ', 1301000),
(5, '2018-04-18 08:24:51', 'CUS000', 'B200788', ' - ', 3118000),
(6, '2018-04-18 08:29:31', 'CUS000', 'B200788', ' - ', 817000),
(7, '2018-04-18 08:34:10', 'CUS000', 'B200788', ' - ', 2060000),
(8, '2018-04-18 08:38:30', 'CUS000', 'B200788', ' - ', 2132000),
(9, '2018-04-18 08:44:18', 'CUS000', 'B200788', ' - ', 2092000),
(10, '2018-04-18 08:47:08', 'CUS000', 'B200788', ' - ', 1156000),
(11, '2018-04-18 08:55:31', 'CUS000', 'B200788', ' - ', 2287000),
(12, '2018-04-18 08:58:13', 'CUS000', 'B200788', ' - ', 1333000),
(13, '2018-04-18 09:04:20', 'CUS000', 'B200788', ' - ', 3202000),
(14, '2018-04-18 09:42:18', 'CUS000', 'B200788', ' - ', 2604000),
(15, '2018-04-19 01:50:33', 'CUS000', 'B200788', ' - ', 1002000),
(16, '2018-04-19 02:01:26', 'CUS000', 'B200788', ' - ', 1691000),
(17, '2018-04-19 02:11:14', 'CUS000', 'B200788', ' - ', 1796000),
(18, '2018-04-19 02:16:25', 'CUS000', 'B200788', ' - ', 483000),
(19, '2018-04-19 09:08:46', 'CUS000', 'B200788', ' - ', 2851000),
(20, '2018-04-20 09:37:35', 'CUS000', 'B200788', ' - ', 3422000),
(21, '2018-04-21 08:49:44', 'CUS000', 'B200788', ' - ', 1255000),
(22, '2018-04-22 07:50:34', 'CUS000', 'B200788', ' - ', 1168000),
(23, '2018-04-23 08:18:37', 'CUS000', 'B200788', ' - ', 1274000),
(24, '2018-04-24 07:01:01', 'CUS000', 'A031088', ' - ', 2809000),
(25, '2018-04-25 08:45:18', 'CUS000', 'B200788', ' - ', 1858000),
(26, '2018-04-26 08:03:35', 'CUS000', 'B200788', ' - ', 2777000),
(27, '2018-04-27 07:53:17', 'CUS000', 'B200788', ' - ', 2587000),
(28, '2018-04-29 08:57:05', 'CUS000', 'B200788', ' - ', 2720000),
(29, '2018-04-29 09:03:22', 'CUS000', 'B200788', ' - ', 2662000),
(30, '2018-04-30 04:17:00', 'CUS000', 'B200788', ' - ', 617000),
(31, '2018-05-01 08:45:30', 'CUS000', 'B200788', ' - ', 2322000),
(32, '2018-05-02 08:34:17', 'CUS000', 'B200788', ' - ', 2507000),
(33, '2018-05-04 07:26:08', 'CUS000', 'B200788', ' - ', 2314000),
(34, '2018-05-04 09:30:51', 'CUS000', 'B200788', ' - ', 2649000),
(35, '2018-05-05 08:19:03', 'CUS000', 'B200788', ' - ', 1814000),
(36, '2018-05-06 07:58:11', 'CUS000', 'B200788', ' - ', 2279000),
(37, '2018-05-17 02:47:19', 'CUS000', 'B200788', ' - ', 2020000),
(38, '2018-05-17 03:03:45', 'CUS000', 'B200788', ' - ', 1710000),
(39, '2018-05-17 03:15:34', 'CUS000', 'B200788', ' - ', 3417000),
(40, '2018-05-17 03:33:51', 'CUS000', 'B200788', ' - ', 1397000),
(41, '2018-05-17 03:40:42', 'CUS000', 'B200788', ' - ', 2134000),
(42, '2018-05-17 04:12:15', 'CUS000', 'B200788', ' - ', 2592000),
(43, '2018-05-17 04:20:14', 'CUS000', 'B200788', ' - ', 2967000),
(44, '2018-05-17 04:28:27', 'CUS000', 'B200788', ' - ', 1381000),
(45, '2018-05-17 04:36:17', 'CUS000', 'B200788', ' - ', 3258000),
(46, '2018-05-17 04:46:20', 'CUS000', 'B200788', ' - ', 910000),
(47, '2018-05-17 08:53:11', 'CUS000', 'B200788', ' - ', 1814000),
(48, '2018-05-18 07:48:05', 'CUS000', 'B200788', ' - ', 2005000),
(49, '2018-05-19 07:44:18', 'CUS000', 'B200788', ' - ', 2122000),
(50, '2018-05-20 07:25:08', 'CUS000', 'B200788', ' - ', 3568000),
(51, '2018-05-21 08:04:01', 'CUS000', 'B200788', ' - ', 1646000),
(52, '2018-05-22 08:29:16', 'CUS000', 'B200788', ' - ', 1977000),
(53, '2018-05-23 08:17:40', 'CUS000', 'B200788', ' - ', 2589000),
(54, '2018-05-25 07:48:40', 'CUS000', 'B200788', ' - ', 585000),
(55, '2018-05-25 07:52:29', 'CUS000', 'B200788', ' - ', 1865000),
(56, '2018-05-26 07:32:19', 'CUS000', 'B200788', ' - ', 1827000),
(57, '2018-05-27 07:57:42', 'CUS000', 'B200788', ' - ', 1721000),
(58, '2018-05-28 08:08:08', 'CUS000', 'B200788', ' - ', 2742000),
(59, '2018-05-30 02:36:45', 'CUS000', 'A031088', ' - ', 1485000),
(60, '2018-05-30 07:32:21', 'CUS000', 'A031088', ' - ', 190000),
(61, '2018-05-30 07:36:23', 'CUS000', 'A031088', ' - ', 2135000),
(62, '2018-05-31 06:45:12', 'CUS000', 'A031088', ' - ', 490000),
(63, '2018-06-01 08:07:53', 'CUS000', 'B200788', ' - ', 3244000),
(64, '2018-06-02 07:47:18', 'CUS000', 'B200788', ' - ', 3575000),
(65, '2018-06-03 08:26:51', 'CUS000', 'B200788', ' - ', 1445000),
(66, '2018-06-04 07:35:53', 'CUS000', 'B200788', ' - ', 2295000),
(67, '2018-06-05 09:05:46', 'CUS000', 'B200788', ' - ', 2197000),
(68, '2018-06-06 06:43:39', 'CUS000', 'A031088', ' - ', 3662000),
(69, '2018-06-07 08:29:40', 'CUS000', 'A031088', ' - ', 0),
(70, '2018-06-09 07:41:48', 'CUS000', 'B200788', ' - ', 3538000),
(71, '2018-06-11 05:32:42', 'CUS000', 'A031088', ' - ', 198000),
(72, '2018-06-11 05:38:49', 'CUS000', 'A031088', ' - ', 4575000),
(73, '2018-06-11 06:11:26', 'CUS000', 'A031088', ' - ', 4395000),
(74, '2018-06-11 07:53:19', 'CUS000', 'A031088', ' - ', 3155000),
(75, '2018-06-11 07:55:40', 'CUS000', 'A031088', ' - ', 730000),
(76, '2018-06-24 09:19:01', 'CUS000', 'A031088', ' - ', 3104000),
(77, '2018-06-24 09:39:58', 'CUS000', 'A031088', ' - ', 0),
(78, '2018-06-24 09:45:44', 'CUS000', 'A031088', ' - ', 4780000),
(79, '2018-06-24 09:50:34', 'CUS000', 'A031088', ' - ', 2197000),
(80, '2018-06-24 09:54:35', 'CUS000', 'A031088', ' - ', 2392000);

-- --------------------------------------------------------

--
-- Table structure for table `_penjualan_treatment`
--

CREATE TABLE `_penjualan_treatment` (
  `id` int(10) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_pelanggan` varchar(10) NOT NULL,
  `id_pegawai` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  `total` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_penjualan_treatment`
--

INSERT INTO `_penjualan_treatment` (`id`, `tgl`, `id_pelanggan`, `id_pegawai`, `keterangan`, `total`) VALUES
(1, '2018-04-18 07:51:39', 'CUS000', 'B200788', ' - ', 65000),
(2, '2018-04-18 08:11:44', 'CUS000', 'B200788', ' - ', 99000),
(3, '2018-04-18 08:34:36', 'CUS000', 'B200788', ' - ', 75000),
(4, '2018-04-18 08:47:37', 'CUS000', 'B200788', ' - ', 99000),
(5, '2018-04-18 09:43:51', 'CUS000', 'B200788', ' - ', 239000),
(6, '2018-04-19 02:17:17', 'CUS000', 'B200788', ' - ', 99000),
(7, '2018-04-19 08:00:18', 'CUS000', 'B200788', ' - ', 620000),
(8, '2018-04-19 08:02:16', 'CUS000', 'B200788', ' - ', 1150000),
(9, '2018-04-19 08:04:48', 'CUS000', 'B200788', ' - ', 1110000),
(10, '2018-04-19 08:05:31', 'CUS000', 'B200788', ' - ', 300000),
(11, '2018-04-19 08:07:17', 'CUS000', 'B200788', ' - ', 250000),
(12, '2018-04-19 08:07:53', 'CUS000', 'B200788', ' - ', 475000),
(13, '2018-04-19 08:10:31', 'CUS000', 'B200788', ' - ', 2050000),
(14, '2018-04-19 08:14:28', 'CUS000', 'A031088', ' - ', 100000),
(15, '2018-04-19 08:20:16', 'CUS000', 'A031088', ' - ', 2400000),
(16, '2018-04-19 08:21:55', 'CUS000', 'A031088', ' - ', 460000),
(17, '2018-04-19 08:23:22', 'CUS000', 'A031088', ' - ', 1360000),
(18, '2018-04-19 08:24:40', 'CUS000', 'A031088', ' - ', 1675000),
(19, '2018-04-19 08:37:57', 'CUS000', 'A031088', ' - ', 2695000),
(20, '2018-04-19 08:40:42', 'CUS000', 'A031088', ' - ', 2535000),
(21, '2018-04-19 08:41:59', 'CUS000', 'A031088', ' - ', 50000),
(22, '2018-04-19 08:46:43', 'CUS000', 'A031088', ' - ', 1840000),
(23, '2018-04-19 08:48:19', 'CUS000', 'A031088', ' - ', 1875000),
(24, '2018-04-19 08:49:39', 'CUS000', 'A031088', ' - ', 2990000),
(25, '2018-04-19 08:50:08', 'CUS000', 'A031088', ' - ', 250000),
(26, '2018-04-19 08:51:43', 'CUS000', 'A031088', ' - ', 2425000),
(27, '2018-04-19 08:52:35', 'CUS000', 'A031088', ' - ', 675000),
(28, '2018-04-19 09:09:21', 'CUS000', 'B200788', ' - ', 99000),
(29, '2018-04-20 09:21:59', 'CUS000', 'B200788', ' - ', 2910000),
(30, '2018-04-21 08:52:19', 'CUS000', 'B200788', ' - ', 140000),
(31, '2018-04-21 09:08:20', 'CUS000', 'A031088', ' - ', 2950000),
(32, '2018-04-22 07:53:02', 'CUS000', 'B200788', ' - ', 1453000),
(33, '2018-04-23 08:21:11', 'CUS000', 'B200788', ' - ', 810000),
(34, '2018-04-24 07:02:37', 'CUS000', 'A031088', ' - ', 469000),
(35, '2018-04-25 08:52:12', 'CUS000', 'B200788', ' - ', 270000),
(36, '2018-04-26 08:18:19', 'CUS000', 'B200788', ' - ', 3188000),
(37, '2018-04-27 07:56:50', 'CUS000', 'B200788', ' - ', 2979000),
(38, '2018-04-29 08:59:32', 'CUS000', 'B200788', ' - ', 895000),
(39, '2018-04-29 09:08:51', 'CUS000', 'B200788', ' - ', 1513000),
(40, '2018-04-30 04:17:55', 'CUS000', 'B200788', ' - ', 270000),
(41, '2018-05-01 08:50:16', 'CUS000', 'B200788', ' - ', 1949000),
(42, '2018-05-02 08:37:12', 'CUS000', 'B200788', ' - ', 2048000),
(43, '2018-05-04 07:38:36', 'CUS000', 'B200788', ' - ', 844000),
(44, '2018-05-04 09:35:01', 'CUS000', 'B200788', ' - ', 3075000),
(45, '2018-05-05 08:20:36', 'CUS000', 'B200788', ' - ', 810000),
(46, '2018-05-06 07:59:21', 'CUS000', 'B200788', ' - ', 1545000),
(47, '2018-05-17 02:53:09', 'CUS000', 'B200788', ' - ', 854000),
(48, '2018-05-17 02:55:25', 'CUS000', 'B200788', ' - ', 250000),
(49, '2018-05-17 03:19:36', 'CUS000', 'B200788', ' - ', 973000),
(50, '2018-05-17 03:35:47', 'CUS000', 'B200788', ' - ', 745000),
(51, '2018-05-17 03:49:14', 'CUS000', 'B200788', ' - ', 1605000),
(52, '2018-05-17 04:14:14', 'CUS000', 'B200788', ' - ', 1189000),
(53, '2018-05-17 04:21:44', 'CUS000', 'B200788', ' - ', 1043000),
(54, '2018-05-17 04:30:04', 'CUS000', 'B200788', ' - ', 1348000),
(55, '2018-05-17 04:44:04', 'CUS000', 'B200788', ' - ', 1815000),
(56, '2018-05-17 04:46:58', 'CUS000', 'B200788', ' - ', 50000),
(57, '2018-05-17 08:59:59', 'CUS000', 'B200788', ' - ', 1700000),
(58, '2018-05-18 07:50:03', 'CUS000', 'B200788', ' - ', 1364000),
(59, '2018-05-19 07:46:38', 'CUS000', 'B200788', ' - ', 985000),
(60, '2018-05-19 07:52:50', 'CUS000', 'B200788', ' - ', 99000),
(61, '2018-05-20 07:28:13', 'CUS000', 'B200788', ' - ', 3985000),
(62, '2018-05-21 08:04:38', 'CUS000', 'B200788', ' - ', 300000),
(63, '2018-05-22 08:33:33', 'CUS000', 'B200788', ' - ', 1525000),
(64, '2018-05-23 08:19:59', 'CUS000', 'B200788', ' - ', 1910000),
(65, '2018-05-25 07:49:24', 'CUS000', 'B200788', ' - ', 535000),
(66, '2018-05-25 07:53:19', 'CUS000', 'B200788', ' - ', 174000),
(67, '2018-05-26 07:34:19', 'CUS000', 'B200788', ' - ', 2145000),
(68, '2018-05-27 07:59:58', 'CUS000', 'B200788', ' - ', 1600000),
(69, '2018-05-28 08:10:01', 'CUS000', 'B200788', ' - ', 1219000),
(70, '2018-05-30 07:37:45', 'CUS000', 'A031088', ' - ', 1419000),
(71, '2018-05-31 06:42:55', 'CUS000', 'A031088', ' - ', 935000),
(79, '2018-06-08 08:00:18', 'CUS000', 'A031088', ' - ', 600000),
(80, '2018-06-08 08:02:25', 'CUS000', 'A031088', ' - ', 660000),
(81, '2018-06-08 08:03:54', 'CUS000', 'A031088', ' - ', 1050000),
(82, '2018-06-08 08:05:13', 'CUS000', 'A031088', ' - ', 805000),
(83, '2018-06-08 08:09:39', 'CUS000', 'A031088', ' - ', 3520000),
(84, '2018-06-08 09:18:16', 'CUS000', 'A031088', ' - ', 1510000),
(85, '2018-06-08 09:20:32', 'CUS000', 'A031088', ' - ', 1089000),
(86, '2018-06-08 09:22:43', 'CUS000', 'A031088', ' - ', 2555000),
(87, '2018-06-09 07:43:06', 'CUS000', 'B200788', ' - ', 1189000),
(88, '2018-06-10 09:21:28', 'CUS000', 'A031088', ' - ', 8517000),
(89, '2018-06-11 07:58:28', 'CUS000', 'A031088', ' - ', 4265000),
(90, '2018-06-11 07:58:40', 'CUS000', 'A031088', ' - ', 375000),
(91, '2018-06-24 08:42:39', 'CUS000', 'A031088', ' - ', 6600000),
(92, '2018-06-24 08:54:08', 'CUS000', 'A031088', ' - ', 10150000),
(93, '2018-06-24 08:55:55', 'CUS000', 'A031088', ' - ', 925000),
(94, '2018-06-24 08:58:34', 'CUS000', 'A031088', ' - ', 1760000),
(95, '2018-06-24 09:00:02', 'CUS000', 'A031088', ' - ', 285000),
(96, '2018-06-24 09:03:03', 'CUS000', 'A031088', ' - ', 1925000),
(97, '2018-06-24 09:07:06', 'CUS000', 'A031088', ' - ', 1405000);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__jatah_bon_produk`
-- (See below for the actual view)
--
CREATE TABLE `__jatah_bon_produk` (
`id` int(10)
,`tgl` timestamp
,`id_peg` varchar(10)
,`nama` varchar(100)
,`keterangan` text
,`total` double
,`jatah` double
,`bon` double
,`batas_bon` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__jatah_bon_treatment`
-- (See below for the actual view)
--
CREATE TABLE `__jatah_bon_treatment` (
`id` int(10)
,`tgl` timestamp
,`id_peg` varchar(10)
,`nama` varchar(100)
,`keterangan` text
,`total` double
,`jatah` double
,`bon` double
,`batas_bon` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__jml_beli`
-- (See below for the actual view)
--
CREATE TABLE `__jml_beli` (
`id_produk` varchar(10)
,`nama` varchar(255)
,`total_beli` bigint(11)
,`harga_aktual` double
,`tgl` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__jml_beli_tr`
-- (See below for the actual view)
--
CREATE TABLE `__jml_beli_tr` (
`id_treatment` varchar(10)
,`nama` varchar(255)
,`total_beli` bigint(11)
,`harga_aktual` double
,`tgl` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__jml_jatah_bon`
-- (See below for the actual view)
--
CREATE TABLE `__jml_jatah_bon` (
`id_produk` varchar(10)
,`nama` varchar(255)
,`total_jual` bigint(11)
,`harga_aktual` double
,`tgl` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__jml_jatah_bon_tr`
-- (See below for the actual view)
--
CREATE TABLE `__jml_jatah_bon_tr` (
`id_treatment` varchar(10)
,`nama` varchar(255)
,`total_jual` bigint(11)
,`harga_aktual` double
,`diskon_aktual` double
,`penyusutan` double
,`tgl` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__jml_jual`
-- (See below for the actual view)
--
CREATE TABLE `__jml_jual` (
`id_produk` varchar(10)
,`nama` varchar(255)
,`total_jual` bigint(11)
,`harga_aktual` double
,`diskon_aktual` double
,`tgl` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__jml_jual_tr`
-- (See below for the actual view)
--
CREATE TABLE `__jml_jual_tr` (
`id_treatment` varchar(10)
,`nama` varchar(255)
,`total_jual` bigint(11)
,`harga_aktual` double
,`diskon_aktual` double
,`penyusutan` double
,`tgl` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__pembelian_produk`
-- (See below for the actual view)
--
CREATE TABLE `__pembelian_produk` (
`id` int(10)
,`tgl` timestamp
,`id_supplier` varchar(10)
,`nama_supplier` varchar(255)
,`id_pegawai` varchar(10)
,`nama_pegawai` varchar(100)
,`keterangan` text
,`id_produk` varchar(10)
,`nama_produk` varchar(255)
,`harga` double
,`diskon` double
,`jumlah` int(5)
,`subtotal` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__pembelian_treatment`
-- (See below for the actual view)
--
CREATE TABLE `__pembelian_treatment` (
`id` int(10)
,`tgl` timestamp
,`id_supplier` varchar(10)
,`nama_supplier` varchar(255)
,`id_pegawai` varchar(10)
,`nama_pegawai` varchar(100)
,`keterangan` text
,`id_treatment` varchar(10)
,`nama_treatment` varchar(255)
,`harga` double
,`diskon` double
,`jumlah` int(5)
,`subtotal` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__penjualan_produk`
-- (See below for the actual view)
--
CREATE TABLE `__penjualan_produk` (
`id` int(10)
,`tgl` timestamp
,`id_pelanggan` varchar(10)
,`nama_pelanggan` varchar(100)
,`id_pegawai` varchar(10)
,`nama_pegawai` varchar(100)
,`keterangan` text
,`id_produk` varchar(10)
,`nama_produk` varchar(255)
,`harga` double
,`diskon` double
,`jumlah` int(5)
,`subtotal` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__penjualan_treatment`
-- (See below for the actual view)
--
CREATE TABLE `__penjualan_treatment` (
`id` int(10)
,`tgl` timestamp
,`id_pelanggan` varchar(10)
,`nama_pelanggan` varchar(100)
,`id_pegawai` varchar(10)
,`nama_pegawai` varchar(100)
,`keterangan` text
,`id_treatment` varchar(10)
,`nama_treatment` varchar(255)
,`harga` double
,`diskon` double
,`penyusutan` double
,`jumlah` int(5)
,`subtotal` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__stok`
-- (See below for the actual view)
--
CREATE TABLE `__stok` (
`id_produk` varchar(10)
,`nama` varchar(255)
,`stok` decimal(43,0)
,`ref_harga_jual` double
,`subtotal_stok` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__stok_tr`
-- (See below for the actual view)
--
CREATE TABLE `__stok_tr` (
`id_treatment` varchar(10)
,`nama` varchar(255)
,`stok` decimal(43,0)
,`ref_harga_jual` double
,`subtotal_stok` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__total_beli`
-- (See below for the actual view)
--
CREATE TABLE `__total_beli` (
`id_produk` varchar(10)
,`total_beli` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__total_beli_tr`
-- (See below for the actual view)
--
CREATE TABLE `__total_beli_tr` (
`id_treatment` varchar(10)
,`total_beli` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__total_jatah_bon`
-- (See below for the actual view)
--
CREATE TABLE `__total_jatah_bon` (
`id_produk` varchar(10)
,`total_jatah_bon` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__total_jatah_bon_tr`
-- (See below for the actual view)
--
CREATE TABLE `__total_jatah_bon_tr` (
`id_treatment` varchar(10)
,`total_jatah_bon` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__total_jual`
-- (See below for the actual view)
--
CREATE TABLE `__total_jual` (
`id_produk` varchar(10)
,`total_jual` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `__total_jual_tr`
-- (See below for the actual view)
--
CREATE TABLE `__total_jual_tr` (
`id_treatment` varchar(10)
,`total_jual` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Structure for view `__jatah_bon_produk`
--
DROP TABLE IF EXISTS `__jatah_bon_produk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__jatah_bon_produk`  AS  select `jbp`.`id` AS `id`,`jbp`.`tgl` AS `tgl`,`jbp`.`id_peg` AS `id_peg`,`p`.`nama` AS `nama`,`jbp`.`keterangan` AS `keterangan`,`jbp`.`total` AS `total`,`jbp`.`jatah` AS `jatah`,`jbp`.`bon` AS `bon`,`jbp`.`batas_bon` AS `batas_bon` from (`_jatah_bon` `jbp` join `pegawai` `p`) where (`jbp`.`id_peg` = `p`.`id`) ;

-- --------------------------------------------------------

--
-- Structure for view `__jatah_bon_treatment`
--
DROP TABLE IF EXISTS `__jatah_bon_treatment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__jatah_bon_treatment`  AS  select `jbp`.`id` AS `id`,`jbp`.`tgl` AS `tgl`,`jbp`.`id_peg` AS `id_peg`,`p`.`nama` AS `nama`,`jbp`.`keterangan` AS `keterangan`,`jbp`.`total` AS `total`,`jbp`.`jatah` AS `jatah`,`jbp`.`bon` AS `bon`,`jbp`.`batas_bon` AS `batas_bon` from (`_jatah_bon_tr` `jbp` join `pegawai` `p`) where (`jbp`.`id_peg` = `p`.`id`) ;

-- --------------------------------------------------------

--
-- Structure for view `__jml_beli`
--
DROP TABLE IF EXISTS `__jml_beli`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__jml_beli`  AS  select `pr`.`id` AS `id_produk`,`pr`.`nama` AS `nama`,if(isnull(`pb`.`id_produk`),0,`pb`.`qty`) AS `total_beli`,if(isnull(`pb`.`harga_aktual`),0,`pb`.`harga_aktual`) AS `harga_aktual`,`pp`.`tgl` AS `tgl` from (`produk` `pr` left join (`_detail_pembelian_produk` `pb` join `_pembelian_produk` `pp` on((`pb`.`id_transaksi` = `pp`.`id`))) on((`pr`.`id` = `pb`.`id_produk`))) ;

-- --------------------------------------------------------

--
-- Structure for view `__jml_beli_tr`
--
DROP TABLE IF EXISTS `__jml_beli_tr`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__jml_beli_tr`  AS  select `pr`.`id` AS `id_treatment`,`pr`.`nama` AS `nama`,if(isnull(`pb`.`id_treatment`),0,`pb`.`qty`) AS `total_beli`,if(isnull(`pb`.`id_treatment`),0,`pb`.`harga_aktual`) AS `harga_aktual`,`pt`.`tgl` AS `tgl` from (`treatment` `pr` left join (`_detail_pembelian_treatment` `pb` join `_pembelian_treatment` `pt` on((`pb`.`id_transaksi` = `pt`.`id`))) on((`pr`.`id` = `pb`.`id_treatment`))) ;

-- --------------------------------------------------------

--
-- Structure for view `__jml_jatah_bon`
--
DROP TABLE IF EXISTS `__jml_jatah_bon`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__jml_jatah_bon`  AS  select `pr`.`id` AS `id_produk`,`pr`.`nama` AS `nama`,if(isnull(`pj`.`id_produk`),0,`pj`.`qty`) AS `total_jual`,if(isnull(`pj`.`id_produk`),0,`pj`.`harga_aktual`) AS `harga_aktual`,`jb`.`tgl` AS `tgl` from (`produk` `pr` left join (`_detail_jatah_bon_produk` `pj` join `_jatah_bon` `jb` on((`pj`.`id_transaksi` = `jb`.`id`))) on((`pr`.`id` = `pj`.`id_produk`))) ;

-- --------------------------------------------------------

--
-- Structure for view `__jml_jatah_bon_tr`
--
DROP TABLE IF EXISTS `__jml_jatah_bon_tr`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__jml_jatah_bon_tr`  AS  select `pr`.`id` AS `id_treatment`,`pr`.`nama` AS `nama`,if(isnull(`pj`.`id_treatment`),0,`pj`.`qty`) AS `total_jual`,if(isnull(`pj`.`id_treatment`),0,`pj`.`harga_aktual`) AS `harga_aktual`,if(isnull(`pj`.`id_treatment`),0,`pj`.`diskon_aktual`) AS `diskon_aktual`,if(isnull(`pj`.`id_treatment`),0,`pj`.`penyusutan`) AS `penyusutan`,`jb`.`tgl` AS `tgl` from (`treatment` `pr` left join (`_detail_jatah_bon_treatment` `pj` join `_jatah_bon_tr` `jb` on((`pj`.`id_transaksi` = `jb`.`id`))) on((`pr`.`id` = `pj`.`id_treatment`))) ;

-- --------------------------------------------------------

--
-- Structure for view `__jml_jual`
--
DROP TABLE IF EXISTS `__jml_jual`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__jml_jual`  AS  select `pr`.`id` AS `id_produk`,`pr`.`nama` AS `nama`,if(isnull(`pj`.`id_produk`),0,`pj`.`qty`) AS `total_jual`,if(isnull(`pj`.`id_produk`),0,`pj`.`harga_aktual`) AS `harga_aktual`,if(isnull(`pj`.`id_produk`),0,`pj`.`diskon_aktual`) AS `diskon_aktual`,`pp`.`tgl` AS `tgl` from (`produk` `pr` left join (`_detail_penjualan_produk` `pj` join `_penjualan_produk` `pp` on((`pj`.`id_transaksi` = `pp`.`id`))) on((`pr`.`id` = `pj`.`id_produk`))) ;

-- --------------------------------------------------------

--
-- Structure for view `__jml_jual_tr`
--
DROP TABLE IF EXISTS `__jml_jual_tr`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__jml_jual_tr`  AS  select `pr`.`id` AS `id_treatment`,`pr`.`nama` AS `nama`,if(isnull(`pj`.`id_treatment`),0,`pj`.`qty`) AS `total_jual`,if(isnull(`pj`.`id_treatment`),0,`pj`.`harga_aktual`) AS `harga_aktual`,if(isnull(`pj`.`id_treatment`),0,`pj`.`diskon_aktual`) AS `diskon_aktual`,if(isnull(`pj`.`id_treatment`),0,`pj`.`penyusutan`) AS `penyusutan`,`pt`.`tgl` AS `tgl` from (`treatment` `pr` left join (`_detail_penjualan_treatment` `pj` join `_penjualan_treatment` `pt` on((`pj`.`id_transaksi` = `pt`.`id`))) on((`pr`.`id` = `pj`.`id_treatment`))) ;

-- --------------------------------------------------------

--
-- Structure for view `__pembelian_produk`
--
DROP TABLE IF EXISTS `__pembelian_produk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__pembelian_produk`  AS  select `p`.`id` AS `id`,`p`.`tgl` AS `tgl`,`p`.`id_supplier` AS `id_supplier`,`c`.`supplier` AS `nama_supplier`,`p`.`id_pegawai` AS `id_pegawai`,`e`.`nama` AS `nama_pegawai`,`p`.`keterangan` AS `keterangan`,`d`.`id_produk` AS `id_produk`,`pr`.`nama` AS `nama_produk`,`d`.`harga_aktual` AS `harga`,`d`.`diskon_aktual` AS `diskon`,`d`.`qty` AS `jumlah`,((`d`.`harga_aktual` * `d`.`qty`) - (`d`.`diskon_aktual` * `d`.`qty`)) AS `subtotal` from ((((`_pembelian_produk` `p` join `_detail_pembelian_produk` `d`) join `supplier` `c`) join `pegawai` `e`) join `produk` `pr`) where ((`d`.`id_transaksi` = `p`.`id`) and (`p`.`id_supplier` = `c`.`id`) and (`p`.`id_pegawai` = `e`.`id`) and (`d`.`id_produk` = `pr`.`id`)) order by `p`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `__pembelian_treatment`
--
DROP TABLE IF EXISTS `__pembelian_treatment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__pembelian_treatment`  AS  select `p`.`id` AS `id`,`p`.`tgl` AS `tgl`,`p`.`id_supplier` AS `id_supplier`,`c`.`supplier` AS `nama_supplier`,`p`.`id_pegawai` AS `id_pegawai`,`e`.`nama` AS `nama_pegawai`,`p`.`keterangan` AS `keterangan`,`d`.`id_treatment` AS `id_treatment`,`pr`.`nama` AS `nama_treatment`,`d`.`harga_aktual` AS `harga`,`d`.`diskon_aktual` AS `diskon`,`d`.`qty` AS `jumlah`,((`d`.`harga_aktual` * `d`.`qty`) - (`d`.`diskon_aktual` * `d`.`qty`)) AS `subtotal` from ((((`_pembelian_treatment` `p` join `_detail_pembelian_treatment` `d`) join `supplier` `c`) join `pegawai` `e`) join `treatment` `pr`) where ((`d`.`id_transaksi` = `p`.`id`) and (`p`.`id_supplier` = `c`.`id`) and (`p`.`id_pegawai` = `e`.`id`) and (`d`.`id_treatment` = `pr`.`id`)) order by `p`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `__penjualan_produk`
--
DROP TABLE IF EXISTS `__penjualan_produk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__penjualan_produk`  AS  select `p`.`id` AS `id`,`p`.`tgl` AS `tgl`,`p`.`id_pelanggan` AS `id_pelanggan`,`c`.`nama` AS `nama_pelanggan`,`p`.`id_pegawai` AS `id_pegawai`,`e`.`nama` AS `nama_pegawai`,`p`.`keterangan` AS `keterangan`,`d`.`id_produk` AS `id_produk`,`pr`.`nama` AS `nama_produk`,`d`.`harga_aktual` AS `harga`,`d`.`diskon_aktual` AS `diskon`,`d`.`qty` AS `jumlah`,((`d`.`harga_aktual` * `d`.`qty`) - (`d`.`diskon_aktual` * `d`.`qty`)) AS `subtotal` from ((((`_penjualan_produk` `p` join `_detail_penjualan_produk` `d`) join `pelanggan` `c`) join `pegawai` `e`) join `produk` `pr`) where ((`d`.`id_transaksi` = `p`.`id`) and (`p`.`id_pelanggan` = `c`.`id`) and (`p`.`id_pegawai` = `e`.`id`) and (`d`.`id_produk` = `pr`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `__penjualan_treatment`
--
DROP TABLE IF EXISTS `__penjualan_treatment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__penjualan_treatment`  AS  select `p`.`id` AS `id`,`p`.`tgl` AS `tgl`,`p`.`id_pelanggan` AS `id_pelanggan`,`c`.`nama` AS `nama_pelanggan`,`p`.`id_pegawai` AS `id_pegawai`,`e`.`nama` AS `nama_pegawai`,`p`.`keterangan` AS `keterangan`,`d`.`id_treatment` AS `id_treatment`,`pr`.`nama` AS `nama_treatment`,`d`.`harga_aktual` AS `harga`,`d`.`diskon_aktual` AS `diskon`,`d`.`penyusutan` AS `penyusutan`,`d`.`qty` AS `jumlah`,(((`d`.`harga_aktual` * `d`.`qty`) - (`d`.`diskon_aktual` * `d`.`qty`)) - (`d`.`penyusutan` * `d`.`qty`)) AS `subtotal` from ((((`_penjualan_treatment` `p` join `_detail_penjualan_treatment` `d`) join `pelanggan` `c`) join `pegawai` `e`) join `treatment` `pr`) where ((`d`.`id_transaksi` = `p`.`id`) and (`p`.`id_pelanggan` = `c`.`id`) and (`p`.`id_pegawai` = `e`.`id`) and (`d`.`id_treatment` = `pr`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `__stok`
--
DROP TABLE IF EXISTS `__stok`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__stok`  AS  select `j`.`id_produk` AS `id_produk`,`pr`.`nama` AS `nama`,(`b`.`total_beli` - (`j`.`total_jual` + `d`.`total_jatah_bon`)) AS `stok`,`pr`.`ref_harga_jual` AS `ref_harga_jual`,((`b`.`total_beli` - (`j`.`total_jual` + `d`.`total_jatah_bon`)) * `pr`.`ref_harga_jual`) AS `subtotal_stok` from (((`__total_jual` `j` join `__total_beli` `b`) join `__total_jatah_bon` `d`) join `produk` `pr`) where ((`b`.`id_produk` = `j`.`id_produk`) and (`b`.`id_produk` = `pr`.`id`) and (`b`.`id_produk` = `d`.`id_produk`)) ;

-- --------------------------------------------------------

--
-- Structure for view `__stok_tr`
--
DROP TABLE IF EXISTS `__stok_tr`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__stok_tr`  AS  select `j`.`id_treatment` AS `id_treatment`,`pr`.`nama` AS `nama`,(`b`.`total_beli` - (`j`.`total_jual` + `d`.`total_jatah_bon`)) AS `stok`,`pr`.`ref_harga_jual` AS `ref_harga_jual`,((`b`.`total_beli` - (`j`.`total_jual` + `d`.`total_jatah_bon`)) * `pr`.`ref_harga_jual`) AS `subtotal_stok` from (((`__total_jual_tr` `j` join `__total_beli_tr` `b`) join `__total_jatah_bon_tr` `d`) join `treatment` `pr`) where ((`b`.`id_treatment` = `j`.`id_treatment`) and (`b`.`id_treatment` = `pr`.`id`) and (`b`.`id_treatment` = `d`.`id_treatment`)) ;

-- --------------------------------------------------------

--
-- Structure for view `__total_beli`
--
DROP TABLE IF EXISTS `__total_beli`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__total_beli`  AS  select `__jml_beli`.`id_produk` AS `id_produk`,sum(`__jml_beli`.`total_beli`) AS `total_beli` from `__jml_beli` group by `__jml_beli`.`id_produk` ;

-- --------------------------------------------------------

--
-- Structure for view `__total_beli_tr`
--
DROP TABLE IF EXISTS `__total_beli_tr`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__total_beli_tr`  AS  select `__jml_beli_tr`.`id_treatment` AS `id_treatment`,sum(`__jml_beli_tr`.`total_beli`) AS `total_beli` from `__jml_beli_tr` group by `__jml_beli_tr`.`id_treatment` ;

-- --------------------------------------------------------

--
-- Structure for view `__total_jatah_bon`
--
DROP TABLE IF EXISTS `__total_jatah_bon`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__total_jatah_bon`  AS  select `__jml_jatah_bon`.`id_produk` AS `id_produk`,sum(`__jml_jatah_bon`.`total_jual`) AS `total_jatah_bon` from `__jml_jatah_bon` group by `__jml_jatah_bon`.`id_produk` ;

-- --------------------------------------------------------

--
-- Structure for view `__total_jatah_bon_tr`
--
DROP TABLE IF EXISTS `__total_jatah_bon_tr`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__total_jatah_bon_tr`  AS  select `__jml_jatah_bon_tr`.`id_treatment` AS `id_treatment`,sum(`__jml_jatah_bon_tr`.`total_jual`) AS `total_jatah_bon` from `__jml_jatah_bon_tr` group by `__jml_jatah_bon_tr`.`id_treatment` ;

-- --------------------------------------------------------

--
-- Structure for view `__total_jual`
--
DROP TABLE IF EXISTS `__total_jual`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__total_jual`  AS  select `__jml_jual`.`id_produk` AS `id_produk`,sum(`__jml_jual`.`total_jual`) AS `total_jual` from `__jml_jual` group by `__jml_jual`.`id_produk` ;

-- --------------------------------------------------------

--
-- Structure for view `__total_jual_tr`
--
DROP TABLE IF EXISTS `__total_jual_tr`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `__total_jual_tr`  AS  select `__jml_jual_tr`.`id_treatment` AS `id_treatment`,sum(`__jml_jual_tr`.`total_jual`) AS `total_jual` from `__jml_jual_tr` group by `__jml_jual_tr`.`id_treatment` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `diskon`
--
ALTER TABLE `diskon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `jatah`
--
ALTER TABLE `jatah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kd_jabatan` (`kd_jabatan`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penyusutan`
--
ALTER TABLE `penyusutan`
  ADD PRIMARY KEY (`id_treatment`),
  ADD KEY `id_treatment` (`id_treatment`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_diskon` (`id_diskon`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `treatment`
--
ALTER TABLE `treatment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_diskon` (`id_diskon`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `_detail_jatah_bon_produk`
--
ALTER TABLE `_detail_jatah_bon_produk`
  ADD PRIMARY KEY (`id_transaksi`,`id_produk`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `_detail_jatah_bon_treatment`
--
ALTER TABLE `_detail_jatah_bon_treatment`
  ADD PRIMARY KEY (`id_transaksi`,`id_treatment`),
  ADD KEY `id_treatment` (`id_treatment`);

--
-- Indexes for table `_detail_pembelian_produk`
--
ALTER TABLE `_detail_pembelian_produk`
  ADD PRIMARY KEY (`id_transaksi`,`id_produk`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `_detail_pembelian_treatment`
--
ALTER TABLE `_detail_pembelian_treatment`
  ADD PRIMARY KEY (`id_transaksi`,`id_treatment`),
  ADD KEY `id_produk` (`id_treatment`);

--
-- Indexes for table `_detail_penjualan_produk`
--
ALTER TABLE `_detail_penjualan_produk`
  ADD PRIMARY KEY (`id_transaksi`,`id_produk`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `_detail_penjualan_treatment`
--
ALTER TABLE `_detail_penjualan_treatment`
  ADD PRIMARY KEY (`id_transaksi`,`id_treatment`),
  ADD KEY `id_treatment` (`id_treatment`);

--
-- Indexes for table `_jatah_bon`
--
ALTER TABLE `_jatah_bon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pelanggan` (`id_peg`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `_jatah_bon_tr`
--
ALTER TABLE `_jatah_bon_tr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pelanggan` (`id_peg`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `_pembelian_produk`
--
ALTER TABLE `_pembelian_produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pelanggan` (`id_supplier`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `_pembelian_treatment`
--
ALTER TABLE `_pembelian_treatment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pelanggan` (`id_supplier`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `_penjualan_produk`
--
ALTER TABLE `_penjualan_produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pelanggan` (`id_pelanggan`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `_penjualan_treatment`
--
ALTER TABLE `_penjualan_treatment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pelanggan` (`id_pelanggan`),
  ADD KEY `id_pegawai` (`id_pegawai`),
  ADD KEY `id_pelanggan_2` (`id_pelanggan`),
  ADD KEY `id_pegawai_2` (`id_pegawai`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jatah`
--
ALTER TABLE `jatah`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `_jatah_bon`
--
ALTER TABLE `_jatah_bon`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `_jatah_bon_tr`
--
ALTER TABLE `_jatah_bon_tr`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `_pembelian_produk`
--
ALTER TABLE `_pembelian_produk`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `_pembelian_treatment`
--
ALTER TABLE `_pembelian_treatment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `_penjualan_produk`
--
ALTER TABLE `_penjualan_produk`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `_penjualan_treatment`
--
ALTER TABLE `_penjualan_treatment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
