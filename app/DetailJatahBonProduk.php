<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DetailJatahBonProduk
 *
 * @mixin \Eloquent
 * @property int $id_transaksi
 * @property string $id_produk
 * @property float $harga_aktual
 * @property float $diskon_aktual
 * @property int $qty
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonProduk whereDiskonAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonProduk whereHargaAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonProduk whereIdProduk($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonProduk whereIdTransaksi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonProduk whereQty($value)
 * @property-read \App\JatahBonProduk $jatahBonProduk
 * @property-read \App\Produk $produk
 */
class DetailJatahBonProduk extends Model
{
    protected $table = '_detail_jatah_bon_produk';
    protected $fillable = ['id_transaksi', 'id_produk', 'harga_aktual', 'diskon_aktual', 'qty'];
    protected $primaryKey = ['id_transaksi', 'id_produk'];
    public $timestamps = false;
    public $incrementing = false;

    public function jatahBonProduk()
    {
        return $this->belongsTo('App\JatahBonProduk', 'id_transaksi');
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk', 'id_produk');
    }
}
