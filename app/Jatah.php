<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Jatah
 *
 * @property int $id
 * @property float $nominal
 * @property string $keterangan
 * @method static \Illuminate\Database\Query\Builder|\App\Jatah whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Jatah whereKeterangan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Jatah whereNominal($value)
 * @mixin \Eloquent
 */
class Jatah extends Model
{
    protected $table = 'jatah';
    protected $fillable = ['id', 'nominal', 'keterangan'];
    public $timestamps = false;
}
