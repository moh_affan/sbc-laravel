<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Jabatan
 *
 * @property string $kd
 * @property string $jabatan
 * @property-read \App\Pegawai $pegawai
 * @method static \Illuminate\Database\Query\Builder|\App\Jabatan whereJabatan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Jabatan whereKd($value)
 * @mixin \Eloquent
 */
class Jabatan extends Model
{
    protected $table = 'jabatan';
    protected $fillable = ['kd', 'jabatan'];
    protected $primaryKey = 'kd';
    public $timestamps = false;
    public $incrementing = false;

    public function pegawai()
    {
        return $this->hasMany('App\Pegawai', 'kd_jabatan', 'kd');
    }
}
