<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PenjualanTreatment
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $tgl
 * @property string $id_pelanggan
 * @property string $id_pegawai
 * @property string $keterangan
 * @property float $total
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailPenjualanTreatment[] $detailPenjualanTreatment
 * @property-read \App\Pegawai $pegawai
 * @property-read \App\Pelanggan $pelanggan
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanTreatment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanTreatment whereIdPegawai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanTreatment whereIdPelanggan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanTreatment whereKeterangan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanTreatment whereTgl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanTreatment whereTotal($value)
 */
class PenjualanTreatment extends Model
{
    protected $table = '_penjualan_treatment';
    protected $fillable = ['id', 'tgl', 'id_pelanggan', 'id_pegawai', 'keterangan', 'total'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function detailPenjualanTreatment()
    {
        return $this->hasMany('App\DetailPenjualanTreatment', 'id_transaksi', 'id');
    }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }

    public function pelanggan()
    {
        return $this->belongsTo('App\Pelanggan', 'id_pelanggan');
    }
}
