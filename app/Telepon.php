<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Telepon
 *
 * @property int $id_siswa
 * @property string $nomor_telepon
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Siswa $siswa
 * @method static \Illuminate\Database\Query\Builder|\App\Telepon whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Telepon whereIdSiswa($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Telepon whereNomorTelepon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Telepon whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Telepon extends Model
{
    protected $table = 'telepon';
    protected $primaryKey = 'id_siswa';
    protected $fillable = ['id_siswa','nomor_telepon'];

    public function siswa(){
        return $this->belongsTo('App\Siswa', 'id_siswa');
    }
}
