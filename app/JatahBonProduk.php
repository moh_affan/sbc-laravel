<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\JatahBonProduk
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $tgl
 * @property string $id_peg
 * @property string $id_pegawai
 * @property string $keterangan
 * @property float $total
 * @property float $jatah
 * @property float $bon
 * @property string $batas_bon
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonProduk whereBatasBon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonProduk whereBon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonProduk whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonProduk whereIdPeg($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonProduk whereIdPegawai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonProduk whereJatah($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonProduk whereKeterangan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonProduk whereTgl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonProduk whereTotal($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailPenjualanProduk[] $detailPenjualanProduk
 * @property-read \App\Pegawai $pegawai
 * @property-read \App\Pelanggan $pelanggan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailJatahBonProduk[] $detailJatahBonProduk
 * @property-read \App\Pegawai $peg
 */
class JatahBonProduk extends Model
{
    protected $table = '_jatah_bon';
    protected $fillable = ['id', 'tgl', 'id_peg', 'id_pegawai', 'keterangan', 'total', 'jatah', 'bon', 'batas_bon'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $dates = ['tgl', 'batas_bon'];

    public function detailJatahBonProduk()
    {
        return $this->hasMany('App\DetailJatahBonProduk', 'id_transaksi', 'id');
    }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }

    public function peg()
    {
        return $this->belongsTo('App\Pegawai', 'id_peg');
    }
}
