<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Pelanggan
 *
 * @property string $id
 * @property string $nama
 * @property string $telp
 * @property string $alamat
 * @property string $tgl_lahir
 * @method static \Illuminate\Database\Query\Builder|\App\Pelanggan whereAlamat($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pelanggan whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pelanggan whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pelanggan whereTelp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pelanggan whereTglLahir($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PenjualanProduk[] $penjualanProduk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PenjualanTreatment[] $penjualanTreatment
 */
class Pelanggan extends Model
{
    protected $table = 'pelanggan';
    protected $fillable = ['id', 'nama', 'telp', 'alamat', 'tgl_lahir'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = false;
//    protected $dates = ['tgl_lahir'];

    public function penjualanProduk()
    {
        return $this->hasMany('App\PenjualanProduk', 'id_pelanggan', 'id');
    }

    public function penjualanTreatment()
    {
        return $this->hasMany('App\PenjualanTreatment', 'id_pelanggan', 'id');
    }

}
