<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DetailPenjualanTreatment
 *
 * @mixin \Eloquent
 * @property int $id_transaksi
 * @property string $id_treatment
 * @property float $harga_aktual
 * @property float $diskon_aktual
 * @property float $penyusutan
 * @property int $qty
 * @property-read \App\PenjualanTreatment $penjualanTreatment
 * @property-read \App\Treatment $treatment
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanTreatment whereDiskonAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanTreatment whereHargaAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanTreatment whereIdTransaksi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanTreatment whereIdTreatment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanTreatment wherePenyusutan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanTreatment whereQty($value)
 */
class DetailPenjualanTreatment extends Model
{
    protected $table = '_detail_penjualan_treatment';
    protected $fillable = ['id_transaksi', 'id_treatment', 'harga_aktual', 'diskon_aktual', 'penyusutan', 'qty'];
    protected $primaryKey = ['id_transaksi', 'id_treatment'];
    public $timestamps = false;
    public $incrementing = false;

    public function penjualanTreatment()
    {
        return $this->belongsTo('App\PenjualanTreatment', 'id_transaksi');
    }

    public function treatment()
    {
        return $this->belongsTo('App\Treatment', 'id_treatment');
    }
}
