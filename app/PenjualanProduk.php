<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PenjualanProduk
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $tgl
 * @property string $id_pelanggan
 * @property string $id_pegawai
 * @property string $keterangan
 * @property float $total
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanProduk whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanProduk whereIdPegawai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanProduk whereIdPelanggan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanProduk whereKeterangan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanProduk whereTgl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PenjualanProduk whereTotal($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailPenjualanProduk[] $detailPenjualanProduk
 * @property-read \App\Pegawai $pegawai
 * @property-read \App\Pelanggan $pelanggan
 */
class PenjualanProduk extends Model
{
    protected $table = '_penjualan_produk';
    protected $fillable = ['id', 'tgl', 'id_pelanggan', 'id_pegawai', 'keterangan', 'total'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function detailPenjualanProduk()
    {
        return $this->hasMany('App\DetailPenjualanProduk', 'id_transaksi', 'id');
    }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }

    public function pelanggan()
    {
        return $this->belongsTo('App\Pelanggan', 'id_pelanggan');
    }
}
