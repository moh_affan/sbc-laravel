<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Siswa
 *
 * @property int $id
 * @property string $nisn
 * @property string $nama_siswa
 * @property \Carbon\Carbon $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Siswa whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Siswa whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Siswa whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Siswa whereJenisKelamin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Siswa whereNamaSiswa($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Siswa whereNisn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Siswa whereTanggalLahir($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Siswa whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Telepon $telepon
 */
class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = ['nisn', 'nama_siswa', 'tanggal_lahir', 'jenis_kelamin', 'image'];
    protected $dates = ['tanggal_lahir'];
    public function telepon(){
        return $this->hasOne('App\Telepon','id_siswa');
    }
}
