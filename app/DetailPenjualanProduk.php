<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DetailPenjualanProduk
 *
 * @mixin \Eloquent
 * @property int $id_transaksi
 * @property string $id_produk
 * @property float $harga_aktual
 * @property float $diskon_aktual
 * @property int $qty
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanProduk whereDiskonAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanProduk whereHargaAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanProduk whereIdProduk($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanProduk whereIdTransaksi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPenjualanProduk whereQty($value)
 * @property-read \App\PenjualanProduk $penjualanProduk
 * @property-read \App\Produk $produk
 */
class DetailPenjualanProduk extends Model
{
    protected $table = '_detail_penjualan_produk';
    protected $fillable = ['id_transaksi', 'id_produk', 'harga_aktual', 'diskon_aktual', 'qty'];
    protected $primaryKey = ['id_transaksi', 'id_produk'];
    public $timestamps = false;
    public $incrementing = false;

    public function penjualanProduk()
    {
        return $this->belongsTo('App\PenjualanProduk', 'id_transaksi');
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk', 'id_produk');
    }
}
