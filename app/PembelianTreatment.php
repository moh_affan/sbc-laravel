<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PembelianTreatment
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $tgl
 * @property string $no_nota
 * @property string $id_supplier
 * @property string $id_pegawai
 * @property string $keterangan
 * @property float $total
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianTreatment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianTreatment whereIdPegawai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianTreatment whereIdSupplier($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianTreatment whereKeterangan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianTreatment whereNoNota($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianTreatment whereTgl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianTreatment whereTotal($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailPembelianTreatment[] $detailPembelianTreatment
 * @property-read \App\Pegawai $pegawai
 * @property-read \App\Supplier $supplier
 */
class PembelianTreatment extends Model
{
    protected $table = '_pembelian_treatment';
    protected $fillable = ['id', 'tgl', 'no_nota', 'id_supplier', 'id_pegawai', 'keterangan', 'total'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $dates = ['tgl'];

    public function detailPembelianTreatment()
    {
        return $this->hasMany('App\DetailPembelianTreatment', 'id_transaksi', 'id');
    }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'id_supplier');
    }
}
