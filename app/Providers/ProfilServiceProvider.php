<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ProfilServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::composer(
            ['main', 'sidebar', 'l1.pembelianproduk', 'l1.pembeliantreatment', 'l1.penjualanproduk', 'l1.penjualantreatment', 'l1.jatahbonproduk', 'l1.jatahbontreatment'],
            'App\Http\ViewComposer\ProfileComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
