<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Treatment
 *
 * @property string $id
 * @property string $id_kategori
 * @property string $id_supplier
 * @property string $nama
 * @property float $ref_harga_jual
 * @property float $ref_harga_beli
 * @property string $id_diskon
 * @method static \Illuminate\Database\Query\Builder|\App\Treatment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Treatment whereIdDiskon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Treatment whereIdKategori($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Treatment whereIdSupplier($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Treatment whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Treatment whereRefHargaBeli($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Treatment whereRefHargaJual($value)
 * @mixin \Eloquent
 * @property-read \App\Diskon $diskon
 * @property-read \App\Kategori $kategori
 * @property-read \App\Supplier $supplier
 * @property-read \App\Penyusutan $penyusutan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailPembelianTreatment[] $detailPembelianTreatment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailPenjualanTreatment[] $detailPenjualanTreatment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailJatahBonTreatment[] $detailJatahBonTreatment
 * @property-read \App\StokTreatment $stokTreatment
 */
class Treatment extends Model
{
    protected $table = 'treatment';
    protected $fillable = ['id', 'id_kategori', 'id_supplier', 'nama', 'ref_harga_jual', 'ref_harga_beli', 'id_diskon'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = false;

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'id_supplier');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'id_kategori');
    }

    public function diskon()
    {
        return $this->belongsTo('App\Diskon', 'id_diskon');
    }

    public function penyusutan()
    {
        return $this->hasOne('App\Penyusutan', 'id_treatment', 'id');
    }

    public function detailPembelianTreatment()
    {
        return $this->hasMany('App\DetailPembelianTreatment', 'id_treatment', 'id');
    }

    public function detailPenjualanTreatment()
    {
        return $this->hasMany('App\DetailPenjualanTreatment', 'id_treatment', 'id');
    }

    public function detailJatahBonTreatment()
    {
        return $this->hasMany('App\DetailJatahBonTreatment', 'id_treatment', 'id');
    }

    public function stokTreatment()
    {
        return $this->hasOne('App\StokTreatment', 'id_treatment', 'id');
    }
}
