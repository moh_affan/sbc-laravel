<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DetailPembelianTreatment
 *
 * @mixin \Eloquent
 * @property int $id_transaksi
 * @property string $id_treatment
 * @property float $harga_aktual
 * @property float $diskon_aktual
 * @property int $qty
 * @property-read \App\PembelianTreatment $pembelianTreatment
 * @property-read \App\Treatment $treatment
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPembelianTreatment whereDiskonAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPembelianTreatment whereHargaAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPembelianTreatment whereIdTransaksi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPembelianTreatment whereIdTreatment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPembelianTreatment whereQty($value)
 */
class DetailPembelianTreatment extends Model
{
    protected $table = '_detail_pembelian_treatment';
    protected $fillable = ['id_transaksi', 'id_treatment', 'harga_aktual', 'diskon_aktual', 'qty'];
    protected $primaryKey = ['id_transaksi', 'id_treatment'];
    public $timestamps = false;
    public $incrementing = false;

    public function pembelianTreatment()
    {
        return $this->belongsTo('App\PembelianTreatment', 'id_transaksi');
    }

    public function treatment()
    {
        return $this->belongsTo('App\Treatment', 'id_treatment');
    }
}
