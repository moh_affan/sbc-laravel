<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Pengguna
 *
 * @property int $id
 * @property string $id_pegawai
 * @property string $username
 * @property string $password
 * @property int $level
 * @method static \Illuminate\Database\Query\Builder|\App\Pengguna whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pengguna whereIdPegawai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pengguna whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pengguna wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pengguna whereUsername($value)
 * @mixin \Eloquent
 * @property-read \App\Pegawai $pegawai
 */
class Pengguna extends Model
{
    protected $table = 'user';
    protected $fillable = ['id_pegawai', 'username', 'password', 'level'];
    public $timestamps = false;

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }
}