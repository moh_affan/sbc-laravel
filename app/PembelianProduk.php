<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PembelianProduk
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $tgl
 * @property string $no_nota
 * @property string $id_supplier
 * @property string $id_pegawai
 * @property string $keterangan
 * @property float $total
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianProduk whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianProduk whereIdPegawai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianProduk whereIdSupplier($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianProduk whereKeterangan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianProduk whereNoNota($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianProduk whereTgl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PembelianProduk whereTotal($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailPembelianProduk[] $detailPembelianProduk
 * @property-read \App\Pegawai $pegawai
 * @property-read \App\Supplier $supplier
 */
class PembelianProduk extends Model
{
    protected $table = '_pembelian_produk';
    protected $fillable = ['id', 'tgl', 'no_nota', 'id_supplier', 'id_pegawai', 'keterangan', 'total'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $dates = ['tgl'];

    public function detailPembelianProduk()
    {
        return $this->hasMany('App\DetailPembelianProduk', 'id_transaksi', 'id');
    }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'id_supplier');
    }
}
