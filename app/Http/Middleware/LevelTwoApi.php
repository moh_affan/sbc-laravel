<?php

namespace App\Http\Middleware;

use Closure;

class LevelTwoApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $logged = !empty($request->session()->get('id', ''))
            && !empty($request->session()->get('id_pegawai', ''))
            && !empty($request->session()->get('username', ''));
        $privilege = $request->session()->get('level', '') == '0' || $request->session()->get('level', '') == '2';
        if ($logged && $privilege) {
            return $next($request);
        } else {
            if (str_contains($request->url(), '/api/'))
                return response()->json(['error' => 'Forbidden'], 403);
            else
                return \Redirect::to('/')->with('redirect', $request->url());//change to login
        }
    }
}
