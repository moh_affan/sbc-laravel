<?php

namespace App\Http\Middleware;

use Closure;

class Api
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $logged = !empty($request->session()->get('id', ''))
            && !empty($request->session()->get('id_pegawai', ''))
            && !empty($request->session()->get('username', ''))
            && !empty($request->session()->get('level', ''));
        $privilege = $request->session()->get('level', '') == '0';
        if ($logged && $privilege) {
            return $next($request);
        } else {
            return redirect('admin/login');
        }
    }
}
