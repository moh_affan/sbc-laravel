<?php

namespace App\Http\Controllers;

use App\DetailPenjualanProduk;
use App\Pelanggan;
use App\PenjualanProduk;
use App\Produk;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PenjualanProdukController extends Controller
{
    public function index()
    {
        $pelanggan = Pelanggan::all()->sortBy('id');
        $transaksiSaya = PenjualanProduk::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get();
        $title = 'Penjualan Produk';
        return view('l1.penjualanproduk', compact('pelanggan', 'title', 'transaksiSaya'));
    }

    public function transasksiSaya()
    {
        $transaksiSaya = PenjualanProduk::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get()->sortByDesc('id');
        $response = "";
        foreach ($transaksiSaya as $transaksi) {
            $response .= '<tr>
                                        <td>' . $transaksi->id . '</td>
                                        <td>' . $transaksi->tgl . '</td>
                                        <td>' . $transaksi->pelanggan->nama . '(' . $transaksi->id_pelanggan . ')</td>
                                        <td>' . $transaksi->keterangan . '</td>
                                        <td>
                                            <button class="btn btn-sm btn-default detail" value=\'{"id":"' . $transaksi->id . '"}\'>Detail</button>
                                        </td>
                                    </tr>';
        }
        return $response;
    }

    public function loadTransaction($id)
    {
        $transaksi = PenjualanProduk::find($id);
        $response = "";
        $response .= '<button style="position:absolute;right:10px;top:0;" class="btn btn-flat btn-primary" id="print-this" value="' . $transaksi->id . '"><span class="fa fa-print"></span></button>';
        $response .= '<h5>ID Transaksi : ' . $transaksi->id . '</h5>';
        $response .= '<h5>Tanggal : ' . $transaksi->tgl . '</h5>';
        $response .= '<h5>Pelanggan : ' . $transaksi->id_pelanggan . ' ' . $transaksi->pelanggan->nama . '</h5>';
        $response .= '<table class="table table-bordered table-responsive">';
        $response .= '<thead>';
        $response .= '<tr>';
        $response .= '<th>ID</th><th>Produk</th><th>Jumlah</th><th>Harga</th><th>Diskon</th><th class="col-subtotal">Subtotal</th>';
        $response .= '</tr>';
        $response .= '</thead>';
        $response .= '<tbody>';
        $total = 0;
        foreach ($transaksi->detailPenjualanProduk as $detailPenjualanProduk) {
            $response .= '<tr>';
            $response .= "<td>$detailPenjualanProduk->id_produk</td><td>" . $detailPenjualanProduk->produk->nama . "</td><td>$detailPenjualanProduk->qty</td><td>" . $detailPenjualanProduk->harga_aktual . "</td><td>" . $detailPenjualanProduk->diskon_aktual . "</td><td>" . (($detailPenjualanProduk->qty * $detailPenjualanProduk->harga_aktual) - ($detailPenjualanProduk->qty * $detailPenjualanProduk->diskon_aktual)) . "</td>";
            $response .= '</tr>';
            $total += (($detailPenjualanProduk->qty * $detailPenjualanProduk->harga_aktual) - ($detailPenjualanProduk->qty * $detailPenjualanProduk->diskon_aktual));
        }
        $response .= '</tbody>';
        $response .= '<tfoot>';
        $response .= '<tr>';
        $response .= '<th colspan="5">Total = ' . $transaksi->total . ' </th > ';
        $response .= '</tr > ';
        $response .= '</tfoot > ';
        $response .= '</table > ';
        return $response;
    }

    public function printTransaction($id)
    {
        $transaksi = PenjualanProduk::find($id);
        return view('l1.printpenjualanproduk', compact('transaksi'));
    }

    public function lastId()
    {
        $penjualanProduk = PenjualanProduk::latest('id')->first();
        if (isset($penjualanProduk['id']))
            return \Response::json($penjualanProduk);
        else
            return \Response::json(['id' => 0]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $penjualanProduk = new PenjualanProduk();
        $penjualanProduk->id_pelanggan = $input['id_pelanggan'];
        $penjualanProduk->tgl = $input['tgl'];
        $penjualanProduk->id_pegawai = $input['id_pegawai'];
        $penjualanProduk->keterangan = ' - ';
        $penjualanProduk->total = 0;
        $total = 0;
        if ($penjualanProduk->save()) {
            for ($i = 0; $i < count($input['id_produk']); $i++) {
                $produk = Produk::find($input['id_produk'][$i]);
                $detail = new DetailPenjualanProduk();
                $detail->id_produk = $produk->id;
                $detail->harga_aktual = $produk->ref_harga_jual;
                if (isset($produk->diskon)) {
                    $diskon = $produk->diskon;
                    if (Carbon::now()->diffInDays(Carbon::parse($diskon->tgl_awal)) >= 0 && Carbon::now()->diffInDays(Carbon::parse($diskon->tgl_akhir)) <= 0)
                        $detail->diskon_aktual = $diskon->jumlah_diskon;
                    else
                        $detail->diskon_aktual = 0;
                } else
                    $detail->diskon_aktual = 0;
                $detail->qty = $input['qty'][$i];
                $total += (($produk->ref_harga_jual * intval($input['qty'][$i])) - ($detail->diskon_aktual * intval($input['qty'][$i])));
                $penjualanProduk->detailPenjualanProduk()->save($detail);
            }
        }
        $penjualanProduk->update(['total' => $total]);
        return \Response::json($penjualanProduk);
    }
}
