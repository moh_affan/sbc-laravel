<?php

namespace App\Http\Controllers;

use App\Pengguna;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        return view('setting');
    }

    public function update(Request $request)
    {
        $password = $request->get('password', '');
        $pengguna = Pengguna::find(\Session::get('id', ''));
        if ($pengguna)
            $pengguna->update(['password' => $password]);
        return \Redirect::to('setting')->with('message', 'Berhasil diubah');
    }
}
