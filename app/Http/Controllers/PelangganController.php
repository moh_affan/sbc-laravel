<?php

namespace App\Http\Controllers;

use App\Pelanggan;
use Illuminate\Http\Request;

class PelangganController extends Controller
{
    public function index()
    {
        $pelanggan = Pelanggan::all()->sortBy('id');
        $title = 'Pelanggan';
        return view('l0.pelanggan', compact('pelanggan', 'title'));
    }

    public function show()
    {
        $pelanggan = Pelanggan::all()->sortBy('id');
        return \Response::json($pelanggan);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|unique:pelanggan,id',
            'nama' => 'required|string',
            'telp' => 'required|digits_between:10,15',
            'alamat' => 'required|string',
            'tgl_lahir' => 'required|date',
        ], [
            'id.required' => 'ID harus diisi',
            'id.unique' => 'ID sudah ada',
            'nama.required' => 'Nama harus diisi',
            'tgl_lahir.required' => 'Tanggal harus diisi',
            'telp.required' => 'No. Telp/HP harus diisi',
            'alamat.required' => 'Alamat harus diisi',
            'tgl_lahir.date' => 'Format tanggal salah',
            'telp.digits_between' => 'No. Telp/HP tidak valid',
        ]);
        $result = Pelanggan::create($request->all());
        return \Response::json($result);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:pelanggan,id',
            'nama' => 'required|string',
            'telp' => 'required|digits_between:10,15',
            'alamat' => 'required|string',
            'tgl_lahir' => 'required|date',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
            'nama.required' => 'Nama harus diisi',
            'tgl_lahir.required' => 'Tanggal harus diisi',
            'telp.required' => 'No. Telp/HP harus diisi',
            'alamat.required' => 'Alamat harus diisi',
            'tgl_lahir.date' => 'Format tanggal salah',
            'telp.digits_between' => 'No. Telp/HP tidak valid',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $pelanggan = Pelanggan::findOrFail($id);
        if ($pelanggan->update($input)) {
            return \Response::json($pelanggan);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:pelanggan,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $pelanggan = Pelanggan::findOrFail($id);
        if ($pelanggan->delete()) {
            return \Response::json($pelanggan);
        }
        return \Response::json(["msg" => "Gagal menghapus"], 422);
    }
}
