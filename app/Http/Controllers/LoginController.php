<?php

namespace App\Http\Controllers;

use App\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    public function showLogin(Request $request)
    {
        $request->session()->reflash();
        $logged = !empty($request->session()->get('id', ''))
            && !empty($request->session()->get('id_pegawai', ''))
            && !empty($request->session()->get('username', ''));
        if ($logged)
            return Redirect::to($request->session()->get('level', '') . '/dashboard');
        return view('login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'Username' => 'required|string|exists:user,username',
            'Password' => 'required'
        ], [
            '*.required' => ':field harus diisi',
            'Username.exists' => 'Username tidak ditemukan'
        ]);
        $input = $request->all();
        $pengguna = Pengguna::whereUsername($input['Username'])->first();
        if ($pengguna && $pengguna->password == $input['Password']) {
            $request->session()->put('username', $pengguna->username);
            $request->session()->put('id', $pengguna->id);
            $request->session()->put('level', $pengguna->level);
            $request->session()->put('id_pegawai', $pengguna->id_pegawai);
            $request->session()->put('nama', $pengguna->pegawai->nama);
            if (session('redirect'))
                return Redirect::to(session('redirect'));
            return Redirect::to($pengguna->level . '/dashboard');
        }
        return Redirect::to('/');
    }
}
