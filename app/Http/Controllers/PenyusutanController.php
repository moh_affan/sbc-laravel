<?php

namespace App\Http\Controllers;

use App\Penyusutan;
use App\Treatment;
use Illuminate\Http\Request;

class PenyusutanController extends Controller
{
    public function index()
    {
        $penyusutan = Penyusutan::all()->sortBy('id_treatment');
        $title = 'Penyusutan';
        return view('l0.penyusutan', compact('penyusutan', 'title'));
    }

    public function show()
    {
        $penyusutan = Penyusutan::all()->sortBy('id');
        return \Response::json($penyusutan);
    }

    public function doesntHave()
    {
        $penyusutan = Treatment::doesntHave('penyusutan')->get();
        $optionsHtml = '<option value="">--Pilih Treatment--</option>';
        foreach ($penyusutan as $item) {
            $optionsHtml = $optionsHtml . '
            <option value="' . $item->id . '">' . $item->nama . '(' . $item->id . ')</option>
            ';
        }
        return $optionsHtml;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id_treatment' => 'required|unique:penyusutan,id_treatment|exists:treatment,id',
            'penyusutan' => 'required|integer',
        ], [
            'id_treatment.required' => 'Treatment harus diisi',
            'id_treatment.unique' => 'Penyusutan sudah ada',
            'id_treatment.exists' => 'Treatment tidak ditemukan',
            'penyusutan.required' => 'Nama harus diisi',
            'penyusutan.integer' => 'Nominal tidak valid',
        ]);
        $result = Penyusutan::create($request->all());
        $treatment = Treatment::find($result->id_treatment);
        $result['treatment'] = $treatment->nama;
        return \Response::json($result);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id_treatment' => 'required|exists:penyusutan,id_treatment',
            'penyusutan' => 'required|integer',
        ], [
            'id_treatment.required' => 'Treatment harus diisi',
            'id_treatment.exists' => 'Treatment tidak ditemukan',
            'penyusutan.required' => 'Nama harus diisi',
            'penyusutan.integer' => 'Nominal tidak valid',
        ]);
        $input = $request->all();
        $id = $input['id_treatment'];
        $penyusutan = Penyusutan::findOrFail($id);
        if ($penyusutan->update($input)) {
            $treatment = Treatment::find($input["id_treatment"]);
            $penyusutan['treatment'] = $treatment->nama;
            return \Response::json($penyusutan);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id_treatment' => 'required|exists:penyusutan,id_treatment',
        ], [
            'id_treatment.required' => 'ID harus diisi',
            'id_treatment.exists' => 'ID tidak ditemukan',
        ]);
        $input = $request->all();
        $id = $input['id_treatment'];
        $penyusutan = Penyusutan::findOrFail($id);
        if ($penyusutan->delete()) {
            return \Response::json($penyusutan);
        }
        return \Response::json(["msg" => "Gagal menghapus"], 422);
    }
}
