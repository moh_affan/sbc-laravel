<?php

namespace App\Http\Controllers;

use App\Diskon;
use Illuminate\Http\Request;

class DiskonController extends Controller
{
    public function index()
    {
        $diskon = Diskon::all()->sortBy('id');
        $title = 'Diskon';
        return view('l0.diskon', compact('diskon', 'title'));
    }

    public function show()
    {
        $diskon = Diskon::all()->sortBy('id');
        return \Response::json($diskon);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|unique:diskon,id',
            'nama' => 'required|string',
            'tgl_awal' => 'required|date',
            'jumlah_diskon' => 'required|integer',
            'tgl_akhir' => 'required|date',
        ], [
            'id.required' => 'ID harus diisi',
            'id.unique' => 'ID sudah ada',
            'nama.required' => 'Nama harus diisi',
            'tgl_akhir.required' => 'Tanggal harus diisi',
            'tgl_awal.required' => 'Tanggal harus diisi',
            '*.date' => 'Format tanggal salah',
        ]);
        $result = Diskon::create($request->all());
        return \Response::json($result);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:diskon,id',
            'nama' => 'required|string',
            'tgl_awal' => 'required|date',
            'jumlah_diskon' => 'required|integer',
            'tgl_akhir' => 'required|date',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
            'nama.required' => 'Nama harus diisi',
            'tgl_akhir.required' => 'Tanggal harus diisi',
            'tgl_awal.required' => 'Tanggal harus diisi',
            '*.date' => 'Format tanggal salah',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $diskon = Diskon::findOrFail($id);
        if ($diskon->update($input)) {
            return \Response::json($diskon);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:diskon,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $diskon = Diskon::findOrFail($id);
        if ($diskon->delete()) {
            return \Response::json($diskon);
        }
        return \Response::json(["msg" => "Gagal menghapus"], 422);
    }
}
