<?php

namespace App\Http\Controllers;

use App\DetailJatahBonTreatment;
use App\Jatah;
use App\JatahBonTreatment;
use App\Pegawai;
use App\Treatment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JatahBonTreatmentController extends Controller
{
    public function index()
    {
        $pegawai = Pegawai::all()->sortBy('id');
        $transaksiSaya = JatahBonTreatment::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get();
        $title = 'Jatah Bon Treatment';
        return view('l1.jatahbontreatment', compact('pegawai', 'title', 'transaksiSaya'));
    }

    public function transasksiSaya()
    {
        $transaksiSaya = JatahBonTreatment::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get()->sortByDesc('id');
        $response = "";
        foreach ($transaksiSaya as $transaksi) {
            $response .= '<tr>
                                        <td>' . $transaksi->id . '</td>
                                        <td>' . $transaksi->tgl . '</td>
                                        <td>' . $transaksi->peg->nama . '(' . $transaksi->id_peg . ')</td>
                                        <td>' . $transaksi->keterangan . '</td>
                                        <td>
                                            <button class="btn btn-sm btn-default detail" value=\'{"id":"' . $transaksi->id . '"}\'>Detail</button>
                                        </td>
                                    </tr>';
        }
        return $response;
    }

    public function loadTransaction($id)
    {
        $transaksi = JatahBonTreatment::find($id);
        $response = "";
        $response .= '<button style="position:absolute;right:10px;top:0;" class="btn btn-flat btn-primary" id="print-this" value="' . $transaksi->id . '"><span class="fa fa-print"></span></button>';
        $response .= '<h5>ID Transaksi : ' . $transaksi->id . '</h5>';
        $response .= '<h5>Tanggal : ' . $transaksi->tgl . '</h5>';
        $response .= '<h5>Pegawai : ' . $transaksi->id_peg . ' ' . $transaksi->peg->nama . '</h5>';
        $response .= '<table class="table table-bordered table-responsive">';
        $response .= '<thead>';
        $response .= '<tr>';
        $response .= '<th>ID</th><th>Treatment</th><th>Jumlah</th><th>Harga</th><th class="col-subtotal">Subtotal</th>';
        $response .= '</tr>';
        $response .= '</thead>';
        $response .= '<tbody>';
        $total = 0;
        foreach ($transaksi->detailJatahBonTreatment as $detailJatahBonTreatment) {
            $response .= '<tr>';
            $response .= "<td>$detailJatahBonTreatment->id_treatment</td><td>" . $detailJatahBonTreatment->treatment->nama . "</td><td>$detailJatahBonTreatment->qty</td><td>" . $detailJatahBonTreatment->harga_aktual . "</td><td>" . ($detailJatahBonTreatment->qty * $detailJatahBonTreatment->harga_aktual) . "</td>";
            $response .= '</tr>';
            $total += ($detailJatahBonTreatment->qty * $detailJatahBonTreatment->harga_aktual);
        }
        $response .= '</tbody>';
        $response .= '<tfoot>';
        $response .= '<tr>';
        $response .= '<th colspan="5">Total = ' . $transaksi->total . ' </th > ';
        $response .= '</tr > ';
        $response .= '</tfoot > ';
        $response .= '</table > ';
        return $response;
    }

    public function printTransaction($id)
    {
        $transaksi = JatahBonTreatment::find($id);
        return view('l1.printjatahbontreatment', compact('transaksi'));
    }

    public function lastId()
    {
        $jatahBonTreatment = JatahBonTreatment::latest('id')->first();
        if (isset($jatahBonTreatment['id']))
            return \Response::json($jatahBonTreatment);
        else
            return \Response::json(['id' => 0]);
    }

    public function getJatahUsed($id)
    {
        $jatahBon = JatahBonTreatment::whereIdPeg($id)->whereMonth('tgl', '=', Carbon::now()->month)->whereYear('tgl', '=', Carbon::now()->year)->get();
        $jatahTreatment = Jatah::whereKeterangan('treatment')->first(['nominal'])->nominal;
        $jatahUsed = 0;
        foreach ($jatahBon as $item) {
            $jatahUsed += $item->total;
        }
        if ($jatahUsed > $jatahTreatment)
            $jatahUsed = $jatahTreatment;
        $jatahLeft = $jatahTreatment - $jatahUsed;
        return \Response::json(['used' => $jatahUsed, 'left' => $jatahLeft]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $jatahBonTreatment = new JatahBonTreatment();
        $jatahBonTreatment->id_pegawai = $input['id_pegawai'];
        $jatahBonTreatment->tgl = $input['tgl'];
        $jatahBonTreatment->id_peg = $input['id_peg'];
        $jatahBonTreatment->batas_bon = Carbon::parse($input['tgl'])->addMonth(2)->toDateString();
        $jatahBonTreatment->keterangan = ' - ';
        $jatahBonTreatment->total = 0;
        $total = 0;
        if ($jatahBonTreatment->save()) {
            for ($i = 0; $i < count($input['id_treatment']); $i++) {
                $treatment = Treatment::find($input['id_treatment'][$i]);
                $detail = new DetailJatahBonTreatment();
                $detail->id_treatment = $treatment->id;
                $detail->harga_aktual = $treatment->ref_harga_jual;
                $detail->diskon_aktual = 0;
                $detail->qty = $input['qty'][$i];
                $detail->penyusutan = isset($treatment->penyusutan) ? $treatment->penyusutan->penyusutan : 0;
                $total += ($treatment->ref_harga_jual * intval($input['qty'][$i]));
                $jatahBonTreatment->detailJatahBonTreatment()->save($detail);
            }
        }
        $jatahBon = JatahBonTreatment::whereIdPeg($input['id_peg'])->whereMonth('tgl', '=', Carbon::now()->month)->whereYear('tgl', '=', Carbon::now()->year)->get();
        $jatahTreatment = Jatah::whereKeterangan('treatment')->first(['nominal'])->nominal;
        $jatahUsed = 0;
        foreach ($jatahBon as $item) {
            $jatahUsed += $item->total;
        }
        if ($jatahUsed > $jatahTreatment)
            $jatahUsed = $jatahTreatment;
        $jatahLeft = $jatahTreatment - $jatahUsed;
        $jatah = $bon = 0;
        $jatah = ($jatahLeft - $total) >= 0 ? $total : $jatahLeft;
        $bon = ($jatahLeft - $total) >= 0 ? 0 : ($total - $jatahLeft);
        $jatahBonTreatment->update(['total' => $total, 'jatah' => $jatah, 'bon' => $bon]);
        return \Response::json($jatahBonTreatment);
    }
}
