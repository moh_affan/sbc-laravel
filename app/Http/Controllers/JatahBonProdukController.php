<?php

namespace App\Http\Controllers;

use App\DetailJatahBonProduk;
use App\Jatah;
use App\JatahBonProduk;
use App\Pegawai;
use App\Produk;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JatahBonProdukController extends Controller
{
    public function index()
    {
        $pegawai = Pegawai::all()->sortBy('id');
        $transaksiSaya = JatahBonProduk::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get();
        $title = 'Jatah Bon Produk';
        return view('l1.jatahbonproduk', compact('pegawai', 'title', 'transaksiSaya'));
    }

    public function transasksiSaya()
    {
        $transaksiSaya = JatahBonProduk::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get()->sortByDesc('id');
        $response = "";
        foreach ($transaksiSaya as $transaksi) {
            $response .= '<tr>
                                        <td>' . $transaksi->id . '</td>
                                        <td>' . $transaksi->tgl . '</td>
                                        <td>' . $transaksi->peg->nama . '(' . $transaksi->id_peg . ')</td>
                                        <td>' . $transaksi->keterangan . '</td>
                                        <td>
                                            <button class="btn btn-sm btn-default detail" value=\'{"id":"' . $transaksi->id . '"}\'>Detail</button>
                                        </td>
                                    </tr>';
        }
        return $response;
    }

    public function loadTransaction($id)
    {
        $transaksi = JatahBonProduk::find($id);
        $response = "";
        $response .= '<button style="position:absolute;right:10px;top:0;" class="btn btn-flat btn-primary" id="print-this" value="' . $transaksi->id . '"><span class="fa fa-print"></span></button>';
        $response .= '<h5>ID Transaksi : ' . $transaksi->id . '</h5>';
        $response .= '<h5>Tanggal : ' . $transaksi->tgl . '</h5>';
        $response .= '<h5>Pegawai : ' . $transaksi->id_peg . ' ' . $transaksi->peg->nama . '</h5>';
        $response .= '<table class="table table-bordered table-responsive">';
        $response .= '<thead>';
        $response .= '<tr>';
        $response .= '<th>ID</th><th>Produk</th><th>Jumlah</th><th>Harga</th><th class="col-subtotal">Subtotal</th>';
        $response .= '</tr>';
        $response .= '</thead>';
        $response .= '<tbody>';
        $total = 0;
        foreach ($transaksi->detailJatahBonProduk as $detailJatahBonProduk) {
            $response .= '<tr>';
            $response .= "<td>$detailJatahBonProduk->id_produk</td><td>" . $detailJatahBonProduk->produk->nama . "</td><td>$detailJatahBonProduk->qty</td><td>" . $detailJatahBonProduk->harga_aktual . "</td><td>" . ($detailJatahBonProduk->qty * $detailJatahBonProduk->harga_aktual) . "</td>";
            $response .= '</tr>';
            $total += ($detailJatahBonProduk->qty * $detailJatahBonProduk->harga_aktual);
        }
        $response .= '</tbody>';
        $response .= '<tfoot>';
        $response .= '<tr>';
        $response .= '<th colspan="5">Total = ' . $transaksi->total . ' </th > ';
        $response .= '</tr > ';
        $response .= '</tfoot > ';
        $response .= '</table > ';
        return $response;
    }

    public function printTransaction($id)
    {
        $transaksi = JatahBonProduk::find($id);
        return view('l1.printjatahbonproduk', compact('transaksi'));
    }

    public function lastId()
    {
        $jatahBonProduk = JatahBonProduk::latest('id')->first();
        if (isset($jatahBonProduk['id']))
            return \Response::json($jatahBonProduk);
        else
            return \Response::json(['id' => 0]);
    }

    public function getJatahUsed($id)
    {
        $jatahBon = JatahBonProduk::whereIdPeg($id)->whereMonth('tgl', '=', Carbon::now()->month)->whereYear('tgl', '=', Carbon::now()->year)->get();
        $jatahProduk = Jatah::whereKeterangan('produk')->first(['nominal'])->nominal;
        $jatahUsed = 0;
        foreach ($jatahBon as $item) {
            $jatahUsed += $item->total;
        }
        if ($jatahUsed > $jatahProduk)
            $jatahUsed = $jatahProduk;
        $jatahLeft = $jatahProduk - $jatahUsed;
        return \Response::json(['used' => $jatahUsed, 'left' => $jatahLeft]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $jatahBonProduk = new JatahBonProduk();
        $jatahBonProduk->id_pegawai = $input['id_pegawai'];
        $jatahBonProduk->tgl = $input['tgl'];
        $jatahBonProduk->id_peg = $input['id_peg'];
        $jatahBonProduk->batas_bon = Carbon::parse($input['tgl'])->addMonth(2)->toDateString();
        $jatahBonProduk->keterangan = ' - ';
        $jatahBonProduk->total = 0;
        $total = 0;
        if ($jatahBonProduk->save()) {
            for ($i = 0; $i < count($input['id_produk']); $i++) {
                $produk = Produk::find($input['id_produk'][$i]);
                $detail = new DetailJatahBonProduk();
                $detail->id_produk = $produk->id;
                $detail->harga_aktual = $produk->ref_harga_jual;
                $detail->diskon_aktual = 0;
                $detail->qty = $input['qty'][$i];
                $total += ($produk->ref_harga_jual * intval($input['qty'][$i]));
                $jatahBonProduk->detailJatahBonProduk()->save($detail);
            }
        }
        $jatahBon = JatahBonProduk::whereIdPeg($input['id_peg'])->whereMonth('tgl', '=', Carbon::now()->month)->whereYear('tgl', '=', Carbon::now()->year)->get();
        $jatahProduk = Jatah::whereKeterangan('produk')->first(['nominal'])->nominal;
        $jatahUsed = 0;
        foreach ($jatahBon as $item) {
            $jatahUsed += $item->total;
        }
        if ($jatahUsed > $jatahProduk)
            $jatahUsed = $jatahProduk;
        $jatahLeft = $jatahProduk - $jatahUsed;
        $jatah = $bon = 0;
        $jatah = ($jatahLeft - $total) >= 0 ? $total : $jatahLeft;
        $bon = ($jatahLeft - $total) >= 0 ? 0 : ($total - $jatahLeft);
        $jatahBonProduk->update(['total' => $total, 'jatah' => $jatah, 'bon' => $bon]);
        return \Response::json($jatahBonProduk);
    }
}
