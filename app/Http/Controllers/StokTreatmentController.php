<?php

namespace App\Http\Controllers;

use App\Treatment;
use Illuminate\Http\Request;

class StokTreatmentController extends Controller
{
    public function index(){
        $treatment = Treatment::all();
        $title = "Stok Treatment";
        return view('l0.stoktreatment', compact('treatment','title'));
    }
}
