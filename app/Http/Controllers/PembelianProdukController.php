<?php

namespace App\Http\Controllers;

use App\DetailPembelianProduk;
use App\PembelianProduk;
use App\Produk;
use App\Supplier;
use Illuminate\Http\Request;

class PembelianProdukController extends Controller
{
    public function index()
    {
        $supplier = Supplier::all()->sortBy('id');
        $transaksiSaya = PembelianProduk::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get();
        $title = 'Pembelian Produk';
        return view('l1.pembelianproduk', compact('supplier', 'title', 'transaksiSaya'));
    }

    public function transasksiSaya()
    {
        $transaksiSaya = PembelianProduk::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get()->sortByDesc('id');
        $response = "";
        foreach ($transaksiSaya as $transaksi) {
            $response .= '<tr>
                                        <td>' . $transaksi->id . '</td>
                                        <td>' . $transaksi->tgl . '</td>
                                        <td>' . $transaksi->no_nota . '</td>
                                        <td>' . $transaksi->supplier->supplier . '(' . $transaksi->id_supplier . ')</td>
                                        <td>' . $transaksi->keterangan . '</td>
                                        <td>
                                            <button class="btn btn-sm btn-default detail" value=\'{"id":"' . $transaksi->id . '"}\'>Detail</button>
                                        </td>
                                    </tr>';
        }
        return $response;
    }

    public function loadTransaction($id)
    {
        $transaksi = PembelianProduk::find($id);
        $response = "";
        $response .= '<h5>ID Transaksi : ' . $transaksi->id . '</h5>';
        $response .= '<h5>Tanggal : ' . $transaksi->tgl . '</h5>';
        $response .= '<h5>No Nota Pembelian : ' . $transaksi->no_nota . '</h5>';
        $response .= '<h5>Supplier : ' . $transaksi->id_supplier . ' ' . $transaksi->supplier->supplier . '</h5>';
        $response .= '<table class="table table-bordered table-responsive">';
        $response .= '<thead>';
        $response .= '<tr>';
        $response .= '<th>ID</th><th>Produk</th><th>Jumlah</th>';
        $response .= '</tr>';
        $response .= '</thead>';
        $response .= '<tbody>';
        foreach ($transaksi->detailPembelianProduk as $detailPembelianProduk) {
            $response .= '<tr>';
            $response .= "<td>$detailPembelianProduk->id_produk</td><td>".$detailPembelianProduk->produk->nama."</td><td>$detailPembelianProduk->qty</td>";
            $response .= '</tr>';
        }
        $response .= '</tbody>';
        $response .= '</table>';
        return $response;
    }

    public function lastId()
    {
        $pembelianProduk = PembelianProduk::latest('id')->first();
        if (isset($pembelianProduk['id']))
            return \Response::json($pembelianProduk);
        else
            return \Response::json(['id' => 0]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $pembelianProduk = new PembelianProduk();
        $pembelianProduk->id_supplier = $input['id_supplier'];
        $pembelianProduk->tgl = $input['tgl'];
        $pembelianProduk->no_nota = isset($input['no_nota']) ? $input['no_nota'] : '';
        $pembelianProduk->id_pegawai = $input['id_pegawai'];
        $pembelianProduk->keterangan = '-';
        $pembelianProduk->total = 0;
        $total = 0;
        if ($pembelianProduk->save()) {
            for ($i = 0; $i < count($input['id_produk']); $i++) {
                $produk = Produk::find($input['id_produk'][$i]);
                $detail = new DetailPembelianProduk();
                $detail->id_produk = $produk->id;
                $detail->harga_aktual = $produk->ref_harga_beli;
                $detail->diskon_aktual = 0;
                $detail->qty = $input['qty'][$i];
                $total += ($produk->ref_harga_beli * intval($input['qty'][$i]));
                $pembelianProduk->detailPembelianProduk()->save($detail);
            }
        }
        $pembelianProduk->update(['total' => $total]);
        return \Response::json($pembelianProduk);
    }
}
