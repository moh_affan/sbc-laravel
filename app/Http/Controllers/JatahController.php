<?php

namespace App\Http\Controllers;

use App\Jatah;
use Illuminate\Http\Request;

class JatahController extends Controller
{
    public function index()
    {
        $jatah = Jatah::all()->sortBy('kd');
        $title = 'Jatah';
        return view('l0.jatah', compact('jatah', 'title'));
    }

    public function show()
    {
        $jatah = Jatah::all()->sortBy('kd');
        return \Response::json($jatah);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:jatah,id',
            'nominal' => 'required|integer',
            'keterangan' => 'required|string',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
            '*.required' => ':attribute harus diisi',
            'nominal.integer' => 'Nominal salah',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $jatah = Jatah::findOrFail($id);
        if ($jatah->update($input)) {
            return \Response::json($jatah);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }
}
