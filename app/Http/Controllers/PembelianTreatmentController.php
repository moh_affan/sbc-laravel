<?php

namespace App\Http\Controllers;

use App\DetailPembelianTreatment;
use App\PembelianTreatment;
use App\Supplier;
use App\Treatment;
use Illuminate\Http\Request;

class PembelianTreatmentController extends Controller
{
    public function index()
    {
        $supplier = Supplier::all()->sortBy('id');
        $transaksiSaya = PembelianTreatment::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get();
        $title = 'Pembelian Treatment';
        return view('l1.pembeliantreatment', compact('supplier', 'title', 'transaksiSaya'));
    }

    public function transasksiSaya()
    {
        $transaksiSaya = PembelianTreatment::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get()->sortByDesc('id');
        $response = "";
        foreach ($transaksiSaya as $transaksi) {
            $response .= '<tr>
                                        <td>' . $transaksi->id . '</td>
                                        <td>' . $transaksi->tgl . '</td>
                                        <td>' . $transaksi->no_nota . '</td>
                                        <td>' . $transaksi->supplier->supplier . '(' . $transaksi->id_supplier . ')</td>
                                        <td>' . $transaksi->keterangan . '</td>
                                        <td>
                                            <button class="btn btn-sm btn-default detail" value=\'{"id":"' . $transaksi->id . '"}\'>Detail</button>
                                        </td>
                                    </tr>';
        }
        return $response;
    }

    public function loadTransaction($id)
    {
        $transaksi = PembelianTreatment::find($id);
        $response = "";
        $response .= '<h5>ID Transaksi : ' . $transaksi->id . '</h5>';
        $response .= '<h5>Tanggal : ' . $transaksi->tgl . '</h5>';
        $response .= '<h5>No Nota Pembelian : ' . $transaksi->no_nota . '</h5>';
        $response .= '<h5>Supplier : ' . $transaksi->id_supplier . ' ' . $transaksi->supplier->supplier . '</h5>';
        $response .= '<table class="table table-bordered table-responsive">';
        $response .= '<thead>';
        $response .= '<tr>';
        $response .= '<th>ID</th><th>Treatment</th><th>Jumlah</th>';
        $response .= '</tr>';
        $response .= '</thead>';
        $response .= '<tbody>';
        foreach ($transaksi->detailPembelianTreatment as $detailPembelianTreatment) {
            $response .= '<tr>';
            $response .= "<td>$detailPembelianTreatment->id_treatment</td><td>" . $detailPembelianTreatment->treatment->nama . "</td><td>$detailPembelianTreatment->qty</td>";
            $response .= '</tr>';
        }
        $response .= '</tbody>';
        $response .= '</table>';
        return $response;
    }

    public function lastId()
    {
        $pembelianTreatment = PembelianTreatment::latest('id')->first();
        if (isset($pembelianTreatment['id']))
            return \Response::json($pembelianTreatment);
        else
            return \Response::json(['id' => 0]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $pembelianTreatment = new PembelianTreatment();
        $pembelianTreatment->id_supplier = $input['id_supplier'];
        $pembelianTreatment->tgl = $input['tgl'];
        $pembelianTreatment->no_nota = isset($input['no_nota']) ? $input['no_nota'] : '';
        $pembelianTreatment->id_pegawai = $input['id_pegawai'];
        $pembelianTreatment->keterangan = '-';
        $pembelianTreatment->total = 0;
        $total = 0;
        if ($pembelianTreatment->save()) {
            for ($i = 0; $i < count($input['id_treatment']); $i++) {
                $treatment = Treatment::find($input['id_treatment'][$i]);
                $detail = new DetailPembelianTreatment();
                $detail->id_treatment = $treatment->id;
                $detail->harga_aktual = $treatment->ref_harga_beli;
                $detail->diskon_aktual = 0;
                $detail->qty = $input['qty'][$i];
                $total += ($treatment->ref_harga_beli * intval($input['qty'][$i]));
                $pembelianTreatment->detailPembelianTreatment()->save($detail);
            }
        }
        $pembelianTreatment->update(['total' => $total]);
        return \Response::json($pembelianTreatment);
    }
}
