<?php

namespace App\Http\Controllers\Reports;

use App\JatahBonProduk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JatahBonProdukController extends Controller
{
    public function index()
    {
        $title = 'Laporan Jatah Bon Produk';
        return view('l2.jatahbonproduk', compact('title'));
    }

    public function hariini()
    {
        $detail = JatahBonProduk::whereDay('tgl', date('d'))->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'))->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->id, 'tgl' => explode(' ', $item->tgl)[0], 'pegawai' => $item->pegawai->nama . ' (' . $item->pegawai->id . ')', 'jatah' => $item->jatah, 'bon' => $item->bon, 'total' => ($item->jatah+$item->bon)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function bulanini()
    {
        $detail = JatahBonProduk::whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'))->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->id, 'tgl' => explode(' ', $item->tgl)[0], 'pegawai' => $item->pegawai->nama . ' (' . $item->pegawai->id . ')', 'jatah' => $item->jatah, 'bon' => $item->bon, 'total' => ($item->jatah+$item->bon)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function range($start, $end)
    {
        $dom = '';
        $detail = JatahBonProduk::whereBetween('tgl', [$start, $end])->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->id, 'tgl' => explode(' ', $item->tgl)[0], 'pegawai' => $item->pegawai->nama . ' (' . $item->pegawai->id . ')', 'jatah' => $item->jatah, 'bon' => $item->bon, 'total' => ($item->jatah+$item->bon)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }
}
