<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PendapatanProdukController extends Controller
{
    public function index()
    {
        $title = 'Laporan Pendapatan Produk';
        return view('l2.pendapatanproduk', compact('title'));
    }

    public function hariini()
    {
        $where = "WHERE YEAR(tgl)=" . date('Y') . " AND MONTH(tgl)=" . date('m') . " AND DAY(tgl)=" . date('d');
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _penjualan_produk INNER JOIN _detail_penjualan_produk ON _penjualan_produk.id = _detail_penjualan_produk.id_transaksi $where GROUP BY _detail_penjualan_produk.id_produk,_detail_penjualan_produk.harga_aktual) penjualan ON produk.id=penjualan.id_produk ORDER BY id ASC");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_produk INNER JOIN _detail_pembelian_produk ON _pembelian_produk.id = _detail_pembelian_produk.id_transaksi $where GROUP BY _detail_pembelian_produk.id_produk, _detail_pembelian_produk.harga_aktual) pembelian ON produk.id=pembelian.id_produk ORDER BY id ASC");
        $produk = \DB::select("SELECT *, 0 as jml_diskon FROM produk ORDER BY id ASC");
        $diskon = \DB::select("SELECT id, IF(ISNULL(diskon),0,diskon) as diskon FROM produk LEFT JOIN (SELECT _detail_penjualan_produk.id_produk as id_produk, max(_detail_penjualan_produk.diskon_aktual) as diskon 
		from (_detail_penjualan_produk inner join (select * from _penjualan_produk $where) pp 
		ON _detail_penjualan_produk.id_transaksi = pp.id) group by _detail_penjualan_produk.id_produk) dis on produk.id = dis.id_produk ORDER BY id ASC");
        $dis = \DB::select(" SELECT id, jml_diskon FROM produk LEFT JOIN (SELECT _detail_penjualan_produk.id_produk as id_produk, sum(IF(_detail_penjualan_produk.diskon_aktual>0,1,0)) as jml_diskon 
		from (_detail_penjualan_produk inner join (select * from _penjualan_produk $where) pp 
		ON _detail_penjualan_produk.id_transaksi = pp.id) group by _detail_penjualan_produk.id_produk) ppp on produk.id
        = ppp.id_produk ORDER BY id ASC");
        $reportArr = [];
        for ($i = 0; $i < count($produk); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $s = $produk[$i];
            $d = $diskon[$i];
            $ds = $dis[$i];
            if ($s->id == $j->id && $s->id == $b->id) {
                $s->no = ($i + 1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $d->diskon; /*$j->diskon;*/
                $s->jml_diskon = $ds->jml_diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }

    public function bulanini()
    {
        $where = "WHERE YEAR(tgl)=" . date('Y') . " AND MONTH(tgl)=" . date('m');
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _penjualan_produk INNER JOIN _detail_penjualan_produk ON _penjualan_produk.id = _detail_penjualan_produk.id_transaksi $where GROUP BY _detail_penjualan_produk.id_produk,_detail_penjualan_produk.harga_aktual) penjualan ON produk.id=penjualan.id_produk ORDER BY id ASC");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_produk INNER JOIN _detail_pembelian_produk ON _pembelian_produk.id = _detail_pembelian_produk.id_transaksi $where GROUP BY _detail_pembelian_produk.id_produk, _detail_pembelian_produk.harga_aktual) pembelian ON produk.id=pembelian.id_produk ORDER BY id ASC");
        $produk = \DB::select("SELECT *, 0 as jml_diskon FROM produk ORDER BY id ASC");
        $diskon = \DB::select("SELECT id, IF(ISNULL(diskon),0,diskon) as diskon FROM produk LEFT JOIN (SELECT _detail_penjualan_produk.id_produk as id_produk, max(_detail_penjualan_produk.diskon_aktual) as diskon 
		from (_detail_penjualan_produk inner join (select * from _penjualan_produk $where) pp 
		ON _detail_penjualan_produk.id_transaksi = pp.id) group by _detail_penjualan_produk.id_produk) dis on produk.id = dis.id_produk ORDER BY id ASC");
        $dis = \DB::select(" SELECT id, jml_diskon FROM produk LEFT JOIN (SELECT _detail_penjualan_produk.id_produk as id_produk, sum(IF(_detail_penjualan_produk.diskon_aktual>0,1,0)) as jml_diskon 
		from (_detail_penjualan_produk inner join (select * from _penjualan_produk $where) pp 
		ON _detail_penjualan_produk.id_transaksi = pp.id) group by _detail_penjualan_produk.id_produk) ppp on produk.id
        = ppp.id_produk ORDER BY id ASC");
        $reportArr = [];
        for ($i = 0; $i < count($produk); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $s = $produk[$i];
            $d = $diskon[$i];
            $ds = $dis[$i];
            if ($s->id == $j->id && $s->id == $b->id) {
                $s->no = ($i + 1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $d->diskon; /*$j->diskon;*/
                $s->jml_diskon = $ds->jml_diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                array_push($reportArr, $s);
            }
        }
		return \Response::json($reportArr);
    }

    public function range($start, $end)
    {
        $where = "WHERE tgl BETWEEN '$start' AND '$end'";
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _penjualan_produk INNER JOIN _detail_penjualan_produk ON _penjualan_produk.id = _detail_penjualan_produk.id_transaksi $where GROUP BY _detail_penjualan_produk.id_produk,_detail_penjualan_produk.harga_aktual) penjualan ON produk.id=penjualan.id_produk ORDER BY id ASC");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_produk INNER JOIN _detail_pembelian_produk ON _pembelian_produk.id = _detail_pembelian_produk.id_transaksi $where GROUP BY _detail_pembelian_produk.id_produk, _detail_pembelian_produk.harga_aktual) pembelian ON produk.id=pembelian.id_produk ORDER BY id ASC");
        $produk = \DB::select("SELECT *, 0 as jml_diskon FROM produk ORDER BY id ASC");
        $diskon = \DB::select("SELECT id, IF(ISNULL(diskon),0,diskon) as diskon FROM produk LEFT JOIN (SELECT _detail_penjualan_produk.id_produk as id_produk, max(_detail_penjualan_produk.diskon_aktual) as diskon 
		from (_detail_penjualan_produk inner join (select * from _penjualan_produk $where) pp 
		ON _detail_penjualan_produk.id_transaksi = pp.id) group by _detail_penjualan_produk.id_produk) dis on produk.id = dis.id_produk ORDER BY id ASC");
        $dis = \DB::select(" SELECT id, jml_diskon FROM produk LEFT JOIN (SELECT _detail_penjualan_produk.id_produk as id_produk, sum(IF(_detail_penjualan_produk.diskon_aktual>0,1,0)) as jml_diskon 
		from (_detail_penjualan_produk inner join (select * from _penjualan_produk $where) pp 
		ON _detail_penjualan_produk.id_transaksi = pp.id) group by _detail_penjualan_produk.id_produk) ppp on produk.id
        = ppp.id_produk ORDER BY id ASC");
        $reportArr = [];
        for ($i = 0; $i < count($produk); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $s = $produk[$i];
            $d = $diskon[$i];
            $ds = $dis[$i];
            if ($s->id == $j->id && $s->id == $b->id) {
                $s->no = ($i + 1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $d->diskon; /*$j->diskon;*/
                $s->jml_diskon = $ds->jml_diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }
}
