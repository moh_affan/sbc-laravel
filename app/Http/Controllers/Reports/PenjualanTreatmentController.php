<?php

namespace App\Http\Controllers\Reports;

use App\DetailPenjualanTreatment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PenjualanTreatmentController extends Controller
{
    public function index()
    {
        $title = 'Laporan Penjualan Treatment';
        return view('l2.penjualantreatment', compact('title'));
    }

    public function hariini()
    {
        $detail = DetailPenjualanTreatment::whereHas('penjualanTreatment', function ($query) {
            $query->whereDay('tgl', date('d'))->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'));
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->penjualanTreatment->id, 'tgl' => explode(' ', $item->penjualanTreatment->tgl)[0], 'treatment' => $item->treatment->nama . ' (' . $item->treatment->id . ')', 'harga' => $item->harga_aktual, 'diskon' => $item->diskon_aktual, 'qty' => $item->qty, 'subtotal' => (($item->harga_aktual * $item->qty) - ($item->diskon_aktual * $item->qty))];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function bulanini()
    {
        $detail = DetailPenjualanTreatment::whereHas('penjualanTreatment', function ($query) {
            $query->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'));
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->penjualanTreatment->id, 'tgl' => explode(' ', $item->penjualanTreatment->tgl)[0], 'treatment' => $item->treatment->nama . ' (' . $item->treatment->id . ')', 'harga' => $item->harga_aktual, 'diskon' => $item->diskon_aktual, 'qty' => $item->qty, 'subtotal' => (($item->harga_aktual * $item->qty) - ($item->diskon_aktual * $item->qty))];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function range($start, $end)
    {
        $dom = '';
        $detail = DetailPenjualanTreatment::whereHas('penjualanTreatment', function ($query) use ($start, $end) {
            $query->whereBetween('tgl', [$start, $end]);
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->penjualanTreatment->id, 'tgl' => explode(' ', $item->penjualanTreatment->tgl)[0], 'treatment' => $item->treatment->nama . ' (' . $item->treatment->id . ')', 'harga' => $item->harga_aktual, 'diskon' => $item->diskon_aktual, 'qty' => $item->qty, 'subtotal' => (($item->harga_aktual * $item->qty) - ($item->diskon_aktual * $item->qty))];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }
}
