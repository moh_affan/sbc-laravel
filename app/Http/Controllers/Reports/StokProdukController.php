<?php

namespace App\Http\Controllers\Reports;

use App\DetailPembelianProduk;
use App\DetailPenjualanProduk;
use App\Produk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class StokProdukController extends Controller
{
    public function index()
    {
        $title = 'Laporan Stok Produk';
        return view('l2.stokproduk', compact('title'));
    }

    public function hariini()
    {
        $where = "WHERE YEAR(tgl)=" . date('Y') . " AND MONTH(tgl)=" . date('m') . " AND DAY(tgl)=" . date('d');
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, IF(ISNULL(harga),0,diskon) as diskon FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga, diskon_aktual as diskon FROM _penjualan_produk INNER JOIN _detail_penjualan_produk ON _penjualan_produk.id = _detail_penjualan_produk.id_transaksi $where GROUP BY _detail_penjualan_produk.id_produk,_detail_penjualan_produk.harga_aktual,_detail_penjualan_produk.diskon_aktual) penjualan ON produk.id=penjualan.id_produk");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_produk INNER JOIN _detail_pembelian_produk ON _pembelian_produk.id = _detail_pembelian_produk.id_transaksi $where GROUP BY _detail_pembelian_produk.id_produk, _detail_pembelian_produk.harga_aktual) pembelian ON produk.id=pembelian.id_produk");
        $jatahBon = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _jatah_bon INNER JOIN _detail_jatah_bon_produk ON _jatah_bon.id = _detail_jatah_bon_produk.id_transaksi $where GROUP BY _detail_jatah_bon_produk.id_produk,_detail_jatah_bon_produk.harga_aktual) jatahbon ON produk.id=jatahbon.id_produk");
        $stok = \DB::select("SELECT * FROM __stok");
        $reportArr = [];
        for ($i = 0; $i < count($stok); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $jb = $jatahBon[$i];
            $s = $stok[$i];
            if ($s->id_produk == $j->id && $s->id_produk == $b->id && $s->id_produk == $jb->id) {
                $s->no = ($i+1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $j->diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->sum_jatahbon = $jb->jml;
                $s->harga_jatahbon = $jb->harga;
                $before = intval($s->stok) + intval($j->jml) + intval($jb->jml) - intval($b->jml);
                $s->stok_priode_sblm = $before < 0 ? 0 : $before;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }

    public function bulanini()
    {
        $where = "WHERE YEAR(tgl)=" . date('Y') . " AND MONTH(tgl)=" . date('m');
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, IF(ISNULL(harga),0,diskon) as diskon FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga, diskon_aktual as diskon FROM _penjualan_produk INNER JOIN _detail_penjualan_produk ON _penjualan_produk.id = _detail_penjualan_produk.id_transaksi $where GROUP BY _detail_penjualan_produk.id_produk,_detail_penjualan_produk.harga_aktual,_detail_penjualan_produk.diskon_aktual) penjualan ON produk.id=penjualan.id_produk");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_produk INNER JOIN _detail_pembelian_produk ON _pembelian_produk.id = _detail_pembelian_produk.id_transaksi $where GROUP BY _detail_pembelian_produk.id_produk, _detail_pembelian_produk.harga_aktual) pembelian ON produk.id=pembelian.id_produk");
        $jatahBon = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM produk LEFT JOIN (SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _jatah_bon INNER JOIN _detail_jatah_bon_produk ON _jatah_bon.id = _detail_jatah_bon_produk.id_transaksi $where GROUP BY _detail_jatah_bon_produk.id_produk,_detail_jatah_bon_produk.harga_aktual) jatahbon ON produk.id=jatahbon.id_produk");
        $stok = \DB::select("SELECT * FROM __stok");
        $reportArr = [];
        for ($i = 0; $i < count($stok); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $jb = $jatahBon[$i];
            $s = $stok[$i];
            if ($s->id_produk == $j->id && $s->id_produk == $b->id && $s->id_produk == $jb->id) {
                $s->no = ($i+1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $j->diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->sum_jatahbon = $jb->jml;
                $s->harga_jatahbon = $jb->harga;
                $before = intval($s->stok) + intval($j->jml) + intval($jb->jml) - intval($b->jml);
                $s->stok_priode_sblm = $before < 0 ? 0 : $before;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }

    public function range($start, $end)
    {
        $where = "WHERE tgl BETWEEN '$start' AND '$end'";
        $penjualan = \DB::select("SELECT id, IF (ISNULL(jml),0,jml) as jml, IF (ISNULL(harga),ref_harga_jual,harga) as harga, IF (ISNULL(harga),0,diskon) as diskon FROM produk LEFT JOIN(SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga, diskon_aktual as diskon FROM _penjualan_produk INNER JOIN _detail_penjualan_produk ON _penjualan_produk . id = _detail_penjualan_produk . id_transaksi $where GROUP BY _detail_penjualan_produk . id_produk,_detail_penjualan_produk . harga_aktual,_detail_penjualan_produk . diskon_aktual) penjualan ON produk . id = penjualan . id_produk");
        $pembelian = \DB::select("SELECT id, IF (ISNULL(jml),0,jml) as jml, IF (ISNULL(harga),ref_harga_beli,harga) as harga FROM produk LEFT JOIN(SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_produk INNER JOIN _detail_pembelian_produk ON _pembelian_produk . id = _detail_pembelian_produk . id_transaksi $where GROUP BY _detail_pembelian_produk . id_produk, _detail_pembelian_produk . harga_aktual) pembelian ON produk . id = pembelian . id_produk");
        $jatahBon = \DB::select("SELECT id, IF (ISNULL(jml),0,jml) as jml, IF (ISNULL(harga),ref_harga_jual,harga) as harga FROM produk LEFT JOIN(SELECT id_produk, SUM(qty) AS jml, harga_aktual AS harga FROM _jatah_bon INNER JOIN _detail_jatah_bon_produk ON _jatah_bon . id = _detail_jatah_bon_produk . id_transaksi $where GROUP BY _detail_jatah_bon_produk . id_produk,_detail_jatah_bon_produk . harga_aktual) jatahbon ON produk . id = jatahbon . id_produk");
        $stok = \DB::select("SELECT * FROM __stok");
        $reportArr = [];
        for ($i = 0; $i < count($stok); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $jb = $jatahBon[$i];
            $s = $stok[$i];
            if ($s->id_produk == $j->id && $s->id_produk == $b->id && $s->id_produk == $jb->id) {
                $s->no = ($i+1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $j->diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->sum_jatahbon = $jb->jml;
                $s->harga_jatahbon = $jb->harga;
                $before = intval($s->stok) + intval($j->jml) + intval($jb->jml) - intval($b->jml);
                $s->stok_priode_sblm = $before < 0 ? 0 : $before;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }
}
