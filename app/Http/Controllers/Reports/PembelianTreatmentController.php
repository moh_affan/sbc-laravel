<?php

namespace App\Http\Controllers\Reports;

use App\DetailPembelianTreatment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PembelianTreatmentController extends Controller
{
    public function index()
    {
        $title = 'Laporan Pembelian Treatment';
        return view('l2.pembeliantreatment', compact('title'));
    }

    public function hariini()
    {
        $detail = DetailPembelianTreatment::whereHas('pembelianTreatment', function ($query) {
            $query->whereDay('tgl', date('d'))->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'));
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->pembelianTreatment->id, 'tgl' => explode(' ', $item->pembelianTreatment->tgl)[0], 'treatment' => $item->treatment->nama . ' (' . $item->treatment->id . ')', 'harga' => $item->harga_aktual, 'qty' => $item->qty, 'subtotal' => ($item->harga_aktual * $item->qty)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function bulanini()
    {
        $detail = DetailPembelianTreatment::whereHas('pembelianTreatment', function ($query) {
            $query->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'));
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->pembelianTreatment->id, 'tgl' => explode(' ', $item->pembelianTreatment->tgl)[0], 'treatment' => $item->treatment->nama . ' (' . $item->treatment->id . ')', 'harga' => $item->harga_aktual, 'qty' => $item->qty, 'subtotal' => ($item->harga_aktual * $item->qty)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function range($start, $end)
    {
        $dom = '';
        $detail = DetailPembelianTreatment::whereHas('pembelianTreatment', function ($query) use ($start, $end) {
            $query->whereBetween('tgl', [$start, $end]);
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->pembelianTreatment->id, 'tgl' => explode(' ', $item->pembelianTreatment->tgl)[0], 'treatment' => $item->treatment->nama . ' (' . $item->treatment->id . ')', 'harga' => $item->harga_aktual, 'qty' => $item->qty, 'subtotal' => ($item->harga_aktual * $item->qty)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }
}
