<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StokTreatmentController extends Controller
{
    public function index()
    {
        $title = 'Laporan Stok Produk';
        return view('l2.stoktreatment', compact('title'));
    }

    public function tes()
    {
        $start="2017-05-01";
        $end="2017-06-31";
        $where = "WHERE tgl BETWEEN '$start' AND '$end'";
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, IF(ISNULL(harga),0,diskon) as diskon FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, diskon_aktual as diskon FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual,_detail_penjualan_treatment.diskon_aktual) penjualan ON treatment.id=penjualan.id_treatment");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_treatment INNER JOIN _detail_pembelian_treatment ON _pembelian_treatment.id = _detail_pembelian_treatment.id_transaksi $where GROUP BY _detail_pembelian_treatment.id_treatment, _detail_pembelian_treatment.harga_aktual) pembelian ON treatment.id=pembelian.id_treatment");
        $jatahBon = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _jatah_bon INNER JOIN _detail_jatah_bon_treatment ON _jatah_bon.id = _detail_jatah_bon_treatment.id_transaksi $where GROUP BY _detail_jatah_bon_treatment.id_treatment,_detail_jatah_bon_treatment.harga_aktual) jatahbon ON treatment.id=jatahbon.id_treatment");
        $stok = \DB::select("SELECT * FROM __stok_tr");
        $reportArr = [];
        for ($i = 0; $i < count($stok); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $jb = $jatahBon[$i];
            $s = $stok[$i];
            if ($s->id_treatment == $j->id && $s->id_treatment == $b->id && $s->id_treatment == $jb->id) {
                $s->no = ($i+1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $j->diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->sum_jatahbon = $jb->jml;
                $s->harga_jatahbon = $jb->harga;
                $before = intval($s->stok) + intval($j->jml) + intval($jb->jml) - intval($b->jml);
                $s->stok_priode_sblm = $before < 0 ? 0 : $before;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($pembelian);
    }

    public function hariini()
    {
        $where = "WHERE YEAR(tgl)=" . date('Y') . " AND MONTH(tgl)=" . date('m') . " AND DAY(tgl)=" . date('d');
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, IF(ISNULL(harga),0,diskon) as diskon FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, diskon_aktual as diskon FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual,_detail_penjualan_treatment.diskon_aktual) penjualan ON treatment.id=penjualan.id_treatment");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_treatment INNER JOIN _detail_pembelian_treatment ON _pembelian_treatment.id = _detail_pembelian_treatment.id_transaksi $where GROUP BY _detail_pembelian_treatment.id_treatment, _detail_pembelian_treatment.harga_aktual) pembelian ON treatment.id=pembelian.id_treatment");
        $jatahBon = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _jatah_bon INNER JOIN _detail_jatah_bon_treatment ON _jatah_bon.id = _detail_jatah_bon_treatment.id_transaksi $where GROUP BY _detail_jatah_bon_treatment.id_treatment,_detail_jatah_bon_treatment.harga_aktual) jatahbon ON treatment.id=jatahbon.id_treatment");
        $stok = \DB::select("SELECT * FROM __stok_tr");
        $reportArr = [];
        for ($i = 0; $i < count($stok); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $jb = $jatahBon[$i];
            $s = $stok[$i];
            if ($s->id_treatment == $j->id && $s->id_treatment == $b->id && $s->id_treatment == $jb->id) {
                $s->no = ($i+1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $j->diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->sum_jatahbon = $jb->jml;
                $s->harga_jatahbon = $jb->harga;
                $before = intval($s->stok) + intval($j->jml) + intval($jb->jml) - intval($b->jml);
                $s->stok_priode_sblm = $before < 0 ? 0 : $before;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }

    public function bulanini()
    {
        $where = "WHERE YEAR(tgl)=" . date('Y') . " AND MONTH(tgl)=" . date('m');
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, IF(ISNULL(harga),0,diskon) as diskon FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, diskon_aktual as diskon FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual,_detail_penjualan_treatment.diskon_aktual) penjualan ON treatment.id=penjualan.id_treatment");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_treatment INNER JOIN _detail_pembelian_treatment ON _pembelian_treatment.id = _detail_pembelian_treatment.id_transaksi $where GROUP BY _detail_pembelian_treatment.id_treatment, _detail_pembelian_treatment.harga_aktual) pembelian ON treatment.id=pembelian.id_treatment");
        $jatahBon = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _jatah_bon INNER JOIN _detail_jatah_bon_treatment ON _jatah_bon.id = _detail_jatah_bon_treatment.id_transaksi $where GROUP BY _detail_jatah_bon_treatment.id_treatment,_detail_jatah_bon_treatment.harga_aktual) jatahbon ON treatment.id=jatahbon.id_treatment");
        $stok = \DB::select("SELECT * FROM __stok_tr");
        $reportArr = [];
        for ($i = 0; $i < count($stok); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $jb = $jatahBon[$i];
            $s = $stok[$i];
            if ($s->id_treatment == $j->id && $s->id_treatment == $b->id && $s->id_treatment == $jb->id) {
                $s->no = ($i+1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $j->diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->sum_jatahbon = $jb->jml;
                $s->harga_jatahbon = $jb->harga;
                $before = intval($s->stok) + intval($j->jml) + intval($jb->jml) - intval($b->jml);
                $s->stok_priode_sblm = $before < 0 ? 0 : $before;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }

    public function range($start, $end)
    {
        $where = "WHERE tgl BETWEEN '$start' AND '$end'";
        $penjualan = \DB::select("SELECT id, IF (ISNULL(jml),0,jml) as jml, IF (ISNULL(harga),ref_harga_jual,harga) as harga, IF (ISNULL(harga),0,diskon) as diskon FROM treatment LEFT JOIN(SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, diskon_aktual as diskon FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment . id = _detail_penjualan_treatment . id_transaksi $where GROUP BY _detail_penjualan_treatment . id_treatment,_detail_penjualan_treatment . harga_aktual,_detail_penjualan_treatment . diskon_aktual) penjualan ON treatment . id = penjualan . id_treatment");
        $pembelian = \DB::select("SELECT id, IF (ISNULL(jml),0,jml) as jml, IF (ISNULL(harga),ref_harga_beli,harga) as harga FROM treatment LEFT JOIN(SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_treatment INNER JOIN _detail_pembelian_treatment ON _pembelian_treatment . id = _detail_pembelian_treatment . id_transaksi $where GROUP BY _detail_pembelian_treatment . id_treatment, _detail_pembelian_treatment . harga_aktual) pembelian ON treatment . id = pembelian . id_treatment");
        $jatahBon = \DB::select("SELECT id, IF (ISNULL(jml),0,jml) as jml, IF (ISNULL(harga),ref_harga_jual,harga) as harga FROM treatment LEFT JOIN(SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _jatah_bon INNER JOIN _detail_jatah_bon_treatment ON _jatah_bon . id = _detail_jatah_bon_treatment . id_transaksi $where GROUP BY _detail_jatah_bon_treatment . id_treatment,_detail_jatah_bon_treatment . harga_aktual) jatahbon ON treatment . id = jatahbon . id_treatment");
        $stok = \DB::select("SELECT * FROM __stok_tr");
        $reportArr = [];
        for ($i = 0; $i < count($stok); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $jb = $jatahBon[$i];
            $s = $stok[$i];
            if ($s->id_treatment == $j->id && $s->id_treatment == $b->id && $s->id_treatment == $jb->id) {
                $s->no = ($i+1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $j->diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->sum_jatahbon = $jb->jml;
                $s->harga_jatahbon = $jb->harga;
                $before = intval($s->stok) + intval($j->jml) + intval($jb->jml) - intval($b->jml);
                $s->stok_priode_sblm = $before < 0 ? 0 : $before;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }
}
