<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PendapatanTreatmentController extends Controller
{
    public function index()
    {
        $title = 'Laporan Pendapatan Treatment';
        return view('l2.pendapatantreatment', compact('title'));
    }

    public function hariini()
    {
        $where = "WHERE YEAR(tgl)=" . date('Y') . " AND MONTH(tgl)=" . date('m') . " AND DAY(tgl)=" . date('d');
//        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual) penjualan ON treatment.id=penjualan.id_treatment ORDER BY id ASC");
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, penyusutan FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, penyusutan FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual, penyusutan) penjualan ON treatment.id=penjualan.id_treatment ORDER BY id ASC");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_treatment INNER JOIN _detail_pembelian_treatment ON _pembelian_treatment.id = _detail_pembelian_treatment.id_transaksi $where GROUP BY _detail_pembelian_treatment.id_treatment, _detail_pembelian_treatment.harga_aktual) pembelian ON treatment.id=pembelian.id_treatment ORDER BY id ASC");
        $treatment = \DB::select("SELECT *, 0 as jml_diskon FROM treatment ORDER BY id ASC");
        $diskon = \DB::select("SELECT id, IF(ISNULL(diskon),0,diskon) as diskon FROM treatment LEFT JOIN (SELECT _detail_penjualan_treatment.id_treatment as id_treatment, max(_detail_penjualan_treatment.diskon_aktual) as diskon 
		from (_detail_penjualan_treatment inner join (select * from _penjualan_treatment $where) pp 
		ON _detail_penjualan_treatment.id_transaksi = pp.id) group by _detail_penjualan_treatment.id_treatment) dis on treatment.id = dis.id_treatment ORDER BY id ASC");
        $dis = \DB::select(" SELECT id, jml_diskon FROM treatment LEFT JOIN (SELECT _detail_penjualan_treatment.id_treatment as id_treatment, sum(IF(_detail_penjualan_treatment.diskon_aktual>0,1,0)) as jml_diskon 
		from (_detail_penjualan_treatment inner join (select * from _penjualan_treatment $where) pp 
		ON _detail_penjualan_treatment.id_transaksi = pp.id) group by _detail_penjualan_treatment.id_treatment) ppp on treatment.id
        = ppp.id_treatment ORDER BY id ASC");
        $reportArr = [];
        for ($i = 0; $i < count($treatment); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $s = $treatment[$i];
            $d = $diskon[$i];
            $ds = $dis[$i];
            if ($s->id == $j->id && $s->id == $b->id) {
                $s->no = ($i + 1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $d->diskon; /*$j->diskon;*/
                $s->jml_diskon = $ds->jml_diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->penyusutan = $j->penyusutan;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }

    public function bulanini()
    {
        $where = "WHERE YEAR(tgl)=" . date('Y') . " AND MONTH(tgl)=" . date('m');
        //$penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual) penjualan ON treatment.id=penjualan.id_treatment ORDER BY id ASC");
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, penyusutan FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, penyusutan FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual, penyusutan) penjualan ON treatment.id=penjualan.id_treatment ORDER BY id ASC");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_treatment INNER JOIN _detail_pembelian_treatment ON _pembelian_treatment.id = _detail_pembelian_treatment.id_transaksi $where GROUP BY _detail_pembelian_treatment.id_treatment, _detail_pembelian_treatment.harga_aktual) pembelian ON treatment.id=pembelian.id_treatment ORDER BY id ASC");
        $treatment = \DB::select("SELECT *, 0 as jml_diskon FROM treatment ORDER BY id ASC");
        $diskon = \DB::select("SELECT id, IF(ISNULL(diskon),0,diskon) as diskon FROM treatment LEFT JOIN (SELECT _detail_penjualan_treatment.id_treatment as id_treatment, max(_detail_penjualan_treatment.diskon_aktual) as diskon 
		from (_detail_penjualan_treatment inner join (select * from _penjualan_treatment $where) pp 
		ON _detail_penjualan_treatment.id_transaksi = pp.id) group by _detail_penjualan_treatment.id_treatment) dis on treatment.id = dis.id_treatment ORDER BY id ASC");
        $dis = \DB::select(" SELECT id, jml_diskon FROM treatment LEFT JOIN (SELECT _detail_penjualan_treatment.id_treatment as id_treatment, sum(IF(_detail_penjualan_treatment.diskon_aktual>0,1,0)) as jml_diskon 
		from (_detail_penjualan_treatment inner join (select * from _penjualan_treatment $where) pp 
		ON _detail_penjualan_treatment.id_transaksi = pp.id) group by _detail_penjualan_treatment.id_treatment) ppp on treatment.id
        = ppp.id_treatment ORDER BY id ASC");
        $reportArr = [];
        for ($i = 0; $i < count($treatment); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $s = $treatment[$i];
            $d = $diskon[$i];
            $ds = $dis[$i];
            if ($s->id == $j->id && $s->id == $b->id) {
                $s->no = ($i + 1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $d->diskon; /*$j->diskon;*/
                $s->jml_diskon = $ds->jml_diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->penyusutan = $j->penyusutan;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }

    public function range($start, $end)
    {
        $where = "WHERE tgl BETWEEN '$start' AND '$end'";
        //$penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual) penjualan ON treatment.id=penjualan.id_treatment ORDER BY id ASC");
        //$penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, penyusutan FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, penyusutan FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual, penyusutan) penjualan ON treatment.id=penjualan.id_treatment ORDER BY id ASC");

$penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, penyusutan FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, penyusutan FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual, penyusutan) penjualan ON treatment.id=penjualan.id_treatment ORDER BY id ASC");

        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_treatment INNER JOIN _detail_pembelian_treatment ON _pembelian_treatment.id = _detail_pembelian_treatment.id_transaksi $where GROUP BY _detail_pembelian_treatment.id_treatment, _detail_pembelian_treatment.harga_aktual) pembelian ON treatment.id=pembelian.id_treatment ORDER BY id ASC");
        $treatment = \DB::select("SELECT *, 0 as jml_diskon FROM treatment ORDER BY id ASC");
        $diskon = \DB::select("SELECT id, IF(ISNULL(diskon),0,diskon) as diskon FROM treatment LEFT JOIN (SELECT _detail_penjualan_treatment.id_treatment as id_treatment, max(_detail_penjualan_treatment.diskon_aktual) as diskon 
		from (_detail_penjualan_treatment inner join (select * from _penjualan_treatment $where) pp 
		ON _detail_penjualan_treatment.id_transaksi = pp.id) group by _detail_penjualan_treatment.id_treatment) dis on treatment.id = dis.id_treatment ORDER BY id ASC");
        $dis = \DB::select(" SELECT id, jml_diskon FROM treatment LEFT JOIN (SELECT _detail_penjualan_treatment.id_treatment as id_treatment, sum(IF(_detail_penjualan_treatment.diskon_aktual>0,1,0)) as jml_diskon 
		from (_detail_penjualan_treatment inner join (select * from _penjualan_treatment $where) pp 
		ON _detail_penjualan_treatment.id_transaksi = pp.id) group by _detail_penjualan_treatment.id_treatment) ppp on treatment.id
        = ppp.id_treatment ORDER BY id ASC");
        $reportArr = [];
        for ($i = 0; $i < count($treatment); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $s = $treatment[$i];
		//echo $j->id." ".$b->id." ".$s->id."\n";
            $d = $diskon[$i];
            $ds = $dis[$i];
            if ($s->id == $j->id && $s->id == $b->id) {
                $s->no = ($i + 1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $d->diskon; /*$j->diskon;*/
                $s->jml_diskon = $ds->jml_diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->penyusutan = $j->penyusutan;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }
}
