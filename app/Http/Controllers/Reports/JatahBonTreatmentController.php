<?php

namespace App\Http\Controllers\Reports;

use App\JatahBonTreatment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JatahBonTreatmentController extends Controller
{
    public function index()
    {
        $title = 'Laporan Jatah Bon Treatment';
        return view('l2.jatahbontreatment', compact('title'));
    }

    public function hariini()
    {
        $detail = JatahBonTreatment::whereDay('tgl', date('d'))->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'))->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->id, 'tgl' => explode(' ', $item->tgl)[0], 'pegawai' => $item->pegawai->nama . ' (' . $item->pegawai->id . ')', 'jatah' => $item->jatah, 'bon' => $item->bon, 'total' => ($item->jatah+$item->bon)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function bulanini()
    {
        $detail = JatahBonTreatment::whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'))->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->id, 'tgl' => explode(' ', $item->tgl)[0], 'pegawai' => $item->pegawai->nama . ' (' . $item->pegawai->id . ')', 'jatah' => $item->jatah, 'bon' => $item->bon, 'total' => ($item->jatah+$item->bon)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function range($start, $end)
    {
        $dom = '';
        $detail = JatahBonTreatment::whereBetween('tgl', [$start, $end])->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->id, 'tgl' => explode(' ', $item->tgl)[0], 'pegawai' => $item->pegawai->nama . ' (' . $item->pegawai->id . ')', 'jatah' => $item->jatah, 'bon' => $item->bon, 'total' => ($item->jatah+$item->bon)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }
}
