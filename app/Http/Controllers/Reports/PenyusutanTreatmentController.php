<?php

namespace App\Http\Controllers\Reports;

use App\DetailPenjualanTreatment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PenyusutanTreatmentController extends Controller
{
    public function index()
    {
        $title = 'Laporan Penyusutan Treatment';
        return view('l2.penyusutantreatment', compact('title'));
    }

    public function hariini()
    {
        $detail = DetailPenjualanTreatment::whereHas('penjualanTreatment', function ($query) {
            $query->whereDay('tgl', date('d'))->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'));
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->penjualanTreatment->id, 'tgl' => explode(' ', $item->penjualanTreatment->tgl)[0], 'treatment' => $item->treatment->nama . ' (' . $item->treatment->id . ')', 'penyusutan' => $item->penyusutan, 'qty' => $item->qty, 'total' => ($item->penyusutan* $item->qty)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function bulanini()
    {
        $detail = DetailPenjualanTreatment::whereHas('penjualanTreatment', function ($query) {
            $query->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'));
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->penjualanTreatment->id, 'tgl' => explode(' ', $item->penjualanTreatment->tgl)[0], 'treatment' => $item->treatment->nama . ' (' . $item->treatment->id . ')', 'penyusutan' => $item->penyusutan, 'qty' => $item->qty, 'total' => ($item->penyusutan* $item->qty)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function range($start, $end)
    {
        $dom = '';
        $detail = DetailPenjualanTreatment::whereHas('penjualanTreatment', function ($query) use ($start, $end) {
            $query->whereBetween('tgl', [$start, $end]);
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->penjualanTreatment->id, 'tgl' => explode(' ', $item->penjualanTreatment->tgl)[0], 'treatment' => $item->treatment->nama . ' (' . $item->treatment->id . ')', 'penyusutan' => $item->penyusutan, 'qty' => $item->qty, 'total' => ($item->penyusutan* $item->qty)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }
}
