<?php

namespace App\Http\Controllers\Reports;

use App\DetailPenjualanProduk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PenjualanProdukController extends Controller
{
    public function index()
    {
        $title = 'Laporan Penjualan Produk';
        return view('l2.penjualanproduk', compact('title'));
    }

    public function hariini()
    {
        $detail = DetailPenjualanProduk::whereHas('penjualanProduk', function ($query) {
            $query->whereDay('tgl', date('d'))->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'));
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->penjualanProduk->id, 'tgl' => explode(' ', $item->penjualanProduk->tgl)[0], 'produk' => $item->produk->nama . ' (' . $item->produk->id . ')', 'harga' => $item->harga_aktual, 'diskon' => $item->diskon_aktual, 'qty' => $item->qty, 'subtotal' => (($item->harga_aktual * $item->qty) - ($item->diskon_aktual * $item->qty))];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function bulanini()
    {
        $detail = DetailPenjualanProduk::whereHas('penjualanProduk', function ($query) {
            $query->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'));
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->penjualanProduk->id, 'tgl' => explode(' ', $item->penjualanProduk->tgl)[0], 'produk' => $item->produk->nama . ' (' . $item->produk->id . ')', 'harga' => $item->harga_aktual, 'diskon' => $item->diskon_aktual, 'qty' => $item->qty, 'subtotal' => (($item->harga_aktual * $item->qty) - ($item->diskon_aktual * $item->qty))];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function range($start, $end)
    {
        $dom = '';
        $detail = DetailPenjualanProduk::whereHas('penjualanProduk', function ($query) use ($start, $end) {
            $query->whereBetween('tgl', [$start, $end]);
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->penjualanProduk->id, 'tgl' => explode(' ', $item->penjualanProduk->tgl)[0], 'produk' => $item->produk->nama . ' (' . $item->produk->id . ')', 'harga' => $item->harga_aktual, 'diskon' => $item->diskon_aktual, 'qty' => $item->qty, 'subtotal' => (($item->harga_aktual * $item->qty) - ($item->diskon_aktual * $item->qty))];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }
}
