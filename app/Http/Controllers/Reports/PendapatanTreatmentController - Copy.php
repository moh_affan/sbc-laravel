<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PendapatanTreatmentController extends Controller
{
    public function index()
    {
        $title = 'Laporan Pendapatan Treatment';
        return view('l2.pendapatantreatment', compact('title'));
    }

    public function hariini()
    {
        $where = "WHERE YEAR(tgl)=" . date('Y') . " AND MONTH(tgl)=" . date('m') . " AND DAY(tgl)=" . date('d');
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, IF(ISNULL(harga),0,diskon) as diskon, penyusutan FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, diskon_aktual as diskon, penyusutan FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual,_detail_penjualan_treatment.diskon_aktual, penyusutan) penjualan ON treatment.id=penjualan.id_treatment");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_treatment INNER JOIN _detail_pembelian_treatment ON _pembelian_treatment.id = _detail_pembelian_treatment.id_transaksi $where GROUP BY _detail_pembelian_treatment.id_treatment, _detail_pembelian_treatment.harga_aktual) pembelian ON treatment.id=pembelian.id_treatment");
        $treatment = \DB::select("SELECT * FROM treatment");
        $reportArr = [];
        for ($i = 0; $i < count($treatment); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $s = $treatment[$i];
            if ($s->id == $j->id && $s->id == $b->id) {
                $s->no = ($i + 1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $j->diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->penyusutan = $j->penyusutan;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }

    public function bulanini()
    {
        $where = "WHERE YEAR(tgl)=" . date('Y') . " AND MONTH(tgl)=" . date('m');
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, IF(ISNULL(harga),0,diskon) as diskon, penyusutan FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, diskon_aktual as diskon, penyusutan FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual,_detail_penjualan_treatment.diskon_aktual, penyusutan) penjualan ON treatment.id=penjualan.id_treatment");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_treatment INNER JOIN _detail_pembelian_treatment ON _pembelian_treatment.id = _detail_pembelian_treatment.id_transaksi $where GROUP BY _detail_pembelian_treatment.id_treatment, _detail_pembelian_treatment.harga_aktual) pembelian ON treatment.id=pembelian.id_treatment");
        $treatment = \DB::select("SELECT * FROM treatment");
        $reportArr = [];
        for ($i = 0; $i < count($treatment); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $s = $treatment[$i];
            if ($s->id == $j->id && $s->id == $b->id) {
                $s->no = ($i + 1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $j->diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->penyusutan = $j->penyusutan;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }

    public function range($start, $end)
    {
        $where = "WHERE tgl BETWEEN '$start' AND '$end'";
        $penjualan = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_jual,harga) as harga, IF(ISNULL(harga),0,diskon) as diskon, penyusutan FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga, diskon_aktual as diskon, penyusutan FROM _penjualan_treatment INNER JOIN _detail_penjualan_treatment ON _penjualan_treatment.id = _detail_penjualan_treatment.id_transaksi $where GROUP BY _detail_penjualan_treatment.id_treatment,_detail_penjualan_treatment.harga_aktual,_detail_penjualan_treatment.diskon_aktual, penyusutan) penjualan ON treatment.id=penjualan.id_treatment");
        $pembelian = \DB::select("SELECT id, IF(ISNULL(jml),0,jml) as jml, IF(ISNULL(harga),ref_harga_beli,harga) as harga FROM treatment LEFT JOIN (SELECT id_treatment, SUM(qty) AS jml, harga_aktual AS harga FROM _pembelian_treatment INNER JOIN _detail_pembelian_treatment ON _pembelian_treatment.id = _detail_pembelian_treatment.id_transaksi $where GROUP BY _detail_pembelian_treatment.id_treatment, _detail_pembelian_treatment.harga_aktual) pembelian ON treatment.id=pembelian.id_treatment");
        $treatment = \DB::select("SELECT * FROM treatment");
        $reportArr = [];
        for ($i = 0; $i < count($treatment); $i++) {
            $j = $penjualan[$i];
            $b = $pembelian[$i];
            $s = $treatment[$i];
            if ($s->id == $j->id && $s->id == $b->id) {
                $s->no = ($i + 1);
                $s->sum_jual = $j->jml;
                $s->harga_jual = $j->harga;
                $s->diskon_jual = $j->diskon;
                $s->sum_beli = $b->jml;
                $s->harga_beli = $b->harga;
                $s->penyusutan = $j->penyusutan;
                array_push($reportArr, $s);
            }
        }
        return \Response::json($reportArr);
    }
}
