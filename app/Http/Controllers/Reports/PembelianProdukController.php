<?php

namespace App\Http\Controllers\Reports;

use App\DetailPembelianProduk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PembelianProdukController extends Controller
{
    public function index()
    {
        $title = 'Laporan Pembelian Produk';
        return view('l2.pembelianproduk', compact('title'));
    }

    public function hariini()
    {
        $detail = DetailPembelianProduk::whereHas('pembelianProduk', function ($query) {
            $query->whereDay('tgl', date('d'))->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'));
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->pembelianProduk->id, 'tgl' => explode(' ', $item->pembelianProduk->tgl)[0], 'produk' => $item->produk->nama . ' (' . $item->produk->id . ')', 'harga' => $item->harga_aktual, 'qty' => $item->qty, 'subtotal' => ($item->harga_aktual * $item->qty)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function bulanini()
    {
        $detail = DetailPembelianProduk::whereHas('pembelianProduk', function ($query) {
            $query->whereMonth('tgl', date('m'))->whereYear('tgl', date('Y'));
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->pembelianProduk->id, 'tgl' => explode(' ', $item->pembelianProduk->tgl)[0], 'produk' => $item->produk->nama . ' (' . $item->produk->id . ')', 'harga' => $item->harga_aktual, 'qty' => $item->qty, 'subtotal' => ($item->harga_aktual * $item->qty)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }

    public function range($start, $end)
    {
        $dom = '';
        $detail = DetailPembelianProduk::whereHas('pembelianProduk', function ($query) use ($start, $end) {
            $query->whereBetween('tgl', [$start, $end]);
        })->get();
        $jsonData = [];
        foreach ($detail as $item) {
            $singleData = ['id' => $item->pembelianProduk->id, 'tgl' => explode(' ', $item->pembelianProduk->tgl)[0], 'produk' => $item->produk->nama . ' (' . $item->produk->id . ')', 'harga' => $item->harga_aktual, 'qty' => $item->qty, 'subtotal' => ($item->harga_aktual * $item->qty)];
            array_push($jsonData, $singleData);
        }
        return \Response::json($jsonData);
    }
}
