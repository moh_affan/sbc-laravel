<?php

namespace App\Http\Controllers;

use App\Pegawai;
use App\Pengguna;
use Illuminate\Http\Request;

class PenggunaController extends Controller
{
    public function index()
    {
        $pengguna = Pengguna::all()->sortBy('id');
        $pegawai = Pegawai::all()->sortBy('id');
        $title = 'Pengguna';
        return view('l0.pengguna', compact('pengguna', 'pegawai', 'title'));
    }

    public function show()
    {
        $pengguna = Pengguna::all()->sortBy('id');
        return \Response::json($pengguna);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string|unique:user,username',
            'id_pegawai' => 'required|string|exists:pegawai,id',
            'password' => 'required|string',
            'level' => 'required|in:0,1,2',
        ], [
            '*.required' => 'Field harus diisi',
            'username.unique' => 'Username sudah ada',
            'id_pegawai.exists' => 'Pegawai tidak ditemukan',
            'level.in' => 'Level salah',
        ]);
        $result = Pengguna::create($request->all());
        $pegawai = Pegawai::find($result->id_pegawai);
        $result['pegawai'] = $pegawai->nama;
        return \Response::json($result);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:user,id',
            'username' => 'required|string|exists:user,username',
            'id_pegawai' => 'required|string|exists:pegawai,id',
            'password' => 'required|string',
            'level' => 'required|in:0,1,2',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
            '*.required' => 'Field harus diisi',
            'username.exists' => 'Username tidak ditemukan',
            'id_pegawai.exists' => 'Pegawai tidak ditemukan',
            'level.in' => 'Level salah',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $pengguna = Pengguna::findOrFail($id);
        if ($pengguna->update($input)) {
            $pegawai = Pegawai::find($input["id_pegawai"]);
            $pengguna['pegawai'] = $pegawai->nama;
            return \Response::json($pengguna);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:user,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $pengguna = Pengguna::findOrFail($id);
        if ($pengguna->delete()) {
            return \Response::json($pengguna);
        }
        return \Response::json(["msg" => "Gagal menghapus"], 422);
    }
}
