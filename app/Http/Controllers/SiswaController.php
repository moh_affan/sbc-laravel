<?php

namespace App\Http\Controllers;

use App\Siswa;
use App\Telepon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class SiswaController extends Controller
{
    public function index()
    {
        $siswa = Siswa::all();
        $halaman = 'siswa';
        $total = Siswa::count();
        return view('siswa.murid', compact('halaman', 'siswa', 'total'));
    }

    public function create()
    {
        $halaman = 'siswa';
        return view('siswa.create', compact('halaman'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nisn' => 'required|size:4|digits:4|unique:siswa,nisn',
            'nama_siswa' => 'required|string',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required|in:L,P',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'telepon' => 'sometimes|numeric|digits_between:10,15|unique:telepon,nomor_telepon'
        ], [
            '*.unique' => ':attribute sudah ada',
            '*.required' => ':attribute harus diisi',
            '*.size' => ':attribute harus memiliki :size digit',
            '*.digits' => ':attribute harus berupa angka',
            '*.date' => 'format tanggal :attribute tidak valid',
            '*.image' => 'Gambar tidak valid',
            '*.mimes' => 'Format gambar tidak cocok',
            '*.max' => 'Ukuran gambar terlalu besar',
            'telepon.*' => 'telepon tidak valid'
        ]);
        $input = $request->all();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $input['imageName'] = time() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('/imgupload/thumbs');
            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $input['imageName']);

            $destinationPath = public_path('/imgupload');
            $image->move($destinationPath, $input['imageName']);
            $input['image'] = $imageName;
        }
        $siswa = Siswa::create($input);
        $telepon = new Telepon();
        $telepon->nomor_telepon = $input['telepon'];
        $siswa->telepon()->save($telepon);
        return redirect('siswa');
    }

    public function show($id)
    {
        $siswa = Siswa::findOrFail($id);
        $halaman = 'siswa';
        return view('siswa.detail', compact('siswa', 'halaman'));
    }

    public function hapus($id)
    {
        $siswa = Siswa::findOrFail($id);
        File::delete('imgupload/thumbs/' . $siswa->image);
        File::delete('imgupload/' . $siswa->image);
        $siswa->delete();
        return redirect('siswa');
    }

    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);
        return view('siswa.edit', compact('siswa'));
    }

    public function update($id, Request $request)
    {
        $siswa = Siswa::findOrFail($id);
        $this->validate($request, [
            'nisn' => 'required|size:4|digits:4',
            'nama_siswa' => 'required|string',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required|in:L,P',
            'image' => 'max:2048',
            'telepon' => 'sometimes|numeric|digits_between:10,15|unique:telepon,nomor_telepon',
        ], [
            '*.required' => ':attribute harus diisi',
            '*.size' => ':attribute harus memiliki :size digit',
            '*.digits' => ':attribute harus berupa angka',
            '*.date' => 'format tanggal :attribute tidak valid',
            '*.image' => 'Gambar tidak valid',
            '*.mimes' => 'Format gambar tidak cocok',
            '*.max' => 'Ukuran gambar terlalu besar',
            'telepon.*' => 'Telepon tidak valid',
        ]);
        $input = $request->all();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $input['imageName'] = time() . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('/imgupload/thumbs');
            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $input['imageName']);

            $destinationPath = public_path('/imgupload');
            $image->move($destinationPath, $input['imageName']);
            $input['image'] = $imageName;
        } else {
            $input['image'] = $siswa->image;
        }
        $siswa->update($input);
        $siswa->telepon()->update(['nomor_telepon' => $input['telepon']]);
        return redirect('siswa');
    }
}
