<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function index()
    {
        $kategori = Kategori::all()->sortBy('id');
        $title = 'Kategori';
        return view('l0.kategori', compact('kategori', 'title'));
    }

    public function show()
    {
        $kategori = Kategori::all()->sortBy('id');
        return \Response::json($kategori);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|unique:kategori,id',
            'kategori' => 'required|string',
        ], [
            'id.required' => 'ID harus diisi',
            'id.unique' => 'ID sudah ada',
            'kategori.required' => 'Nama Kategori harus diisi',
        ]);
        $result = Kategori::create($request->all());
        return \Response::json($result);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:kategori,id',
            'kategori' => 'required|string',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
            'kategori.required' => 'Nama Kategori harus diisi',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $kategori = Kategori::findOrFail($id);
        if ($kategori->update($input)) {
            return \Response::json($kategori);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:kategori,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $kategori = Kategori::findOrFail($id);
        if ($kategori->delete()) {
            return \Response::json($kategori);
        }
        return \Response::json(["msg" => "Gagal menghapus"], 422);
    }
}
