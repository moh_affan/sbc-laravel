<?php

namespace App\Http\Controllers;

use App\Diskon;
use App\Kategori;
use App\Penyusutan;
use App\Supplier;
use App\Treatment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TreatmentController extends Controller
{
    public function index()
    {
        $treatment = Treatment::all()->sortBy('id');
        $kategori = Kategori::all();
        $supplier = Supplier::all();
        $diskon = Diskon::all();
        $title = 'Treatment';
        return view('l0.treatment', compact('treatment', 'kategori', 'supplier', 'diskon', 'title'));
    }

    public function show()
    {
        $treatment = Treatment::all()->sortBy('id');
        return \Response::json($treatment);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|unique:treatment,id',
            'id_kategori' => 'required|exists:kategori,id',
            'id_supplier' => 'required|exists:supplier,id',
            'nama' => 'required|string',
            'ref_harga_jual' => 'required|integer',
            'ref_harga_beli' => 'required|integer',
            'id_diskon' => 'required|exists:diskon,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.unique' => 'ID sudah ada',
            'nama.required' => 'Nama harus diisi',
            '*.required' => 'Field ini harus diisi',
            'id_kategori.exists' => 'Kategori tidak ditemukan',
            'id_supplier.exists' => 'Supplier tidak ditemukan',
            'id_diskon.exists' => 'Diskon tidak ditemukan',
            '*.integer' => 'Nominal tidak valid',
        ]);
        $result = Treatment::create($request->all());
        $treatment = Treatment::find($result->id);
        $penyusutan = new Penyusutan(['penyusutan' => 0]);
        $treatment->penyusutan()->save($penyusutan);
        $kategori = Kategori::find($result->id_kategori);
        $result['kategori'] = $kategori->kategori;
        $supplier = Supplier::find($result->id_supplier);
        $result['supplier'] = $supplier->supplier;
        $diskon = Diskon::find($result->id_diskon);
        $result['diskon'] = $diskon->nama;
        return \Response::json($result);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:treatment,id',
            'id_kategori' => 'required|exists:kategori,id',
            'id_supplier' => 'required|exists:supplier,id',
            'nama' => 'required|string',
            'ref_harga_jual' => 'required|integer',
            'ref_harga_beli' => 'required|integer',
            'id_diskon' => 'required|exists:diskon,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
            'nama.required' => 'Nama harus diisi',
            '*.required' => 'Field ini harus diisi',
            'id_kategori.exists' => 'Kategori tidak ditemukan',
            'id_supplier.exists' => 'Supplier tidak ditemukan',
            'id_diskon.exists' => 'Diskon tidak ditemukan',
            '*.integer' => 'Nominal tidak valid',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $treatment = Treatment::findOrFail($id);
        if ($treatment->update($input)) {
            $kategori = Kategori::find($treatment->id_kategori);
            $treatment['kategori'] = $kategori->kategori;
            $supplier = Supplier::find($treatment->id_supplier);
            $treatment['supplier'] = $supplier->supplier;
            $diskon = Diskon::find($treatment->id_diskon);
            $treatment['diskon'] = $diskon->nama;
            return \Response::json($treatment);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:treatment,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $treatment = Treatment::findOrFail($id);
        if ($treatment->delete()) {
            if ($treatment->penyusutan())
                $treatment->penyusutan()->delete();
            return \Response::json($treatment);
        }
        return \Response::json(["msg" => "Gagal menghapus"], 422);
    }

    public function getTreatmentBySupplier($id_supplier)
    {
        $treatment = Treatment::whereIdSupplier($id_supplier)->get();
        $optionsHtml = '<option value="">--Pilih Treatment--</option>';
        foreach ($treatment as $item) {
            $optionsHtml = $optionsHtml . '
            <option value=\'{"id":"' . $item->id . '","treatment":"' . $item->nama . '"}\'>' . $item->nama . ' | ' . $item->id . '</option>';
        }
        return $optionsHtml;
    }

    public function getTreatmentBySupplier2($id_supplier)
    {
        $treatment = Treatment::whereIdSupplier($id_supplier)->get();
        $optionsHtml = '<option value="">--Pilih Treatment--</option>';
        foreach ($treatment as $item) {
            $optionsHtml = $optionsHtml . '
            <option value=\'{"id":"' . $item->id . '","treatment":"' . $item->nama . '","harga":"' . $item->ref_harga_jual . '"}\'>' . $item->nama . ' | ' . $item->id . ' | Rp' . $item->ref_harga_jual . ' </option>
            ';
        }
        return $optionsHtml;
    }

    public function getTreatmentAll()
    {
        $treatment = Treatment::all();
        $optionsHtml = '<option value="">--Pilih Treatment--</option>';
        foreach ($treatment as $item) {
            $diskon = 0;
            if (isset($item->diskon)) {
                if (Carbon::now()->diffInDays(Carbon::parse($item->diskon->tgl_awal)) >= 0 && Carbon::now()->diffInDays(Carbon::parse($item->diskon->tgl_akhir)) <= 0)
                    $diskon = $item->diskon->jumlah_diskon;
                else
                    $diskon = 0;
            } else
                $diskon = 0;
            $optionsHtml = $optionsHtml . '
            <option value=\'{"id":"' . $item->id . '","treatment":"' . $item->nama . '","diskon":"' . $diskon . '","harga":"' . $item->ref_harga_jual . '","stok":"' . $item->stokTreatment->stok . '"}\'>' . $item->nama . ' | ' . $item->id . ' | Rp' . $item->ref_harga_jual . ' | Stok : ' . $item->stokTreatment->stok . ' </option>
            ';
        }
        return $optionsHtml;
    }
}
