<?php

namespace App\Http\Controllers;

use App\Jabatan;
use App\Pegawai;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    public function index()
    {
        $pegawai = Pegawai::all()->sortBy('id');
        $jabatan = Jabatan::all()->sortBy('kd');
        $title = 'Pegawai';
        return view('l0.pegawai', compact('pegawai', 'jabatan', 'title'));
    }

    public function show()
    {
        $pegawai = Pegawai::all()->sortBy('id');
        return \Response::json($pegawai);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|unique:pegawai,id',
            'nama' => 'required|string',
            'kd_jabatan' => 'required|string|exists:jabatan,kd',
            'telp' => 'required|digits_between:10,15',
            'alamat' => 'required|string',
        ], [
            'id.required' => 'ID harus diisi',
            'id.unique' => 'ID sudah ada',
            'nama.required' => 'Nama harus diisi',
            'kd_jabatan.required' => 'Jabatan harus diisi',
            'telp.required' => 'No. Telp/HP harus diisi',
            'alamat.required' => 'Alamat harus diisi',
            'kd_jabatan.exists' => 'Jabatan tidak ditemukan',
            'telp.digits_between' => 'No. Telp/HP tidak valid',
        ]);
        $result = Pegawai::create($request->all());
        $jabatan = Jabatan::find($result->kd_jabatan);
        $result['jabatan'] = $jabatan->jabatan;
        return \Response::json($result);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:pegawai,id',
            'nama' => 'required|string',
            'kd_jabatan' => 'required|string|exists:jabatan,kd',
            'telp' => 'required|digits_between:10,15',
            'alamat' => 'required|string',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
            'nama.required' => 'Nama harus diisi',
            'kd_jabatan.required' => 'Jabatan harus diisi',
            'telp.required' => 'No. Telp/HP harus diisi',
            'alamat.required' => 'Alamat harus diisi',
            'kd_jabatan.exists' => 'Jabatan tidak ditemukan',
            'telp.digits_between' => 'No. Telp/HP tidak valid',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $pegawai = Pegawai::findOrFail($id);
        if ($pegawai->update($input)) {
            $jabatan = Jabatan::find($input["kd_jabatan"]);
            $pegawai['jabatan'] = $jabatan->jabatan;
            return \Response::json($pegawai);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:pegawai,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $pegawai = Pegawai::findOrFail($id);
        if ($pegawai->delete()) {
            return \Response::json($pegawai);
        }
        return \Response::json(["msg" => "Gagal menghapus"], 422);
    }
}
