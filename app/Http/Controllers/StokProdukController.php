<?php

namespace App\Http\Controllers;

use App\Produk;
use App\StokProduk;
use Illuminate\Http\Request;

class StokProdukController extends Controller
{
    public function index(){
        $produk = Produk::all();
        $title = "Stok Produk";
        return view('l0.stokproduk', compact('produk','title'));
    }
}
