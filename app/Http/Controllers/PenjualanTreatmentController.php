<?php

namespace App\Http\Controllers;

use App\DetailPenjualanTreatment;
use App\Pelanggan;
use App\PenjualanTreatment;
use App\Treatment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PenjualanTreatmentController extends Controller
{
    public function index()
    {
        $pelanggan = Pelanggan::all()->sortBy('id');
        $transaksiSaya = PenjualanTreatment::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get();
        $title = 'Penjualan Treatment';
        return view('l1.penjualantreatment', compact('pelanggan', 'title', 'transaksiSaya'));
    }

    public function transasksiSaya()
    {
        $transaksiSaya = PenjualanTreatment::whereIdPegawai(\Session::get('id_pegawai', 0))->limit(100)->get()->sortByDesc('id');
        $response = "";
        foreach ($transaksiSaya as $transaksi) {
            $response .= '<tr>
                                        <td>' . $transaksi->id . '</td>
                                        <td>' . $transaksi->tgl . '</td>
                                        <td>' . $transaksi->pelanggan->nama . '(' . $transaksi->id_pelanggan . ')</td>
                                        <td>' . $transaksi->keterangan . '</td>
                                        <td>
                                            <button class="btn btn-sm btn-default detail" value=\'{"id":"' . $transaksi->id . '"}\'>Detail</button>
                                        </td>
                                    </tr>';
        }
        return $response;
    }

    public function loadTransaction($id)
    {
        $transaksi = PenjualanTreatment::find($id);
        $response = "";
        $response .= '<button style="position:absolute;right:10px;top:0;" class="btn btn-flat btn-primary" id="print-this" value="' . $transaksi->id . '"><span class="fa fa-print"></span></button>';
        $response .= '<h5>ID Transaksi : ' . $transaksi->id . '</h5>';
        $response .= '<h5>Tanggal : ' . $transaksi->tgl . '</h5>';
        $response .= '<h5>Pelanggan : ' . $transaksi->id_pelanggan . ' ' . $transaksi->pelanggan->nama . '</h5>';
        $response .= '<table class="table table-bordered table-responsive">';
        $response .= '<thead>';
        $response .= '<tr>';
        $response .= '<th>ID</th><th>Treatment</th><th>Jumlah</th><th>Harga</th><th>Diskon</th><th class="col-subtotal">Subtotal</th>';
        $response .= '</tr>';
        $response .= '</thead>';
        $response .= '<tbody>';
        $total = 0;
        foreach ($transaksi->detailPenjualanTreatment as $detailPenjualanTreatment) {
            $response .= '<tr>';
            $response .= "<td>$detailPenjualanTreatment->id_treatment</td><td>" . $detailPenjualanTreatment->treatment->nama . "</td><td>$detailPenjualanTreatment->qty</td><td>" . $detailPenjualanTreatment->harga_aktual . "</td><td>" . $detailPenjualanTreatment->diskon_aktual . "</td><td>" . (($detailPenjualanTreatment->qty * $detailPenjualanTreatment->harga_aktual) - ($detailPenjualanTreatment->qty * $detailPenjualanTreatment->diskon_aktual)) . "</td>";
            $response .= '</tr>';
            $total += (($detailPenjualanTreatment->qty * $detailPenjualanTreatment->harga_aktual) - ($detailPenjualanTreatment->qty * $detailPenjualanTreatment->diskon_aktual));
        }
        $response .= '</tbody>';
        $response .= '<tfoot>';
        $response .= '<tr>';
        $response .= '<th colspan="5">Total = ' . $transaksi->total . '</th > ';
        $response .= '</tr > ';
        $response .= '</tfoot > ';
        $response .= '</table > ';
        return $response;
    }

    public function printTransaction($id)
    {
        $transaksi = PenjualanTreatment::find($id);
        return view('l1.printpenjualantreatment', compact('transaksi'));
    }

    public function lastId()
    {
        $penjualanTreatment = PenjualanTreatment::latest('id')->first();
        if (isset($penjualanTreatment['id']))
            return \Response::json($penjualanTreatment);
        else
            return \Response::json(['id' => 0]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $penjualanTreatment = new PenjualanTreatment();
        $penjualanTreatment->id_pelanggan = $input['id_pelanggan'];
        $penjualanTreatment->tgl = $input['tgl'];
        $penjualanTreatment->id_pegawai = $input['id_pegawai'];
        $penjualanTreatment->keterangan = ' - ';
        $penjualanTreatment->total = 0;
        $total = 0;
        if ($penjualanTreatment->save()) {
            for ($i = 0; $i < count($input['id_treatment']); $i++) {
                $treatment = Treatment::find($input['id_treatment'][$i]);
                $detail = new DetailPenjualanTreatment();
                $detail->id_treatment = $treatment->id;
                $detail->harga_aktual = $treatment->ref_harga_jual;
                if (isset($treatment->diskon)) {
                    $diskon = $treatment->diskon;
                    if (Carbon::now()->diffInDays(Carbon::parse($diskon->tgl_awal)) >= 0 && Carbon::now()->diffInDays(Carbon::parse($diskon->tgl_akhir)) <= 0)
                        $detail->diskon_aktual = $diskon->jumlah_diskon;
                    else
                        $detail->diskon_aktual = 0;
                } else
                    $detail->diskon_aktual = 0;
                $detail->qty = $input['qty'][$i];
                if (isset($treatment->penyusutan))
                    $detail->penyusutan = $treatment->penyusutan->penyusutan;
                else
                    $detail->penyusutan = 0;
                $total += (($treatment->ref_harga_jual * intval($input['qty'][$i])) - ($detail->diskon_aktual * intval($input['qty'][$i])));
                $penjualanTreatment->detailPenjualanTreatment()->save($detail);
            }
        }
        $penjualanTreatment->update(['total' => $total]);
        return \Response::json($penjualanTreatment);
    }
}
