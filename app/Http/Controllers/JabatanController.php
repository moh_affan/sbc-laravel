<?php

namespace App\Http\Controllers;

use App\Jabatan;
use Illuminate\Http\Request;

class JabatanController extends Controller
{
    public function index()
    {
        $jabatan = Jabatan::all()->sortBy('kd');
        $title = 'Jabatan';
        return view('l0.jabatan', compact('jabatan', 'title'));
    }

    public function show()
    {
        $jabatan = Jabatan::all()->sortBy('kd');
        return \Response::json($jabatan);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'kd' => 'required|unique:jabatan,kd',
            'jabatan' => 'required|string',
        ], [
            'kd.required' => 'Kode harus diisi',
            'kd.unique' => 'Kode sudah ada',
            'jabatan.required' => 'Nama Jabatan harus diisi',
        ]);
        $result = Jabatan::create($request->all());
        return \Response::json($result);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'kd' => 'required|exists:jabatan,kd',
            'jabatan' => 'required|string',
        ], [
            'kd.required' => 'Kode harus diisi',
            'kd.exists' => 'Kode tidak ditemukan',
            'jabatan.required' => 'Nama Jabatan harus diisi',
        ]);
        $input = $request->all();
        $kd = $input['kd'];
        $jabatan = Jabatan::findOrFail($kd);
        if ($jabatan->update($input)) {
            return \Response::json($jabatan);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'kd' => 'required|exists:jabatan,kd',
        ], [
            'kd.required' => 'Kode harus diisi',
            'kd.exists' => 'Kode tidak ditemukan',
        ]);
        $input = $request->all();
        $kd = $input['kd'];
        $jabatan = Jabatan::findOrFail($kd);
        if ($jabatan->delete()) {
            return \Response::json($jabatan);
        }
        return \Response::json(["msg" => "Gagal menghapus"], 422);
    }
}
