<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        $supplier = Supplier::all()->sortBy('id');
        $title = 'Supplier';
        return view('l0.supplier', compact('supplier', 'title'));
    }

    public function show()
    {
        $supplier = Supplier::all()->sortBy('id');
        return \Response::json($supplier);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|unique:supplier,id',
            'supplier' => 'required|string',
            'telp' => 'required|digits_between:10,15',
            'alamat' => 'required|string',
        ], [
            'id.required' => 'ID harus diisi',
            'id.unique' => 'ID sudah ada',
            'supplier.required' => 'Nama supplier harus diisi',
            'telp.required' => 'No. Telp/HP harus diisi',
            'alamat.required' => 'Alamat harus diisi',
            'telp.digits_between' => 'No. Telp/HP tidak valid',
        ]);
        $result = Supplier::create($request->all());
        return \Response::json($result);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:supplier,id',
            'supplier' => 'required|string',
            'telp' => 'required|digits_between:10,15',
            'alamat' => 'required|string',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
            'supplier.required' => 'Nama supplier harus diisi',
            'telp.required' => 'No. Telp/HP harus diisi',
            'alamat.required' => 'Alamat harus diisi',
            'telp.digits_between' => 'No. Telp/HP tidak valid',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $supplier = Supplier::findOrFail($id);
        if ($supplier->update($input)) {
            return \Response::json($supplier);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:supplier,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $supplier = Supplier::findOrFail($id);
        if ($supplier->delete()) {
            return \Response::json($supplier);
        }
        return \Response::json(["msg" => "Gagal menghapus"], 422);
    }
}
