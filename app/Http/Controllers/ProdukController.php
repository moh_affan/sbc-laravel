<?php

namespace App\Http\Controllers;

use App\Diskon;
use App\Kategori;
use App\Produk;
use App\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    public function index()
    {
        $produk = Produk::all()->sortBy('id');
        $kategori = Kategori::all();
        $supplier = Supplier::all();
        $diskon = Diskon::all();
        $title = 'Produk';
        return view('l0.produk', compact('produk', 'kategori', 'supplier', 'diskon', 'title'));
    }

    public function show()
    {
        $produk = Produk::all()->sortBy('id');
        return \Response::json($produk);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|unique:produk,id',
            'id_kategori' => 'required|exists:kategori,id',
            'id_supplier' => 'required|exists:supplier,id',
            'nama' => 'required|string',
            'ref_harga_jual' => 'required|integer',
            'ref_harga_beli' => 'required|integer',
            'id_diskon' => 'required|exists:diskon,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.unique' => 'ID sudah ada',
            'nama.required' => 'Nama harus diisi',
            '*.required' => 'Field ini harus diisi',
            'id_kategori.exists' => 'Kategori tidak ditemukan',
            'id_supplier.exists' => 'Supplier tidak ditemukan',
            'id_diskon.exists' => 'Diskon tidak ditemukan',
            '*.integer' => 'Nominal tidak valid',
        ]);
        $result = Produk::create($request->all());
        $kategori = Kategori::find($result->id_kategori);
        $result['kategori'] = $kategori->kategori;
        $supplier = Supplier::find($result->id_supplier);
        $result['supplier'] = $supplier->supplier;
        $diskon = Diskon::find($result->id_diskon);
        $result['diskon'] = $diskon->nama;
        return \Response::json($result);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:produk,id',
            'id_kategori' => 'required|exists:kategori,id',
            'id_supplier' => 'required|exists:supplier,id',
            'nama' => 'required|string',
            'ref_harga_jual' => 'required|integer',
            'ref_harga_beli' => 'required|integer',
            'id_diskon' => 'required|exists:diskon,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
            'nama.required' => 'Nama harus diisi',
            '*.required' => 'Field ini harus diisi',
            'id_kategori.exists' => 'Kategori tidak ditemukan',
            'id_supplier.exists' => 'Supplier tidak ditemukan',
            'id_diskon.exists' => 'Diskon tidak ditemukan',
            '*.integer' => 'Nominal tidak valid',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $produk = Produk::findOrFail($id);
        if ($produk->update($input)) {
            $kategori = Kategori::find($produk->id_kategori);
            $produk['kategori'] = $kategori->kategori;
            $supplier = Supplier::find($produk->id_supplier);
            $produk['supplier'] = $supplier->supplier;
            $diskon = Diskon::find($produk->id_diskon);
            $produk['diskon'] = $diskon->nama;
            return \Response::json($produk);
        }
        return \Response::json(["msg" => "Gagal mengedit"], 422);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:produk,id',
        ], [
            'id.required' => 'ID harus diisi',
            'id.exists' => 'ID tidak ditemukan',
        ]);
        $input = $request->all();
        $id = $input['id'];
        $produk = Produk::findOrFail($id);
        if ($produk->delete()) {
            return \Response::json($produk);
        }
        return \Response::json(["msg" => "Gagal menghapus"], 422);
    }

    public function getProdukBySupplier($id_supplier)
    {
        $produk = Produk::whereIdSupplier($id_supplier)->get();
        $optionsHtml = '<option value="">--Pilih Produk--</option>';
        foreach ($produk as $item) {
            $optionsHtml = $optionsHtml . '
            <option value=\'{"id":"' . $item->id . '","produk":"' . $item->nama . '"}\'>' . $item->nama . ' | ' . $item->id . '</option>';
        }
        return $optionsHtml;
    }

    public function getProdukAll()
    {
        $produk = Produk::all();
        $optionsHtml = '<option value="">--Pilih Produk--</option>';
        foreach ($produk as $item) {
            $diskon = 0;
            if (isset($item->diskon)) {
                if (Carbon::now()->diffInDays( Carbon::parse($item->diskon->tgl_awal)) >= 0 && Carbon::now()->diffInDays(Carbon::parse($item->diskon->tgl_akhir)) <= 0)
                    $diskon = $item->diskon->jumlah_diskon;
                else
                    $diskon = 0;
            } else
                $diskon = 0;
            $optionsHtml = $optionsHtml . '
            <option value=\'{"id":"' . $item->id . '","produk":"' . $item->nama . '","diskon":"' . $diskon . '","harga":"' . $item->ref_harga_jual . '","stok":"' . $item->stokProduk->stok . '"}\'>' . $item->nama . ' | ' . $item->id . ' | Rp' . $item->ref_harga_jual . ' | Stok : ' . $item->stokProduk->stok . ' </option>
            ';
        }
        return $optionsHtml;
    }
}
