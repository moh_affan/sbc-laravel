<?php
/**
 * Created by PhpStorm.
 * User: affanmohammad
 * Date: 12/1/16
 * Time: 07:27
 */

namespace App\Http\ViewComposer;


use App\Pengguna;
use Illuminate\View\View;

class ProfileComposer
{
    protected $pengguna;

    public function __construct()
    {
        $this->pengguna = Pengguna::whereId(\Session::get('id', 0))->get()->first();
    }

    public function compose(View $view)
    {
        $view->with('pengguna', $this->pengguna);
    }
}