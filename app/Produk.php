<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Produk
 *
 * @property int $id
 * @property string $id_kategori
 * @property string $id_supplier
 * @property string $nama
 * @property float $ref_harga_jual
 * @property float $ref_harga_beli
 * @property string $id_diskon
 * @method static \Illuminate\Database\Query\Builder|\App\Produk whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Produk whereIdDiskon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Produk whereIdKategori($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Produk whereIdSupplier($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Produk whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Produk whereRefHargaBeli($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Produk whereRefHargaJual($value)
 * @mixin \Eloquent
 * @property-read \App\Supplier $supplier
 * @property-read \App\Diskon $diskon
 * @property-read \App\Kategori $kategori
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailPembelianProduk[] $detailPembelianProduk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailPenjualanProduk[] $detailPenjualanProduk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailJatahBonProduk[] $detailJatahBonProduk
 * @property-read \App\StokProduk $stokProduk
 */
class Produk extends Model
{
    protected $table = 'produk';
    protected $fillable = ['id', 'id_kategori', 'id_supplier', 'nama', 'ref_harga_jual', 'ref_harga_beli', 'id_diskon'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = false;

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'id_supplier');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'id_kategori');
    }

    public function diskon()
    {
        return $this->belongsTo('App\Diskon', 'id_diskon');
    }

    public function detailPembelianProduk()
    {
        return $this->hasMany('App\DetailPembelianProduk', 'id_produk', 'id');
    }

    public function detailPenjualanProduk()
    {
        return $this->hasMany('App\DetailPenjualanProduk', 'id_produk', 'id');
    }

    public function detailJatahBonProduk()
    {
        return $this->hasMany('App\DetailJatahBonProduk', 'id_produk', 'id');
    }

    public function stokProduk()
    {
        return $this->hasOne('App\StokProduk', 'id_produk', 'id');
    }
}
