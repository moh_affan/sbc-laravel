<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\StokTreatment
 *
 * @property string $id_treatment
 * @property string $nama
 * @property float $stok
 * @property float $ref_harga_jual
 * @property float $subtotal_stok
 * @property-read \App\Treatment $treatment
 * @method static \Illuminate\Database\Query\Builder|\App\StokTreatment whereIdTreatment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\StokTreatment whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\App\StokTreatment whereRefHargaJual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\StokTreatment whereStok($value)
 * @method static \Illuminate\Database\Query\Builder|\App\StokTreatment whereSubtotalStok($value)
 * @mixin \Eloquent
 */
class StokTreatment extends Model
{
    protected $table = '__stok_tr';

    public function treatment()
    {
        return $this->belongsTo('App\Treatment', 'id_treatment');
    }
}
