<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\JatahBonTreatment
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $tgl
 * @property string $id_peg
 * @property string $id_pegawai
 * @property string $keterangan
 * @property float $total
 * @property float $jatah
 * @property float $bon
 * @property string $batas_bon
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonTreatment whereBatasBon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonTreatment whereBon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonTreatment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonTreatment whereIdPeg($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonTreatment whereIdPegawai($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonTreatment whereJatah($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonTreatment whereKeterangan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonTreatment whereTgl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JatahBonTreatment whereTotal($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DetailJatahBonTreatment[] $detailJatahBonTreatment
 * @property-read \App\Pegawai $peg
 * @property-read \App\Pegawai $pegawai
 */
class JatahBonTreatment extends Model
{
    protected $table = '_jatah_bon_tr';
    protected $fillable = ['id', 'tgl', 'id_peg', 'id_pegawai', 'keterangan', 'total', 'jatah', 'bon', 'batas_bon'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $dates = ['tgl', 'batas_bon'];

    public function detailJatahBonTreatment()
    {
        return $this->hasMany('App\DetailJatahBonTreatment', 'id_transaksi', 'id');
    }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }

    public function peg()
    {
        return $this->belongsTo('App\Pegawai', 'id_peg');
    }
}
