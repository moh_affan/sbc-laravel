<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Penyusutan
 *
 * @property string $id_treatment
 * @property float $penyusutan
 * @method static \Illuminate\Database\Query\Builder|\App\Penyusutan whereIdTreatment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Penyusutan wherePenyusutan($value)
 * @mixin \Eloquent
 * @property-read \App\Treatment $treatment
 */
class Penyusutan extends Model
{
    protected $table = 'penyusutan';
    protected $fillable = ['id_treatment', 'penyusutan'];
    protected $primaryKey = 'id_treatment';
    public $timestamps = false;
    public $incrementing = false;

    public function treatment()
    {
        return $this->belongsTo('App\Treatment', 'id_treatment');
    }
}
