<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\StokProduk
 *
 * @property string $id_produk
 * @property string $nama
 * @property float $stok
 * @property float $ref_harga_jual
 * @property float $subtotal_stok
 * @method static \Illuminate\Database\Query\Builder|\App\StokProduk whereIdProduk($value)
 * @method static \Illuminate\Database\Query\Builder|\App\StokProduk whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\App\StokProduk whereRefHargaJual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\StokProduk whereStok($value)
 * @method static \Illuminate\Database\Query\Builder|\App\StokProduk whereSubtotalStok($value)
 * @mixin \Eloquent
 * @property-read \App\Produk $produk
 */
class StokProduk extends Model
{
    protected $table = '__stok';

    public function produk()
    {
        return $this->belongsTo('App\Produk', 'id_produk');
    }
}
