<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Supplier
 *
 * @property string $id
 * @property string $supplier
 * @property string $alamat
 * @property string $telp
 * @method static \Illuminate\Database\Query\Builder|\App\Supplier whereAlamat($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Supplier whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Supplier whereSupplier($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Supplier whereTelp($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Produk[] $produk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Treatment[] $treatment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PembelianProduk[] $pembelianProduk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PembelianTreatment[] $pembelianTreatment
 */
class Supplier extends Model
{
    protected $table = 'supplier';
    protected $fillable = ['id', 'supplier', 'alamat', 'telp'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = false;

    public function produk()
    {
        return $this->hasMany('App\Produk', 'id_supplier', 'id');
    }

    public function treatment()
    {
        return $this->hasMany('App\Treatment', 'id_supplier', 'id');
    }

    public function pembelianProduk()
    {
        return $this->hasMany('App\PembelianProduk', 'id_supplier', 'id');
    }

    public function pembelianTreatment()
    {
        return $this->hasMany('App\PembelianTreatment', 'id_supplier', 'id');
    }
}
