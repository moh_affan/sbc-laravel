<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Pegawai
 *
 * @property string $id
 * @property string $nama
 * @property string $kd_jabatan
 * @property string $telp
 * @property string $alamat
 * @property-read \App\Jabatan $jabatan
 * @method static \Illuminate\Database\Query\Builder|\App\Pegawai whereAlamat($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pegawai whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pegawai whereKdJabatan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pegawai whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pegawai whereTelp($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Pengguna[] $pengguna
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PembelianProduk[] $pembelianProduk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\JatahBonProduk[] $jatahBonProduk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\JatahBonProduk[] $pegJatahBonProduk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PembelianTreatment[] $pembelianTreatment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PenjualanProduk[] $penjualanProduk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PenjualanTreatment[] $penjualanTreatment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\JatahBonTreatment[] $jatahBonTreatment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\JatahBonTreatment[] $pegJatahBonTreatment
 */
class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $fillable = ['id', 'nama', 'kd_jabatan', 'telp', 'alamat'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = false;

    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan', 'kd_jabatan');
    }

    public function pengguna()
    {
        return $this->hasMany('App\Pengguna', 'id_pegawai', 'id');
    }

    public function pembelianProduk()
    {
        return $this->hasMany('App\PembelianProduk', 'id_pegawai', 'id');
    }

    public function penjualanProduk()
    {
        return $this->hasMany('App\PenjualanProduk', 'id_pegawai', 'id');
    }

    public function jatahBonProduk()
    {
        return $this->hasMany('App\JatahBonProduk', 'id_pegawai', 'id');
    }

    public function pegJatahBonProduk()
    {
        return $this->hasMany('App\JatahBonProduk', 'id_peg', 'id');
    }

    public function pembelianTreatment()
    {
        return $this->hasMany('App\PembelianTreatment', 'id_pegawai', 'id');
    }

    public function penjualanTreatment()
    {
        return $this->hasMany('App\PenjualanTreatment', 'id_pegawai', 'id');
    }

    public function jatahBonTreatment()
    {
        return $this->hasMany('App\JatahBonTreatment', 'id_pegawai', 'id');
    }

    public function pegJatahBonTreatment()
    {
        return $this->hasMany('App\JatahBonTreatment', 'id_peg', 'id');
    }
}
