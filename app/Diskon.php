<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Diskon
 *
 * @property string $id
 * @property string $nama
 * @property \Carbon\Carbon $tgl_awal
 * @property float $jumlah_diskon
 * @property \Carbon\Carbon $tgl_akhir
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\App\Diskon whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Diskon whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Diskon whereJumlahDiskon($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Diskon whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Diskon whereTglAkhir($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Diskon whereTglAwal($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Produk[] $produk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Treatment[] $treatment
 */
class Diskon extends Model
{
    protected $table = 'diskon';
    protected $fillable = ['id', 'nama', 'tgl_awal', 'jumlah_diskon', 'tgl_akhir', 'created_at'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = false;
//    protected $dates = ['tgl_awal', 'tgl_akhir', 'created_at'];

    public function produk()
    {
        return $this->hasMany('App\Produk', 'id_diskon', 'id');
    }

    public function treatment()
    {
        return $this->hasMany('App\Treatment', 'id_diskon', 'id');
    }

}
