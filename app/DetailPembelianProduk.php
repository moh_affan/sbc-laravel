<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DetailPembelianProduk
 *
 * @mixin \Eloquent
 * @property int $id_transaksi
 * @property string $id_produk
 * @property float $harga_aktual
 * @property float $diskon_aktual
 * @property int $qty
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPembelianProduk whereDiskonAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPembelianProduk whereHargaAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPembelianProduk whereIdProduk($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPembelianProduk whereIdTransaksi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailPembelianProduk whereQty($value)
 * @property-read \App\PembelianProduk $pembelianProduk
 * @property-read \App\Produk $produk
 */
class DetailPembelianProduk extends Model
{
    protected $table = '_detail_pembelian_produk';
    protected $fillable = ['id_transaksi', 'id_produk', 'harga_aktual', 'diskon_aktual', 'qty'];
    protected $primaryKey = ['id_transaksi', 'id_produk'];
    public $timestamps = false;
    public $incrementing = false;

    public function pembelianProduk()
    {
        return $this->belongsTo('App\PembelianProduk', 'id_transaksi');
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk', 'id_produk');
    }
}
