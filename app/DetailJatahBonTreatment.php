<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DetailJatahBonTreatment
 *
 * @mixin \Eloquent
 * @property int $id_transaksi
 * @property float $harga_aktual
 * @property float $diskon_aktual
 * @property int $qty
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonTreatment whereDiskonAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonTreatment whereHargaAktual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonTreatment whereIdProduk($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonTreatment whereIdTransaksi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonTreatment whereQty($value)
 * @property string $id_treatment
 * @property float $penyusutan
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonTreatment whereIdTreatment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DetailJatahBonTreatment wherePenyusutan($value)
 * @property-read \App\JatahBonTreatment $jatahBonTreatment
 * @property-read \App\Treatment $treatment
 */
class DetailJatahBonTreatment extends Model
{
    protected $table = '_detail_jatah_bon_treatment';
    protected $fillable = ['id_transaksi', 'id_treatment', 'harga_aktual', 'diskon_aktual', 'qty', 'penyusutan'];
    protected $primaryKey = ['id_transaksi', 'id_produk'];
    public $timestamps = false;
    public $incrementing = false;

    public function jatahBonTreatment()
    {
        return $this->belongsTo('App\JatahBonTreatment', 'id_transaksi');
    }

    public function treatment()
    {
        return $this->belongsTo('App\Treatment', 'id_treatment');
    }
}
