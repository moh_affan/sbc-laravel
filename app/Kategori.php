<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Kategori
 *
 * @mixin \Eloquent
 * @property string $id
 * @property string $kategori
 * @method static \Illuminate\Database\Query\Builder|\App\Kategori whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Kategori whereKategori($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Produk[] $produk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Treatment[] $treatment
 */
class Kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = ['id', 'kategori'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = false;

    public function produk()
    {
        return $this->hasMany('App\Produk', 'id_kategori', 'id');
    }
    public function treatment()
    {
        return $this->hasMany('App\Treatment', 'id_kategori', 'id');
    }
}
