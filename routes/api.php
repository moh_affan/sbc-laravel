<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group(['middleware' => 'level0'], function () {
    /*
     * Pegawai
     */
    Route::get('/get-pegawai', 'PegawaiController@show');
    Route::post('/store-pegawai', 'PegawaiController@store');
    Route::patch('/update-pegawai', 'PegawaiController@update');
    Route::delete('/delete-pegawai', 'PegawaiController@delete');

    /*
     * Jabatan
     */
    Route::get('/get-jabatan', 'JabatanController@show');
    Route::post('/store-jabatan', 'JabatanController@store');
    Route::patch('/update-jabatan', 'JabatanController@update');
    Route::delete('/delete-jabatan', 'JabatanController@delete');

    /*
     * Pelanggan
     */
    Route::get('/get-pelanggan', 'PelangganController@show');
    Route::post('/store-pelanggan', 'PelangganController@store');
    Route::patch('/update-pelanggan', 'PelangganController@update');
    Route::delete('/delete-pelanggan', 'PelangganController@delete');

    /*
     * Supplier
     */
    Route::get('/get-supplier', 'SupplierController@show');
    Route::post('/store-supplier', 'SupplierController@store');
    Route::patch('/update-supplier', 'SupplierController@update');
    Route::delete('/delete-supplier', 'SupplierController@delete');

    /*
     * Kategori
     */
    Route::get('/get-kategori', 'KategoriController@show');
    Route::post('/store-kategori', 'KategoriController@store');
    Route::patch('/update-kategori', 'KategoriController@update');
    Route::delete('/delete-kategori', 'KategoriController@delete');

    /*
     * Diskon
     */
    Route::get('/get-diskon', 'DiskonController@show');
    Route::post('/store-diskon', 'DiskonController@store');
    Route::patch('/update-diskon', 'DiskonController@update');
    Route::delete('/delete-diskon', 'DiskonController@delete');

    /*
     * Produk
     */
    Route::get('/get-produk', 'ProdukController@show');
    Route::get('/get-produk-by-supplier/{supplier}', 'ProdukController@getProdukBySupplier');
    Route::get('/get-produk-all', 'ProdukController@getProdukAll');
    Route::post('/store-produk', 'ProdukController@store');
    Route::patch('/update-produk', 'ProdukController@update');
    Route::delete('/delete-produk', 'ProdukController@delete');

    /*
     * Treatment
     */
    Route::get('/get-treatment', 'TreatmentController@show');
    Route::get('/get-treatment-by-supplier/{supplier}', 'TreatmentController@getTreatmentBySupplier');
    Route::get('/get-treatment-all', 'TreatmentController@getTreatmentAll');
    Route::post('/store-treatment', 'TreatmentController@store');
    Route::patch('/update-treatment', 'TreatmentController@update');
    Route::delete('/delete-treatment', 'TreatmentController@delete');

    /*
     * Penyusutan
     */
    Route::get('/get-penyusutan', 'PenyusutanController@show');
    Route::get('/get-doesnothave', 'PenyusutanController@doesntHave');
    Route::post('/store-penyusutan', 'PenyusutanController@store');
    Route::patch('/update-penyusutan', 'PenyusutanController@update');
    Route::delete('/delete-penyusutan', 'PenyusutanController@delete');

    /*
     * Jatah
     */
    Route::get('/get-jatah', 'JatahController@show');
    Route::patch('/update-jatah', 'JatahController@update');

    /*
     * Pengguna
     */
    Route::get('/get-pengguna', 'PenggunaController@show');
    Route::post('/store-pengguna', 'PenggunaController@store');
    Route::patch('/update-pengguna', 'PenggunaController@update');
    Route::delete('/delete-pengguna', 'PenggunaController@delete');

});

Route::group(['middleware' => 'level1'], function () {
	Route::get('/get-produk-by-supplier/{supplier}', 'ProdukController@getProdukBySupplier');
    Route::get('/get-produk-all', 'ProdukController@getProdukAll');
	Route::get('/get-treatment-by-supplier/{supplier}', 'TreatmentController@getTreatmentBySupplier');
    Route::get('/get-treatment-all', 'TreatmentController@getTreatmentAll');
    /*
     * Pembelian Produk
     */
    Route::get('/pembelian-produk/get-last-id', 'PembelianProdukController@lastId');
    Route::post('/pembelian-produk/store', 'PembelianProdukController@store');
    Route::get('/pembelian-produk/transaksi-saya', 'PembelianProdukController@transasksiSaya');
    Route::get('/pembelian-produk/load-detail/{id}', 'PembelianProdukController@loadTransaction');

    /*
     * Penjualan Produk
     */
    Route::get('/penjualan-produk/get-last-id', 'PenjualanProdukController@lastId');
    Route::post('/penjualan-produk/store', 'PenjualanProdukController@store');
    Route::get('/penjualan-produk/transaksi-saya', 'PenjualanProdukController@transasksiSaya');
    Route::get('/penjualan-produk/load-detail/{id}', 'PenjualanProdukController@loadTransaction');

    /*
     * Jatah Bon Produk
     */
    Route::get('/jatah-bon-produk/get-last-id', 'JatahBonProdukController@lastId');
    Route::get('/jatah-bon-produk/get-jatah-used/{id}', 'JatahBonProdukController@getJatahUsed');
    Route::post('/jatah-bon-produk/store', 'JatahBonProdukController@store');
    Route::get('/jatah-bon-produk/transaksi-saya', 'JatahBonProdukController@transasksiSaya');
    Route::get('/jatah-bon-produk/load-detail/{id}', 'JatahBonProdukController@loadTransaction');

    /*
     * Pembelian Treatment
     */
    Route::get('/pembelian-treatment/get-last-id', 'PembelianTreatmentController@lastId');
    Route::post('/pembelian-treatment/store', 'PembelianTreatmentController@store');
    Route::get('/pembelian-treatment/transaksi-saya', 'PembelianTreatmentController@transasksiSaya');
    Route::get('/pembelian-treatment/load-detail/{id}', 'PembelianTreatmentController@loadTransaction');

    /*
     * Penjualan Treatment
     */
    Route::get('/penjualan-treatment/get-last-id', 'PenjualanTreatmentController@lastId');
    Route::post('/penjualan-treatment/store', 'PenjualanTreatmentController@store');
    Route::get('/penjualan-treatment/transaksi-saya', 'PenjualanTreatmentController@transasksiSaya');
    Route::get('/penjualan-treatment/load-detail/{id}', 'PenjualanTreatmentController@loadTransaction');

    /*
     * Jatah Bon Treatment
     */
    Route::get('/jatah-bon-treatment/get-last-id', 'JatahBonTreatmentController@lastId');
    Route::get('/jatah-bon-treatment/get-jatah-used/{id}', 'JatahBonTreatmentController@getJatahUsed');
    Route::post('/jatah-bon-treatment/store', 'JatahBonTreatmentController@store');
    Route::get('/jatah-bon-treatment/transaksi-saya', 'JatahBonTreatmentController@transasksiSaya');
    Route::get('/jatah-bon-treatment/load-detail/{id}', 'JatahBonTreatmentController@loadTransaction');
});

Route::group(['middleware' => 'level2'], function () {
    /*
     * Laporan Pembelian Produk
     */
    Route::get('/pembelian-produk/today', 'Reports\PembelianProdukController@hariini');
    Route::get('/pembelian-produk/bulanini', 'Reports\PembelianProdukController@bulanini');
    Route::get('/pembelian-produk/{start}/{end}', 'Reports\PembelianProdukController@range');

    /*
     * Laporan Penjualan Produk
     */
    Route::get('/penjualan-produk/today', 'Reports\PenjualanProdukController@hariini');
    Route::get('/penjualan-produk/bulanini', 'Reports\PenjualanProdukController@bulanini');
    Route::get('/penjualan-produk/{start}/{end}', 'Reports\PenjualanProdukController@range');

    /*
     * Laporan Jatah Bon Produk
     */
    Route::get('/jatahbon-produk/today', 'Reports\JatahBonProdukController@hariini');
    Route::get('/jatahbon-produk/bulanini', 'Reports\JatahBonProdukController@bulanini');
    Route::get('/jatahbon-produk/{start}/{end}', 'Reports\JatahBonProdukController@range');

    /*
     * Laporan Stok Produk
     */
    Route::get('/stok-produk/today', 'Reports\StokProdukController@hariini');
    Route::get('/stok-produk/bulanini', 'Reports\StokProdukController@bulanini');
    Route::get('/stok-produk/{start}/{end}', 'Reports\StokProdukController@range');

    /*
     * Laporan Pendapatan Produk
     */
    Route::get('/pendapatan-produk/today', 'Reports\PendapatanProdukController@hariini');
    Route::get('/pendapatan-produk/bulanini', 'Reports\PendapatanProdukController@bulanini');
    Route::get('/pendapatan-produk/{start}/{end}', 'Reports\PendapatanProdukController@range');
    
    /*
     * Laporan Pembelian Treatment
     */
    Route::get('/pembelian-treatment/today', 'Reports\PembelianTreatmentController@hariini');
    Route::get('/pembelian-treatment/bulanini', 'Reports\PembelianTreatmentController@bulanini');
    Route::get('/pembelian-treatment/{start}/{end}', 'Reports\PembelianTreatmentController@range');

    /*
     * Laporan Penjualan Treatment
     */
    Route::get('/penjualan-treatment/today', 'Reports\PenjualanTreatmentController@hariini');
    Route::get('/penjualan-treatment/bulanini', 'Reports\PenjualanTreatmentController@bulanini');
    Route::get('/penjualan-treatment/{start}/{end}', 'Reports\PenjualanTreatmentController@range');

    /*
     * Laporan Jatah Bon Treatment
     */
    Route::get('/jatahbon-treatment/today', 'Reports\JatahBonTreatmentController@hariini');
    Route::get('/jatahbon-treatment/bulanini', 'Reports\JatahBonTreatmentController@bulanini');
    Route::get('/jatahbon-treatment/{start}/{end}', 'Reports\JatahBonTreatmentController@range');

    /*
     * Laporan Penyusutan Treatment
     */
    Route::get('/penyusutan-treatment/today', 'Reports\PenyusutanTreatmentController@hariini');
    Route::get('/penyusutan-treatment/bulanini', 'Reports\PenyusutanTreatmentController@bulanini');
    Route::get('/penyusutan-treatment/{start}/{end}', 'Reports\PenyusutanTreatmentController@range');

    /*
     * Laporan Stok Treatment
     */
    Route::get('/stok-treatment/today', 'Reports\StokTreatmentController@hariini');
    Route::get('/stok-treatment/tes', 'Reports\StokTreatmentController@tes');
    Route::get('/stok-treatment/bulanini', 'Reports\StokTreatmentController@bulanini');
    Route::get('/stok-treatment/{start}/{end}', 'Reports\StokTreatmentController@range');

    /*
     * Laporan Pendapatan Treatment
     */
    Route::get('/pendapatan-treatment/today', 'Reports\PendapatanTreatmentController@hariini');
    Route::get('/pendapatan-treatment/bulanini', 'Reports\PendapatanTreatmentController@bulanini');
    Route::get('/pendapatan-treatment/{start}/{end}', 'Reports\PendapatanTreatmentController@range');
});