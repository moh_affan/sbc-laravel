<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@showLogin');
Route::post('/', 'LoginController@login')->name('prosesLogin');
Route::get('logout', function () {
    Request::session()->forget('id');
    Request::session()->forget('username');
    Request::session()->forget('id_pegawai');
    Request::session()->forget('level');
    Request::session()->forget('nama');
    return Redirect::to('/');
});
Route::get('setting', 'SettingController@index');
Route::patch('setting', 'SettingController@update');

Route::group(['prefix' => '0', 'middleware' => \App\Http\Middleware\LevelNolApi::class], function () {
    Route::get('/dashboard', function () {
        return view('l0.dashboard');
    });
    /*
     * Master
     */
    Route::get('/pegawai', 'PegawaiController@index')->name('pegawai');
    Route::get('/jabatan', 'JabatanController@index')->name('jabatan');
    Route::get('/pelanggan', 'PelangganController@index')->name('pelanggan');
    Route::get('/supplier', 'SupplierController@index')->name('supplier');
    Route::get('/kategori', 'KategoriController@index')->name('kategori');
    Route::get('/diskon', 'DiskonController@index')->name('diskon');
    Route::get('/produk', 'ProdukController@index')->name('produk');
    Route::get('/treatment', 'TreatmentController@index')->name('treatment');
    Route::get('/penyusutan', 'PenyusutanController@index')->name('penyusutan');
    Route::get('/jatah', 'JatahController@index')->name('jatah');
    Route::get('/pengguna', 'PenggunaController@index')->name('pengguna');

    /*
     * Transaksi
     */
    Route::get('/transaksi/pembelian-produk', 'PembelianProdukController@index');
    Route::get('/transaksi/penjualan-produk', 'PenjualanProdukController@index');
    Route::get('/transaksi/jatah-bon-produk', 'JatahBonProdukController@index');
    Route::get('/transaksi/pembelian-treatment', 'PembelianTreatmentController@index');
    Route::get('/transaksi/penjualan-treatment', 'PenjualanTreatmentController@index');
    Route::get('/transaksi/jatah-bon-treatment', 'JatahBonTreatmentController@index');
    Route::get('/transaksi/penjualan-produk/print/{id}', 'PenjualanProdukController@printTransaction');
    Route::get('/transaksi/jatah-bon-produk/print/{id}', 'JatahBonProdukController@printTransaction');
    Route::get('/transaksi/penjualan-treatment/print/{id}', 'PenjualanTreatmentController@printTransaction');
    Route::get('/transaksi/jatah-bon-treatment/print/{id}', 'JatahBonTreatmentController@printTransaction');

    /*
     * Laporan
     */
    Route::get('/laporan/pembelian-produk', 'Reports\PembelianProdukController@index');
    Route::get('/laporan/penjualan-produk', 'Reports\PenjualanProdukController@index');
    Route::get('/laporan/jatahbon-produk', 'Reports\JatahBonProdukController@index');
    Route::get('/laporan/stok-produk', 'Reports\StokProdukController@index');
    Route::get('/laporan/pendapatan-produk', 'Reports\PendapatanProdukController@index');
    Route::get('/laporan/pembelian-treatment', 'Reports\PembelianTreatmentController@index');
    Route::get('/laporan/penjualan-treatment', 'Reports\PenjualanTreatmentController@index');
    Route::get('/laporan/jatahbon-treatment', 'Reports\JatahBonTreatmentController@index');
    Route::get('/laporan/penyusutan-treatment', 'Reports\PenyusutanTreatmentController@index');
    Route::get('/laporan/stok-treatment', 'Reports\StokTreatmentController@index');
    Route::get('/laporan/pendapatan-treatment', 'Reports\PendapatanTreatmentController@index');

    /*
     * Stok
     */
    Route::get('/stok/produk','StokProdukController@index');
    Route::get('/stok/treatment','StokTreatmentController@index');

});
Route::group(['prefix' => '1', 'middleware' => \App\Http\Middleware\LevelOneApi::class], function () {
    Route::get('/dashboard', function () {
        return view('l1.dashboard');
    });
    /*
     * Transaksi
     */
    Route::get('/transaksi/pembelian-produk', 'PembelianProdukController@index');
    Route::get('/transaksi/penjualan-produk', 'PenjualanProdukController@index');
    Route::get('/transaksi/jatah-bon-produk', 'JatahBonProdukController@index');
    Route::get('/transaksi/pembelian-treatment', 'PembelianTreatmentController@index');
    Route::get('/transaksi/penjualan-treatment', 'PenjualanTreatmentController@index');
    Route::get('/transaksi/jatah-bon-treatment', 'JatahBonTreatmentController@index');
    Route::get('/transaksi/penjualan-produk/print/{id}', 'PenjualanProdukController@printTransaction');
    Route::get('/transaksi/jatah-bon-produk/print/{id}', 'JatahBonProdukController@printTransaction');
    Route::get('/transaksi/penjualan-treatment/print/{id}', 'PenjualanTreatmentController@printTransaction');
    Route::get('/transaksi/jatah-bon-treatment/print/{id}', 'JatahBonTreatmentController@printTransaction');

    /*
     * Stok
     */
    Route::get('/stok/produk','StokProdukController@index');
    Route::get('/stok/treatment','StokTreatmentController@index');
});

Route::group(['prefix' => '2', 'middleware' => \App\Http\Middleware\LevelOneApi::class], function () {
    Route::get('/dashboard', function () {
        return view('l2.dashboard');
    });

    /*
     * Laporan
     */
    Route::get('/laporan/pembelian-produk', 'Reports\PembelianProdukController@index');
    Route::get('/laporan/penjualan-produk', 'Reports\PenjualanProdukController@index');
    Route::get('/laporan/jatahbon-produk', 'Reports\JatahBonProdukController@index');
    Route::get('/laporan/stok-produk', 'Reports\StokProdukController@index');
    Route::get('/laporan/pendapatan-produk', 'Reports\PendapatanProdukController@index');
    Route::get('/laporan/pembelian-treatment', 'Reports\PembelianTreatmentController@index');
    Route::get('/laporan/penjualan-treatment', 'Reports\PenjualanTreatmentController@index');
    Route::get('/laporan/jatahbon-treatment', 'Reports\JatahBonTreatmentController@index');
    Route::get('/laporan/penyusutan-treatment', 'Reports\PenyusutanTreatmentController@index');
    Route::get('/laporan/stok-treatment', 'Reports\StokTreatmentController@index');
    Route::get('/laporan/pendapatan-treatment', 'Reports\PendapatanTreatmentController@index');

    /*
     * Stok
     */
    Route::get('/stok/produk','StokProdukController@index');
    Route::get('/stok/treatment','StokTreatmentController@index');
});

//Testing
Route::get('test', function () {
    $produk = \App\Treatment::find('TR001');
    echo $produk->nama . ' ' . $produk->supplier->supplier . ' ' . $produk->kategori->kategori . ' ' . $produk->diskon->nama . ' ' . $produk->stokTreatment->stok . '<br>';
    echo \Carbon\Carbon::now()->format('Y-m-d H:i:s').'<br>';
    echo date('m').'<br>';

});
