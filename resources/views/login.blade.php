<!DOCTYPE html>
<html class="bg-blur">
<head>
    <meta charset="UTF-8">
    <title>SBC | Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- font Awesome -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="{{asset('css/AdminLTE.css')}}" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bg-blur">

<div class="form-box" id="login-box">
    <h2 style="display: block;text-align: center;color: white">Soraya Beauty Center</h2>
    <div class="header">Silahkan Login</div>
    @if(\Illuminate\Support\Facades\Cookie::get('username','')!='') {{\Illuminate\Support\Facades\Cookie::get('username','')}} @endif
    {!! Form::open(['route'=>'prosesLogin','method'=>'POST','id'=>'login']) !!}
    <div class="body bg-gray">
        <br>
        <br>
        <div class="username error-place">
            @if($errors->has('Username'))
                <span class="text-danger login-error">{{$errors->get('Username')[0]}}</span>
            @endif
        </div>
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-user"></i>
            </div>
            <input type="text" name="Username" id="username" class="form-control" placeholder="Username"/>
        </div>
        <br>
        <div class="password error-place">
            @if($errors->has('Password'))
                <span class="text-danger login-error">{{$errors->get('Password')[0]}}</span>
            @endif
        </div>
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-key"></i>
            </div>
            <input type="password" name="Password" id="password" class="form-control" placeholder="Password"/>
        </div>
        <br>
        <br>
    </div>
    <div class="footer">
        <button type="submit" class="btn bg-olive btn-block">Login</button>
    </div>
    {!! Form::close() !!}
</div>


<!-- jQuery 2.0.2 -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('#username').blur(function () {
            var span = $('<span></span>');
            span.addClass("text-danger login-error");
            if (!$(this).val()) {
                span.text("Username harus diisi");
                $('body').find('.username.error-place').html(span);
            } else {
                $('body').find('.username.error-place').empty();
            }
        });

        $('#password').blur(function () {
            var span = $('<span></span>');
            span.addClass("text-danger login-error");
            if (!$(this).val()) {
                span.text("Password harus diisi");
                $('body').find('.password.error-place').html(span);
            } else {
                $('body').find('.password.error-place').empty();
            }
        });

        $('#username').keyup(function () {
            if ($(this).val())
                $('body').find('.username.error-place').empty();
        });

        $('#password').keyup(function () {
            if ($(this).val())
                $('body').find('.password.error-place').empty();
        });
    });
</script>
</body>
</html>