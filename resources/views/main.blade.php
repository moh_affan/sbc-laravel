<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{$pengguna->pegawai->nama}} | @if(isset($title)){{$title}}@else SBC @endif</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- font Awesome -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Ionicons -->
    <link href="{{asset('css/ionicons.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- fullCalendar -->
    <link href="{{asset('css/fullcalendar/fullcalendar.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/jQueryUI/jquery-ui-1.10.3.custom.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/jquery-confirm/jquery-confirm.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('css/datatables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatables/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-datepicker/bootstrap-datepicker3.css')}}" rel="stylesheet">
    <link href="{{asset('css/daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{asset('css/AdminLTE.css')}}" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="{{url('/')}}" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        SBC
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span>{{$pengguna->pegawai->nama}} <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            <img src="{{asset('img/avatar3.png')}}" class="img-circle" alt="User Image"/>
                            <p>
                                {{$pengguna->pegawai->nama}} - {{$pengguna->pegawai->jabatan->jabatan}}
                                <small>Soraya Beauty Center</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-12 text-center">

                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{url('setting')}}" class="btn btn-default btn-flat"><span class="fa fa-cog"></span> Setting</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{url('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
@if($pengguna->level=='0')
    @include('l0.sidebar')
@elseif($pengguna->level=='1')
    @include('l1.sidebar')
@endif

<!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        {{--<div class="loading-box">--}}
            {{--<img class="img-thumbnail" src="{{asset('img/ajax-loader1.gif')}}">--}}
        {{--</div>--}}
        <!-- Content Header (Page header) -->
        @yield('konten');
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->


<!-- jQuery 2.0.2 -->
<script src="{{asset('js/jquery.min.js')}}" type="text/javascript"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{asset('js/jquery-ui-1.10.3.min.js')}}" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<!-- fullCalendar -->
<script src="{{asset('js/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{asset('js/plugins/jqueryConfirm/jquery-confirm.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/plugins/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('js/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{asset('js/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('js/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('js/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/plugins/bootstrap-datepicker/bootstrap-datepicker.id.min.js')}}"></script>
<script src="{{asset('js/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('js/AdminLTE/app.js')}}" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('js/AdminLTE/dashboard.js')}}" type="text/javascript"></script>

@yield('script');
</body>
</html>