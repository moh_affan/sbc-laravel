@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Treatment
            <small>Data Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-th"></i> Home</a></li>
            <li>Data Master</li>
            <li class="active">Treatment</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-place">

                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Treatment</h3>
                        <div class="pull-right box-tools">
                            <div data-toggle="tooltip" title="Tambah data treatment baru"
                                 data-original-title="Tambah data treatment baru">
                                <button class="btn btn-primary btn-sm" type="button" data-target="#tambah-treatment"
                                        data-toggle="modal"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover datatable">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Kategori</th>
                                <th>Supplier</th>
                                <th>Nama Treatment</th>
                                <th>Harga Jual</th>
                                <th>Harga Beli</th>
                                <th>Diskon</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($treatment as $item)
                                <tr class="fade in">
                                    <td class="id">{{$item->id}}</td>
                                    <td class="kategori">{{$item->kategori->kategori}} ({{$item->id_kategori}})</td>
                                    <td class="supplier">{{$item->supplier->supplier}} ({{$item->id_supplier}})</td>
                                    <td class="nama">{{$item->nama}}</td>
                                    <td class="ref_harga_jual">{{$item->ref_harga_jual}}</td>
                                    <td class="ref_harga_beli">{{$item->ref_harga_beli}}</td>
                                    <td class="diskon">{{$item->diskon->nama}} ({{$item->id_diskon}})</td>
                                    <td class="aksi-edit hidden-print">
                                        <button data-target=".edit-treatment"
                                                data-toggle="modal" class="btn btn-success btn-sm edit" type="button"
                                                value='{"id":"{{$item->id}}","id_kategori":"{{$item->id_kategori}}","id_supplier":"{{$item->id_supplier}}","nama":"{{$item->nama}}","ref_harga_jual":"{{$item->ref_harga_jual}}","ref_harga_beli":"{{$item->ref_harga_beli}}","id_diskon":"{{$item->id_diskon}}"}'>
                                            <span class="fa fa-edit"></span></button>
                                    </td>
                                    <td class="aksi-hapus hidden-print">
                                        {!! Form::open(['method'=>'DELETE','id'=>$item->id]) !!}
                                        {!! Form::hidden('id',$item->id,[]) !!}
                                        {!! Form::close() !!}
                                        <button value="{{$item->id}}" class="btn btn-danger btn-sm delete"
                                                type="button"><span class="fa fa-trash-o"></span></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Kategori</th>
                                <th>Supplier</th>
                                <th>Nama Treatment</th>
                                <th>Harga Jual</th>
                                <th>Harga Beli</th>
                                <th>Diskon</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    {{--Tambah treatment--}}
    <div class="modal fade" id="tambah-treatment" tabindex="-1" role="dialog" aria-labelledby="tambah-treatment-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-treatment-label">Tambah Treatment</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','class'=>'form form-treatment','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('id','ID') !!}
                        {!! Form::text('id',null,['class'=>'form-control','placeholder'=>'Masukkan ID','required']) !!}
                    </div>
                    @php
                        $choices = [''=>'--Pilihan Kategori--'];
                        foreach ($kategori as $ch){
                            $choices[$ch->id]=$ch->kategori;
                        }
                    @endphp
                    <div class="form-group">
                        {!! Form::label('id_kategori','Kategori') !!}
                        {!! Form::select('id_kategori',$choices,null,['class'=>'form-control','required']) !!}
                    </div>
                    @php
                        $choices = [''=>'--Pilihan Supplier--'];
                        foreach ($supplier as $ch){
                            $choices[$ch->id]=$ch->supplier;
                        }
                    @endphp
                    <div class="form-group">
                        {!! Form::label('id_supplier','Supplier') !!}
                        {!! Form::select('id_supplier',$choices,null,['class'=>'form-control','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('nama','Nama') !!}
                        {!! Form::text('nama',null,['class'=>'form-control','placeholder'=>'Masukkan Nama', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ref_harga_jual','Harga Jual') !!}
                        {!! Form::number('ref_harga_jual',null,['class'=>'form-control','placeholder'=>'Nominal Harga Jual','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ref_harga_beli','Harga Beli') !!}
                        {!! Form::number('ref_harga_beli',null,['class'=>'form-control','placeholder'=>'Nominal Harga Beli','required']) !!}
                    </div>
                    @php
                        $choices = [''=>'--Pilihan Diskon--'];
                        foreach ($diskon as $ch){
                            $choices[$ch->id]=$ch->nama;
                        }
                    @endphp
                    <div class="form-group">
                        {!! Form::label('id_diskon','Diskon') !!}
                        {!! Form::select('id_diskon',$choices,null,['class'=>'form-control','required']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary submit']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--Edit Treatment--}}
    <div class="modal fade edit-treatment" tabindex="-1" role="dialog" aria-labelledby="tambah-treatment-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-treatment-label">Edit Treatment</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'PATCH','class'=>'form form-edit-treatment','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('id','ID') !!}
                        <p class="form-control"></p>
                        {!! Form::hidden('id',null,['required']) !!}
                    </div>
                    @php
                        $choices = [''=>'--Pilihan Kategori--'];
                        foreach ($kategori as $ch){
                            $choices[$ch->id]=$ch->kategori;
                        }
                    @endphp
                    <div class="form-group">
                        {!! Form::label('id_kategori','Kategori') !!}
                        {!! Form::select('id_kategori',$choices,null,['class'=>'form-control','required']) !!}
                    </div>
                    @php
                        $choices = [''=>'--Pilihan Supplier--'];
                        foreach ($supplier as $ch){
                            $choices[$ch->id]=$ch->supplier;
                        }
                    @endphp
                    <div class="form-group">
                        {!! Form::label('id_supplier','Supplier') !!}
                        {!! Form::select('id_supplier',$choices,null,['class'=>'form-control','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('nama','Nama') !!}
                        {!! Form::text('nama',null,['class'=>'form-control','placeholder'=>'Masukkan Nama', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ref_harga_jual','Harga Jual') !!}
                        {!! Form::number('ref_harga_jual',null,['class'=>'form-control','placeholder'=>'Nominal Harga Jual','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ref_harga_beli','Harga Beli') !!}
                        {!! Form::number('ref_harga_beli',null,['class'=>'form-control','placeholder'=>'Nominal Harga Beli','required']) !!}
                    </div>
                    @php
                        $choices = [''=>'--Pilihan Diskon--'];
                        foreach ($diskon as $ch){
                            $choices[$ch->id]=$ch->nama;
                        }
                    @endphp
                    <div class="form-group">
                        {!! Form::label('id_diskon','Diskon') !!}
                        {!! Form::select('id_diskon',$choices,null,['class'=>'form-control','required']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary apply']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(function () {
            $.fn.dataTable.Buttons.swfPath = window.location.protocol + '//' + window.location.hostname + '/swf/flashExport.swf';
            var urlget = window.location.protocol + '//' + window.location.hostname + '/api/get-treatment';
            var datatable = $('body').find('.datatable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                dom: 'Blfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Data Master Treatment',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'pdfHtml5',
                    title: 'Data Master Treatment',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'print',
                    footer: true,
                    title: 'Data Master Treatment',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }],
            });
            var form = $('body').find('.form-treatment');
            var formEdit = $('body').find('.form-edit-treatment');
            var url = window.location.protocol + '//' + window.location.hostname + '/api/store-treatment';
            var urlEdit = window.location.protocol + '//' + window.location.hostname + '/api/update-treatment';
            var urlDelete = window.location.protocol + '//' + window.location.hostname + '/api/delete-treatment';
            form.submit(function () {
                var data = form.serialize();
                $.post(url, data)
                    .done(function (d) {
                        $('#tambah-treatment').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true,
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data treatment berhasil ditambahkan.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        datatable.row.add([d.id, d.kategori + ' (' + d.id_kategori + ')', d.supplier + ' (' + d.id_supplier + ')', d.nama, d.ref_harga_jual, d.ref_harga_beli, d.diskon + ' (' + d.id_diskon + ')', '<button data-target=".edit-treatment" data-toggle="modal" class="btn btn-success btn-sm edit" type="button" value=\'{"id":"' + d.id + '","id_kategori":"' + d.id_kategori + '","id_supplier":"' + d.id_supplier + '","nama":"' + d.nama + '","ref_harga_jual":"' + d.ref_harga_jual + '", "ref_harga_beli":"' + d.ref_harga_beli + '","id_diskon":"' + d.id_diskon + '"}\'><span class="fa fa-edit"></span></button>', '<form method="POST" accept-charset="UTF-8" id="' + d.id + '"><input name="_method" value="DELETE" type="hidden">{!! csrf_field() !!}<input name="id" value="' + d.id + '" type="hidden"></form><button value="' + d.id + '" class="btn btn-danger btn-sm delete" type="button"><span class="fa fa-trash-o"></span></button>']).draw(false);
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.id_kategori !== undefined) {
                            var a, b;
                            a = $('body').find('[name="id_kategori"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id_kategori.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.id_supplier !== undefined) {
                            var a, b;
                            a = $('body').find('[name="id_supplier"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id_supplier.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.nama !== undefined) {
                            var a, b;
                            a = $('body').find('[name="nama"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.nama.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.ref_harga_jual !== undefined) {
                            var a, b;
                            a = $('body').find('[name="ref_harga_jual"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.ref_harga_jual.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.ref_harga_beli !== undefined) {
                            var a, b;
                            a = $('body').find('[name="ref_harga_beli"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.ref_harga_beli.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.id_diskon !== undefined) {
                            var a, b;
                            a = $('body').find('[name="id_diskon"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id_diskon.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.submit').click(function () {
                $('.ajax-loader').removeClass('hide');
                form.submit();
            });
            var currentRow = undefined;
            $('body').find('.datatable').on('click', '.edit', function () {
                currentRow = $(this).parent().parent();
                var str = $(this).val();
                var dj = $.parseJSON(str);
                formEdit.find('[name="id"]').val(dj.id);
                formEdit.find('p.form-control').text(dj.id);
                formEdit.find('[name="nama"]').val(dj.nama);
                formEdit.find('[name="id_kategori"]').val(dj.id_kategori);
                formEdit.find('[name="id_supplier"]').val(dj.id_supplier);
                formEdit.find('[name="ref_harga_jual"]').val(dj.ref_harga_jual);
                formEdit.find('[name="ref_harga_beli"]').val(dj.ref_harga_beli);
                formEdit.find('[name="id_diskon"]').val(dj.id_diskon);
                datatable.draw();
            });
            formEdit.submit(function () {
                var data = formEdit.serialize();
                $.post(urlEdit, data)
                    .done(function (d) {
                        $('.edit-treatment').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data treatment berhasil diubah.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        //ubah tr disini
                        currentRow.find('.id').text(d.id);
                        currentRow.find('.kategori').text(d.kategori + ' (' + d.id_kategori + ')');
                        currentRow.find('.supplier').text(d.supplier + ' (' + d.id_supplier + ')');
                        currentRow.find('.nama').text(d.nama);
                        currentRow.find('.ref_harga_jual').text(d.ref_harga_jual);
                        currentRow.find('.ref_harga_beli').text(d.ref_harga_beli);
                        currentRow.find('.diskon').text(d.diskon + ' (' + d.id_diskon + ')');
                        currentRow.find('.aksi-edit').find('.edit').val('{"id":"' + d.id + '","id_kategori":"' + d.id_kategori + '","id_supplier":"' + d.id_supplier + '","nama":"' + d.nama + '","ref_harga_jual":"' + d.ref_harga_jual + '", "ref_harga_beli":"' + d.ref_harga_beli + '","id_diskon":"' + d.id_diskon + '"}');
                        datatable.draw();
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.id_kategori !== undefined) {
                            var a, b;
                            a = $('body').find('[name="id_kategori"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id_kategori.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.id_supplier !== undefined) {
                            var a, b;
                            a = $('body').find('[name="id_supplier"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id_supplier.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.nama !== undefined) {
                            var a, b;
                            a = $('body').find('[name="nama"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.nama.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.ref_harga_jual !== undefined) {
                            var a, b;
                            a = $('body').find('[name="ref_harga_jual"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.ref_harga_jual.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.ref_harga_beli !== undefined) {
                            var a, b;
                            a = $('body').find('[name="ref_harga_beli"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.ref_harga_beli.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.id_diskon !== undefined) {
                            var a, b;
                            a = $('body').find('[name="id_diskon"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id_diskon.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.apply').click(function () {
                $('.ajax-loader').removeClass('hide');
                formEdit.submit();
            });
            $('body').find('.datatable').on('click', '.delete', function () {
                var btnn = $(this);
                var jc = $.confirm({
                    title: 'Menghapus data',
                    content: 'Apa Anda yakin ingin menghapus data treatment dg ID ' + btnn.val(),
                    buttons: {
                        confirm: function () {
                            var data = $('#' + btnn.val()).serialize();
                            $.post(urlDelete, data)
                                .done(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-success alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-check');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Sukses!</b> Data treatment berhasil dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                    //hapus tr disini
//                                    btnn.parent().parent().remove();
                                    var row = datatable.row(btnn.parents('tr'));
                                    row.remove().draw();
                                })
                                .fail(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-danger alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-ban');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Gagal!</b> Data treatment gagal dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                })
                                .always(function () {
                                    console.log('finish');
                                });
                        },
                        cancel: function () {

                        }
                    }
                });
            });
        });
    </script>
@stop