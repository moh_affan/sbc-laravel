@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Jatah
            <small>Data Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-th"></i> Home</a></li>
            <li>Data Master</li>
            <li class="active">Jatah</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-place">

                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Jatah</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover datatable">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nominal</th>
                                <th>Keterangan</th>
                                <th class="hidden-print">Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($jatah as $item)
                                <tr class="fade in">
                                    <td class="id">{{$item->id}}</td>
                                    <td class="nominal">{{$item->nominal}}</td>
                                    <td class="keterangan">{{$item->keterangan}}</td>
                                    <td class="aksi-edit hidden-print">
                                        <button data-target=".edit-jatah"
                                                data-toggle="modal" class="btn btn-success btn-sm edit" type="button"
                                                value='{"id":"{{$item->id}}","nominal":"{{$item->nominal}}","keterangan":"{{$item->keterangan}}"}'><span
                                                    class="fa fa-edit"></span></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Nominal</th>
                                <th>Keterangan</th>
                                <th class="hidden-print">Edit</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>

    {{--Edit Jatah--}}
    <div class="modal fade edit-jatah" tabindex="-1" role="dialog" aria-labelledby="tambah-jatah-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-jatah-label">Edit Jatah</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'PATCH','class'=>'form form-edit-jatah','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('id','ID Jatah') !!}
                        <p class="form-control"></p>
                        {!! Form::hidden('id',null,['required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('nominal','Nominal') !!}
                        {!! Form::text('nominal',null,['class'=>'form-control','placeholder'=>'Masukkan Nama Jatah', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('keterangan','ID Jatah') !!}
                        <span class="form-control"></span>
                        {!! Form::hidden('keterangan',null,['required']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary apply']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(function () {
            $.fn.dataTable.Buttons.swfPath = window.location.protocol + '//' + window.location.hostname + '/swf/flashExport.swf';
            var datatable = $('body').find('.datatable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                dom: 'Blfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Data Master Pegawai',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'pdfHtml5',
                    title: 'Data Master Pegawai',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'print',
                    title: 'Data Master Pegawai',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }],
            });
            var formEdit = $('body').find('.form-edit-jatah');
            var urlEdit = window.location.protocol + '//' + window.location.hostname + '/api/update-jatah';
            var currentRow = undefined;
            $('body').find('.datatable').on('click', '.edit', function () {
                currentRow = $(this).parent().parent();
                var str = $(this).val();
                var dj = $.parseJSON(str);
                formEdit.find('[name="id"]').val(dj.id);
                formEdit.find('p.form-control').text(dj.id);
                formEdit.find('[name="nominal"]').val(dj.nominal);
                formEdit.find('[name="keterangan"]').val(dj.keterangan);
                formEdit.find('span.form-control').text(dj.keterangan);
                datatable.draw();
            });
            formEdit.submit(function () {
                var data = formEdit.serialize();
                $.post(urlEdit, data)
                    .done(function (d) {
                        $('.edit-jatah').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true,
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data jatah berhasil diubah.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        //ubah tr disini
                        currentRow.find('.id').text(d.id);
                        currentRow.find('.nominal').text(d.nominal);
                        currentRow.find('.keterangan').text(d.keterangan);
                        currentRow.find('.aksi-edit').find('.edit').val('{"id":"' + d.id + '","nominal":"' + d.nominal + '", "keterangan":"' + d.keterangan + '"}');
                        datatable.draw();
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.nominal !== undefined) {
                            var a, b;
                            a = $('body').find('[name="nominal"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.nominal.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.keterangan !== undefined) {
                            var a, b;
                            a = $('body').find('[name="keterangan"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.keterangan.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.apply').click(function () {
                $('.ajax-loader').removeClass('hide');
                formEdit.submit();
            });
        });
    </script>
@stop