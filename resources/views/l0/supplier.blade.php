@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Supplier
            <small>Data Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-th"></i> Home</a></li>
            <li>Data Master</li>
            <li class="active">Supplier</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-place">

                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Supplier</h3>
                        <div class="pull-right box-tools">
                            <div data-toggle="tooltip" title="Tambah data supplier baru"
                                 data-original-title="Tambah data supplier baru">
                                <button class="btn btn-primary btn-sm" type="button" data-target="#tambah-supplier"
                                        data-toggle="modal"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover datatable">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($supplier as $item)
                                <tr class="fade in">
                                    <td class="id">{{$item->id}}</td>
                                    <td class="nama">{{$item->supplier}}</td>
                                    <td class="alamat">{{$item->alamat}}</td>
                                    <td class="telp">{{$item->telp}}</td>
                                    <td class="aksi-edit hidden-print">
                                        <button data-target=".edit-supplier"
                                                data-toggle="modal" class="btn btn-success btn-sm edit" type="button"
                                                value='{"id":"{{$item->id}}","supplier":"{{$item->supplier}}","telp":"{{$item->telp}}","alamat":"{{$item->alamat}}"}'>
                                            <span class="fa fa-edit"></span></button>
                                    </td>
                                    <td class="aksi-hapus hidden-print">
                                        {!! Form::open(['method'=>'DELETE','id'=>$item->id]) !!}
                                        {!! Form::hidden('id',$item->id,[]) !!}
                                        {!! Form::close() !!}
                                        <button value="{{$item->id}}" class="btn btn-danger btn-sm delete"
                                                type="button"><span class="fa fa-trash-o"></span></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    {{--Tambah supplier--}}
    <div class="modal fade" id="tambah-supplier" tabindex="-1" role="dialog" aria-labelledby="tambah-supplier-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-supplier-label">Tambah Supplier</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','class'=>'form form-supplier','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('id','ID') !!}
                        {!! Form::text('id',null,['class'=>'form-control','placeholder'=>'Masukkan ID','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('supplier','Nama Supplier') !!}
                        {!! Form::text('supplier',null,['class'=>'form-control','placeholder'=>'Masukkan Nama Supplier', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('alamat','Alamat') !!}
                        {!! Form::text('alamat',null,['class'=>'form-control','placeholder'=>'Masukkan Alamat','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('telp','Telepon/HP') !!}
                        {!! Form::text('telp',null,['class'=>'form-control','placeholder'=>'Masukkan No. Telepon/HP','required']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary submit']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--Edit Supplier--}}
    <div class="modal fade edit-supplier" tabindex="-1" role="dialog" aria-labelledby="tambah-supplier-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-supplier-label">Edit Supplier</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'PATCH','class'=>'form form-edit-supplier','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('id','ID') !!}
                        <p class="form-control"></p>
                        {!! Form::hidden('id',null,['required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('supplier','Nama Supplier') !!}
                        {!! Form::text('supplier',null,['class'=>'form-control','placeholder'=>'Masukkan Nama Supplier', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('alamat','Alamat') !!}
                        {!! Form::text('alamat',null,['class'=>'form-control','placeholder'=>'Masukkan Alamat','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('telp','Telepon/HP') !!}
                        {!! Form::text('telp',null,['class'=>'form-control','placeholder'=>'Masukkan No. Telepon/HP','required']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary apply']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(function () {
            $.fn.dataTable.Buttons.swfPath = window.location.protocol + '//' + window.location.hostname + '/swf/flashExport.swf';
            var urlget = window.location.protocol + '//' + window.location.hostname + '/api/get-supplier';
            var datatable = $('body').find('.datatable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                dom: 'Blfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Data Master Supplier',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'pdfHtml5',
                    title: 'Data Master Supplier',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'print',
                    footer: true,
                    title: 'Data Master Supplier',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }],
            });
            var form = $('body').find('.form-supplier');
            var formEdit = $('body').find('.form-edit-supplier');
            var url = window.location.protocol + '//' + window.location.hostname + '/api/store-supplier';
            var urlEdit = window.location.protocol + '//' + window.location.hostname + '/api/update-supplier';
            var urlDelete = window.location.protocol + '//' + window.location.hostname + '/api/delete-supplier';
            form.submit(function () {
                var data = form.serialize();
                $.post(url, data)
                    .done(function (d) {
                        $('#tambah-supplier').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true,
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data supplier berhasil ditambahkan.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        datatable.row.add([d.id, d.supplier,d.alamat, d.telp, '<button data-target=".edit-supplier" data-toggle="modal" class="btn btn-success btn-sm edit" type="button" value=\'{"id":"' + d.id + '","supplier":"' + d.supplier + '", "telp":"' + d.telp + '","alamat":"' + d.alamat + '"}\'><span class="fa fa-edit"></span></button>', '<form method="POST" accept-charset="UTF-8" id="' + d.id + '"><input name="_method" value="DELETE" type="hidden">{!! csrf_field() !!}<input name="id" value="' + d.id + '" type="hidden"></form><button value="' + d.id + '" class="btn btn-danger btn-sm delete" type="button"><span class="fa fa-trash-o"></span></button>']).draw(false);
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.supplier !== undefined) {
                            var a, b;
                            a = $('body').find('[name="supplier"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.supplier.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.telp !== undefined) {
                            var a, b;
                            a = $('body').find('[name="telp"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.telp.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.alamat !== undefined) {
                            var a, b;
                            a = $('body').find('[name="alamat"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.alamat.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.submit').click(function () {
                $('.ajax-loader').removeClass('hide');
                form.submit();
            });
            var currentRow = undefined;
            $('body').find('.datatable').on('click', '.edit', function () {
                currentRow = $(this).parent().parent();
                var str = $(this).val();
                var dj = $.parseJSON(str);
                formEdit.find('[name="id"]').val(dj.id);
                formEdit.find('p.form-control').text(dj.id);
                formEdit.find('[name="supplier"]').val(dj.supplier);
                formEdit.find('[name="telp"]').val(dj.telp);
                formEdit.find('[name="alamat"]').val(dj.alamat);
                datatable.draw();
            });
            formEdit.submit(function () {
                var data = formEdit.serialize();
                $.post(urlEdit, data)
                    .done(function (d) {
                        $('.edit-supplier').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true,
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data supplier berhasil diubah.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        //ubah tr disini
                        currentRow.find('.id').text(d.id);
                        currentRow.find('.supplier').text(d.supplier);
                        currentRow.find('.telp').text(d.telp);
                        currentRow.find('.alamat').text(d.alamat);
                        currentRow.find('.aksi-edit').find('.edit').val('{"id":"' + d.id + '","supplier":"' + d.supplier + '","telp":"' + d.telp + '","alamat":"' + d.alamat + '"}');
                        datatable.draw();
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.supplier !== undefined) {
                            var a, b;
                            a = $('body').find('[name="supplier"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.supplier.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.telp !== undefined) {
                            var a, b;
                            a = $('body').find('[name="telp"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.telp.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.alamat !== undefined) {
                            var a, b;
                            a = $('body').find('[name="alamat"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.alamat.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.apply').click(function () {
                $('.ajax-loader').removeClass('hide');
                formEdit.submit();
            });
            $('body').find('.datatable').on('click', '.delete', function () {
                var btnn = $(this);
                var jc = $.confirm({
                    title: 'Menghapus data',
                    content: 'Apa Anda yakin ingin menghapus data supplier dg ID ' + btnn.val(),
                    buttons: {
                        confirm: function () {
                            var data = $('#' + btnn.val()).serialize();
                            $.post(urlDelete, data)
                                .done(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-success alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-check');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Sukses!</b> Data supplier berhasil dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                    //hapus tr disini
//                                    btnn.parent().parent().remove();
                                    var row = datatable.row(btnn.parents('tr'));
                                    row.remove().draw();
                                })
                                .fail(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-danger alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-ban');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Gagal!</b> Data supplier gagal dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                })
                                .always(function () {
                                    console.log('finish');
                                });
                        },
                        cancel: function () {

                        }
                    }
                });
            });
        });
    </script>
@stop