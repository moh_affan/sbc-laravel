@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Penyusutan
            <small>Data Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-th"></i> Home</a></li>
            <li>Data Master</li>
            <li class="active">Penyusutan</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-place">

                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Penyusutan</h3>
                        <div class="pull-right box-tools">
                            <div data-toggle="tooltip" title="Tambah data penyusutan baru"
                                 data-original-title="Tambah data penyusutan baru">
                                <button class="btn btn-primary btn-sm" type="button" data-target="#tambah-penyusutan"
                                        data-toggle="modal"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover datatable">
                            <thead>
                            <tr>
                                <th>Treatment</th>
                                <th>Penyusutan</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($penyusutan as $item)
                                <tr class="fade in">
                                    <td class="id_treatment">{{$item->treatment->nama}}( {{$item->id_treatment}} )</td>
                                    <td class="penyusutan">{{$item->penyusutan}}</td>
                                    <td class="aksi-edit hidden-print">
                                        <button data-target=".edit-penyusutan"
                                                data-toggle="modal" class="btn btn-success btn-sm edit" type="button"
                                                value='{"id_treatment":"{{$item->id_treatment}}","penyusutan":"{{$item->penyusutan}}"}'>
                                            <span class="fa fa-edit"></span></button>
                                    </td>
                                    <td class="aksi-hapus hidden-print">
                                        {!! Form::open(['method'=>'DELETE','id'=>$item->id_treatment]) !!}
                                        {!! Form::hidden('id_treatment',$item->id_treatment,[]) !!}
                                        {!! Form::close() !!}
                                        <button value="{{$item->id_treatment}}" class="btn btn-danger btn-sm delete"
                                                type="button"><span class="fa fa-trash-o"></span></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Treatment</th>
                                <th>Penyusutan</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    {{--Tambah penyusutan--}}
    <div class="modal fade" id="tambah-penyusutan" tabindex="-1" role="dialog"
         aria-labelledby="tambah-penyusutan-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-penyusutan-label">Tambah Penyusutan</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','class'=>'form form-penyusutan','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('id_treatment','Treatment') !!}
                        {!! Form::select('id_treatment',[],null,['class'=>'form-control','placeholder'=>'Masukkan ID','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('penyusutan','Nominal Penyusutan') !!}
                        {!! Form::number('penyusutan',null,['class'=>'form-control','placeholder'=>'Masukkan Nominal Penyusutan', 'required']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary submit']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--Edit Penyusutan--}}
    <div class="modal fade edit-penyusutan" tabindex="-1" role="dialog" aria-labelledby="tambah-penyusutan-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-penyusutan-label">Edit Penyusutan</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'PATCH','class'=>'form form-edit-penyusutan','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('id_treatment','Treatment') !!}
                        <p class="form-control"></p>
                        {!! Form::hidden('id_treatment',null,['required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('penyusutan','Nominal Penyusutan') !!}
                        {!! Form::number('penyusutan',null,['class'=>'form-control','placeholder'=>'Masukkan Nominal Penyusutan', 'required']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary apply']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(function () {
            $.fn.dataTable.Buttons.swfPath = window.location.protocol + '//' + window.location.hostname + '/swf/flashExport.swf';
            var urlget = window.location.protocol + '//' + window.location.hostname + '/api/get-doesnothave';
            $('#tambah-penyusutan').on('shown.bs.modal', function () {
                console.log('shown');
                $(this).find('#id_treatment').load(urlget);
            });
            var datatable = $('body').find('.datatable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                dom: 'Blfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Data Master Penyusutan',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'pdfHtml5',
                    title: 'Data Master Penyusutan',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'print',
                    footer: true,
                    title: 'Data Master Penyusutan',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }],
            });
            var form = $('body').find('.form-penyusutan');
            var formEdit = $('body').find('.form-edit-penyusutan');
            var url = window.location.protocol + '//' + window.location.hostname + '/api/store-penyusutan';
            var urlEdit = window.location.protocol + '//' + window.location.hostname + '/api/update-penyusutan';
            var urlDelete = window.location.protocol + '//' + window.location.hostname + '/api/delete-penyusutan';
            form.submit(function () {
                var data = form.serialize();
                $.post(url, data)
                    .done(function (d) {
                        $('#tambah-penyusutan').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data penyusutan berhasil ditambahkan.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        datatable.row.add([d.treatment + ' (' + d.id_treatment + ')', d.penyusutan, '<button data-target=".edit-penyusutan" data-toggle="modal" class="btn btn-success btn-sm edit" type="button" value=\'{"id_treatment":"' + d.id_treatment + '","penyusutan":"' + d.penyusutan + '"}\'><span class="fa fa-edit"></span></button>', '<form method="POST" accept-charset="UTF-8" id="' + d.id_treatment + '"><input name="_method" value="DELETE" type="hidden">{!! csrf_field() !!}<input name="id_treatment" value="' + d.id_treatment + '" type="hidden"></form><button value="' + d.id + '" class="btn btn-danger btn-sm delete" type="button"><span class="fa fa-trash-o"></span></button>']).draw(false);
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id_treatment !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id_treatment"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id_treatment.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.penyusutan !== undefined) {
                            var a, b;
                            a = $('body').find('[name="penyusutan"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.penyusutan.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.submit').click(function () {
                $('.ajax-loader').removeClass('hide');
                form.submit();
            });
            var currentRow = undefined;
            $('body').find('.datatable').on('click', '.edit', function () {
                currentRow = $(this).parent().parent();
                var str = $(this).val();
                var dj = $.parseJSON(str);
                formEdit.find('[name="id_treatment"]').val(dj.id_treatment);
                formEdit.find('p.form-control').text(dj.id_treatment);
                formEdit.find('[name="penyusutan"]').val(dj.penyusutan);
                datatable.draw();
            });
            formEdit.submit(function () {
                var data = formEdit.serialize();
                $.post(urlEdit, data)
                    .done(function (d) {
                        $('.edit-penyusutan').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data penyusutan berhasil diubah.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        //ubah tr disini
                        currentRow.find('.id_treatment').text(d.treatment + ' (' + d.id_treatment + ')');
                        currentRow.find('.penyusutan').text(d.penyusutan);
                        currentRow.find('.aksi-edit').find('.edit').val('{"id_treatment":"' + d.id_treatment + '","penyusutan":"' + d.penyusutan + '"}');
                        datatable.draw();
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id_treatment !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id_treatment"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id_treatment.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.penyusutan !== undefined) {
                            var a, b;
                            a = $('body').find('[name="penyusutan"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.penyusutan.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.apply').click(function () {
                $('.ajax-loader').removeClass('hide');
                formEdit.submit();
            });
            $('body').find('.datatable').on('click', '.delete', function () {
                var btnn = $(this);
                var jc = $.confirm({
                    title: 'Menghapus data',
                    content: 'Apa Anda yakin ingin menghapus data penyusutan dg ID ' + btnn.val(),
                    buttons: {
                        confirm: function () {
                            var data = $('#' + btnn.val()).serialize();
                            console.log(data);
                            console.log(btnn.val());
                            $.post(urlDelete, data)
                                .done(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-success alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-check');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Sukses!</b> Data penyusutan berhasil dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                    //hapus tr disini
//                                    btnn.parent().parent().remove();
                                    var row = datatable.row(btnn.parents('tr'));
                                    row.remove().draw();
                                })
                                .fail(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-danger alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-ban');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Gagal!</b> Data penyusutan gagal dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                })
                                .always(function () {
                                    console.log('finish');
                                });
                        },
                        cancel: function () {

                        }
                    }
                });
            });
        });
    </script>
@stop