@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Diskon
            <small>Data Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-th"></i> Home</a></li>
            <li>Data Master</li>
            <li class="active">Diskon</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-place">

                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Diskon</h3>
                        <div class="pull-right box-tools">
                            <div data-toggle="tooltip" title="Tambah data diskon baru"
                                 data-original-title="Tambah data diskon baru">
                                <button class="btn btn-primary btn-sm" type="button" data-target="#tambah-diskon"
                                        data-toggle="modal"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover datatable">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Tanggal Awal</th>
                                <th>Jumlah Diskon (Rp)</th>
                                <th>Tanggal Akhir</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($diskon as $item)
                                <tr class="fade in">
                                    <td class="id">{{$item->id}}</td>
                                    <td class="nama">{{$item->nama}}</td>
                                    <td class="tgl_awal">{{$item->tgl_awal}}</td>
                                    <td class="jumlah_diskon">{{$item->jumlah_diskon}}</td>
                                    <td class="tgl_akhir">{{$item->tgl_akhir}}</td>
                                    <td class="aksi-edit hidden-print">
                                        <button data-target=".edit-diskon"
                                                data-toggle="modal" class="btn btn-success btn-sm edit" type="button"
                                                value='{"id":"{{$item->id}}","nama":"{{$item->nama}}","tgl_awal":"{{$item->tgl_awal}}","jumlah_diskon":"{{$item->jumlah_diskon}}","tgl_akhir":"{{$item->tgl_akhir}}"}'><span
                                                    class="fa fa-edit"></span></button>
                                    </td>
                                    <td class="aksi-hapus hidden-print">
                                        {!! Form::open(['method'=>'DELETE','id'=>$item->id]) !!}
                                        {!! Form::hidden('id',$item->id,[]) !!}
                                        {!! Form::close() !!}
                                        <button value="{{$item->id}}" class="btn btn-danger btn-sm delete"
                                                type="button"><span class="fa fa-trash-o"></span></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Tanggal Awal</th>
                                <th>Jumlah Diskon (Rp)</th>
                                <th>Tanggal Akhir</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    {{--Tambah diskon--}}
    <div class="modal fade" id="tambah-diskon" tabindex="-1" role="dialog" aria-labelledby="tambah-diskon-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-diskon-label">Tambah Diskon</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','class'=>'form form-diskon','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('id','ID') !!}
                        {!! Form::text('id',null,['class'=>'form-control','placeholder'=>'Masukkan ID','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('nama','Nama') !!}
                        {!! Form::text('nama',null,['class'=>'form-control','placeholder'=>'Masukkan Nama', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tgl_awal','Tanggal Awal') !!}
                        {!! Form::date('tgl_awal',null,['class'=>'form-control','required','placeholder'=>'yyyy-mm-dd']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('jumlah_diskon','Jumlah Diskon (Rp)') !!}
                        {!! Form::number('jumlah_diskon',null,['class'=>'form-control','placeholder'=>'Masukkan Jumlah Diskon','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tgl_akhir','Tanggal Akhir') !!}
                        {!! Form::date('tgl_akhir',null,['class'=>'form-control','required','placeholder'=>'yyyy-mm-dd']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary submit']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--Edit Diskon--}}
    <div class="modal fade edit-diskon" tabindex="-1" role="dialog" aria-labelledby="tambah-diskon-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-diskon-label">Edit Diskon</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'PATCH','class'=>'form form-edit-diskon','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('id','ID') !!}
                        <p class="form-control"></p>
                        {!! Form::hidden('id',null,['required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('nama','Nama') !!}
                        {!! Form::text('nama',null,['class'=>'form-control','placeholder'=>'Masukkan Nama', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tgl_awal','Tanggal Awal') !!}
                        {!! Form::date('tgl_awal',null,['class'=>'form-control','required','placeholder'=>'yyyy-mm-dd']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('jumlah_diskon','Jumlah Diskon (Rp)') !!}
                        {!! Form::number('jumlah_diskon',null,['class'=>'form-control','placeholder'=>'Masukkan Jumlah Diskon','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tgl_akhir','Tanggal Akhir') !!}
                        {!! Form::date('tgl_akhir',null,['class'=>'form-control','required','placeholder'=>'yyyy-mm-dd']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary apply']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(function () {
            $('input[type="date"]').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                language: 'id'
            });
            $.fn.dataTable.Buttons.swfPath = window.location.protocol + '//' + window.location.hostname + '/swf/flashExport.swf';
            var urlget = window.location.protocol + '//' + window.location.hostname + '/api/get-diskon';
            var datatable = $('body').find('.datatable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                dom: 'Blfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Data Master Diskon',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'pdfHtml5',
                    title: 'Data Master Diskon',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'print',
                    footer: true,
                    title: 'Data Master Diskon',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }],
            });
            var form = $('body').find('.form-diskon');
            var formEdit = $('body').find('.form-edit-diskon');
            var url = window.location.protocol + '//' + window.location.hostname + '/api/store-diskon';
            var urlEdit = window.location.protocol + '//' + window.location.hostname + '/api/update-diskon';
            var urlDelete = window.location.protocol + '//' + window.location.hostname + '/api/delete-diskon';
            form.submit(function () {
                var data = form.serialize();
                $.post(url, data)
                    .done(function (d) {
                        $('#tambah-diskon').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true,
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data diskon berhasil ditambahkan.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        datatable.row.add([d.id, d.nama, d.tgl_awal, d.jumlah_diskon, d.tgl_akhir, '<button data-target=".edit-diskon" data-toggle="modal" class="btn btn-success btn-sm edit" type="button" value=\'{"id":"' + d.id + '","nama":"' + d.nama + '", "tgl_awal":"' + d.tgl_awal + '","jumlah_diskon":"' + d.jumlah_diskon + '","tgl_akhir":"' + d.tgl_akhir + '"}\'><span class="fa fa-edit"></span></button>', '<form method="POST" accept-charset="UTF-8" id="' + d.id + '"><input name="_method" value="DELETE" type="hidden">{!! csrf_field() !!}<input name="id" value="' + d.id + '" type="hidden"></form><button value="' + d.id + '" class="btn btn-danger btn-sm delete" type="button"><span class="fa fa-trash-o"></span></button>']).draw(false);
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.nama !== undefined) {
                            var a, b;
                            a = $('body').find('[name="nama"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.nama.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.tgl_awal !== undefined) {
                            var a, b;
                            a = $('body').find('[name="tgl_awal"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.tgl_awal.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.tgl_akhir !== undefined) {
                            var a, b;
                            a = $('body').find('[name="tgl_akhir"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.tgl_akhir.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.jumlah_diskon !== undefined) {
                            var a, b;
                            a = $('body').find('[name="jumlah_diskon"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.jumlah_diskon.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.submit').click(function () {
                $('.ajax-loader').removeClass('hide');
                form.submit();
            });
            var currentRow = undefined;
            $('body').find('.datatable').on('click', '.edit', function () {
                currentRow = $(this).parent().parent();
                var str = $(this).val();
                var dj = $.parseJSON(str);
                formEdit.find('[name="id"]').val(dj.id);
                formEdit.find('p.form-control').text(dj.id);
                formEdit.find('[name="nama"]').val(dj.nama);
                formEdit.find('[name="tgl_awal"]').val(dj.tgl_awal);
                formEdit.find('[name="tgl_akhir"]').val(dj.tgl_akhir);
                formEdit.find('[name="jumlah_diskon"]').val(dj.jumlah_diskon);
                datatable.draw();
            });
            formEdit.submit(function () {
                var data = formEdit.serialize();
                $.post(urlEdit, data)
                    .done(function (d) {
                        $('.edit-diskon').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data diskon berhasil diubah.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        //ubah tr disini
                        currentRow.find('.id').text(d.id);
                        currentRow.find('.nama').text(d.nama);
                        currentRow.find('.tgl_awal').text(d.tgl_awal);
                        currentRow.find('.tgl_akhir').text(d.tgl_akhir);
                        currentRow.find('.jumlah_diskon').text(d.jumlah_diskon);
                        currentRow.find('.aksi-edit').find('.edit').val('{"id":"' + d.id + '","nama":"' + d.nama + '", "tgl_awal":"' + d.tgl_awal + '","jumlah_diskon":"' + d.jumlah_diskon + '","tgl_akhir":"' + d.tgl_akhir + '"}');
                        datatable.draw();
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.nama !== undefined) {
                            var a, b;
                            a = $('body').find('[name="nama"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.nama.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.tgl_awal !== undefined) {
                            var a, b;
                            a = $('body').find('[name="tgl_awal"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.tgl_awal.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.tgl_akhir !== undefined) {
                            var a, b;
                            a = $('body').find('[name="tgl_akhir"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.tgl_akhir.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.jumlah_diskon !== undefined) {
                            var a, b;
                            a = $('body').find('[name="jumlah_diskon"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.jumlah_diskon.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.apply').click(function () {
                $('.ajax-loader').removeClass('hide');
                formEdit.submit();
            });
            $('body').find('.datatable').on('click', '.delete', function () {
                var btnn = $(this);
                var jc = $.confirm({
                    title: 'Menghapus data',
                    content: 'Apa Anda yakin ingin menghapus data diskon dg ID ' + btnn.val(),
                    buttons: {
                        confirm: function () {
                            var data = $('#' + btnn.val()).serialize();
                            $.post(urlDelete, data)
                                .done(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-success alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-check');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Sukses!</b> Data diskon berhasil dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                    //hapus tr disini
//                                    btnn.parent().parent().remove();
                                    var row = datatable.row(btnn.parents('tr'));
                                    row.remove().draw();
                                })
                                .fail(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-danger alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-ban');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Gagal!</b> Data diskon gagal dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                })
                                .always(function () {
                                    console.log('finish');
                                });
                        },
                        cancel: function () {

                        }
                    }
                });
            });
        });
    </script>
@stop