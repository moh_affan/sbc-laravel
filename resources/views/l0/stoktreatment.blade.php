@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Treatment
            <small>Stok</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-th"></i> Home</a></li>
            <li>Stok</li>
            <li class="active">Treatment</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-place">

                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Treatment</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover datatable">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Supplier</th>
                                <th>Nama Treatment</th>
                                <th>Stok</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($treatment as $item)
                                <tr class="fade in">
                                    <td class="id">{{$item->id}}</td>
                                    <td class="supplier">{{$item->supplier->supplier}} ({{$item->id_supplier}})</td>
                                    <td class="nama">{{$item->nama}}</td>
                                    <td class="stok">{{$item->stokTreatment->stok}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Supplier</th>
                                <th>Nama Treatment</th>
                                <th>Stok</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>

@stop

@section('script')
    <script>
        $(function () {
            $.fn.dataTable.Buttons.swfPath = window.location.protocol + '//' + window.location.hostname + '/swf/flashExport.swf';
            var datatable = $('body').find('.datatable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                dom: 'Blfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Data Master Treatment',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'pdfHtml5',
                    title: 'Data Master Treatment',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'print',
                    footer: true,
                    title: 'Data Master Treatment',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }],
            });
        });
    </script>
@stop