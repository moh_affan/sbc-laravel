@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Jabatan
            <small>Data Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-th"></i> Home</a></li>
            <li>Data Master</li>
            <li class="active">Jabatan</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-place">

                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Jabatan</h3>
                        <div class="pull-right box-tools">
                            <div data-toggle="tooltip" title="Tambah data jabatan baru"
                                 data-original-title="Tambah data jabatan baru">
                                <button class="btn btn-primary btn-sm" type="button" data-target="#tambah-jabatan"
                                        data-toggle="modal"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover datatable">
                            <thead>
                            <tr>
                                <th>Kode</th>
                                <th>Jabatan</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($jabatan as $item)
                                <tr class="fade in">
                                    <td class="kd">{{$item->kd}}</td>
                                    <td class="jabatan">{{$item->jabatan}}</td>
                                    <td class="aksi-edit hidden-print">
                                        <button data-target=".edit-jabatan"
                                                data-toggle="modal" class="btn btn-success btn-sm edit" type="button"
                                                value='{"kd":"{{$item->kd}}",
                            "jabatan":"{{$item->jabatan}}"}'><span
                                                    class="fa fa-edit"></span></button>
                                    </td>
                                    <td class="aksi-hapus hidden-print">
                                        {!! Form::open(['method'=>'DELETE','id'=>$item->kd]) !!}
                                        {!! Form::hidden('kd',$item->kd,[]) !!}
                                        {!! Form::close() !!}
                                        <button value="{{$item->kd}}" class="btn btn-danger btn-sm delete"
                                                type="button"><span class="fa fa-trash-o"></span></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Kode</th>
                                <th>Jabatan</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    {{--Tambah jabatan--}}
    <div class="modal fade" id="tambah-jabatan" tabindex="-1" role="dialog" aria-labelledby="tambah-jabatan-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-jabatan-label">Tambah Jabatan</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','class'=>'form form-jabatan','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('kd','Kode Jabatan') !!}
                        {!! Form::text('kd',null,['class'=>'form-control','placeholder'=>'Masukkan Kode Jabatan','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('jabatan','Nama Jabatan') !!}
                        {!! Form::text('jabatan',null,['class'=>'form-control','placeholder'=>'Masukkan Nama Jabatan', 'required']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary submit']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--Edit Jabatan--}}
    <div class="modal fade edit-jabatan" tabindex="-1" role="dialog" aria-labelledby="tambah-jabatan-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-jabatan-label">Edit Jabatan</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'PATCH','class'=>'form form-edit-jabatan','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('kd','Kode Jabatan') !!}
                        <p class="form-control"></p>
                        {!! Form::hidden('kd',null,['required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('jabatan','Nama Jabatan') !!}
                        {!! Form::text('jabatan',null,['class'=>'form-control','placeholder'=>'Masukkan Nama Jabatan', 'required']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary apply']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(function () {
            $.fn.dataTable.Buttons.swfPath = window.location.protocol + '//' + window.location.hostname + '/swf/flashExport.swf';
            var urlget = window.location.protocol + '//' + window.location.hostname + '/api/get-jabatan';
            var datatable = $('body').find('.datatable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                dom: 'Blfrtip',
                buttons: [{
                    extend : 'excelHtml5',
                    title:'Data Master Pegawai',
                    footer:true,
                    exportOptions : {
                        columns:"th:not(.hidden-print)"
                    }
                }, {
                    extend : 'pdfHtml5',
                    title:'Data Master Pegawai',
                    footer:true,
                    exportOptions : {
                        columns:"th:not(.hidden-print)"
                    }
                }, {
                    extend : 'print',
                    title:'Data Master Pegawai',
                    footer:true,
                    exportOptions : {
                        columns:"th:not(.hidden-print)"
                    }
                },{
                    extend : 'copyHtml5',
                    exportOptions : {
                        columns:"th:not(.hidden-print)"
                    }
                }],
            });
            var form = $('body').find('.form-jabatan');
            var formEdit = $('body').find('.form-edit-jabatan');
            var url = window.location.protocol + '//' + window.location.hostname + '/api/store-jabatan';
            var urlEdit = window.location.protocol + '//' + window.location.hostname + '/api/update-jabatan';
            var urlDelete = window.location.protocol + '//' + window.location.hostname + '/api/delete-jabatan';
            form.submit(function () {
                var data = form.serialize();
                $.post(url, data)
                    .done(function (d) {
                        $('#tambah-jabatan').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true,
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data jabatan berhasil ditambahkan.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        datatable.row.add([d.kd, d.jabatan, '<button data-target=".edit-jabatan" data-toggle="modal" class="btn btn-success btn-sm edit" type="button" value=\'{"kd":"' + d.kd + '","jabatan":"' + d.jabatan + '"}\'><span class="fa fa-edit"></span></button>', '<form method="POST" accept-charset="UTF-8" id="' + d.kd + '"><input name="_method" value="DELETE" type="hidden">{!! csrf_field() !!}<input name="kd" value="' + d.kd + '" type="hidden"></form><button value="' + d.kd + '" class="btn btn-danger btn-sm delete" type="button"><span class="fa fa-trash-o"></span></button>']).draw(false);
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.kd !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="kd"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.kd.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.jabatan !== undefined) {
                            var a, b;
                            a = $('body').find('[name="jabatan"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.jabatan.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.submit').click(function () {
                $('.ajax-loader').removeClass('hide');
                form.submit();
            });
            var currentRow = undefined;
            $('body').find('.datatable').on('click', '.edit', function () {
                currentRow = $(this).parent().parent();
                var str = $(this).val();
                var dj = $.parseJSON(str);
                formEdit.find('[name="kd"]').val(dj.kd);
                formEdit.find('p.form-control').text(dj.kd);
                formEdit.find('[name="jabatan"]').val(dj.jabatan);
                datatable.draw();
            });
            formEdit.submit(function () {
                var data = formEdit.serialize();
                $.post(urlEdit, data)
                    .done(function (d) {
                        $('.edit-jabatan').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true,
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data jabatan berhasil diubah.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        //ubah tr disini
                        currentRow.find('.kd').text(d.kd);
                        currentRow.find('.jabatan').text(d.jabatan);
                        currentRow.find('.aksi-edit').find('.edit').val('{"kd":"' + d.kd + '","jabatan":"' + d.jabatan + '"}');
                        datatable.draw();
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.kd !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="kd"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.kd.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.jabatan !== undefined) {
                            var a, b;
                            a = $('body').find('[name="jabatan"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.jabatan.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.apply').click(function () {
                $('.ajax-loader').removeClass('hide');
                formEdit.submit();
            });
            $('body').find('.datatable').on('click', '.delete', function () {
                var btnn = $(this);
                var jc = $.confirm({
                    title: 'Menghapus data',
                    content: 'Apa Anda yakin ingin menghapus data jabatan dg kode ' + btnn.val(),
                    buttons: {
                        confirm: function () {
                            var data = $('#' + btnn.val()).serialize();
                            $.post(urlDelete, data)
                                .done(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-success alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-check');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Sukses!</b> Data jabatan berhasil dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                    //hapus tr disini
                                    var row = datatable.row(btnn.parents('tr'));
                                    row.remove().draw();
                                })
                                .fail(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-danger alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-ban');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Gagal!</b> Data jabatan gagal dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                })
                                .always(function () {

                                });
                        },
                        cancel: function () {

                        }
                    }
                });
            });
        });
    </script>
@stop