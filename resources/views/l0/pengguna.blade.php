@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Pengguna
            <small>Data Master</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-th"></i> Home</a></li>
            <li>Data Master</li>
            <li class="active">Pengguna</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-place">

                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Pengguna</h3>
                        <div class="pull-right box-tools">
                            <div data-toggle="tooltip" title="Tambah data pengguna baru"
                                 data-original-title="Tambah data pengguna baru">
                                <button class="btn btn-primary btn-sm" type="button" data-target="#tambah-pengguna"
                                        data-toggle="modal"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover datatable">
                            <thead>
                            <tr>
                                <th>Pegawai</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Level</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pengguna as $item)
                                <tr class="fade in">
                                    <td class="pegawai">{{$item->pegawai->nama}} ({{$item->id_pegawai}})</td>
                                    <td class="username">{{$item->username}}</td>
                                    <td class="password">{{$item->password}}</td>
                                    <td class="level">{{$item->level}}</td>
                                    <td class="aksi-edit hidden-print">
                                        <button data-target=".edit-pengguna"
                                                data-toggle="modal" class="btn btn-success btn-sm edit" type="button"
                                                value='{"id":"{{$item->id}}","id_pegawai":"{{$item->id_pegawai}}","username":"{{$item->username}}","password":"{{$item->password}}","level":"{{$item->level}}"}'><span
                                                    class="fa fa-edit"></span></button>
                                    </td>
                                    <td class="aksi-hapus hidden-print">
                                        {!! Form::open(['method'=>'DELETE','id'=>$item->id]) !!}
                                        {!! Form::hidden('id',$item->id,[]) !!}
                                        {!! Form::close() !!}
                                        <button value="{{$item->id}}" class="btn btn-danger btn-sm delete"
                                                type="button"><span class="fa fa-trash-o"></span></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Pegawai</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Level</th>
                                <th class="hidden-print">Edit</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    {{--Tambah pengguna--}}
    <div class="modal fade" id="tambah-pengguna" tabindex="-1" role="dialog" aria-labelledby="tambah-pengguna-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-pengguna-label">Tambah Pengguna</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','class'=>'form form-pengguna','role'=>'form']) !!}
                    @php
                        $choices = [''=>'--Pilihan Pegawai--'];
                        foreach ($pegawai as $ch){
                            $choices[$ch->id]=$ch->nama.' ('.$ch->id.')';
                        }
                    @endphp
                    <div class="form-group">
                        {!! Form::label('id_pegawai','Pegawai') !!}
                        {!! Form::select('id_pegawai',$choices,null,['class'=>'form-control','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('username','Username') !!}
                        {!! Form::text('username',null,['class'=>'form-control','placeholder'=>'Masukkan Username', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password','Password') !!}
                        {!! Form::text('password',null,['class'=>'form-control','placeholder'=>'Masukkan Password','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('level','Level') !!}<br>
                        {!! Form::radio('level','0',false,[]) !!} Level 0
                        {!! Form::radio('level','1',false,[]) !!} Level 1
                        {!! Form::radio('level','2',false,[]) !!} Level 2
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary submit']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--Edit Pengguna--}}
    <div class="modal fade edit-pengguna" tabindex="-1" role="dialog" aria-labelledby="tambah-pengguna-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-light-blue">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="tambah-pengguna-label">Edit Pengguna</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'PATCH','class'=>'form form-edit-pengguna','role'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('id','ID') !!}
                        <p class="form-control"></p>
                        {!! Form::hidden('id',null,['required']) !!}
                    </div>
                    @php
                        $choices = [''=>'--Pilihan Pegawai--'];
                        foreach ($pegawai as $ch){
                            $choices[$ch->id]=$ch->nama.' ('.$ch->id.')';
                        }
                    @endphp
                    <div class="form-group">
                        {!! Form::label('id_pegawai','Pegawai') !!}
                        {!! Form::select('id_pegawai',$choices,null,['class'=>'form-control','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('username','Username') !!}
                        {!! Form::text('username',null,['class'=>'form-control','placeholder'=>'Masukkan Username', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password','Password') !!}
                        {!! Form::text('password',null,['class'=>'form-control','placeholder'=>'Masukkan Password','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('level','Level') !!}<br>
                        {!! Form::radio('level','0',false,[]) !!} Level 0
                        {!! Form::radio('level','1',false,[]) !!} Level 1
                        {!! Form::radio('level','2',false,[]) !!} Level 2
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer bg-gray">
                    <img class="ajax-loader hide" src="{{asset('img/ajax-loader.gif')}}" width="30">
                    {!! Form::button('Simpan',['class'=>'btn btn-primary apply']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(function () {
            $.fn.dataTable.Buttons.swfPath = window.location.protocol + '//' + window.location.hostname + '/swf/flashExport.swf';
            var urlget = window.location.protocol + '//' + window.location.hostname + '/api/get-pengguna';
            var datatable = $('body').find('.datatable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                dom: 'Blfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Data Master Pengguna',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'pdfHtml5',
                    title: 'Data Master Pengguna',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'print',
                    footer: true,
                    title: 'Data Master Pengguna',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }],
            });
            var form = $('body').find('.form-pengguna');
            var formEdit = $('body').find('.form-edit-pengguna');
            var url = window.location.protocol + '//' + window.location.hostname + '/api/store-pengguna';
            var urlEdit = window.location.protocol + '//' + window.location.hostname + '/api/update-pengguna';
            var urlDelete = window.location.protocol + '//' + window.location.hostname + '/api/delete-pengguna';
            form.submit(function () {
                var data = form.serialize();
                $.post(url, data)
                    .done(function (d) {
                        $('#tambah-pengguna').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true,
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data pengguna berhasil ditambahkan.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        datatable.row.add([d.pegawai + ' (' + d.id_pegawai + ')', d.username, d.password, d.level, '<button data-target=".edit-pengguna" data-toggle="modal" class="btn btn-success btn-sm edit" type="button" value=\'{"id":"' + d.id + '","id_pegawai":"' + d.id_pegawai + '","username":"' + d.username + '", "password":"' + d.password + '","level":"' + d.level + '"}\'><span class="fa fa-edit"></span></button>', '<form method="POST" accept-charset="UTF-8" id="' + d.id + '"><input name="_method" value="DELETE" type="hidden">{!! csrf_field() !!}<input name="id" value="' + d.id + '" type="hidden"></form><button value="' + d.id + '" class="btn btn-danger btn-sm delete" type="button"><span class="fa fa-trash-o"></span></button>']).draw(false);
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.id_pegawai !== undefined) {
                            var a, b;
                            a = $('body').find('[name="id_pegawai"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id_pegawai.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.username !== undefined) {
                            var a, b;
                            a = $('body').find('[name="username"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.username.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.password !== undefined) {
                            var a, b;
                            a = $('body').find('[name="password"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.password.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.level !== undefined) {
                            var a, b;
                            a = $('body').find('[name="level"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.level.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.submit').click(function () {
                $('.ajax-loader').removeClass('hide');
                form.submit();
            });
            var currentRow = undefined;
            $('body').find('.datatable').on('click', '.edit', function () {
                currentRow = $(this).parent().parent();
                var str = $(this).val();
                var dj = $.parseJSON(str);
                formEdit.find('[name="id"]').val(dj.id);
                formEdit.find('p.form-control').text(dj.id);
                formEdit.find('[name="id_pegawai"]').val(dj.id_pegawai);
                formEdit.find('[name="username"]').val(dj.username);
                formEdit.find('[name="password"]').val(dj.password);
                var a = formEdit.find('[name="level"]').filter('[value=' + dj.level + ']').prop('checked', true);
                console.log(a);
                datatable.draw();
            });
            formEdit.submit(function () {
                var data = formEdit.serialize();
                $.post(urlEdit, data)
                    .done(function (d) {
                        $('.edit-pengguna').modal('hide');
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true,
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Data pengguna berhasil diubah.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        //ubah tr disini
                        currentRow.find('.pegawai').text(d.pegawai + ' (' + d.id_pegawai + ')');
                        currentRow.find('.username').text(d.username);
                        currentRow.find('.password').text(d.password);
                        currentRow.find('.level').text(d.level);
                        currentRow.find('.aksi-edit').find('.edit').val('{"id":"' + d.id + '","id_pegawai":"' + d.id_pegawai + '","username":"' + d.username + '", "password":"' + d.password + '","level":"' + d.level + '"}');
                        datatable.draw();
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        if (res.id !== undefined) {
                            var a, b;
                            a = $('body').find('.modal').find('[name="id"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.id_pegawai !== undefined) {
                            var a, b;
                            a = $('body').find('[name="id_pegawai"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.id_pegawai.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.username !== undefined) {
                            var a, b;
                            a = $('body').find('[name="username"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.username.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.password !== undefined) {
                            var a, b;
                            a = $('body').find('[name="password"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.password.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                        if (res.level !== undefined) {
                            var a, b;
                            a = $('body').find('[name="level"]').parent();
                            b = $('<p></p>');
                            b.addClass('help-block text-danger error');
                            res.level.forEach(function (entry) {
                                b.append(entry);
                            });
                            a.append(b);
                        }
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('hide');
                    });
                return false;
            });
            $('.apply').click(function () {
                $('.ajax-loader').removeClass('hide');
                formEdit.submit();
            });
            $('body').find('.datatable').on('click', '.delete', function () {
                var btnn = $(this);
                var jc = $.confirm({
                    title: 'Menghapus data',
                    content: 'Apa Anda yakin ingin menghapus data pengguna dg ID ' + btnn.val(),
                    buttons: {
                        confirm: function () {
                            var data = $('#' + btnn.val()).serialize();
                            $.post(urlDelete, data)
                                .done(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-success alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-check');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Sukses!</b> Data pengguna berhasil dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                    //hapus tr disini
//                                    btnn.parent().parent().remove();
                                    var row = datatable.row(btnn.parents('tr'));
                                    row.remove().draw();
                                })
                                .fail(function (d) {
                                    var div = $('<div></div>');
                                    div.addClass('alert alert-danger alert-dismissable fade in')
                                    var icheck = $('<i></i>');
                                    icheck.addClass('fa fa-ban');
                                    var btn = $('<button></button>');
                                    btn.addClass('close');
                                    btn.attr({
                                        'type': "button",
                                        'data-dismiss': "alert",
                                        'aria-hidden': true,
                                    });
                                    div.append(icheck);
                                    div.append(btn);
                                    div.append('<b>Gagal!</b> Data pengguna gagal dihapus.');
                                    $('.alert-place').html(div);
                                    setTimeout(function () {
                                        $('body').find('button[data-dismiss="alert"]').trigger("click");
                                    }, 4000);
                                })
                                .always(function () {
                                    console.log('finish');
                                });
                        },
                        cancel: function () {

                        }
                    }
                });
            });
        });
    </script>
@stop