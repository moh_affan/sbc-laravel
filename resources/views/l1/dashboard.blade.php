@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- top row -->
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Point of Sales</h2>
                <h2>Soraya Beauty Center</h2>
            </div><!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                <!-- Calendar -->
                <div class="box box-warning">
                    <div class="box-header">
                        <i class="fa fa-calendar"></i>
                        <div class="box-title">Kalender</div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                        <!--The calendar -->
                        <div id="calendar"></div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </section><!-- /.Left col -->
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
@stop