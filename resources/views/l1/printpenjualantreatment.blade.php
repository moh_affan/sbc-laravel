<html>
<head>
    <title>Penjualan Treatment No {{$transaksi->id}}</title>
    <link rel="stylesheet" href="{{asset('css/print.css')}}">
</head>
<body onload="printPage()">
<table class="fluid">
    <tr>
        <td class="text-center"><span class="bold-2">Soraya Beauty Center</span></td>
    </tr>
    <tr>
        <td class="text-center"><span class="bold-2">SBC</span></td>
    </tr>
    <tr>
        <td class="text-center padding-bottom-sm"><span class="address">Jl. Sultan Abdurrahman Gg 1<br>Bumi Sumekar - Sumenep</span>
        </td>
    </tr>
    <tr>
        <td class="bordered padding-top-sm padding-bottom-sm">{{$transaksi->id}} {{\Carbon\Carbon::parse($transaksi->tgl)->format('Ymd His')}} {{$transaksi->id_pegawai}}
            /{{explode(' ',$transaksi->pegawai->nama)[0]}}</td>
    </tr>
</table>
<br>
@php
    $total=0;
@endphp
<table class="detail">
    @foreach($transaksi->detailPenjualanTreatment as $item)
        <tr>
            <td>{{$item->treatment->nama}}</td>
            <td>{{$item->harga_aktual}}</td>
            <td>{{'@'}}{{$item->qty}}</td>
            <td>{{$item->qty*$item->harga_aktual}}</td>
            @php
                $total+=($item->qty*$item->harga_aktual);
            @endphp
        </tr>
        @if($item->diskon_aktual>0)
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>({{$item->qty*$item->diskon_aktual}})</td>
                @php
                    $total-=($item->qty*$item->diskon_aktual);
                @endphp
            </tr>
        @endif
    @endforeach
    <tr>
        <td>&nbsp;</td>
        <td colspan="2" class="text-right bold-1 bordered padding-bottom-sm padding-top-sm">Total Pembelian</td>
        <td class="text-right bold-1 bordered">{{$total}}</td>
    </tr>
</table>
<hr>
<p class="text-center padding-top-sm padding-bottom-sm"><b>Terima Kasih</b></p>
<script>
    function printPage() {
        if (typeof (window.print) != 'undifined') {
            window.print();
            window.close();
        }
    }
</script>
</body>
</html>