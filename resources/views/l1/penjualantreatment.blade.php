@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Penjualan Treatment
            <small>Transaksi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Penjualan Treatment</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-place">

                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Penjualan Treatment</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        {!! Form::open(['method'=>'POST','role'=>'form','id'=>'penjualan-treatment']) !!}
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group-sm">
                                    {!! Form::label('id_pegawai','Petugas',['class'=>'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        <p class="form-control disabled"
                                           id="id_pegawai">{{$pengguna->pegawai->nama}}</p>
                                        {!! Form::hidden('id_pegawai',$pengguna->id_pegawai) !!}
                                    </div>
                                </div>
                                <div class="form-group-sm">
                                    {!! Form::label('tgl','Tanggal',['class'=>'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        <p class="form-control realtime disabled">{{\Carbon\Carbon::now()->format('d:m:Y H:i:s')}}</p>
                                        {!! Form::hidden('tgl','',['class'=>'realtime']) !!}
                                    </div>
                                </div>
                                @php
                                    foreach ($pelanggan as $ch){
                                        $choices[$ch->id]=$ch->nama;
                                    }
                                @endphp
                                <div class="form-group-sm">
                                    {!! Form::label('id_pelanggan','Pelanggan',['class'=>'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::select('id_pelanggan',$choices,null,['class'=>'form-control','required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group-sm">
                                    {!! Form::label('id','No Transaksi',['class'=>'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        <p class="form-control disabled"
                                           id="id"></p>
                                        {!! Form::hidden('id','') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div style="padding: 10px 0;margin-bottom: 10px;">
                                <div class="form-inline">
                                    <div class="form-group-sm">
                                        {!! Form::label('treatment','Treatment',['class'=>'col-md-2 control-label text-right']) !!}
                                        <div class="col-md-6">
                                            {!! Form::select('treatment',[],null,['class'=>'form-control','required','style'=>'width:100%']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group-sm">
                                        {!! Form::label('jumlah','Jumlah',['class'=>'col-md-1 control-label']) !!}
                                        <div class="col-md-2">
                                            {!! Form::text('jumlah','',['class'=>'form-control']) !!}
                                        </div>
                                        <div class="col-md-1">
                                            {!! Form::button('<span class="fa fa-plus"></span>',['class'=>'btn btn-primary tambah-treatment']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <br>
                        <div class="col-md-1">&nbsp;</div>
                        <div class="col-md-10">
                            <table class="table table-bordered table-hover datatable">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Treatment</th>
                                    <th>Jumlah</th>
                                    <th>Harga</th>
                                    <th>Diskon</th>
                                    <th>Subtotal</th>
                                    <th class="hidden-print">Hapus</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="6">Total</th>
                                    <th id="total">0</th>
                                </tr>
                                <tr>
                                    <th colspan="7" class="text-right">
                                        Bayar
                                        <div id="bayar" contenteditable="true" class="form-control"
                                             style="width: 150px;"></div>
                                        Sisa
                                        <div id="sisa" class="form-control"
                                             style="width: 150px;margin: 0 10px"></div>
                                        <input type="checkbox" name="autoprint" value="true" class="checkbox"> Auto
                                        print
                                        <img class="ajax-loader invisible" src="{{asset('img/ajax-loader.gif')}}"
                                             width="30">
                                        <button class="btn btn-success simpan" disabled type="button">Simpan</button>
                                    </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-md-1">&nbsp;</div>
                        {!! Form::close() !!}
                        <div class="clearfix"></div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

        <div class="row" id="#transaksi-saya">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Transaksi Saya</h3>
                        <div class="pull-right box-tools">
                            <a class="btn btn-success btn-sm" type="button" data-toggle="tooltip"
                               href="{{Request::url()}}/transaksi-saya"
                               title="Lihat semua transaksi"
                               data-original-title="Lihat semua transaksi"><span style="color: #fff;">Lihat semua transaksi</span></a>
                            <button class="btn btn-success btn-sm reload" type="button" data-toggle="tooltip"
                                    title="Reload"
                                    data-original-title="Reload"><span style="color: #fff;"
                                                                       class="fa fa-refresh"></span></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover datatable2">
                                <thead>
                                <tr>
                                    <th>No Transaksi</th>
                                    <th>Tanggal</th>
                                    <th>Pelanggan</th>
                                    <th>Keterangan</th>
                                    <th>Detail</th>
                                </tr>
                                </thead>
                                <tbody class="trx-saya">

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No Transaksi</th>
                                    <th>Tanggal</th>
                                    <th>Pelanggan</th>
                                    <th>Keterangan</th>
                                    <th>Detail</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section><!-- /.content -->

    <div class="modal fade" id="detail-transaksi" tabindex="-1" role="dialog" aria-labelledby="detail-transaksi-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-olive">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="detail-transaksi-label">Detail Transaksi</h4>
                </div>
                <div class="modal-body">
                    <img class="ajax-loader hide center-block" src="{{asset('img/ajax-loader1.gif')}}">
                    <div class="isi-detail"></div>
                </div>
                <div class="modal-footer bg-gray">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(function () {
            $.widget("custom.combobox", {
                _create: function () {
                    this.wrapper = $("<span>")
                        .addClass("custom-combobox")
                        .insertAfter(this.element);

                    this.element.hide();
                    this._createAutocomplete();
                    this._createShowAllButton();
                },

                _createAutocomplete: function () {
                    var selected = this.element.children(":selected"),
                        value = selected.val() ? selected.text() : "";

                    this.input = $("<input>")
                        .appendTo(this.wrapper)
                        .val(value)
                        .attr("title", "")
                        .addClass("form-control")
                        .css('width', '100%')
                        .autocomplete({
                            delay: 0,
                            minLength: 0,
                            source: $.proxy(this, "_source")
                        })
                        .tooltip({
                            classes: {
                                "ui-tooltip": "ui-state-highlight"
                            }
                        });

                    this._on(this.input, {
                        autocompleteselect: function (event, ui) {
                            ui.item.option.selected = true;
                            this._trigger("select", event, {
                                item: ui.item.option
                            });
                        },

                        autocompletechange: "_removeIfInvalid"
                    });
                },

                _createShowAllButton: function () {
                    var input = this.input,
                        wasOpen = false;

                    $("<a>")
                        .attr("tabIndex", -1)
                        .attr("title", "Show All Items")
                        .tooltip()
                        .appendTo(this.wrapper)
                        .button({
                            icons: {
                                primary: "ui-icon-triangle-1-s"
                            },
                            text: false
                        })
                        .removeClass("ui-corner-all")
                        .addClass("custom-combobox-toggle ui-corner-right")
                        .on("mousedown", function () {
                            wasOpen = input.autocomplete("widget").is(":visible");
                        })
                        .on("click", function () {
                            input.trigger("focus");

                            // Close if already visible
                            if (wasOpen) {
                                return;
                            }

                            // Pass empty string as value to search for, displaying all results
                            input.autocomplete("search", "");
                        });
                },

                _source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response(this.element.children("option").map(function () {
                        var text = $(this).text();
                        if (this.value && ( !request.term || matcher.test(text) ))
                            return {
                                label: text,
                                value: text,
                                option: this
                            };
                    }));
                },

                _removeIfInvalid: function (event, ui) {

                    // Selected an item, nothing to do
                    if (ui.item) {
                        return;
                    }

                    // Search for a match (case-insensitive)
                    var value = this.input.val(),
                        valueLowerCase = value.toLowerCase(),
                        valid = false;
                    this.element.children("option").each(function () {
                        if ($(this).text().toLowerCase() === valueLowerCase) {
                            this.selected = valid = true;
                            return false;
                        }
                    });

                    // Found a match, nothing to do
                    if (valid) {
                        return;
                    }

                    // Remove invalid value
                    this.input
                        .val("")
                        .attr("title", value + " didn't match any item")
                        .tooltip("open");
                    this.element.val("");
                    this._delay(function () {
                        this.input.tooltip("close").attr("title", "");
                    }, 2500);
                    this.input.autocomplete("instance").term = "";
                },

                _destroy: function () {
                    this.wrapper.remove();
                    this.element.show();
                }
            });
        });
        (function () {
            var urlget = window.location.protocol + '//' + window.location.hostname + '/api/get-treatment-all/';
            var urlpost = window.location.protocol + '//' + window.location.hostname + '/api/penjualan-treatment/store';
            var printurl = window.location.href + '/print/';
            $('body').find('select#treatment').load(urlget, function () {
                $("select#treatment").combobox();
            });
            var datatable = $('body').find('.datatable').DataTable({
                "bPaginate": false,
                "bLengthChange": true,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
            });
            var datatable2 = $('body').find('.datatable2').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": false,
                "bAutoWidth": false,
            });
            $('.tambah-treatment').on('click', function () {
                var jsn = $.parseJSON($('select#treatment').val());
                console.log(jsn);
                var juml = parseInt($('[name="jumlah"]').val());

                if (juml <= jsn.stok) {
                    var subtotal = (juml * jsn.harga) - (juml * jsn.diskon);
                    datatable.row.add([jsn.id + '<input type="hidden" name="id_treatment[]" value="' + jsn.id + '">', jsn.treatment, '<div class="jumlah">' + juml + '</div><input type="hidden" name="qty[]" value="' + juml + '"/>', jsn.harga, jsn.diskon, subtotal, '<a href="#" class="btn btn-danger btn-sm hapus"><span class="fa fa-trash-o"></span></a>']).draw(false);
                    $('[name="jumlah"]').val('');
                    $('select#treatment').val('');
                    $('input.ui-autocomplete-input').val('');
                    var total = parseInt($('th#total').text());
                    $('th#total').text(total + subtotal);
                } else {
                    alert('Stok tidak mencukupi, stok tersisa ' + jsn.stok);
                }
            });
            $('body').find('.datatable').on('click', '.hapus', function () {
                var btnn = $(this);
                var row = datatable.row(btnn.parents('tr'));
                row.remove().draw();
            });
            $('body').find('.datatable  ').on('click', '.jumlah', function () {
                console.log('klik');
                var jml = $(this);
                jml.attr('contenteditable', 'true');
                jml.addClass('form-control');
            });
            $('body').find('.datatable').on('blur', '.jumlah', function () {
                console.log('blur');
                var jml = $(this);
                jml.attr('contenteditable', 'false');
                jml.removeClass('form-control');
                jml.parent().find('input').val(jml.text());
                var tr = jml.parents('tr');
                var total = parseInt($('th#total').text());
                var harga = tr.children().eq(3).text();
                var diskon = tr.children().eq(4).text();
                var sub = parseInt(tr.children().eq(5).text());
                total -= sub;
                console.log(harga);
                var subtotal = (parseInt(harga) * parseInt(jml.text())) - (parseInt(diskon) * parseInt(jml.text()));
                console.log(subtotal);
                total += subtotal
                tr.children().eq(5).text(subtotal);
                $('th#total').text(total);
            });
            getLastId();
            loadMyTrx();
            setInterval(function () {
                var now = new Date();
                var strDate = now.getFullYear() + '-' + (now.getMonth()+1) + '-' + now.getDate() + ' ';
                if (now.getHours() > 9)
                    strDate += now.getHours() + ':';
                else
                    strDate += '0' + now.getHours() + ':';

                if (now.getMinutes() > 9)
                    strDate += now.getMinutes() + ':';
                else
                    strDate += '0' + now.getMinutes() + ':';

                if (now.getSeconds() > 9)
                    strDate += now.getSeconds();
                else
                    strDate += '0' + now.getSeconds();
                $('input.realtime').val(strDate);
                $('p.realtime').text(strDate);
            }, 100);
            var form = $('body').find('#penjualan-treatment');
            form.submit(function () {
                var data = form.serialize();
                $.post(urlpost, data)
                    .done(function (d) {
                        var div = $('<div></div>');
                        div.addClass('alert alert-success alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-check');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Sukses!</b> Penjualan treatment berhasil ditambahkan.');
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                        getLastId();
                        datatable.clear().draw();
                        loadMyTrx();
                        $('#total').text('0');
                        $('#bayar').text('');
                        $('#sisa').text('');
                        if ($('[name="autoprint"]:checked').length > 0) {
                            window.open(printurl + d.id, '_blank', 'width=300,toolbar=no,location=no,menubar=no,resizable=no,scrollbars=yes,top=0,left=0');
                        }
                    })
                    .fail(function (d) {
                        $('body').find('.error').remove();
                        var res = d.responseJSON;
                        var div = $('<div></div>');
                        div.addClass('alert alert-danger alert-dismissable fade in')
                        var icheck = $('<i></i>');
                        icheck.addClass('fa fa-ban');
                        var btn = $('<button></button>');
                        btn.addClass('close');
                        btn.attr({
                            'type': "button",
                            'data-dismiss': "alert",
                            'aria-hidden': true,
                        });
                        div.append(icheck);
                        div.append(btn);
                        div.append('<b>Gagal!</b> Penjualan treatment gagal disimpan.<br>' + JSON.stringify(res));
                        $('.alert-place').html(div);
                        setTimeout(function () {
                            $('body').find('button[data-dismiss="alert"]').trigger("click");
                        }, 4000);
                    })
                    .always(function () {
                        $('.ajax-loader').addClass('invisible');
                    });
                return false;
            });
            $('.simpan').on('click', function () {
                $('.ajax-loader').removeClass('invisible');
                form.submit();
            });
            $('#bayar').on('blur keyup paste input', function () {
                var bayar = parseInt($(this).text());
                var total = parseInt($('th#total').text());
                var sisa = bayar - total;
                $('#sisa').text(sisa);
                if (sisa < 0)
                    $('.simpan').attr('disabled', true);
                else
                    $('.simpan').attr('disabled', false);
            });
            $('.trx-saya').on('click', '.detail', function () {
                $('.ajax-loader').removeClass('hide');
                var data = $.parseJSON($(this).val());
                var url = window.location.protocol + '//' + window.location.hostname + '/api/penjualan-treatment/load-detail/' + data.id;
                $('.isi-detail').html('');
                $('.isi-detail').load(url, function () {
                    $('.ajax-loader').addClass('hide');
                });
                $('#detail-transaksi').modal();
                console.log(data);
            });
            $('.reload').click(function () {
                loadMyTrx();
            });
            $('body').on('click', '#print-this', function () {
                var id = $(this).val();
                window.open(printurl + id, '_blank', 'width=300,toolbar=no,location=no,menubar=no,resizable=no,scrollbars=yes,top=0,left=0');
            });
        })(jQuery);
        function getLastId() {
            var urlget = window.location.protocol + '//' + window.location.hostname + '/api/penjualan-treatment/get-last-id';
            $.get(urlget).done(function (d) {
                $('#id,input[name="id"]').text(d.id + 1);
            });
        }
        function loadMyTrx() {
            var urlsaya = window.location.protocol + '//' + window.location.hostname + '/api/penjualan-treatment/transaksi-saya';
            $('.trx-saya').load(urlsaya, function () {
                var a = $('body').find('.datatable2').DataTable();
            });
        }
    </script>
@stop