<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('img/avatar3.png')}}" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Halo, {{explode(' ', $pengguna->pegawai->nama)[0]}}</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        {{--<span class="center-block badge bg-light-blue">&nbsp;</span>--}}
        <div class="progress xs">
            <div class="progress-bar progress-bar-light-blue" style="width: 100%;"></div>
        </div>
        <br>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active">
                <a href="{{url('/')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ion ion-bag"></i>
                    <span>Transaksi</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url($pengguna->level.'/transaksi/pembelian-produk')}}"><i
                                    class="fa fa-angle-double-right"></i> Pembelian Produk</a></li>
                    <li><a href="{{url($pengguna->level.'/transaksi/penjualan-produk')}}"><i
                                    class="fa fa-angle-double-right"></i> Penjualan Produk</a></li>
                    <li><a href="{{url($pengguna->level.'/transaksi/jatah-bon-produk')}}"><i
                                    class="fa fa-angle-double-right"></i> Jatah Bon Produk</a></li>
                    <li><a href="{{url($pengguna->level.'/transaksi/pembelian-treatment')}}"><i
                                    class="fa fa-angle-double-right"></i> Pembelian Treatment</a></li>
                    <li><a href="{{url($pengguna->level.'/transaksi/penjualan-treatment')}}"><i
                                    class="fa fa-angle-double-right"></i> Penjualan Treatment</a></li>
                    <li><a href="{{url($pengguna->level.'/transaksi/jatah-bon-treatment')}}"><i
                                    class="fa fa-angle-double-right"></i> Jatah Bon Treatment</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dropbox"></i> <span>Stok</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url($pengguna->level.'/stok/produk')}}"><i class="fa fa-angle-double-right"></i> Produk</a></li>
                    <li><a href="{{url($pengguna->level.'/stok/treatment')}}"><i class="fa fa-angle-double-right"></i> Treatment</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{url('logout')}}">
                    <i class="fa fa-sign-out"></i> <span>Logout</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>