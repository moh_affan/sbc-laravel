@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Pembelian Produk
            <small>Transaksi</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pembelian Produk</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-place">

                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Pembelian Produk</h3>
                        <div class="pull-right box-tools">
                            <a class="btn btn-primary btn-sm" type="button" data-toggle="tooltip" href="#"
                               title="Lihat transaksi saya"
                               data-original-title="Lihat transaksi saya"><span style="color: #fff;">Lihat transaksi saya</span></a>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        {!! Form::open(['method'=>'POST','role'=>'form']) !!}
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group-sm">
                                    {!! Form::label('id_pegawai','Petugas',['class'=>'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        <p class="form-control disabled"
                                           id="id_pegawai">{{$pengguna->pegawai->nama}}</p>
                                    </div>
                                </div>
                                <div class="form-group-sm">
                                    {!! Form::label('tgl','Tanggal',['class'=>'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        <p class="form-control realtime disabled">{{\Carbon\Carbon::now()->format('d:m:Y H:i:s')}}</p>
                                        {!! Form::hidden('tgl','',['class'=>'realtime']) !!}
                                    </div>
                                </div>
                                @php
                                    $choices = [''=>'--Pilihan Supplier--'];
                                    foreach ($supplier as $ch){
                                        $choices[$ch->id]=$ch->supplier;
                                    }
                                @endphp
                                <div class="form-group-sm">
                                    {!! Form::label('id_supplier','Supplier',['class'=>'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::select('id_supplier',$choices,null,['class'=>'form-control','required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group-sm">
                                    {!! Form::label('id','No Transaksi',['class'=>'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        <p class="form-control disabled"
                                           id="id"></p>
                                        {!! Form::hidden('id','') !!}
                                    </div>
                                </div>
                                <div class="form-group-sm">
                                    {!! Form::label('no_nota','No Nota Pembelian',['class'=>'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::text('no_nota','',['class'=>'form-control','placeholder'=>'Isi jika ada']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div style="padding: 10px 0;margin-bottom: 10px;">
                                <div class="form-inline">
                                    <div class="form-group-sm">
                                        {!! Form::label('barang','Barang',['class'=>'col-md-2 control-label text-right']) !!}
                                        <div class="col-md-6">
                                            {!! Form::select('barang',[],null,['class'=>'form-control','required','style'=>'width:100%']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group-sm">
                                        {!! Form::label('jumlah','Jumlah',['class'=>'col-md-1 control-label']) !!}
                                        <div class="col-md-2">
                                            {!! Form::text('jumlah','',['class'=>'form-control']) !!}
                                        </div>
                                        <div class="col-md-1">
                                            {!! Form::button('<span class="fa fa-plus"></span>',['class'=>'btn btn-primary tambah-barang']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <br>
                        <table class="table table-bordered table-hover datatable">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Subtotal</th>
                                <th class="hidden-print">Hapus</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="5" class="text-right">Total</th>
                                <th colspan="1">Rp. xxx xxxx</th>
                            </tr>
                            </tfoot>
                        </table>
                        {!! Form::close() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section><!-- /.content -->
@stop

@section('script')
    <script>
        $(function () {
            $.widget("custom.combobox", {
                _create: function () {
                    this.wrapper = $("<span>")
                        .addClass("custom-combobox")
                        .insertAfter(this.element);

                    this.element.hide();
                    this._createAutocomplete();
                    this._createShowAllButton();
                },

                _createAutocomplete: function () {
                    var selected = this.element.children(":selected"),
                        value = selected.val() ? selected.text() : "";

                    this.input = $("<input>")
                        .appendTo(this.wrapper)
                        .val(value)
                        .attr("title", "")
                        .addClass("form-control")
                        .css('width', '100%')
                        .autocomplete({
                            delay: 0,
                            minLength: 0,
                            source: $.proxy(this, "_source")
                        })
                        .tooltip({
                            classes: {
                                "ui-tooltip": "ui-state-highlight"
                            }
                        });

                    this._on(this.input, {
                        autocompleteselect: function (event, ui) {
                            ui.item.option.selected = true;
                            this._trigger("select", event, {
                                item: ui.item.option
                            });
                        },

                        autocompletechange: "_removeIfInvalid"
                    });
                },

                _createShowAllButton: function () {
                    var input = this.input,
                        wasOpen = false;

                    $("<a>")
                        .attr("tabIndex", -1)
                        .attr("title", "Show All Items")
                        .tooltip()
                        .appendTo(this.wrapper)
                        .button({
                            icons: {
                                primary: "ui-icon-triangle-1-s"
                            },
                            text: false
                        })
                        .removeClass("ui-corner-all")
                        .addClass("custom-combobox-toggle ui-corner-right")
                        .on("mousedown", function () {
                            wasOpen = input.autocomplete("widget").is(":visible");
                        })
                        .on("click", function () {
                            input.trigger("focus");

                            // Close if already visible
                            if (wasOpen) {
                                return;
                            }

                            // Pass empty string as value to search for, displaying all results
                            input.autocomplete("search", "");
                        });
                },

                _source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response(this.element.children("option").map(function () {
                        var text = $(this).text();
                        if (this.value && ( !request.term || matcher.test(text) ))
                            return {
                                label: text,
                                value: text,
                                option: this
                            };
                    }));
                },

                _removeIfInvalid: function (event, ui) {

                    // Selected an item, nothing to do
                    if (ui.item) {
                        return;
                    }

                    // Search for a match (case-insensitive)
                    var value = this.input.val(),
                        valueLowerCase = value.toLowerCase(),
                        valid = false;
                    this.element.children("option").each(function () {
                        if ($(this).text().toLowerCase() === valueLowerCase) {
                            this.selected = valid = true;
                            return false;
                        }
                    });

                    // Found a match, nothing to do
                    if (valid) {
                        return;
                    }

                    // Remove invalid value
                    this.input
                        .val("")
                        .attr("title", value + " didn't match any item")
                        .tooltip("open");
                    this.element.val("");
                    this._delay(function () {
                        this.input.tooltip("close").attr("title", "");
                    }, 2500);
                    this.input.autocomplete("instance").term = "";
                },

                _destroy: function () {
                    this.wrapper.remove();
                    this.element.show();
                }
            });

            $("#combobox").combobox();
            $("#toggle").on("click", function () {
                $("#combobox").toggle();
            });
        });
        (function () {
            var urlget = window.location.protocol + '//' + window.location.hostname + '/api/get-produk-by-supplier/';
            $('#id_supplier').on('change', function () {
                var url = urlget + $(this).val();
                $('body').find('select#barang').load(url);
                $("select#barang").combobox();
                console.log('change');
            });
            var datatable = $('body').find('.datatable').DataTable({
                "bPaginate": false,
                "bLengthChange": true,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
            });
            $('.tambah-barang').on('click', function () {
                var jsn = $.parseJSON($('select#barang').val());
                console.log(jsn);
                var juml = $('[name="jumlah"]').val();
                var subtotal = juml * jsn.harga;
                datatable.row.add([jsn.id + '<input type="hidden" name="id[]" value="' + jsn.id + '">', jsn.barang, '<div class="jumlah">' + juml + '</div><input type="hidden" name="jumlah[]" value="' + juml + '"/>', jsn.harga, '<span class="subtotal">' + subtotal + '</span>', '<a href="#" class="btn btn-danger btn-sm hapus"><span class="fa fa-trash-o"></span></a>']).draw(false);
            });
            $('body').find('.datatable').on('click', '.hapus', function () {
                var btnn = $(this);
                var row = datatable.row(btnn.parents('tr'));
                row.remove().draw();
            });
            $('body').find('.datatable  ').on('click', '.jumlah', function () {
                console.log('klik');
                var jml = $(this);
//                var tr = jml.parents('tr');
//                var jumlah = parseInt(jml.text());
                jml.attr('contenteditable', 'true');
                jml.addClass('form-control');
            });
            $('body').find('.datatable').on('blur', '.jumlah', function () {
                console.log('blur');
                var jml = $(this);
                jml.attr('contenteditable', 'false');
                jml.removeClass('form-control');
                jml.parent().find('input').val(jml.text());
            });
            getLastId();
            setInterval(function () {
                var now = new Date();
                var strDate = now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate() + ' ' + now.getHours() + ':' + now.getMinutes() + ':';
                if (now.getSeconds() > 9)
                    strDate += now.getSeconds();
                else
                    strDate += '0' + now.getSeconds();
                $('input.realtime').val(strDate);
                $('p.realtime').text(strDate);
            }, 100);
        })(jQuery);
        function getLastId() {
            var urlget = window.location.protocol + '//' + window.location.hostname + '/api/pembelian-produk/get-last-id';
            $.get(urlget).done(function (d) {
                $('#id,input[name="id"]').text(d.id + 1);
            });
        }
    </script>
@stop