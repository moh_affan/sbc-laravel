@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Pendapatan Produk
            <small>Laporan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pendapatan Produk</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" id="#transaksi-saya">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Laporan</h3>
                        <div class="pull-right box-tools">
                            <button class="btn btn-success btn-sm today" type="button" data-toggle="tooltip"
                                    title="Hari ini"
                                    data-original-title="Hari ini"><span style="color: #fff;">Hari ini</span></button>
                            <button class="btn btn-success btn-sm month" type="button" data-toggle="tooltip"
                                    title="Bulan ini"
                                    data-original-title="Bulan ini"><span style="color: #fff;">Bulan ini</span></button>
                            <input type="text" class="date-input" title="Pilih jangka waktu"
                                   data-original-title="Pilih jangka waktu" data-toggle="tooltip">
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover datatable2">
                                <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">ID Produk</th>
                                    <th rowspan="2">Produk</th>
                                    <th colspan="3">Penjualan</th>
                                    <th colspan="3">Diskon</th>
                                    <th colspan="3">HPP</th>
                                    <th rowspan="2">Laba</th>
                                </tr>
                                <tr>
                                    <th>QTY</th>
                                    <th>Harga</th>
                                    <th>Subtotal</th>
                                    <th>QTY</th>
                                    <th>Harga</th>
                                    <th>Subtotal</th>
                                    <th>QTY</th>
                                    <th>Harga</th>
                                    <th>Subtotal</th>
                                </tr>
                                </thead>
                                <tbody class="transaksi">

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th colspan="2">Total Penjualan</th>
                                    <th></th>
                                    <th colspan="2">Total Diskon</th>
                                    <th></th>
                                    <th colspan="2">Total HPP</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section><!-- /.content -->
@stop

@section('script')
    <script>
        (function () {

            var datatable2 = $('body').find('.datatable2').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                dom: 'Blfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Laporan Pendapatan Produk',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'pdfHtml5',
                    pageSize: 'Folio',
                    orientation: 'landscape',
                    title: 'Laporan Pendapatan Produk',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'print',
                    title: 'Laporan Pendapatan Produk',
                    pageSize: 'Folio',
                    orientation: 'landscape',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }],
                footerCallback: function (row, data, start, end, display) {
                    var api = this.api();
                    var intVal = function (i) {
                        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                    };
                    var totalPenjualan = api.column(5).data().reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    var totalDiskon = api.column(8).data().reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    var totalHpp = api.column(11).data().reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    var totalLaba = api.column(12).data().reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    $(api.column(5).footer()).html('Rp' + totalPenjualan);
                    $(api.column(8).footer()).html('Rp' + totalDiskon);
                    $(api.column(11).footer()).html('Rp' + totalHpp);
                    $(api.column(12).footer()).html('Rp' + totalLaba);
                }
            });

            function intvalue(i) {
                return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
            };
            $('.date-input').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function (a, b) {
                toggleLoading();
                var urlsaya = window.location.protocol + '//' + window.location.host + '/api/pendapatan-produk/' + a.format('YYYY-MM-DD') + '/' + b.format('YYYY-MM-DD');
                $.get(urlsaya).done(function (data) {
                    datatable2.clear().draw();
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        datatable2.row.add([d.no, d.id, d.nama, d.sum_jual, d.harga_jual, intvalue(d.sum_jual) * intvalue(d.harga_jual), d.jml_diskon, d.diskon_jual, intvalue(d.jml_diskon) * intvalue(d.diskon_jual), d.sum_jual, d.harga_beli, intvalue(d.sum_jual) * intvalue(d.harga_beli), intvalue(d.sum_jual) * intvalue(d.harga_jual) - intvalue(d.jml_diskon) * intvalue(d.diskon_jual) - intvalue(d.sum_jual) * intvalue(d.harga_beli)]).draw(false);
                    }
                    toggleLoading();
                });
            });


            $('button.today').click(function () {
                toggleLoading();
                var urlsaya = window.location.protocol + '//' + window.location.host + '/api/pendapatan-produk/today';
                $.get(urlsaya).done(function (data) {
                    datatable2.clear().draw();
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        datatable2.row.add([d.no, d.id, d.nama, d.sum_jual, d.harga_jual, intvalue(d.sum_jual) * intvalue(d.harga_jual), d.jml_diskon, d.diskon_jual, intvalue(d.jml_diskon) * intvalue(d.diskon_jual), d.sum_jual, d.harga_beli, intvalue(d.sum_jual) * intvalue(d.harga_beli), intvalue(d.sum_jual) * intvalue(d.harga_jual) - intvalue(d.jml_diskon) * intvalue(d.diskon_jual) - intvalue(d.sum_jual) * intvalue(d.harga_beli)]).draw(false);
                    }
                    toggleLoading();
                });
            });
            $('button.month').click(function () {
                toggleLoading();
                var urlsaya = window.location.protocol + '//' + window.location.host + '/api/pendapatan-produk/bulanini';
                $.get(urlsaya).done(function (data) {
                    datatable2.clear().draw();
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        datatable2.row.add([d.no, d.id, d.nama, d.sum_jual, d.harga_jual, intvalue(d.sum_jual) * intvalue(d.harga_jual), d.jml_diskon, d.diskon_jual, intvalue(d.jml_diskon) * intvalue(d.diskon_jual), d.sum_jual, d.harga_beli, intvalue(d.sum_jual) * intvalue(d.harga_beli), intvalue(d.sum_jual) * intvalue(d.harga_jual) - intvalue(d.jml_diskon) * intvalue(d.diskon_jual) - intvalue(d.sum_jual) * intvalue(d.harga_beli)]).draw(false);
                    }
                    toggleLoading();
                });
            });
        })(jQuery);
        function genTable() {

        }
        function toggleLoading() {
            var place = $('.right-side');
            console.log(place.find('.loading-box'));
            if (place.find('.loading-box').length == 0) {
                var div = $('<div></div>');
                div.addClass('loading-box');
                var img = $('<img>');
                img.prop('src', window.location.protocol + '//' + window.location.host + '/img/ajax-loader1.gif');
                img.addClass('img-thumbnail');
                div.append(img);
                place.append(div);
            } else {
                place.find('.loading-box').remove();
            }
        }
    </script>
@stop