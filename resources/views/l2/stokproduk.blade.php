@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Stok Produk
            <small>Laporan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Stok Produk</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" id="#transaksi-saya">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Laporan</h3>
                        <div class="pull-right box-tools">
                            <button class="btn btn-success btn-sm today" type="button" data-toggle="tooltip"
                                    title="Hari ini"
                                    data-original-title="Hari ini"><span style="color: #fff;">Hari ini</span></button>
                            <button class="btn btn-success btn-sm month" type="button" data-toggle="tooltip"
                                    title="Bulan ini"
                                    data-original-title="Bulan ini"><span style="color: #fff;">Bulan ini</span></button>
                            <input type="text" class="date-input" title="Pilih jangka waktu"
                                   data-original-title="Pilih jangka waktu" data-toggle="tooltip">
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover datatable2">
                                <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">ID Produk</th>
                                    <th rowspan="2">Produk</th>
                                    <th colspan="3">Stok Periode Sebelumnya</th>
                                    <th colspan="3">Pembelian</th>
                                    <th colspan="3">Penjualan</th>
                                    <th colspan="3">Jatah/Bon</th>
                                    <th colspan="3">Stok Periode Sekarang</th>
                                </tr>
                                <tr>
                                    <th>QTY</th>
                                    <th>Harga</th>
                                    <th>Subtotal</th>
                                    <th>QTY</th>
                                    <th>Harga</th>
                                    <th>Subtotal</th>
                                    <th>QTY</th>
                                    <th>Harga</th>
                                    <th>Subtotal</th>
                                    <th>QTY</th>
                                    <th>Harga</th>
                                    <th>Subtotal</th>
                                    <th>QTY</th>
                                    <th>Harga</th>
                                    <th>Subtotal</th>
                                </tr>
                                </thead>
                                <tbody class="transaksi">

                                </tbody>
                                <tfoot>
                                {{--<tr>--}}
                                    {{--<th></th>--}}
                                    {{--<th></th>--}}
                                    {{--<th></th>--}}
                                    {{--<th></th>--}}
                                    {{--<th></th>--}}
                                    {{--<th>Total Keseluruhan</th>--}}
                                    {{--<th>Subtotal</th>--}}
                                {{--</tr>--}}
                                </tfoot>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section><!-- /.content -->
@stop

@section('script')
    <script>
        (function () {

            var datatable2 = $('body').find('.datatable2').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                dom: 'Blfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Laporan Stok Produk',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'pdfHtml5',
                    pageSize: 'Folio',
                    orientation: 'landscape',
                    title: 'Laporan Stok Produk',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'print',
                    title: 'Laporan Stok Produk',
                    pageSize: 'Folio',
                    orientation: 'landscape',
                    footer: true,
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }, {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: "th:not(.hidden-print)"
                    }
                }],
                footerCallback: function (row, data, start, end, display) {
                    var api = this.api();
                    var intVal = function (i) {
                        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                    };
                    var total = api.column(6).data().reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                    var pageTotal = api.column(6, {page: 'current'}).data().reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                    $(api.column(6).footer()).html('Rp' + total);
                }
            });
            function intvalue(i) {
                return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
            };
            $('.date-input').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                }
            }, function (a, b) {
                toggleLoading();
                var urlsaya = window.location.protocol + '//' + window.location.hostname + '/api/stok-produk/' + a.format('YYYY-MM-DD') + '/' + b.format('YYYY-MM-DD');
                $.get(urlsaya).done(function (data) {
                    datatable2.clear().draw();
                    for (var i =0; i<data.length;i++) {
                        var d = data[i];
                        datatable2.row.add([d.no, d.id_produk, d.nama, d.stok_priode_sblm, d.harga_jual, intvalue(d.stok_priode_sblm)*intvalue(d.harga_jual), d.sum_beli, d.harga_beli, intvalue(d.sum_beli)*intvalue(d.harga_beli), d.sum_jual, d.harga_jual, intvalue(d.sum_jual)*intvalue(d.harga_jual),d.sum_jatahbon, d.harga_jatahbon, intvalue(d.sum_jatahbon)*intvalue(d.harga_jatahbon),d.stok, d.ref_harga_jual, intvalue(d.stok)*intvalue(d.ref_harga_jual)]).draw(false);
                    }
                    toggleLoading();
                });
            });


            $('button.today').click(function () {
                toggleLoading();
                var urlsaya = window.location.protocol + '//' + window.location.hostname + '/api/stok-produk/today';
                $.get(urlsaya).done(function (data) {
                    datatable2.clear().draw();
                    for (var i =0; i<data.length;i++) {
                        var d = data[i];
                        datatable2.row.add([d.no, d.id_produk, d.nama, d.stok_priode_sblm, d.harga_jual, intvalue(d.stok_priode_sblm)*intvalue(d.harga_jual), d.sum_beli, d.harga_beli, intvalue(d.sum_beli)*intvalue(d.harga_beli), d.sum_jual, d.harga_jual, intvalue(d.sum_jual)*intvalue(d.harga_jual),d.sum_jatahbon, d.harga_jatahbon, intvalue(d.sum_jatahbon)*intvalue(d.harga_jatahbon),d.stok, d.ref_harga_jual, intvalue(d.stok)*intvalue(d.ref_harga_jual)]).draw(false);
                    }
                    toggleLoading();
                });
            });
            $('button.month').click(function () {
                toggleLoading();
                var urlsaya = window.location.protocol + '//' + window.location.hostname + '/api/stok-produk/bulanini';
                $.get(urlsaya).done(function (data) {
                    datatable2.clear().draw();
                    for (var i =0; i<data.length;i++) {
                        var d = data[i];
                        datatable2.row.add([d.no, d.id_produk, d.nama, d.stok_priode_sblm, d.harga_jual, intvalue(d.stok_priode_sblm)*intvalue(d.harga_jual), d.sum_beli, d.harga_beli, intvalue(d.sum_beli)*intvalue(d.harga_beli), d.sum_jual, d.harga_jual, intvalue(d.sum_jual)*intvalue(d.harga_jual),d.sum_jatahbon, d.harga_jatahbon, intvalue(d.sum_jatahbon)*intvalue(d.harga_jatahbon),d.stok, d.ref_harga_jual, intvalue(d.stok)*intvalue(d.ref_harga_jual)]).draw(false);
                    }
                    toggleLoading();
                });
            });
        })(jQuery);
        function genTable() {

        }
        function toggleLoading() {
            var place = $('.right-side');
            console.log(place.find('.loading-box'));
            if (place.find('.loading-box').length == 0) {
                var div = $('<div></div>');
                div.addClass('loading-box');
                var img = $('<img>');
                img.prop('src', window.location.protocol + '//' + window.location.hostname + '/img/ajax-loader1.gif');
                img.addClass('img-thumbnail');
                div.append(img);
                place.append(div);
            } else {
                place.find('.loading-box').remove();
            }
        }
    </script>
@stop