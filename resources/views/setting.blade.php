@extends('main')

@section('konten')
    <section class="content-header">
        <h1>
            Setting
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Setting</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-6 col-lg-push-3 connectedSortable">
                @if(isset($message))
                    <div class="alert alert-success">{{$message}}</div>
                @endif
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="fa fa-calendar"></i>
                        <div class="box-title">Ubah password</div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                        {!! Form::open(['method'=>'PATCH']) !!}
                        <div class="form-group">
                            {!! Form::label('username', 'USERNAME') !!}
                            {!! Form::text('username',Session::get('username',''),['class'=>'form-control','required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'PASSWORD') !!}
                            {!! Form::password('password',['class'=>'form-control','required']) !!}
                        </div>
                        {!! Form::button('simpan',['type'=>'submit','class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </section><!-- /.Left col -->
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
@stop