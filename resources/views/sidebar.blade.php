<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('img/avatar3.png')}}" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Halo, {{explode(' ', $pengguna->pegawai->nama)[0]}}</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        {{--<span class="center-block badge bg-light-blue">&nbsp;</span>--}}
        <div class="progress xs">
            <div class="progress-bar progress-bar-light-blue" style="width: 100%;"></div>
        </div>
        <br>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active">
                <a href="{{url('/')}}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-th"></i>
                    <span>Data Master</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url($pengguna->level.'/pegawai')}}"><i class="fa fa-angle-double-right"></i> Pegawai</a>
                    </li>
                    <li><a href="{{url($pengguna->level.'/jabatan')}}"><i class="fa fa-angle-double-right"></i> Jabatan</a>
                    </li>
                    <li><a href="{{url($pengguna->level.'/pelanggan')}}"><i class="fa fa-angle-double-right"></i>
                            Pelanggan</a></li>
                    <li><a href="{{url($pengguna->level.'/supplier')}}"><i class="fa fa-angle-double-right"></i>
                            Supplier</a></li>
                    <li><a href="{{url($pengguna->level.'/kategori')}}"><i class="fa fa-angle-double-right"></i>
                            Kategori</a></li>
                    <li><a href="{{url($pengguna->level.'/diskon')}}"><i class="fa fa-angle-double-right"></i>
                            Diskon</a></li>
                    <li><a href="{{url($pengguna->level.'/produk')}}"><i class="fa fa-angle-double-right"></i>
                            Produk</a></li>
                    <li><a href="{{url($pengguna->level.'/treatment')}}"><i class="fa fa-angle-double-right"></i>
                            Treatment</a></li>
                    <li><a href="{{url($pengguna->level.'/penyusutan')}}"><i class="fa fa-angle-double-right"></i>
                            Penyusutan</a></li>
                    <li><a href="{{url($pengguna->level.'/jatah')}}"><i class="fa fa-angle-double-right"></i> Jatah</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ion ion-bag"></i>
                    <span>Transaksi</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Pembelian Produk</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Penjualan Produk</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Jatah Bon Produk</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Pembelian Treatment</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Penjualan Treatment</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Jatah Bon Treatment</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i> <span>Laporan</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Pembelian Produk</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Penjualan Produk</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Bon Jatah Produk</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Pembelian Treatment</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Penjualan Treatment</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Bon Jatah Treatment</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Penyusutan Traetment</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Stok Produk</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Stok Treatment</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Pendapatan Produk</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Pendapatan Treatment</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dropbox"></i> <span>Stok</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Produk</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> Treatment</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-users"></i> <span>User</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>